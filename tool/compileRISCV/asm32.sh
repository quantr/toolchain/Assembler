#!/bin/bash

if [ $# -ne 1 ]; then
        exit;
fi

#random=$((1 + RANDOM % 1000000))
random=$(shuf -i 10000000-20000000 -n 1)

parameter=${1/ /\\\ }
# echo $parameter
sed "s/{instruction}/$1/g" a.template > temp$random.s
riscv64-unknown-elf-as -march=rv32imfc temp$random.s -o temp$random.o
if test -f "temp$random.o"; then
        riscv64-unknown-elf-ld -melf32lriscv temp$random.o -T temp.ld -o temp$random
		if test -f "temp$random"; then
			riscv64-unknown-elf-objcopy -O binary --only-section=.text temp$random temp$random.text
			cat temp$random.text
		fi
        # riscv64-unknown-elf-objdump -d temp
        # riscv64-unknown-elf-readelf -x .text temp
        # od -t x1 a.text
fi
rm -fr temp$random.s temp$random.o temp$random temp$random.text
