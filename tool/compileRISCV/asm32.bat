@echo off

riscv-none-embed-as.exe -march=rv32imfc %1 -o temp%2.o
riscv-none-embed-ld.exe -melf32lriscv temp%2.o -T temp.ld -o temp%2
riscv-none-embed-objcopy.exe -O binary --only-section=.text temp%2 temp%2.text
type temp%2.text
del %1 temp%2 temp%2.o temp%2.text
