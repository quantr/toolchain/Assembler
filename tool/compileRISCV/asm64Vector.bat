@echo off

rem Toolchain for Linux from https://github.com/Imperas/riscv-toolchains/tree/rvv-1.0.0 -- Please install WSL, clone the toolchain of correct branch, adjust commands below
rem git clone https://github.com/Imperas/riscv-toolchains.git -b rvv-1.0.0 /opt/riscv-toolchains-rvv-1.0.0

wsl /opt/riscv-toolchains-rvv-1.0.0/Linux64/bin/riscv64-unknown-elf-as -march=rv64gcv %1 -o temp%2.o
wsl /opt/riscv-toolchains-rvv-1.0.0/Linux64/bin/riscv64-unknown-elf-ld -melf64lriscv temp%2.o -T temp.ld -o temp%2
wsl /opt/riscv-toolchains-rvv-1.0.0/Linux64/bin/riscv64-unknown-elf-objcopy -O binary --only-section=.text temp%2 temp%2.text

type temp%2.text
del %1 temp%2 temp%2.o temp%2.text
