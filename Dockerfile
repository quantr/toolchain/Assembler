FROM maven:3.9.7 as builder

WORKDIR /usr/src/assembler
COPY . .
RUN mvn -DskipTests package

FROM openjdk:24-jdk
WORKDIR /app
COPY --from=builder /usr/src/assembler/target/assembler-*-dep*.jar .
ENTRYPOINT ["java", "-jar", "/app/assembler-1.5-jar-with-dependencies.jar"]

