.section .text
.globl _start
_start:
	sll x8,x6,x3
slli x8,x6,0x2
srl x8,x6,x3
srli x8,x6,0x4
sra x8,x6,x3
srai x8,x6,0x1f

add x8,x6,x3
addi x8,x6,0x12
sub x8,x6,x3
lui x8,0x12
auipc x8,0x12

xor x8,x6,x3
xori x8,x6,0x12
or x8,x6,x3
ori x8,x6,0x12
and x8,x6,x3
andi x8,x6,0x12

slt x8,x6,x3
slti x8,x6,0x12
sltu x8,x6,x3
	sltiu x8,x6,0x12
