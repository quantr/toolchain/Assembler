/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.assembler.ia32;

import org.junit.Test;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestConsole {
	
	@Test
	public void test() throws Exception {
		
		String ANSI_RESET = "\u001B[0m";
		String ANSI_RED = "\u001B[31m";
		System.out.println(ANSI_RED + "XX");
		System.out.flush();
		System.out.println(ANSI_RESET);
		System.out.println("\033[31;1mHello\033[0m, \033[32;1;2mworld!\033[0m");
		System.out.println((char) 27 + "[32m" + "ERROR MESSAGE IN GREEN");
		System.out.println((char) 27 + "[33mYELLOW");

//		System.setProperty("jansi.force", "true");
		System.setProperty("jansi.passthrough", "true");
//		System.setProperty("jansi.strip", "true");
//		System.setProperty("jansi.force", "true");

//		AnsiConsole.systemInstall();
//		AnsiMain.main();
//		
//		System.out.println(ansi().eraseScreen().render("@|red Hello|@ @|green World|@"));
		System.out.println(ansi().fg(RED).a("Hello").fg(GREEN).a(" World").reset());
//		AnsiMain.main();


		System.out.println("\033[38;5;3mPeter\033[0m"); // 24 bits color, refer to https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
	}
}
