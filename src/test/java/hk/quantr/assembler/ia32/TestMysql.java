package hk.quantr.assembler.ia32;

import hk.quantr.javalib.CommonLib;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestMysql {

	static String mysql_username = "assembler";
	static String mysql_password = "assembler";
//	static String mysql_host = "192.168.10.124";
	static String mysql_host = "localhost";
	static String mysql_database = "assembler";

	@Test
	public void test() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		//Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database, mysql_username, mysql_password);

		Statement statement = conn.createStatement();
		String code = "aaa";
		ResultSet resultSet = statement.executeQuery("select count(*) from nasm where code='" + code + "';");

		resultSet.next();
		int count = resultSet.getInt(1);
		if (count == 0) {
			PreparedStatement pstmt = conn.prepareStatement("insert into nasm values(0, now(), ?, ?, ?, ?, ?, ?, ?);");

			String translatedInstruction = "bits 16\n" + code;
			Pair<String, byte[]> bytesNasm16 = TestUtil.compileNasm(translatedInstruction, 16);
			Pair<String, byte[]> bytesNasm32 = TestUtil.compileNasm(translatedInstruction, 32);
			Pair<String, byte[]> bytesNasm64 = TestUtil.compileNasm(translatedInstruction, 64);
			int z = 1;
			pstmt.setString(z++, code);
			pstmt.setString(z++, CommonLib.arrayToHexString(bytesNasm16.getValue()));
			pstmt.setString(z++, bytesNasm16.getKey());
			pstmt.setString(z++, CommonLib.arrayToHexString(bytesNasm32.getValue()));
			pstmt.setString(z++, bytesNasm32.getKey());
			pstmt.setString(z++, CommonLib.arrayToHexString(bytesNasm64.getValue()));
			pstmt.setString(z++, bytesNasm64.getKey());
			pstmt.execute();
		}

//		PreparedStatement pstmt = conn.prepareStatement("insert into test2 values(0, now(), -1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
//		
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, "0");
//		pstmt.setString(z++, null);
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, null);
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, null);
//		pstmt.addBatch();
//
//		z = 1;
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, "1");
//		pstmt.setString(z++, null);
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, null);
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, "");
//		pstmt.setString(z++, null);
//		pstmt.addBatch();
//
//		pstmt.executeBatch();
//
//		conn.createStatement().execute("SET @pos := -1");
//		conn.createStatement().execute("update `test2` SET rowNo = ( SELECT @pos := @pos + 1 ) WHERE `CI_JOB_ID`='' order by datetime");
		conn.close();
	}
}
