package hk.quantr.assembler.ia32;

import hk.quantr.assembler.ia32.MyTableModel.Data;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.javalib.CommonLib;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class FullTest2 {

	public static final Logger logger = Logger.getLogger(FullTest2.class.getName());
	boolean runInPeterMac = false;

	static String mysql_username;
	static String mysql_password;
	static String mysql_host;
	static String mysql_database;

	@Test
	public void fullTest() throws Exception {
		Date from = new Date();

		String CI_JOB_ID;
		String GITLAB_USER_ID;
		String GITLAB_USER_NAME;
		String GITLAB_USER_EMAIL;
		String GITLAB_USER_LOGIN;
		if (runInPeterMac) {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "localhost";
			mysql_database = "assembler";
			CI_JOB_ID = "CI_JOB_ID";
			GITLAB_USER_ID = "GITLAB_USER_ID";
			GITLAB_USER_NAME = "GITLAB_USER_NAME";
			GITLAB_USER_EMAIL = "GITLAB_USER_EMAIL";
			GITLAB_USER_LOGIN = "GITLAB_USER_LOGIN";
		} else {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "192.168.0.176";
			mysql_database = "assembler";

			CI_JOB_ID = System.getProperty("CI_JOB_ID");
			GITLAB_USER_ID = System.getProperty("GITLAB_USER_ID");
			GITLAB_USER_NAME = System.getProperty("GITLAB_USER_NAME");
			GITLAB_USER_EMAIL = System.getProperty("GITLAB_USER_EMAIL");
			GITLAB_USER_LOGIN = System.getProperty("GITLAB_USER_LOGIN");
		}

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);

		if (CI_JOB_ID != null) {
			PreparedStatement pstmt2 = conn.prepareStatement("insert into `job` values(0, now(), ?, ?, ?, ?, ?);");
			int z2 = 1;
			pstmt2.setString(z2++, CI_JOB_ID);
			pstmt2.setString(z2++, GITLAB_USER_ID);
			pstmt2.setString(z2++, GITLAB_USER_NAME);
			pstmt2.setString(z2++, GITLAB_USER_EMAIL);
			pstmt2.setString(z2++, GITLAB_USER_LOGIN);
			pstmt2.execute();
		}

		ResultSet resultSet = conn.createStatement().executeQuery("select * from nasm;");
		HashSet<String> nasmCodes = new HashSet<>();
		while (resultSet.next()) {
			nasmCodes.add(resultSet.getString("code"));
		}
		System.out.println("nasmCodes=" + nasmCodes.size());

		MyTableModel model = new MyTableModel();

//		boolean allBits16Pass = true;
//		boolean allBits32Pass = true;
//		boolean allBits64Pass = true;
		ExecutorService executorService = Executors.newFixedThreadPool(8);
		int x = 0;

		List<String> instructions = GetAllInstruction.getAllInstructionsFromFile("testcase.txt");
		for (String instruction : instructions) {
//			System.out.println("instruction=" + instruction);
			String[] translatedInstructions = TestUtil.translateInstruction(instruction);
			x += translatedInstructions.length;
		}

		System.out.println("number of instructions = " + x);
		CountDownLatch latch = new CountDownLatch(x);
		x = 0;
		PreparedStatement pstmt_nasm = conn.prepareStatement("insert into nasm values(0, now(), ?, ?, ?, ?, ?, ?, ?)");
		for (String instruction : instructions) {
			String[] translatedInstructions = TestUtil.translateInstruction(instruction);
			for (String translatedInstruction : translatedInstructions) {
//				RunNasm r = new RunNasm(nasmCodes, translatedInstruction, pstmt_nasm, latch, model);
//				executorService.submit(r);
				executorService.submit(() -> {
					System.out.println(">" + translatedInstruction);

					try {
						byte bytes16[] = TestUtil.compileIA32(translatedInstruction, 16);
						byte bytes32[] = TestUtil.compileIA32(translatedInstruction, 32);
						byte bytes64[] = TestUtil.compileIA32(translatedInstruction, 64);
						
//						final Statement tempStmt = conn2.createStatement();
//						ResultSet resultSet = tempStmt.executeQuery("select count(*) from nasm where code='" + translatedInstruction + "';");
//						resultSet.next();
//						int count = resultSet.getInt(1);
						Pair<String, byte[]> bytesNasm16 = null;
						Pair<String, byte[]> bytesNasm32 = null;
						Pair<String, byte[]> bytesNasm64 = null;

						if (!nasmCodes.contains(translatedInstruction)) {
							bytesNasm16 = TestUtil.compileNasm(translatedInstruction, 16);
							bytesNasm32 = TestUtil.compileNasm(translatedInstruction, 32);
							bytesNasm64 = TestUtil.compileNasm(translatedInstruction, 64);
//						Pair<String, byte[]> bytesNasm16 = new ImmutablePair(null, new byte[]{0});
//						Pair<String, byte[]> bytesNasm32 = new ImmutablePair(null, new byte[]{0});
//						Pair<String, byte[]> bytesNasm64 = new ImmutablePair(null, new byte[]{0});

							int z = 1;
							synchronized (pstmt_nasm) {
								pstmt_nasm.setString(z++, translatedInstruction);
								pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm16.getValue()));
								pstmt_nasm.setString(z++, bytesNasm16.getKey());
								pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm32.getValue()));
								pstmt_nasm.setString(z++, bytesNasm32.getKey());
								pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm64.getValue()));
								pstmt_nasm.setString(z++, bytesNasm64.getKey());
								pstmt_nasm.addBatch();
							}
						}
//						else {
//							resultSet = tempStmt.executeQuery("select * from nasm where code='" + translatedInstruction + "';");
//							resultSet.next();
//						}

						synchronized (model) {
							model.add("", translatedInstruction, translatedInstruction, translatedInstruction, bytes16, bytes32, bytes64);
						}
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
					latch.countDown();
				});

				x++;
			}
		}

//		StringBuilder sb = new StringBuilder();
		while (latch.getCount() > 0) {
//			sb.append("=");
			double percent = Math.round((double) ((x - latch.getCount()) * 100) / x);
//			System.out.println(sb.toString() + percent + "% (" + (x - latch.getCount()) + " / " + x + ")");
			System.out.println(percent + "% (" + (x - latch.getCount()) + " / " + x + ")");
			TimeUnit.MILLISECONDS.sleep(1000);
		}
		pstmt_nasm.executeBatch();

		conn.createStatement().execute("delete t1 from nasm t1\n"
				+ "inner join nasm t2 \n"
				+ "where \n"
				+ "    t1.id < t2.id and \n"
				+ "    t1.code = t2.code;");

		conn.createStatement().execute("delete t1 from test t1\n"
				+ "inner join test t2 \n"
				+ "where \n"
				+ "    t1.id < t2.id and \n"
				+ "    t1.code = t2.code\n"
				+ "    and t1.CI_JOB_ID='" + CI_JOB_ID + "'\n"
				+ "    and t2.CI_JOB_ID='" + CI_JOB_ID + "';");

		conn.createStatement().execute("delete from test where CI_JOB_ID not in (select b.* from (SELECT CI_JOB_ID FROM `job` group by CI_JOB_ID order by min(datetime) desc limit 0,4) as b)");

		System.out.println("done " + model.data.size());

		//remove useless instructions
//		for (int y = model.data.size() - 1; y >= 0; y--) {
//			Data data = model.data.get(y);
//			if (data.bytes16 == null && data.bytes32 == null && data.bytes64 == null && data.bytesNasm16 == null && data.bytesNasm32 == null && data.bytesNasm64 == null) {
//				model.data.remove(y);
//			}
//		}
		//end remove useless instructions
		if (CI_JOB_ID != null) {
			PreparedStatement pstmt = conn.prepareStatement("insert into test values(0, now(), -1, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (int y = 0; y < model.data.size(); y++) {
				Data data = model.data.get(y);
				System.out.println(data.code16 + " > " + CommonLib.arrayToHexString(data.bytes16));
				try {
					int z = 1;
					pstmt.setString(z++, CI_JOB_ID);
					pstmt.setString(z++, GITLAB_USER_ID);
					pstmt.setString(z++, GITLAB_USER_NAME);
					pstmt.setString(z++, GITLAB_USER_EMAIL);
					pstmt.setString(z++, GITLAB_USER_LOGIN);
					pstmt.setString(z++, data.code16);
					pstmt.setString(z++, CommonLib.arrayToHexString(data.bytes16));
					pstmt.setString(z++, CommonLib.arrayToHexString(data.bytes32));
					pstmt.setString(z++, CommonLib.arrayToHexString(data.bytes64));
//					pstmt.execute();
					pstmt.addBatch();
				} catch (SQLException ex) {
					MessageHandler.errorPrintln(ex);
				}
			}
			pstmt.executeBatch();

			conn.createStatement().execute("SET @pos := -1;");
			conn.createStatement().execute("update `test` SET rowNo = ( SELECT @pos := @pos + 1 ) WHERE `CI_JOB_ID`='" + CI_JOB_ID + "' order by datetime");
		}

//		for (int y = model.data.size() - 1; y >= 0; y--) {
//			Data data = model.data.get(y);
//			if (data.bytesNasm16.getValue() != data.bytes16) {
//				allBits16Pass = false;
//			}
//			if (data.bytesNasm32.getValue() != data.bytes32) {
//				allBits32Pass = false;
//			}
//			if (data.bytesNasm64.getValue() != data.bytes64) {
//				allBits64Pass = false;
//			}
//		}
//		TestUtil.generateConsoleTable(model);
//		TestUtil.generateHtmlTable(model, "output.html");
//
//		System.out.println("row=" + model.getRowCount());
//
//		if (allBits16Pass && allBits32Pass && allBits64Pass) {
//			//https://my.asciiart.club
//			System.out.println(" \n"
//					+ "                ▓▓▓▓▓▓▓▌                       ,▄▓▓▌\n"
//					+ "                ▓▓▓▓▓▓`                       ▀▓▓▓▓▓▓▄\n"
//					+ "                ▓▓▓▓▓▌                          ▀▓▓▓▓▓▓\n"
//					+ "                ╟▓▓▓▓▌                            ▀▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓¬                            ¬¬¬^ ```\n"
//					+ "      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓  ▓▓▓▓▓              ▓▓▓▓▓▓▀\n"
//					+ "      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓¬ ▓▓▓▓▓              ╟▓▓▓▓▌\n"
//					+ "      ▀▀▀▀▀`└^^└▐▓▓▓▓▌╙``▀▀▀▀▀  ▓▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ╟▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ╟▓▓▓▓              ╟▓▓▓▓▄▄▄▄▄▄▄▄▄▄▄▄▄▄▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▐▓▓▓▓              ╟▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌      ▄▓  ▐▓▓▓▓              ╟▓▓▓▓▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀\n"
//					+ "                ▐▓▓▓▓▌  ▄▄▓▓▓▓⌐ ▐▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▓▓▓▓▓▓▓▓▓▌ ▐▓▓▓▓              ╟▓▓▓▓\n"
//					+ "              ,▄▓▓▓▓▓▓▓▓▓▓▀▀¬   ╟▓▓▓▓              ╟▓▓▓▓\n"
//					+ "        ,▄▄▓▓▓▓▓▓▓▓▓▓▓▀`        ▓▓▓▓▓   ▄▄▄▄▄▄▄▄▄▄▄▓▓▓▓▓▄▄▄▄▄▄▄▄▄▄▄▄▄▄m\n"
//					+ "    ²▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▌          ▓▓▓▓▓   ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓∩\n"
//					+ "     `▓▓▓▓▓▓▀▀  ▐▓▓▓▓▌          ▓▓▓▓▓   ▓▓▓▓▓▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▓▓▓▓▓\n"
//					+ "       ▓▀▀      ▐▓▓▓▓▌          ▓▓▓▓▌   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▐▓▓▓▓▌   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▓▓▓▓▓─   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▓▓▓▓▓    ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌        ▓▓▓▓▓▌    ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌       ▐▓▓▓▓▓     ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌      ╓▓▓▓▓▓▀     ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "        ▄,      ▄▓▓▓▓▌     ▄▓▓▓▓▓▀      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "         ▓▓▓▓▓▓▓▓▓▓▓▓`    ▓▓▓▓▓▓▀       ▓▓▓▓▓                    ▓▓▓▓▓∩\n"
//					+ "          ▓▓▓▓▓▓▓▓▓▓▀    ▀▀▓▓▓▓▀       ▐▓▓▓▓▓                    ▓▓▓▓▓▌\n"
//					+ "          ▀▀▀▀▀▀▀^           ╙▀        ╙▀▀▀▀▀\n"
//					+ " ");
//		}
		if (conn != null) {
			conn.close();
		}

		Date to = new Date();
		long seconds = (to.getTime() - from.getTime()) / 1000;
		System.out.println(seconds + " seconds");
	}

//	public boolean isMysql() throws SocketException {
//		for (InetAddress addr : getListOfIPsFromNIs()) {
//			if (addr.getHostAddress().startsWith("192.168.10.")) {
//				return true;
//			}
//		}
//		return false;
//	}
	public List<InetAddress> getListOfIPsFromNIs() throws SocketException {
		List<InetAddress> addrList = new ArrayList<InetAddress>();
		Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();
		while (enumNI.hasMoreElements()) {
			NetworkInterface ifc = enumNI.nextElement();
			if (ifc.isUp()) {
				Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
				while (enumAdds.hasMoreElements()) {
					InetAddress addr = enumAdds.nextElement();
					addrList.add(addr);
					System.out.println(addr.getHostAddress());   //<---print IP
				}
			}
		}
		return addrList;
	}

//	private void generateMyConsoleTable(MyTableModel model) {
//		int widths[] = new int[model.getColumnCount()];
//		for (int x = 0; x < model.getColumnCount(); x++) {
//			for (int y = 0; y < model.getRowCount(); y++) {
//
//			}
//		}
//	}
//	class RunNasm implements Runnable {
//
//		HashSet<String> nasmCodes;
//		String translatedInstruction;
//		PreparedStatement pstmt_nasm;
//		CountDownLatch latch;
//		MyTableModel model;
//
//		public RunNasm(HashSet<String> nasmCodes, String translatedInstruction, PreparedStatement pstmt_nasm, CountDownLatch latch, MyTableModel model) {
//			this.nasmCodes = nasmCodes;
//			this.translatedInstruction = translatedInstruction;
//			this.pstmt_nasm = pstmt_nasm;
//			this.latch = latch;
//			this.model = model;
//		}
//
//		@Override
//		public void run() {
//			byte bytes16[] = TestUtil.compileIA32(translatedInstruction, 16);
//			byte bytes32[] = TestUtil.compileIA32(translatedInstruction, 32);
//			byte bytes64[] = TestUtil.compileIA32(translatedInstruction, 64);
//
//			try {
////						final Statement tempStmt = conn2.createStatement();
////						ResultSet resultSet = tempStmt.executeQuery("select count(*) from nasm where code='" + translatedInstruction + "';");
////						resultSet.next();
////						int count = resultSet.getInt(1);
//				Pair<String, byte[]> bytesNasm16 = null;
//				Pair<String, byte[]> bytesNasm32 = null;
//				Pair<String, byte[]> bytesNasm64 = null;
//
//				if (!nasmCodes.contains(translatedInstruction)) {
//					bytesNasm16 = TestUtil.compileNasm(translatedInstruction, 16);
//					bytesNasm32 = TestUtil.compileNasm(translatedInstruction, 32);
//					bytesNasm64 = TestUtil.compileNasm(translatedInstruction, 64);
////						Pair<String, byte[]> bytesNasm16 = new ImmutablePair(null, new byte[]{0});
////						Pair<String, byte[]> bytesNasm32 = new ImmutablePair(null, new byte[]{0});
////						Pair<String, byte[]> bytesNasm64 = new ImmutablePair(null, new byte[]{0});
//
//					int z = 1;
//					synchronized (pstmt_nasm) {
//						pstmt_nasm.setString(z++, translatedInstruction);
//						pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm16.getValue()));
//						pstmt_nasm.setString(z++, bytesNasm16.getKey());
//						pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm32.getValue()));
//						pstmt_nasm.setString(z++, bytesNasm32.getKey());
//						pstmt_nasm.setString(z++, CommonLib.arrayToHexString(bytesNasm64.getValue()));
//						pstmt_nasm.setString(z++, bytesNasm64.getKey());
//						pstmt_nasm.addBatch();
//					}
//				}
////						else {
////							resultSet = tempStmt.executeQuery("select * from nasm where code='" + translatedInstruction + "';");
////							resultSet.next();
////						}
//
//				synchronized (model) {
//					model.add("", translatedInstruction, translatedInstruction, translatedInstruction, bytes16, bytes32, bytes64);
//				}
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			}
//			latch.countDown();
//		}
//	}
}
