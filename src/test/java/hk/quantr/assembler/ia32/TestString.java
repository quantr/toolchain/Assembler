package hk.quantr.assembler.ia32;

import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;
import hk.quantr.assembler.antlr.AssemblerLexer;
import hk.quantr.assembler.antlr.AssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.Pair;

public class TestString {

	@Test
	public void test() throws Exception {
//		String instruction = "adc byte [si+0x12], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+si], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+di], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+di], 0x1+0x2*0x3";
//		String instruction = "adc byte [si], 0x1+0x2*0x3";
//		String instruction = "adc byte [di], 0x1+0x2*0x3";
//		String instruction = "adc byte [0xabcd], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+si+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+di+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+si+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+di+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [si+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [di+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+0x7f], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+si+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+di+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+si+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+di+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [si+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [di+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [bp+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc byte [bx+0x1234], 0x1+0x2*0x3";
//		String instruction = "adc mm0, 0x1+0x2*0x3";
//		String instruction = "adc cl, 0x1+0x2*0x3";
//		String instruction = "adc dl, 0x1+0x2*0x3";
//		String instruction = "adc bl, 0x1+0x2*0x3";
//		String instruction = "adc sp, 0x1+0x2*0x3";
//		String instruction = "adc bp, 0x1+0x2*0x3";
//		String instruction = "adc si, 0x1+0x2*0x3";
//		String instruction = "adc di, 0x1+0x2*0x3";

		// 32 bits
//		String instruction = "adc byte [eax], 0x1+0x2*0x3";
//		String instruction = "adc byte [ecx], 0x1+0x2*0x3";
//		String instruction = "adc byte [edx], 0x1+0x2*0x3";
//		String instruction = "adc byte [ebx], 0x1+0x2*0x3";
//		String instruction = "adc byte [eax+0x12345678], 0x1+0x2*0x3";
		int bit = 16;
		String instruction ="mov [0x1234],gx";
		String instruction2 = "";
		String instruction3 = "";
		String instructions = instruction + "\n" + instruction2 + instruction3 + "\n";
		AssemblerLexer lexer = new AssemblerLexer(CharStreams.fromString("(bits " + bit + "){\n"
				+ instructions + "\n"
				+ "}\n"
		));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		AssemblerParser parser = new AssemblerParser(tokenStream);
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());
//		ParseTreeWalker walker = new ParseTreeWalker();

//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		MAL listener = new MAL(parser, out);
		AssemblerParser.AssembleContext context = parser.assemble();
//		walker.walk(listener, context);
		for (byte b : parser.mal.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println("\n---");

		Pair<String, byte[]> bytesNasmBytes = TestUtil.compileNasm(instructions, bit);
		for (byte b : bytesNasmBytes.getValue()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println();
//		for (Token t : tokenStream.getTokens()) {
//			System.out.println(">" + t.getText());
//		}
	}

}
