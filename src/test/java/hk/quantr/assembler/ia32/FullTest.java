//package hk.quantr.assembler.ia32;
//
//import hk.quantr.assembler.MyTableModel.Data;
//import java.lang.annotation.Annotation;
//import java.lang.reflect.Method;
//import org.junit.Test;
//import hk.quantr.assembler.test.AssemblerTest;
//import hk.quantr.assembler.test.AssemblerTest.Architecture;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.logging.Logger;
//import org.apache.commons.lang3.tuple.Pair;
//
///**
// *
// * @author Peter (peter@quantr.hk)
// */
//public class FullTest {
//
//	public static Logger logger = Logger.getLogger(FullTest.class.getName());
//
//	@Test
//	public void fullTest() throws Exception {
//		Class<MAL> obj = MAL.class;
//
//		MyTableModel model = new MyTableModel();
//
//		boolean allBits16Pass = true;
//		boolean allBits32Pass = true;
//		boolean allBits64Pass = true;
//
//		ExecutorService executorService = Executors.newFixedThreadPool(16);
//		int x = 0;
//		for (Method method : obj.getDeclaredMethods()) {
//			if (method.isAnnotationPresent(AssemblerTest.class)) {
//				Annotation annotation = method.getAnnotation(AssemblerTest.class);
//				AssemblerTest testerInfo = (AssemblerTest) annotation;
//				if (testerInfo.architecture() == Architecture.ia32) {
//					for (String testingInstruction : testerInfo.instructions()) {
//						String[] instructions = TestUtil.translateInstruction(testingInstruction);
//						x += instructions.length;
//					}
//				}
//			}
//		}
//		System.out.println("number of instructions = " + x);
//		CountDownLatch latch = new CountDownLatch(x);
//		x = 0;
//		for (Method method : obj.getDeclaredMethods()) {
//			if (method.isAnnotationPresent(AssemblerTest.class)) {
//				Annotation annotation = method.getAnnotation(AssemblerTest.class);
//				AssemblerTest testerInfo = (AssemblerTest) annotation;
//				if (testerInfo.architecture() == Architecture.ia32) {
//					for (String testingInstruction : testerInfo.instructions()) {
//						String[] instructions = TestUtil.translateInstruction(testingInstruction);
//						for (String instruction : instructions) {
////							System.out.println(instruction);
//							executorService.submit(() -> {
//								byte bytes16[] = TestUtil.compileIA32(instruction, 16);
//								byte bytes32[] = TestUtil.compileIA32(instruction, 32);
//								byte bytes64[] = TestUtil.compileIA32(instruction, 64);
//								Pair<String, byte[]> bytesNasm16 = TestUtil.compileNasm(instruction, 16);
//								Pair<String, byte[]> bytesNasm32 = TestUtil.compileNasm(instruction, 32);
//								Pair<String, byte[]> bytesNasm64 = TestUtil.compileNasm(instruction, 64);
////								byte bytesNasm16[] = new byte[]{0};
////								byte bytesNasm32[] = new byte[]{0};
////								byte bytesNasm64[] = new byte[]{0};
//
//								synchronized (model) {
//									model.add(method.getName(), instruction, instruction, instruction, bytes16, bytesNasm16, bytes32, bytesNasm32, bytes64, bytesNasm64);
//								}
//								latch.countDown();
//							});
//							x++;
//						}
//					}
//				}
//			}
//		}
//		System.out.println("number of testing instructions = " + x);
//		latch.await();
//
//		System.out.println("done");
//
//		//remove useless instructions
//		for (int y = model.data.size() - 1; y >= 0; y--) {
//			Data data = model.data.get(y);
//			if (data.bytesNasm16 == null && data.bytesNasm32 == null && data.bytesNasm64 == null) {
//				model.data.remove(y);
//			}
//		}
//		//end remove useless instructions
//
//		TestUtil.generateConsoleTable(model);
//		System.out.println("row=" + model.getRowCount());
//
//		if (allBits16Pass && allBits32Pass && allBits64Pass) {
//			//https://my.asciiart.club
//			System.out.println(" \n"
//					+ "                ▓▓▓▓▓▓▓▌                       ,▄▓▓▌\n"
//					+ "                ▓▓▓▓▓▓`                       ▀▓▓▓▓▓▓▄\n"
//					+ "                ▓▓▓▓▓▌                          ▀▓▓▓▓▓▓\n"
//					+ "                ╟▓▓▓▓▌                            ▀▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▓▓▓▓▓¬                            ¬¬¬^ ```\n"
//					+ "      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓  ▓▓▓▓▓              ▓▓▓▓▓▓▀\n"
//					+ "      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓¬ ▓▓▓▓▓              ╟▓▓▓▓▌\n"
//					+ "      ▀▀▀▀▀`└^^└▐▓▓▓▓▌╙``▀▀▀▀▀  ▓▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ╟▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ╟▓▓▓▓              ╟▓▓▓▓▄▄▄▄▄▄▄▄▄▄▄▄▄▄▓▓▓\n"
//					+ "                ▐▓▓▓▓▌          ▐▓▓▓▓              ╟▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌      ▄▓  ▐▓▓▓▓              ╟▓▓▓▓▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀\n"
//					+ "                ▐▓▓▓▓▌  ▄▄▓▓▓▓⌐ ▐▓▓▓▓              ╟▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▓▓▓▓▓▓▓▓▓▌ ▐▓▓▓▓              ╟▓▓▓▓\n"
//					+ "              ,▄▓▓▓▓▓▓▓▓▓▓▀▀¬   ╟▓▓▓▓              ╟▓▓▓▓\n"
//					+ "        ,▄▄▓▓▓▓▓▓▓▓▓▓▓▀`        ▓▓▓▓▓   ▄▄▄▄▄▄▄▄▄▄▄▓▓▓▓▓▄▄▄▄▄▄▄▄▄▄▄▄▄▄m\n"
//					+ "    ²▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▌          ▓▓▓▓▓   ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓∩\n"
//					+ "     `▓▓▓▓▓▓▀▀  ▐▓▓▓▓▌          ▓▓▓▓▓   ▓▓▓▓▓▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▓▓▓▓▓\n"
//					+ "       ▓▀▀      ▐▓▓▓▓▌          ▓▓▓▓▌   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▐▓▓▓▓▌   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▓▓▓▓▓─   ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌         ▓▓▓▓▓    ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌        ▓▓▓▓▓▌    ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌       ▐▓▓▓▓▓     ▓▓▓▓▓                    ▓▓▓▓▓\n"
//					+ "                ▐▓▓▓▓▌      ╓▓▓▓▓▓▀     ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "        ▄,      ▄▓▓▓▓▌     ▄▓▓▓▓▓▀      ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
//					+ "         ▓▓▓▓▓▓▓▓▓▓▓▓`    ▓▓▓▓▓▓▀       ▓▓▓▓▓                    ▓▓▓▓▓∩\n"
//					+ "          ▓▓▓▓▓▓▓▓▓▓▀    ▀▀▓▓▓▓▀       ▐▓▓▓▓▓                    ▓▓▓▓▓▌\n"
//					+ "          ▀▀▀▀▀▀▀^           ╙▀        ╙▀▀▀▀▀\n"
//					+ " ");
//		}
//	}
//
////	private void generateMyConsoleTable(MyTableModel model) {
////		int widths[] = new int[model.getColumnCount()];
////		for (int x = 0; x < model.getColumnCount(); x++) {
////			for (int y = 0; y < model.getRowCount(); y++) {
////
////			}
////		}
////	}
//}
