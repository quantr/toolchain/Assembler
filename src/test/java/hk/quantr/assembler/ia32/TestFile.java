package hk.quantr.assembler.ia32;

import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;
import hk.quantr.assembler.antlr.AssemblerLexer;
import hk.quantr.assembler.antlr.AssemblerParser;
import org.antlr.v4.runtime.CharStreams;

public class TestFile {

	@Test
	public void test() throws Exception {
		AssemblerLexer lexer = new AssemblerLexer(CharStreams.fromStream(getClass().getResourceAsStream("1.asm")));

		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		AssemblerParser parser = new AssemblerParser(tokenStream);
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());
		AssemblerParser.AssembleContext context = parser.assemble();

		System.out.println(parser.labels.size());
		for (Label label : parser.labels) {
			System.out.println(label);
		}

		for (byte b : parser.mal.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println();
	}

}
