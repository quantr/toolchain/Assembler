package hk.quantr.assembler.ia32;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestNasm {

	@Test
	public void test1() {
		String instruction = "call 0x12:0x9";
		Pair<String, byte[]> bytesNasm16 = compileNasm(instruction, 16);
		Pair<String, byte[]> bytesNasm32 = compileNasm(instruction, 32);
		Pair<String, byte[]> bytesNasm64 = compileNasm(instruction, 64);
		System.out.println(instruction);
		System.out.println("bit 16 = " + bytesToString(bytesNasm16.getValue()));
		System.out.println("bit 32 = " + bytesToString(bytesNasm32.getValue()));
		System.out.println("bit 64 = " + bytesToString(bytesNasm64.getValue()));
	}

	String bytesToString(byte[] bytes) {
		if (bytes == null) {
			return "";
		}
		String str = "";
		for (byte b : bytes) {
			str += Integer.toHexString(b & 0xff) + " ";
		}
		return str;
	}

	Pair<String, byte[]> compileNasm(String instruction, int bits) {
		try {
			File temp = File.createTempFile("temp", ".asm");
			File tempBin = File.createTempFile("temp", ".bin");
			String code = "bits " + bits + "\n";
			code += instruction + "\n";
			FileUtils.write(temp, code, "UTF-8");
			String line;
			if (System.getProperty("os.name").contains("Mac")) {
				line = "/opt/local/bin/nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			} else if (System.getProperty("os.name").toLowerCase().contains("linux")) {
				line = "nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			} else {
				line = "nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			}

//			System.out.println(line);
//			CommandLine cmdLine = CommandLine.parse(line);
//			DefaultExecutor exec = new DefaultExecutor();
//			exec.execute(cmdLine);
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec(line.split(" "));
			p.waitFor();

			String err = IOUtils.toString(p.getErrorStream(), "utf8");
			err = err.replaceFirst(".*error:", "error:");
			System.out.println(err);

//			System.out.println("len=" + tempBin.length());
			if (tempBin.exists()) {
				byte bytes[] = FileUtils.readFileToByteArray(tempBin);

				if (temp.exists()) {
					temp.delete();
				}
				if (tempBin.exists()) {
//					System.out.println("remove " + tempBin.getAbsolutePath());
					tempBin.delete();
				}

				return new ImmutablePair<>(err, bytes);
			}
			return new ImmutablePair<>(err, null);
		} catch (IOException | InterruptedException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
