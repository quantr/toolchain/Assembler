package hk.quantr.assembler.riscv;

import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestSomething {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ExecutorService executorService = Executors.newFixedThreadPool(8);
		for (long x = 0; x <= 0xffffffffl; x++) {
//			System.out.printf("> %x\n", x);
			executorService.submit(new MyRunnable(x));
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}
	}

	class MyRunnable implements Runnable {

		long x;

		public MyRunnable(long x) {
			this.x = x;
		}

		@Override
		public void run() {
			try {
				System.out.printf("%x\n", x);
				System.out.flush();
				byte bytes[] = CommonLib.getByteArray(x);
				File temp = File.createTempFile("temp", ".bin");
				FileUtils.writeByteArrayToFile(temp, bytes);
				if (temp.exists()) {
					temp.delete();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				MessageHandler.errorPrintln(ex);
			}
		}
	}
}
