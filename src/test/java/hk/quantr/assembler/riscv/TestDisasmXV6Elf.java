package hk.quantr.assembler.riscv;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.il.Line;
import java.io.File;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDisasmXV6Elf {

	@Test
	public void test() throws IOException, Exception {
		File file = new File(TestDisasmXV6Elf.class.getResource("xv6/kernel").getPath());
		RISCVDisassembler disassembler = new RISCVDisassembler(false);
		DisasmStructure struct = disassembler.disasm(file);

		for (Line line : struct.lines) {
			System.out.println(line);
		}
	}
}
