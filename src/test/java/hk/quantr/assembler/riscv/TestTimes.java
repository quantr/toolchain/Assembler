///*
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
// */
//package hk.quantr.assembler.riscv;
//
//import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
//import hk.quantr.assembler.antlr.RISCVAssemblerParser;
////import hk.quantr.assembler.riscv.visitor.TimesVisitor;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import org.antlr.v4.runtime.ANTLRInputStream;
//import org.antlr.v4.runtime.CharStreams;
//import org.antlr.v4.runtime.CommonTokenStream;
//import org.junit.Test;
//
///**
// *
// * @author walter
// */
//public class TestTimes {
//	@Test
//	public void test1() throws FileNotFoundException, IOException {
//		String test = "%times 5 add x1 x2 x3";
//		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(test));
//		RISCVAssemblerParser parser = new RISCVAssemblerParser(new CommonTokenStream(lexer));
//		new TimesVisitor().visit(parser.assemble());
//	}
//}
