package hk.quantr.assembler.riscv;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.riscv.listener.ByteListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author lucas
 */
public class Testbyte {

	@Test
	public void test1() throws FileNotFoundException, IOException {
		String content = IOUtils.toString(new FileInputStream(new File("testbench/a.s")), "utf-8");
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
//	   parser.encoder.arch = "rv64";
//	   parser.removeErrorListeners();
//	   parser.addErrorListener(new UnderlineListener());

		ByteListener byteListener = new ByteListener();
		parser.addParseListener(byteListener);

		RISCVAssemblerParser.AssembleContext context = parser.assemble();

		for (int i : byteListener.bytelist) {
			System.out.println(i);
		}
	}
}
