package hk.quantr.assembler.riscv;

import hk.quantr.assembler.riscv.listener.LabelListener;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.BitSet;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestError {

	class MyErorrListener extends BaseErrorListener {

		String content;

		public MyErorrListener(String content) {
			this.content = content;
		}

		@Override
		public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
			System.out.println("syntaxError, " + msg);
			System.out.println(recognizer);
			System.out.println(offendingSymbol);
			String srcName = recognizer.getInputStream().getSourceName();
			System.err.println(srcName+" line "+line+":"+charPositionInLine+" "+msg);

			String lineStr = content.split("\n")[line - 2];
			System.out.println("lineStr=" + lineStr);
			if (lineStr.split(",").length <= 2) {
				System.out.println("hey, it is addi x1,x2,0x123");
			}
		}

		public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
			System.out.println("reportAmbiguity");
		}

		public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
			System.out.println("reportAttemptingFullContext");
		}

		public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {
			System.out.println("reportContextSensitivity");
		}
	}

	@Test
	public void test() throws FileNotFoundException, IOException {
		String content = IOUtils.toString(new FileInputStream("testbench/error1.s"), "utf-8");
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
		MyErorrListener errorListener = new MyErorrListener(content);
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		LabelListener labelListener = new LabelListener();
		parser.addParseListener(labelListener);
		parser.addErrorListener(errorListener);
		parser.assemble();
		for (byte b : parser.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println();
	}
}
