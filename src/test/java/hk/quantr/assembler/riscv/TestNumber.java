package hk.quantr.assembler.riscv;

import java.math.BigInteger;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestNumber {

	@Test
	public void test() {
		long b = (0xffL << 56) | (0xffL << 48) | (0xffL << 40) | (0xffL << 32) | (0x80L << 24) | (0xff << 16) | (0xff << 8) | 0xff;
//		System.out.printf("%x\n", b);
//		System.out.println(b);

		//ffffffff80ffffff - 1000000 = ffffffff70ffffff
//		BigInteger b2 = BigInteger.valueOf(b);
//		System.out.printf("%x\n", b2);
//		BigInteger bb = new BigInteger(1, new byte[]{(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff});
		BigInteger bb = BigInteger.valueOf(b);

		System.out.printf("%x\n", bb);

		long a = 0x80000000L;
		System.out.printf("%x\n", a);
	}
}
