package hk.quantr.assembler.riscv;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.riscv.il.Line;
import java.io.ByteArrayInputStream;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDisasmBytes2 {

	@Test
	public void test() throws Exception {
		RISCVDisassembler disassembler = new RISCVDisassembler();
		disassembler.disasm(new ByteArrayInputStream(new byte[]{(byte) 0xd9, (byte) 0x8f}));

		for (Line line : disassembler.disasmStructure.lines) {
			System.out.println(line);
		}

	}
}
