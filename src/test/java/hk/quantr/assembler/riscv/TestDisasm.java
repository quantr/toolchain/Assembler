package hk.quantr.assembler.riscv;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDisasm {

	@Test
	public void test() throws Exception {
		String[] words = {"ace", "boom", "crew", "dog", "eon"};
		List<String> wordList = Arrays.asList(words);
		System.out.println(wordList.stream().map(line -> line + "\n").reduce("", String::concat));

//		ClassLoader classLoader = getClass().getClassLoader();
//		InputStream inputStream = classLoader.getResourceAsStream(Paths.get("hk", "quantr", "assembler", "riscv", "big", "big.bin").toString());
//		RISCVDisassembler disassembler = new RISCVDisassembler();
//		disassembler.disasm(inputStream);
//		for (Line line : disassembler.disasmStructure.lines) {
//			System.out.println(line.code);
//		}
	}

}
