package hk.quantr.assembler.riscv.testcompiler;

import hk.quantr.assembler.AssemblerLib;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import hk.quantr.assembler.riscv.TestDisasmElf;
import java.io.File;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestMissingMacro {

	@Test
	public void test() throws IOException {
		File file = new File(TestMissingMacro.class.getResource("missing_macro.s").getPath());
		String content = AssemblerLib.preProcess(file, "rv64");
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = "RV64";
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());

		RISCVAssemblerParser.AssembleContext context = parser.assemble();
	}
}
