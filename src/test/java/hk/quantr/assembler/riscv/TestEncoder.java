package hk.quantr.assembler.riscv;

import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestEncoder {

	@Test
	public void test() throws Exception {

		String file = "encoderTestcase/test_compressed.txt";

		InputStream inputStream = getClass().getResourceAsStream(file);
		try ( BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				String arch = "rv64";
				String instruction = line;
				String gasInstruction = line;
				String instructions = instruction;
				
				RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(instructions));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
			
				parser.encoder.arch = arch;
				parser.removeErrorListeners();
				parser.addErrorListener(new UnderlineListener());
				RISCVAssemblerParser.AssembleContext context = parser.assemble();

				System.out.print("instruction: " + instruction + " Our Result: ");
				for (byte b : parser.encoder.out.toByteArray()) {
					System.out.print(Integer.toHexString(b & 0xff) + " ");
				}
				System.out.println("\n---");

				Pair<String, byte[]> returnValue = TestUtil.compileGas64(gasInstruction);
				byte[] bytes = returnValue.getValue();
				if (bytes == null) {
					System.out.println(returnValue.getKey());
				} else {
					if (bytes.length == 2) {
						System.out.println(Integer.toHexString(bytes[0] & 0xff) + " " + Integer.toHexString(bytes[1] & 0xff));

					} else {
						for (int z = 0; z < bytes.length; z++) {
							byte b = bytes[z];
							System.out.print(Integer.toHexString(b & 0xff) + " ");
						}
					}
				}
				System.out.println("\n---");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
