package hk.quantr.assembler.riscv.xml;

import java.io.File;
import java.io.FileInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ReadXPath {
	
	@Test
	public void test() throws Exception {
		//creating a constructor of file class and parsing an XML file  
		File file = new File(ReadXPath.class.getResource("XMLFile.xml").getPath());
		
		FileInputStream fileIS = new FileInputStream(file);
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document xmlDocument = builder.parse(fileIS);
		XPath xPath = XPathFactory.newInstance().newXPath();
		String expression = "/class/student";
		NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
		
		for (int x = 0; x < nodeList.getLength(); x++) {
			System.out.println("Try: "+nodeList.item(x) + ",value: " + nodeList.item(x).getTextContent());
		}
	}
}
