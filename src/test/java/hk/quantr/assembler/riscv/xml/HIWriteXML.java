package hk.quantr.assembler.riscv.xml;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.riscv.listener.RISCVErrorListener;
import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class HIWriteXML {

	@Test
	public void test() throws TransformerException, ParserConfigurationException, IOException {
		String content = IOUtils.toString(new FileInputStream("testbench/q.s"), "utf-8");
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
		RISCVErrorListener errorListener = new RISCVErrorListener(content);
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.addErrorListener(errorListener);
		ParseTree tree = parser.assemble();

//		System.out.println(errorListener.errorList.size());


//		for (HiError error : errorListener.errorList) {
//			System.out.println("hello\n");
//			String xml = xstream.toXML(error);
//			FileUtils.writeStringToFile(new File("hi_test.xml"), xml, "utf-8", true);
//		}

//		if (!errorListener.errorList.isEmpty()) {
//			XStream xstream = new XStream();
//			xstream.processAnnotations(ErrorMsg.class);
//			String xml = xstream.toXML(errorListener.errorList);
//			FileUtils.writeStringToFile(new File("hi_test.xml"), xml, "utf-8");
//
//			File file = new File("hi_test.xml");
//			System.out.println(file.getAbsolutePath());
//		}
		
	}
}
