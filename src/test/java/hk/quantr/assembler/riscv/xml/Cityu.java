package hk.quantr.assembler.riscv.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.List;

/**
 *
 * @author peter
 */
@XStreamAlias("Cityu")
public class Cityu {

	public String name;

	public int age;
	public List<Hobby> hobby;

	@XStreamAlias("student_number")
	public String StudentNo;
}
