package hk.quantr.assembler.riscv.xml;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ReadDeserialize {

	@Test
	public void test() throws TransformerException, ParserConfigurationException, IOException {
		String xml = IOUtils.toString(new FileReader(new File("staff-dom.xml")));
		XStream xstream = new XStream();
		xstream.processAnnotations(Cityu.class);
		Cityu darren = (Cityu) xstream.fromXML(xml);
		System.out.println(darren.name);
		System.out.println(darren.age);
		for (Hobby h : darren.hobby) {
			System.out.println("> " + h.name);
		}
	}
}
