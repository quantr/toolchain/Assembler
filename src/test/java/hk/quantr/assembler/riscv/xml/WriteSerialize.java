package hk.quantr.assembler.riscv.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class WriteSerialize {

    @Test
    public void test() throws TransformerException, ParserConfigurationException, IOException {
	   Cityu darren = new Cityu();
	   darren.name = "darren";
	   darren.age = 2;
	   darren.hobby = new ArrayList<>();
	   Hobby h1 = new Hobby();
	   h1.name = "sccor";
	   darren.hobby.add(h1);
	   Hobby h2 = new Hobby();
	   h2.name = "swim";
	   darren.hobby.add(h2);

	   darren.StudentNo = "S121212";

	   XStream xstream = new XStream();
	   xstream.processAnnotations(Cityu.class);
	   String xml = xstream.toXML(darren);
	   FileUtils.writeStringToFile(new File("staff-dom.xml"), xml, "utf-8");
	   File file = new File("staff-dom.xml");
	   System.out.println(file.getAbsolutePath());
    }
}
