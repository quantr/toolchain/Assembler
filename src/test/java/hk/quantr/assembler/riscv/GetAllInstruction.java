package hk.quantr.assembler.riscv;

import hk.quantr.assembler.antlr.AssemblerParser.InstructionsContext;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class GetAllInstruction {

	static String[] filters = new String[]{"get", "enterRule", "exitRule"};

	static boolean startsWithAny(String[] filters, String str) {
		for (String f : filters) {
			if (str.startsWith(f)) {
				return true;
			}
		}
		return false;
	}

	@Test
	public void testGetAllInstruction() throws Exception {
		for (String s : getAllInstructions()) {
			System.out.println(s);
		}
	}

	public static String[] getAllInstructions() throws Exception {
		Class c = InstructionsContext.class;
		Method[] m = c.getDeclaredMethods();

		Stream<Method> stream1 = Arrays.stream(m);
		//List<Method> list1 = stream1.map(y->y.getName).filter(x -> !startsWithAny(filters, x.getName())).collect(Collectors.toList());
		List<String> list1 = stream1.map(y -> y.getName()).filter(x -> !startsWithAny(filters, x)).collect(Collectors.toList());

		return list1.toArray(new String[0]);
	}

	public static List<String> getAllInstructionsFromFile(String filename) throws Exception {
		InputStream inputStream = GetAllInstruction.class.getResourceAsStream(filename);
		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, "utf-8");
		String theString = writer.toString();
		String lines[] = theString.split("\n");
		Stream<String> stream1 = Arrays.stream(lines);
		List<String> list1 = stream1.filter(x -> x.trim().length() > 0 && x.trim().charAt(0) != '#').map(x -> x.trim()).collect(Collectors.toList());
		return list1;
	}
}
