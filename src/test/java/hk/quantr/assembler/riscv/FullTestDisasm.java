package hk.quantr.assembler.riscv;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.javalib.CommonLib;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */

/*
	echo "73 05 00 00 00 00 00 00"  | xxd -r -p > a
	riscv64-unknown-elf-objdump -b binary -m riscv:rv64 -D a
 */
public class FullTestDisasm {

	static boolean runInPeterMac = false;
	static String mysql_username;
	static String mysql_password;
	static String mysql_host;
	static String mysql_database;

	static String CI_JOB_ID;
	static String GITLAB_USER_ID;
	static String GITLAB_USER_NAME;
	static String GITLAB_USER_EMAIL;
	static String GITLAB_USER_LOGIN;
	static int noOfThread = 8;

	static {
		if (runInPeterMac) {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "localhost";
			mysql_database = "assembler";
			CI_JOB_ID = "CI_JOB_ID";
			GITLAB_USER_ID = "GITLAB_USER_ID";
			GITLAB_USER_NAME = "GITLAB_USER_NAME";
			GITLAB_USER_EMAIL = "GITLAB_USER_EMAIL";
			GITLAB_USER_LOGIN = "GITLAB_USER_LOGIN";
		} else {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "192.168.0.176";
			mysql_database = "assembler";

			CI_JOB_ID = System.getProperty("CI_JOB_ID");
			GITLAB_USER_ID = System.getProperty("GITLAB_USER_ID");
			GITLAB_USER_NAME = System.getProperty("GITLAB_USER_NAME");
			GITLAB_USER_EMAIL = System.getProperty("GITLAB_USER_EMAIL");
			GITLAB_USER_LOGIN = System.getProperty("GITLAB_USER_LOGIN");
		}
	}

	@Test
	public void fullTest() throws Exception {
		Date from = new Date();

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);

		testTypeR(conn);
		testTypeI(conn);
		testTypeS(conn);
		testTypeU(conn);

		conn.close();

		Date to = new Date();
		long seconds = (to.getTime() - from.getTime()) / 1000;
		System.out.println(seconds + " seconds");
	}

	public void testTypeR(Connection conn) throws Exception {
		int opcodes[] = new int[]{0b1100011, 0b0010011, 0b1110011, 0b0110011, 0b0111011, 0b0101111};
		int rds[] = new int[]{0, 10};
		int funct3s[] = new int[]{0, 1, 2, 3, 4, 5, 6, 7};
		int rs1s[] = new int[]{0, 10};
		int rs2s[] = new int[]{0, 10};
		int funct7s[] = new int[]{0b0, 0b0100000, 0b0000001, 0b0000100, 0b0001000, 0b0001100, 0b0101100, 0b0010000, 0b0010100, 0b1100000, 0b1110000, 0b1010000, 0b1101000, 0b1111000, 0b0010101, 0b0100001, 0b1101001, 0b111001, 0b1111001};

		PreparedStatement pstmt = conn.prepareStatement("insert into disasm values(0, '" + CI_JOB_ID + "', ?, ?, ?, ?, ?, ?)");

		ExecutorService executorService = Executors.newFixedThreadPool(noOfThread);

		int rowNo = 0;
		outer:
		for (long funct7 : funct7s) {
			for (long rs2 : rs2s) {
				for (long rs1 : rs1s) {
					for (long funct3 : funct3s) {
						for (long rd : rds) {
							for (long opcode : opcodes) {

								long testingBytes = funct7 << 25 | rs2 << 20 | rs1 << 15 | funct3 << 12 | rd << 7 | opcode;
								String s = String.format("%32s", Long.toBinaryString(testingBytes)).replace(' ', '0');
								System.out.println(rowNo + " = " + s + " = " + Long.toHexString(testingBytes));
//								System.out.println(CommonLib.arrayToHexStringReverse(CommonLib.getByteArray(x), 4));
								executorService.submit(new MyRunnable(pstmt, testingBytes, rowNo, "R"));
								rowNo++;

								if (s.length() > 32) {
									System.err.println("Crazy number " + s);
									break outer;
								}
							}
						}
					}
				}
			}
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}
		pstmt.executeBatch();
	}

	public void testTypeI(Connection conn) throws Exception {

		int opcodes[] = new int[]{0b0010011, 0b0011011, 0b1110011, 0b0000111, 0b0000011};
		int rds[] = new int[]{0, 1};
		int funct3s[] = new int[]{0, 1, 2, 3, 4, 5, 6, 7};
		int rs1s[] = new int[]{0, 10};
		int imms[] = new int[]{0b0, 0b0100000, 0b1111001};

		PreparedStatement pstmt = conn.prepareStatement("insert into disasm values(0, '" + CI_JOB_ID + "', ?, ?, ?, ?, ?,?)");

		ExecutorService executorService = Executors.newFixedThreadPool(noOfThread);

		int rowNo = 0;

		outer:
		for (long imm : imms) {
			for (long rs1 : rs1s) {
				for (long funct3 : funct3s) {
					for (long rd : rds) {
						for (long opcode : opcodes) {
							long testingBytes = imm << 20 | rs1 << 15 | funct3 << 12 | rd << 7 | opcode;
							String s = String.format("%32s", Long.toBinaryString(testingBytes)).replace(' ', '0');
							System.out.println(rowNo + " = " + s + " = " + Long.toHexString(testingBytes));
//								System.out.println(CommonLib.arrayToHexStringReverse(CommonLib.getByteArray(x), 4));
							executorService.submit(new MyRunnable(pstmt, testingBytes, rowNo, "I"));
							rowNo++;

							if (s.length() > 32) {
								System.err.println("Crazy number " + s);
								break outer;
							}
						}
					}
				}
			}
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}
		pstmt.executeBatch();
	}

	public void testTypeS(Connection conn) throws Exception {

		int opcodes[] = new int[]{0b0100011, 0b0100111};
		int funct3s[] = new int[]{0, 1, 2, 3};
		int rs1s[] = new int[]{0, 10};
		int rs2s[] = new int[]{0, 10};
		int imms[] = new int[]{0b0, 0b0100000, 0b1111001};
		
		PreparedStatement pstmt = conn.prepareStatement("insert into disasm values(0, '" + CI_JOB_ID + "', ?, ?, ?, ?, ?, ?)");

		ExecutorService executorService = Executors.newFixedThreadPool(noOfThread);

		int rowNo = 0;

		
		outer:
		for (long imm : imms) {
			for (long rs2 : rs2s) {
				for (long rs1 : rs1s) {
					for (long funct3 : funct3s) {

						for (long opcode : opcodes) {
							
							long imm4_0 = imm & 0x1f; 
							long imm5_11 = (imm & 0xfe0) >> 5 ;
							long testingBytes = imm5_11 << 25 |rs2<<20| rs1 << 15 | funct3 << 12 | imm4_0<< 7 | opcode;
							String s = String.format("%32s", Long.toBinaryString(testingBytes)).replace(' ', '0');
							System.out.println(rowNo + " = " + s + " = " + Long.toHexString(testingBytes));
//								System.out.println(CommonLib.arrayToHexStringReverse(CommonLib.getByteArray(x), 4));
							executorService.submit(new MyRunnable(pstmt, testingBytes, rowNo,"S"));
							rowNo++;

							if (s.length() > 32) {
								System.err.println("Crazy number " + s);
								break outer;
							}
						}

					}
				}
			}
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}
		pstmt.executeBatch();
	}
	public void testTypeU(Connection conn) throws Exception {

		int opcodes[] = new int[]{0b0010111, 0b0110111};
		int rds[] = new int[]{0, 1};
		int imms[] = new int[]{0b0, 0b0100000, 0b11110010000000000000};

		PreparedStatement pstmt = conn.prepareStatement("insert into disasm values(0, '" + CI_JOB_ID + "', ?, ?, ?, ?, ?,?)");

		ExecutorService executorService = Executors.newFixedThreadPool(noOfThread);

		int rowNo = 0;

		outer:
		for (long imm : imms) {
			for (long rd : rds) {
				
					
						for (long opcode : opcodes) {
							long testingBytes = imm << 12 | rd << 7 | opcode;
							String s = String.format("%32s", Long.toBinaryString(testingBytes)).replace(' ', '0');
							System.out.println(rowNo + " = " + s + " = " + Long.toHexString(testingBytes));
//								System.out.println(CommonLib.arrayToHexStringReverse(CommonLib.getByteArray(x), 4));
							executorService.submit(new MyRunnable(pstmt, testingBytes, rowNo, "U"));
							rowNo++;

							if (s.length() > 32) {
								System.err.println("Crazy number " + s);
								break outer;
							}
						}
					
				
			}
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}
		pstmt.executeBatch();
	}

	class MyRunnable implements Runnable {

		final PreparedStatement pstmt;
		long testingBytes;
		int rowNo;
		String type;

		public MyRunnable(PreparedStatement pstmt, long testingBytes, int rowNo, String type) {
			this.pstmt = pstmt;
			this.testingBytes = testingBytes;
			this.rowNo = rowNo;
			this.type = type;
		}

		@Override
		public void run() {
			try {
				byte bytes[] = CommonLib.getByteArray(testingBytes);

				RISCVDisassembler disassembler = new RISCVDisassembler();
				disassembler.disasm(new ByteArrayInputStream(bytes));

				String lines = "";
				for (Line line : disassembler.disasmStructure.lines) {
					if (line.code == null) {
						continue;
					}
					lines += line.code + "\n";
				}
				lines = lines.trim();

				File temp = File.createTempFile("temp", ".bin");
				FileUtils.writeByteArrayToFile(temp, bytes);

				String output = CommonLib.runCommand("riscv64-unknown-elf-objdump -b binary -m riscv:rv64 -D " + temp.getAbsolutePath());
				output = output.replaceAll("\\.\\.\\.", "");
				output = output.trim();
				if (temp.exists()) {
					temp.delete();
				}
				int distance = FuzzySearch.ratio(lines, output);

				synchronized (pstmt) {
					int z = 1;
//					pstmt.setString(z++,s CommonLib.arrayToHexStringReverse(bytes, 4));
					pstmt.setString(z++, lines);
					pstmt.setString(z++, FullTestDisasm.getLastLines(output, 2).stream().map(line -> line.replaceFirst(".*?\t.*?\t", "") + "\n").filter(line -> !line.contains(".data")).reduce("", String::concat));
					pstmt.setInt(z++, distance);
					pstmt.setString(z++, type);
					pstmt.setInt(z++, rowNo);
					pstmt.addBatch();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				MessageHandler.errorPrintln(ex);
			}
		}
	}

	private static final String SEPARATOR = "\n";

	public static List<String> getLastLines(String string, int numLines) {
		List<String> lines = Arrays.asList(string.split(SEPARATOR));
		int lineCount = lines.size();
		return lineCount > numLines ? lines.subList(lineCount - numLines, lineCount) : lines;
	}
}
