package hk.quantr.assembler.riscv;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestFuzzyCompare {

	@Test
	public void test() {
		String str1 = "c.lw s0,24(a2)";
		String str2 = "4e00                	lw	s0,24(a2)";
		int distance = FuzzySearch.ratio(str1, str2);
		System.out.println(distance);
	}
}
