package hk.quantr.assembler.riscv;

import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestInstructionTranslate {

	@Test
	public void test() throws Exception {
		List<String> instructions = GetAllInstruction.getAllInstructionsFromFile("testcase_2.txt");
		for (String instruction : instructions) {
			System.out.println("instruction=" + instruction);
			Pair<String[], String[]> pair = TestUtil.translateInstruction(instruction);
			String[] translatedInstructions = pair.getLeft();
			String[] translatedInstructionsGas = pair.getRight();

			for (int x = 0; x < translatedInstructions.length; x++) {
				System.out.println("\t" + translatedInstructions[x] + " \t - \t " + translatedInstructionsGas[x].replaceAll("\n", " "));
			}
		}
	}
}
