package hk.quantr.assembler.riscv;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.il.Line;
import java.io.File;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDisasmElf {

	@Test
	public void test() throws IOException, Exception {
		File file = new File(TestDisasmElf.class.getResource("Running_Led/Running_Led.elf").getPath());
		RISCVDisassembler disassembler = new RISCVDisassembler(false);
		DisasmStructure struct = disassembler.disasm(file);

		for (Line line : struct.lines) {
			System.out.println(line);
		}
	}
}
