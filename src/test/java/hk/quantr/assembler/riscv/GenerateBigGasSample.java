package hk.quantr.assembler.riscv;

import hk.quantr.assembler.print.MessageHandler;
import static hk.quantr.assembler.riscv.TestUtil.concatArraysToList;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class GenerateBigGasSample {

	static final HashMap<String, ArrayList<String>> translatedMapGas = new HashMap<String, ArrayList<String>>();

	static {
		translatedMapGas.put("${rd}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMapGas.put("${rs1}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMapGas.put("${rs2}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMapGas.put("${rt}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMapGas.put("${shamt}", concatArraysToList(new String[]{"0x00", "0x1e"}));
		translatedMapGas.put("${imm}", concatArraysToList(new String[]{"0x00", "0x1f", "0x3f"}));
		translatedMapGas.put("${offset}", concatArraysToList(new String[]{"0x00", "0x1f", "0x3f", "0x4f", "0xff", "0x1ff", "0xffff"}));
		translatedMapGas.put("${rwio}", concatArraysToList(new String[]{"r", "w", "rw", "i", "o", "io", "ior", "iow", "iorw"}));
		translatedMapGas.put("${label}", concatArraysToList(new String[]{"0x12", "0x16", "0x3f", "0xff", "0x1234"}));

//		translatedMapGas.putAll(translatedMap);
//		translatedMapGas.remove("${label}");
//		List<String> temp = translatedMap.get("${label}").stream().map(s -> "label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s) - 4) + "\nlabel1:").collect(Collectors.toList());
//		translatedMapGas.put("${label}", (ArrayList<String>) temp);
//		ArrayList<String> temp = new ArrayList<>();
//		for (String s : translatedMap.get("${label}")) {
//			if (s.startsWith("-")) {
//				temp.add("label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s) - 4) + "\nlabel1:");
//			} else {
//				temp.add("label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s) - 4) + "\nlabel1:");
//			}
//		}
//		translatedMapGas.put("${label}", temp);
	}

	@Test
	public void test() throws Exception {
		System.out.println("GenerateBigGasSample");
		FileOutputStream fos = new FileOutputStream("src/test/resources/hk/quantr/assembler/riscv/big/big.s");
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		bw.write(".section .text\n"
				+ ".globl _start\n"
				+ "_start:\n");

		List<String> instructions = getAllInstructionsFromFile("testcase_for_GenerateBigGasSample.txt");
		for (String instruction : instructions) {
			String[] translatedInstructions = translateInstruction(instruction);
			for (String translatedInstruction : translatedInstructions) {
				bw.write("\t" + translatedInstruction);
				System.out.println(translatedInstruction);
				bw.newLine();
			}
		}
		bw.close();
		fos.close();
	}

	public List<String> getAllInstructionsFromFile(String filename) throws Exception {
		InputStream inputStream = GetAllInstruction.class.getResourceAsStream(filename);
		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, "utf-8");
		String theString = writer.toString();
		String lines[] = theString.split("\n");
		Stream<String> stream1 = Arrays.stream(lines);
		List<String> list1 = stream1.filter(x -> x.trim().length() > 0 && x.trim().charAt(0) != '#').map(x -> x.trim()).collect(Collectors.toList());
		return list1;
	}

	public static String[] translateInstruction(String testingInstruction) {
		return translateInstruction(translatedMapGas, testingInstruction);
	}

	public static String[] translateInstruction(HashMap<String, ArrayList<String>> myTranslatedMap, String testingInstruction) {
//		System.out.println("\t\t\t" + testingInstruction);
		String patternStr = "\\$\\{[^\\}]+\\}";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(testingInstruction);

		ArrayList<String> groups = new ArrayList<>();
		ArrayList<Integer> starts = new ArrayList<>();
		ArrayList<Integer> ends = new ArrayList<>();
		while (matcher.find()) {
//			System.out.println("matcher.group() : " + matcher.group() + ", " + matcher.start() + " > " + matcher.end());
			groups.add(matcher.group());
			starts.add(matcher.start());
			ends.add(matcher.end());
		}

		if (groups.isEmpty()) {
			return new String[]{testingInstruction};
		} else {
			LinkedHashSet<String> instructions = new LinkedHashSet<>();
			for (int x = 0; x < groups.size(); x++) {
				String group = groups.get(x);
//				int start = starts.get(x);
//				int end = ends.get(x);
				ArrayList<String> elements = myTranslatedMap.get(group);
				if (elements == null) {
					MessageHandler.errorPrintln("Wrong test case : " +testingInstruction);
					System.exit(10);
				}
//				if (instructions.isEmpty()) {
				for (String element : elements) {
					String translatedInstruction = testingInstruction.replaceFirst(Pattern.quote(group), element);
					instructions.addAll(Arrays.asList(translateInstruction(myTranslatedMap, translatedInstruction)));
				}
//				}
			}

			return instructions.toArray(new String[0]);
		}
	}
}
