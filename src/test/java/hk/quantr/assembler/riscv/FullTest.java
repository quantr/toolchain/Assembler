package hk.quantr.assembler.riscv;

import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.MyTableModel.Data;
import hk.quantr.javalib.CommonLib;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class FullTest {

	public static final Logger logger = Logger.getLogger(FullTest.class.getName());
	boolean runInPeterMac = false;

	static String mysql_username;
	static String mysql_password;
	static String mysql_host;
	static String mysql_database;

	@Test
	public void fullTest() throws Exception {
		Date from = new Date();

		String CI_JOB_ID;
		String GITLAB_USER_ID;
		String GITLAB_USER_NAME;
		String GITLAB_USER_EMAIL;
		String GITLAB_USER_LOGIN;
		if (runInPeterMac) {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "localhost";
			mysql_database = "assembler";
			CI_JOB_ID = "CI_JOB_ID";
			GITLAB_USER_ID = "GITLAB_USER_ID";
			GITLAB_USER_NAME = "GITLAB_USER_NAME";
			GITLAB_USER_EMAIL = "GITLAB_USER_EMAIL";
			GITLAB_USER_LOGIN = "GITLAB_USER_LOGIN";
		} else {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "192.168.0.176";
			mysql_database = "assembler";

			CI_JOB_ID = System.getProperty("CI_JOB_ID");
			GITLAB_USER_ID = System.getProperty("GITLAB_USER_ID");
			GITLAB_USER_NAME = System.getProperty("GITLAB_USER_NAME");
			GITLAB_USER_EMAIL = System.getProperty("GITLAB_USER_EMAIL");
			GITLAB_USER_LOGIN = System.getProperty("GITLAB_USER_LOGIN");
		}

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);

		if (CI_JOB_ID != null) {
			PreparedStatement pstmt2 = conn.prepareStatement("insert into `job` values(0, now(), ?, ?, ?, ?, ?);");
			int z2 = 1;
			pstmt2.setString(z2++, CI_JOB_ID);
			pstmt2.setString(z2++, GITLAB_USER_ID);
			pstmt2.setString(z2++, GITLAB_USER_NAME);
			pstmt2.setString(z2++, GITLAB_USER_EMAIL);
			pstmt2.setString(z2++, GITLAB_USER_LOGIN);
			pstmt2.execute();
		}

		ResultSet resultSet = conn.createStatement().executeQuery("select * from riscv_gas;");
		HashSet<String> gasCodes = new HashSet<>();
		while (resultSet.next()) {
			gasCodes.add(resultSet.getString("code"));
		}

		MyTableModel model = new MyTableModel();

		ExecutorService executorService = Executors.newFixedThreadPool(8);
		int x = 0;

		List<String> instructions = GetAllInstruction.getAllInstructionsFromFile("testcase.txt");
		for (String instruction : instructions) {
//			System.out.println("instruction=" + instruction);
			if (instruction.trim().equals("")) {
				continue;
			}
			String[] translatedInstructions = TestUtil.translateInstruction(instruction).getLeft();
			x += translatedInstructions.length;

//			for (String i : translatedInstructions) {
//				System.out.println(i);
//			}
		}

		System.out.println("number of instructions = " + x);

//		x = 1;
//		instructions.clear();
//		instructions.add("fabs.s x2");
		CountDownLatch latch = new CountDownLatch(x);
		x = 0;
		PreparedStatement pstmt_riscv = conn.prepareStatement("insert into riscv_gas values(0, now(), ?, ?, ?, ?, ?, ?)");
		for (String instruction : instructions) {
			Pair<String[], String[]> pair = TestUtil.translateInstruction(instruction);
			String[] translatedInstructions = pair.getLeft();
			String[] gasTranslatedInstructions = pair.getRight();
			if (translatedInstructions.length != gasTranslatedInstructions.length) {
				MessageHandler.errorPrintln(translatedInstructions.length + "!=" + gasTranslatedInstructions.length);
			}
			for (int x2 = 0; x2 < translatedInstructions.length; x2++) {
				executorService.submit(new MyRunnable(translatedInstructions, gasTranslatedInstructions, x2, gasCodes, pstmt_riscv, model, latch));
				//new MyRunnable(translatedInstructions, gasTranslatedInstructions, x2, gasCodes, pstmt_riscv, model, latch).run(); // this test multithread bug
				x++;
			}
			if (x % 100 == 0) {
				synchronized (pstmt_riscv) {
					pstmt_riscv.executeBatch();
				}
			}
		}

		while (latch.getCount() > 0) {
			double percent = Math.round((double) ((x - latch.getCount()) * 100) / x);
//			System.out.println(sb.toString() + percent + "% (" + (x - latch.getCount()) + " / " + x + ")");
			System.out.println(percent + "% (" + (x - latch.getCount()) + " / " + x + ")");
			TimeUnit.MILLISECONDS.sleep(1000);
		}
		pstmt_riscv.executeBatch();

		conn.createStatement().execute("delete t1 from riscv_gas t1\n"
				+ "inner join riscv_gas t2 \n"
				+ "where \n"
				+ "    t1.id < t2.id and \n"
				+ "    t1.quantrCode = t2.quantrCode;");

		conn.createStatement().execute("delete t1 from test_riscv t1\n"
				+ "inner join test_riscv t2 \n"
				+ "where \n"
				+ "    t1.id < t2.id and \n"
				+ "    t1.quantrCode = t2.quantrCode\n"
				+ "    and t1.CI_JOB_ID='" + CI_JOB_ID + "'\n"
				+ "    and t2.CI_JOB_ID='" + CI_JOB_ID + "';");

		conn.createStatement().execute("delete from test_riscv where CI_JOB_ID not in (select b.* from (SELECT CI_JOB_ID FROM `job` group by CI_JOB_ID order by min(datetime) desc limit 0,4) as b)");

		System.out.println("done " + model.data.size());

		//remove useless instructions
//		for (int y = model.data.size() - 1; y >= 0; y--) {
//			Data data = model.data.get(y);
//			if (data.bytes16 == null && data.bytes32 == null && data.bytes64 == null && data.bytesNasm16 == null && data.bytesNasm32 == null && data.bytesNasm64 == null) {
//				model.data.remove(y);
//			}
//		}
		//end remove useless instructions
		if (CI_JOB_ID != null) {
			PreparedStatement pstmt = conn.prepareStatement("insert into test_riscv values(0, now(), -1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (int y = 0; y < model.data.size(); y++) {
				Data data = model.data.get(y);
				System.out.println(data.quantrCode + " > " + CommonLib.arrayToHexString(data.quantr32Bytes.getValue()));
				try {
					int z = 1;
					pstmt.setString(z++, CI_JOB_ID);
					pstmt.setString(z++, GITLAB_USER_ID);
					pstmt.setString(z++, GITLAB_USER_NAME);
					pstmt.setString(z++, GITLAB_USER_EMAIL);
					pstmt.setString(z++, GITLAB_USER_LOGIN);
					pstmt.setString(z++, data.quantrCode);
					pstmt.setString(z++, CommonLib.arrayToHexString(data.quantr32Bytes.getValue()));
					pstmt.setString(z++, data.quantr32Bytes.getKey());
					pstmt.setString(z++, CommonLib.arrayToHexString(data.quantr64Bytes.getValue()));
					pstmt.setString(z++, data.quantr64Bytes.getKey());
//					pstmt.execute();
					pstmt.addBatch();
				} catch (SQLException ex) {
					MessageHandler.errorPrintln(ex);
				}
			}
			pstmt.executeBatch();

			conn.createStatement().execute("SET @pos := -1;");
			conn.createStatement().execute("update test_riscv SET rowNo = ( SELECT @pos := @pos + 1 ) WHERE `CI_JOB_ID`='" + CI_JOB_ID + "' order by datetime");
		}
		conn.close();

		Date to = new Date();
		long seconds = (to.getTime() - from.getTime()) / 1000;
		System.out.println(seconds + " seconds");
	}

	public List<InetAddress> getListOfIPsFromNIs() throws SocketException {
		List<InetAddress> addrList = new ArrayList<InetAddress>();
		Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();
		while (enumNI.hasMoreElements()) {
			NetworkInterface ifc = enumNI.nextElement();
			if (ifc.isUp()) {
				Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
				while (enumAdds.hasMoreElements()) {
					InetAddress addr = enumAdds.nextElement();
					addrList.add(addr);
					System.out.println(addr.getHostAddress());   //<---print IP
				}
			}
		}
		return addrList;
	}

	class MyRunnable implements Runnable {

		String translatedInstruction;
		String gasTranslatedInstruction;
		HashSet<String> gasCodes;
		final PreparedStatement pstmt_riscv;
		MyTableModel model;
		CountDownLatch latch;

		public MyRunnable(String[] translatedInstructions, String[] gasTranslatedInstructions, int x, HashSet<String> gasCodes, PreparedStatement pstmt_riscv, MyTableModel model, CountDownLatch latch) {
			this.translatedInstruction = translatedInstructions[x];
			this.gasTranslatedInstruction = gasTranslatedInstructions[x];
			this.gasCodes = gasCodes;
			this.pstmt_riscv = pstmt_riscv;
			this.model = model;
			this.latch = latch;
		}

		@Override
		public void run() {
			System.out.println("quantr > " + translatedInstruction);
			System.out.println("gas    > " + gasTranslatedInstruction);
			try {
				Pair<String, byte[]> bytesQuantr32 = TestUtil.compileRISCV(translatedInstruction, 32);
				Pair<String, byte[]> bytesQuantr64 = TestUtil.compileRISCV(translatedInstruction, 64);
				Pair<String, byte[]> bytesGas32;
				Pair<String, byte[]> bytesGas64;

				if (!gasCodes.contains(gasTranslatedInstruction)) {
					bytesGas32 = TestUtil.compileGas32(gasTranslatedInstruction);
					bytesGas64 = TestUtil.compileGas64(gasTranslatedInstruction);
					int z = 1;
					synchronized (pstmt_riscv) {
						pstmt_riscv.setString(z++, gasTranslatedInstruction);
						pstmt_riscv.setString(z++, translatedInstruction);

						// 32s bits gas
						byte[] bytes32 = bytesGas32.getValue();
						if (bytes32 == null) {
							pstmt_riscv.setString(z++, null);
						} else {
							byte[] bytesInCorrectOrder;
							if (bytes32.length == 2) {
								bytesInCorrectOrder = new byte[2];
								bytesInCorrectOrder[0] = bytes32[0];
								bytesInCorrectOrder[1] = bytes32[1];
							} else {
								bytesInCorrectOrder = new byte[4];
								int index = 0;
								for (int xx = 0; xx < 4; xx++) {
									if (xx < bytes32.length) {
										bytesInCorrectOrder[index++] = bytes32[xx];
									}
								}
							}
							pstmt_riscv.setString(z++, CommonLib.arrayToHexString(bytesInCorrectOrder));
						}
						pstmt_riscv.setString(z++, bytesGas32.getKey());
						// 32 bits gas end

						// 64 bits gas
						byte[] bytes64 = bytesGas64.getValue();
						if (bytes64 == null) {
							pstmt_riscv.setString(z++, null);
						} else {
							byte[] bytesInCorrectOrder;
							if (bytes64.length == 2) {
								bytesInCorrectOrder = new byte[2];
								bytesInCorrectOrder[0] = bytes64[0];
								bytesInCorrectOrder[1] = bytes64[1];
							} else {
								bytesInCorrectOrder = new byte[4];
								int index = 0;
								for (int xx = 0; xx < 4; xx++) {
									if (xx < bytes64.length) {
										bytesInCorrectOrder[index++] = bytes64[xx];
									}
								}
							}
							pstmt_riscv.setString(z++, CommonLib.arrayToHexString(bytesInCorrectOrder));
						}
						pstmt_riscv.setString(z++, bytesGas64.getKey());
						// end 64 bits gas
						pstmt_riscv.addBatch();
					}
				}

				synchronized (model) {
					model.add("", translatedInstruction, gasTranslatedInstruction, bytesQuantr32, bytesQuantr64);
				}
			} catch (Exception ex) {
				// it has to handle Exception here, otherwise when it throw any other exception, latch.countDown won't run, then hang
				ex.printStackTrace();
				MessageHandler.errorPrintln(ex);
			}
			latch.countDown();
		}
	}
}
