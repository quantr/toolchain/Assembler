package hk.quantr.assembler.riscv;

import hk.quantr.assembler.Assembler;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestListing {


	@Test
	public void test() {
		Assembler.main("-a rv32 -l a.lst -o a.bin src/test/resources/hk/quantr/assembler/riscv/a.asm".split(" "));


	}
}
