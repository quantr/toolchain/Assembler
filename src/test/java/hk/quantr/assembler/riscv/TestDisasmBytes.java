package hk.quantr.assembler.riscv;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.il.Line;
import java.io.ByteArrayInputStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDisasmBytes {

	@Test
	public void test() throws Exception {
		String arch = "rv64";
//		String instruction = "addi x1,x2,0x1fff";
//		String instruction = "sret";
//		String instruction = "c.sw a4,32(a0)";
		String instruction = "c.addw a5,a5,a4";
//		String instruction = "vwsubu.wx v0, v2, a3";
				//		String instruction = "vsetvli a0, a0, e32, m8, ta, ma";
		String instructions = instruction;
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(instructions));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		System.out.println(instruction);
		System.out.println("\n---");

		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = arch;
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());

		RISCVAssemblerParser.AssembleContext context = parser.assemble();
		for (byte b : parser.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println("\n---");

		RISCVDisassembler disassembler = new RISCVDisassembler();
//		disassembler.disasm(new ByteArrayInputStream(parser.encoder.out.toByteArray()));
//		disassembler.disasm(new ByteArrayInputStream(new byte[]{(byte) 0x73, (byte) 0x00, (byte) 0x00, (byte) 0x00}));
		disassembler.disasm(new ByteArrayInputStream(new byte[]{(byte) 0xb9, (byte) 0x9f}));
//		byte[] bytes = CommonLib.getByteArray((int) 16777875l);
//		CommonLib.reverseByteArray(bytes);
//		disassembler.disasm(new ByteArrayInputStream(bytes));

		for (Line line : disassembler.disasmStructure.lines) {
			System.out.println(line);
			System.out.println(line.rd);
			System.out.println(line.rs2);
		}

//		disassembler.disasm(new ByteArrayInputStream(CommonLib.getByteArray(0x1213075l)));
//		for (Line line : disassembler.disasmStructure.lines) {
//			System.out.println(line);
//
//		}
	}
}
