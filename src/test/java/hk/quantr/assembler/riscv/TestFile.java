package hk.quantr.assembler.riscv;

import hk.quantr.assembler.riscv.listener.LabelListener;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestFile {

	@Test
	public void test() throws FileNotFoundException, IOException {
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromStream(new FileInputStream(TestFile.class.getResource("dotbyte/a.s").getPath())));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = "rv64";
		LabelListener labelListener = new LabelListener();
		parser.addParseListener(labelListener);
		parser.assemble();
		for (byte b : parser.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println();
	}
}
