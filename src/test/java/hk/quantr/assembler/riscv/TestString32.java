package hk.quantr.assembler.riscv;

import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import hk.quantr.assembler.print.MessageHandler;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestString32 {

	@Test
	public void test() throws Exception {
		String arch = "rv32";
		String instruction = "c.lwsp x1, 10(x2)";
		String gasInstruction = "c.lwsp x1, 10";
		String instructions = instruction;
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(instructions));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = arch;
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());
//		ParseTreeWalker walker = new ParseTreeWalker();

//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		MAL listener = new MAL(parser, out);
		RISCVAssemblerParser.AssembleContext context = parser.assemble();
//		walker.walk(listener, context);
		for (byte b : parser.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println("\n---");

		Pair<String, byte[]> returnValue = TestUtil.compileGas32(gasInstruction);
		byte[] bytes = returnValue.getValue();
		if (bytes == null) {
			System.out.println(returnValue.getKey());
		} else {
			if (bytes.length == 2) {
				System.out.print(Integer.toHexString(bytes[1] & 0xff) + " ");
				System.out.print(Integer.toHexString(bytes[0] & 0xff) + " ");
			} else {
				for (int z = 0; z < bytes.length; z++) {
//					for (int x = z + 3; x >= z; x--) {
					byte b = bytes[z];
					System.out.print(Integer.toHexString(b & 0xff) + " ");
				}
				/*			for (int z = 0; z < bytes.length; z += 4) {
//					for (int x = z + 3; x >= z; x--) {
					for (int x = 0; x < z + 4; x++) {
						byte b = bytes[x];
						System.out.print(Integer.toHexString(b & 0xff) + " ");
					}*/
//				}
			}
			System.out.println();
		}
//		for (Token t : tokenStream.getTokens()) {
//			System.out.println(">" + t.getText());
//		}
	}
}
