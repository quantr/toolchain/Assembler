package hk.quantr.assembler.riscv;

import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import org.antlr.v4.runtime.CharStreams;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestString64 {

	@Test
	public void test() throws Exception {
		String arch = "rv64";
		String instruction = "c.ld a2,4(a0)";
		String gasInstruction = "c.ld a2,4(a0)";
//		String instruction = "c.addi16sp x2,64";
//		String gasInstruction = "c.addi16sp x2,64";
//		String instruction = "c.fld f8,4(f9)";
//		String gasInstruction = "c.fld f8,4(f9)";
//		String instruction = "c.lw x8,0x40(x9)";
//		String gasInstruction = "c.lw x8,0x40(x9)";
		String instructions = instruction;
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(instructions));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = arch;
		parser.removeErrorListeners();
		parser.addErrorListener(new UnderlineListener());
//		ParseTreeWalker walker = new ParseTreeWalker();

//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		MAL listener = new MAL(parser, out);
		RISCVAssemblerParser.AssembleContext context = parser.assemble();
//		walker.walk(listener, context);
		System.out.println("instruction: "+instruction);

		for (byte b : parser.encoder.out.toByteArray()) {
			System.out.print(Integer.toHexString(b & 0xff) + " ");
		}
		System.out.println("\n---");

		Pair<String, byte[]> returnValue = TestUtil.compileGas64(gasInstruction);
		byte[] bytes = returnValue.getValue();
		if (bytes == null) {
			System.out.println(returnValue.getKey());
		} else {
			if (bytes.length == 2) {
				System.out.println(Integer.toHexString(bytes[0] & 0xff) + " " + Integer.toHexString(bytes[1] & 0xff));
			
			} else {
				for (int z = 0; z < bytes.length; z++) {
//					for (int x = z + 3; x >= z; x--) {
					byte b = bytes[z];
					System.out.print(Integer.toHexString(b & 0xff) + " ");
				}
				/*			for (int z = 0; z < bytes.length; z += 4) {
//					for (int x = z + 3; x >= z; x--) {
					for (int x = 0; x < z + 4; x++) {
						byte b = bytes[x];
						System.out.print(Integer.toHexString(b & 0xff) + " ");
					}*/
//				}
			}
	
		}
//		for (Token t : tokenStream.getTokens()) {
//			System.out.println(">" + t.getText());
//		}
	}
}
