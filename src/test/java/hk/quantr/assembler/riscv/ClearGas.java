package hk.quantr.assembler.riscv;

import java.sql.Connection;
import java.sql.DriverManager;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ClearGas {

	boolean runInPeterMac = false;

	static String mysql_username;
	static String mysql_password;
	static String mysql_host;
	static String mysql_database;

	@Test
	public void test() throws Exception {

		if (runInPeterMac) {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "localhost";
			mysql_database = "assembler";
		} else {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "192.168.0.176";
			mysql_database = "assembler";
		}

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);

		conn.createStatement().execute("truncate table riscv_gas;");
	}
}

