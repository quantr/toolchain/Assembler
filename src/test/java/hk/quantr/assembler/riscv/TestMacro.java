package hk.quantr.assembler.riscv;

import hk.quantr.assembler.AssemblerLib;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestMacro {

	@Test
	public void test1() throws FileNotFoundException, IOException {
		String content = AssemblerLib.preProcess(new File("testbench/define.s"), "rv64");
		System.out.println(content);
//	   String content = IOUtils.toString(new FileInputStream(new File("testbench/a.s")), "utf-8");
//	   RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
//	   CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//
//	   RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
//	   parser.encoder.arch = "rv64";
//	   parser.removeErrorListeners();
//	   parser.addErrorListener(new UnderlineListener());
//
//	   macroListener includeListener = new macroListener();
//	   parser.addParseListener(includeListener);
//
//	   RISCVAssemblerParser.AssembleContext context = parser.assemble();
//	   for (byte b : parser.encoder.out.toByteArray()) {
//		  System.out.print(Integer.toHexString(b & 0xff) + " ");
//	   }
//	   System.out.println("\n---");
//
////	   for (Integer line : macroListener.includes.keySet()) {
////		  String filename = macroListener.includes.get(line);
////		  try {
////			 String temp = IOUtils.toString(new FileInputStream(new File("testbench/" + filename)), "utf-8");
////			 content = temp + content;
////		  } catch (Exception ex) {
////			 System.out.println("File not exist: " + filename);
////		  }
////	   }
//
//	   for (String i : macroListener.includes.keySet()) {
//		  content = content.replace(i, macroListener.includes.get(i));
//	   }
//
//	   while (content.contains("%include")) {
//		  macroListener.includes.clear();
//		  lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
//		  tokenStream = new CommonTokenStream(lexer);
//		  parser = new RISCVAssemblerParser(tokenStream);
//		  parser.encoder.arch = "rv64";
//		  parser.removeErrorListeners();
//		  parser.addErrorListener(new UnderlineListener());
//		  includeListener = new macroListener();
//		  parser.addParseListener(includeListener);
//		  context = parser.assemble();
//		  for (String i : macroListener.includes.keySet()) {
//			 content = content.replace(i, macroListener.includes.get(i));
//		  }
//	   }
//
//	   System.out.println(content);
	}
}
