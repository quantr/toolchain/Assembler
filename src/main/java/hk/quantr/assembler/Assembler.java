package hk.quantr.assembler;

import hk.quantr.assembler.ia32.MAL;
import hk.quantr.assembler.ia32.TestUtil;
import hk.quantr.assembler.ia32.UnderlineListener;
import hk.quantr.assembler.antlr.AssemblerLexer;
import hk.quantr.assembler.antlr.AssemblerParser;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.outputstream.MyDisasmOutputStream;
import hk.quantr.assembler.ia32.test.AssemblerTest;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.listener.RISCVErrorListener;
import hk.quantr.assembler.riscv.listing.Line;
import hk.quantr.executablelibrary.elf64.ELF64;
import hk.quantr.executablelibrary.elf64.ELF64WriteSection;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Assembler {

	public static ArrayList<Token> allTokens = new ArrayList<>();

	public static void main(String args[]) {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help", false, "help");
		options.addOption("t", false, "get all test cases");
		options.addOption("ft", false, "run full test");
		options.addOption("d", false, "disassemble");
		options.addOption("x", false, "print error message in xml format");
		options.addOption("e", false, "input file is elf for disassemble");
		options.addOption(Option.builder("o")
				.required(false)
				.hasArg()
				.argName("file")
				.desc("output file")
				.longOpt("output")
				.build());
		options.addOption(Option.builder("a")
				.required(true)
				.hasArg()
				.argName("ia32/rv32/rv64")
				.desc("architecture")
				.longOpt("arch")
				.build());
		options.addOption(Option.builder("l")
				.required(false)
				.hasArg()
				.argName("list")
				.desc("list file")
				.longOpt("list")
				.build());
		options.addOption(Option.builder("f")
				.required(false)
				.hasArg()
				.argName("bin/elf")
				.desc("output format")
				.build());

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar assembler-xx.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			MessageHandler.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				MessageHandler.errorPrintln(ex);
			}
			MessageHandler.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}
		if (Arrays.asList(args).contains("-t")) {
			Class<MAL> obj = MAL.class;
			for (Method method : obj.getDeclaredMethods()) {
				if (method.isAnnotationPresent(AssemblerTest.class)) {
					Annotation annotation = method.getAnnotation(AssemblerTest.class);
					AssemblerTest testerInfo = (AssemblerTest) annotation;
					if (testerInfo.architecture() == AssemblerTest.Architecture.ia32) {
						for (String testingInstruction : testerInfo.instructions()) {
							String[] instructions = TestUtil.translateInstruction(testingInstruction);
							for (String instruction : instructions) {
								MessageHandler.println(instruction);
							}
						}
					}
				}
			}
			return;
		}

		CommandLineParser cliParser = new DefaultParser();
		try {
			CommandLine cmd = cliParser.parse(options, args);

			String arch = cmd.getOptionValue("a");

			List<String> arguments = cmd.getArgList();
			if (arguments.isEmpty()) {
				MessageHandler.errorPrintln("Please specific input file");
				MessageHandler.flushXML();
				System.exit(2);
			} else {
				if (!new File(arguments.get(0)).exists() || !new File(arguments.get(0)).isFile()) {
					MessageHandler.errorPrintln(arguments.get(0) + " not exist");
					MessageHandler.flushXML();
					System.exit(4);
				}
				if (cmd.hasOption("x")) {
					MessageHandler.isXML = true;
				}
				if (cmd.hasOption("d")) {
					if (cmd.hasOption("e")) {
						// elf goes here
						MessageHandler.println("Disassemble elf is not support yet");
					} else {
						if (arch.equals("ia32")) {
							MessageHandler.println("IA32 disassembler is not finish");
						} else if (arch.equals("rv32") || arch.equals("rv64")) {
							try {
								byte bytes[] = Files.readAllBytes(Paths.get(arguments.get(0)));
								RISCVDisassembler disassembler = new RISCVDisassembler(false);
								DisasmStructure disasmStructure = disassembler.disasm(new ByteArrayInputStream(bytes), 0);
								for (hk.quantr.assembler.riscv.il.Line line : disasmStructure.lines) {
									MessageHandler.println(line.toString());
								}
							} catch (IOException | RecognitionException ex) {
								MessageHandler.errorPrintln(ex);
								MessageHandler.flushXML();
								System.exit(4);
							} catch (Exception ex) {
								MessageHandler.errorPrintln(ex);
							}
						}
					}
				} else {
					try {
						String output = cmd.getOptionValue("o");
						if (arch.equals("ia32")) {
							AssemblerLexer lexer = new AssemblerLexer(CharStreams.fromStream(new FileInputStream(arguments.get(0))));
							CommonTokenStream tokenStream = new CommonTokenStream(lexer);
							AssemblerParser parser = new AssemblerParser(tokenStream);

							parser.removeErrorListeners();
							parser.addErrorListener(new UnderlineListener());

							AssemblerParser.AssembleContext context = parser.assemble();
							OutputStream out = new MyDisasmOutputStream();

							if (cmd.hasOption("o")) {
								for (byte b : parser.mal.encoder.out.toByteArray()) {
									out.write(b);
								}
							} else {
								for (byte b : parser.mal.encoder.out.toByteArray()) {
									MessageHandler.println(Integer.toHexString(b & 0xff) + " ");
								}
							}
						} else if (arch.equals("rv32") || arch.equals("rv64")) {
							String content = AssemblerLib.preProcess(new File(arguments.get(0)), arch);
							RISCVErrorListener errorListener = new RISCVErrorListener(content);
							RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
							allTokens.clear();
							for (Token token : lexer.getAllTokens()) {
								allTokens.add(token);
							}
							lexer.reset();
							CommonTokenStream tokenStream = new CommonTokenStream(lexer);
							RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
							parser.encoder.arch = arch;

							parser.removeErrorListeners();
							parser.addErrorListener(errorListener);
							parser.assemble();

							if (cmd.hasOption("o")) {
								if (!cmd.hasOption("f") || cmd.getOptionValue("f").equals("bin")) {
									File file = new File(output);
									try ( FileOutputStream fs = new FileOutputStream(file)) {
										fs.write(parser.encoder.out.toByteArray());
									}
								} else if (cmd.getOptionValue("f").equals("elf")) {
									RandomAccessFile fo = new RandomAccessFile(output, "rw");

									ELF64 elf = new ELF64();
									elf.writeSections.add(new ELF64WriteSection(".text", parser.encoder.out.toByteArray()));
//									elf.writeSections.add(new ELF64WriteSection(".data", new byte[]{1, 2, 3}));
//									elf.writeSections.add(new ELF64WriteSection(".shstrtab", new byte[]{4, 5, 6}));

									elf.write(fo);
									fo.close();
								} else {
									System.err.println("unsupport option -f " + cmd.getOptionValue("f"));
									System.exit(1);
								}
							} else {
//								System.err.println("please support -o");
//								System.exit(1);
								if (!MessageHandler.isXML) {
									for (byte b : parser.encoder.out.toByteArray()) {
										System.out.print(Integer.toHexString(b & 0xff) + " ");
									}
									System.out.println();
								}
							}

							if (cmd.hasOption("l")) {
								File listingFile = new File(cmd.getOptionValue("l"));
								try ( FileWriter fw = new FileWriter(listingFile)) {
									int lineNo = 1;
									for (Line l : parser.encoder.listing.lines) {
										while (lineNo < l.lineNo) {
											fw.write(String.format("%-5d\n", lineNo));
											lineNo++;
										}
										if (lineNo > l.lineNo) {
											lineNo--;
										}
										if (l.bytes != null) {
											fw.write(String.format("%-5d\t%-8s\t%08x\t%-11s\t%s\n", l.lineNo, l.section, l.address, CommonLib.arrayToHexString(l.bytes).replaceAll("0x", ""), l.code));
										} else if (l.code != null) { //section or labels
											fw.write(String.format("%-28d\t%s\n", l.lineNo, l.code));
										}
										lineNo++;
									}
								}
							}

						} else {
							MessageHandler.errorPrintln("arch not Supported. Try --help for available archs");
						}
					} catch (IOException | RecognitionException ex) {
						MessageHandler.errorPrintln(ex);
						MessageHandler.flushXML();
						System.exit(3);
					}
				}
			}
		} catch (MissingOptionException ex) {
			MessageHandler.errorPrintln("Missing option " + ex.getMissingOptions());
		} catch (ParseException ex) {
			MessageHandler.errorPrintln(ex);
		}
		MessageHandler.flushXML();
	}
}
