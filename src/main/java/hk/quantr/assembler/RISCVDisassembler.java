package hk.quantr.assembler;

import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.elf.Elf_Shdr;
import hk.quantr.dwarf.elf.SectionFinder;
import hk.quantr.javalib.CommonLib;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class RISCVDisassembler {

	public boolean verbose;
	private long currentOffset = 0;
	public String InsName = "nothing";
	public DisasmStructure disasmStructure = new DisasmStructure();

	public enum DecodeType {
		compressed, jal, jalr, fence, decodeTypeB, decodeTypeI, decodeTypeS, decodeTypeFS, decodeTypeR, decodeTypeR4, decodeTypeFR, decodeTypeU, ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU, magic, decodeTypeV, unimp

	}

	private static HashMap<Integer, DecodeType> op32map2 = new HashMap<>();
	private static HashMap<Integer, DecodeType> op16map = new HashMap<>();
	private String arch = "rv64";

	private static final Logger logger = Logger.getLogger(RISCVDisassembler.class.getName());

	public enum InstructionType {
		add, sub, sll, slt, sltu, xor, srl, sra, or, and, mul, mulh, mulhsu, mulhu, div, divu, rem, remu, slliw, srliw, sraiw, addw, subw,
		sllw, srlw, sraw, mulw, divw, divuw, remw, remuw, lr_w_aqrl, lr_w_aq, lr_w_rl, lr_w, sc_w_aqrl, sc_w_aq, sc_w_rl, sc_w, amoadd_w_aqrl,
		amoadd_w_aq, amoadd_w_rl, amoadd_w, amoswap_w_aqrl, amoswap_w_aq, amoswap_w_rl, amoswap_w, amoxor_w_aqrl, amoxor_w_aq, amoxor_w_rl,
		amoxor_w, amoand_w_aqrl, amoand_w_aq, amoand_w_rl, amoand_w, amoor_w_aqrl, amoor_w_aq, amoor_w_rl, amoor_w, amomin_w_aqrl,
		amomin_w_aq, amomin_w_rl, amomin_w, amomax_w_aqrl, amomax_w_aq, amomax_w_rl, amomax_w, amominu_w_aqrl, amominu_w_aq,
		amominu_w_rl, amominu_w, amomaxu_w_aqrl, amomaxu_w_aq, amomaxu_w_rl, amomaxu_w, lr_d_aqrl, lr_d_aq, lr_d_rl, lr_d, sc_d_aqrl,
		sc_d_aq, sc_d_rl, sc_d, amoadd_d_aqrl, amoadd_d_aq, amoadd_d_rl, amoadd_d,
		amoswap_d_aqrl, amoswap_d_aq, amoswap_d_rl, amoswap_d, amoxor_d_aqrl, amoxor_d_aq, amoxor_d_rl, amoxor_d, amoand_d_aqrl, amoand_d_aq, amoand_d_rl, amoand_d, amoor_d_aqrl, amoor_d_aq, amoor_d_rl, amoor_d, amomin_d_aqrl, amomin_d_aq, amomin_d_rl, amomin_d, amomax_d_aqrl, amomax_d_aq, amomax_d_rl, amomax_d, amominu_d_aqrl, amominu_d_aq, amominu_d_rl, amominu_d, amomaxu_d_aqrl, amomaxu_d_aq, amomaxu_d_rl, amomaxu_d, fmadd_s, fmsub_s, fnmsub_s, fnmadd_s, fmadd_d, fmsub_d, fnmsub_d, fnmadd_d, fmadd_q, fmsub_q, fnmsub_q, fnmadd_q, slli, srli, srai, lb, lh, lw, lbu, lhu, lwu, ld, addi, slti, sltiu, xori, ori, andi, flw, fld, flq, addiw, sb, sh, sw, sd, unimp, lui, auipc, jal, ret, jr, jalr, fsw, fsd, fsq, fence_i, fence, vadd_vv, vadd_vx, vadd_vi, vsub_vv, vsub_vx, vrsub_vx, vrsub_vi, vwaddu_vv, vwaddu_vx, vwadd_vv, vwadd_vx, vwsubu_vv, vwsubu_vx, vwsub_vv, vwsub_vx, vwaddu_wv, vwaddu_wx, vwsubu_wv, vwsubu_wx, vwadd_wv, vwadd_wx, vwsub_wv, vwsub_wx, vsetvli, vsetivli, vsetvl, c_addi4spn, c_fld, c_lq, c_lw, c_flw, c_ld, reserved, c_fsd, c_sq, c_sw, c_fsw, c_sd, c_nop, c_addi, c_addiw, c_li, c_addi16sp, c_lui, c_srli, c_srai, c_andi, c_sub, c_xor, c_or, c_and, c_subw, c_addw, c_j, c_beqz, c_bnez, c_slli64, c_slli, c_flwsp, c_lwsp, c_ldsp, c_ebreak, c_jr, c_mv, c_jalr, c_add, c_fsdsp, c_swsp, c_sdsp, fadd_s, fsub_s, fmul_s, fdiv_s, fsqrt_s, fsgnj_s, fmv_s, fsgnjn_s, fneg_s, fsgnjx_s, fabs_s, fmin_s, fmax_s, fcvt_w_s, fcvt_wu_s, fmv_x_w, feq_s, flt_s, fle_s, fclass_s, fcvt_s_w, fcvt_s_wu, fmv_w_x, fcvt_l_s, fcvt_lu_s, fcvt_s_l, fcvt_s_lu, fadd_d, fsub_d, fmul_d, fdiv_d, fsqrt_d, fsgnj_d, fmv_d, fsgnjn_d, fneg_d, fsgnjx_d, fabs_d, fmin_d, fmax_d, fcvt_s_d, fcvt_d_s, fmv_x_d, feq_d, flt_d, fle_d, fclass_d, fcvt_w_d, fcvt_wu_d, fmv_d_x, fcvt_l_d, fcvt_lu_d, fcvt_d_l, fcvt_d_lu, fadd_q, fsub_q, fmul_q, fdiv_q, fsqrt_q, fsgnj_q, fsgnjn_q, fsgnjx_q, fmin_q, fmax_q, fcvt_s_q, fcvt_q_s, fcvt_d_q, fcvt_q_d, feq_q, flt_q, fle_q, fclass_q, fcvt_w_q, fcvt_wu_q, fcvt_q_w, fcvt_q_wu, fcvt_l_q, fcvt_lu_q, fcvt_q_l, fcvt_q_lu, pause_sim, ecall, ebreak, sret, mret, wfi, sfence_vm, csrrw, csrrs, csrrc, csrrwi, csrrsi, csrrci, beq, bne, blt, bge, bltu, bgeu, beqz, bnez, bltz, bgtz, blez, bgez
	}

	static {
		InputStream stream = RISCVDisassembler.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	static {
		// op16, compressed instruction
		op16map.put(0, DecodeType.compressed);
		op16map.put(1, DecodeType.compressed);
		op16map.put(2, DecodeType.compressed);

		// op32
		// different opcode but same type
		op32map2.put(0b1101111, DecodeType.jal);
		op32map2.put(0b1100111, DecodeType.jalr);
		op32map2.put(0b0001111, DecodeType.fence);
		op32map2.put(0b1100011, DecodeType.decodeTypeB); // type B
		op32map2.put(0b0000011, DecodeType.decodeTypeI); // type I
		op32map2.put(0b0010011, DecodeType.decodeTypeI); // type I
		op32map2.put(0b0011011, DecodeType.decodeTypeI);
		op32map2.put(0b1110011, DecodeType.decodeTypeI); // ecall & ebreak
		op32map2.put(0b0000111, DecodeType.decodeTypeI); //floating
		op32map2.put(0b0100011, DecodeType.decodeTypeS); // type S
		op32map2.put(0b0100111, DecodeType.decodeTypeFS); //type FS
		op32map2.put(0b0110011, DecodeType.decodeTypeR);
		op32map2.put(0b0111011, DecodeType.decodeTypeR);// type R64
		op32map2.put(0b0101111, DecodeType.decodeTypeR);
		op32map2.put(0b1000011, DecodeType.decodeTypeR4);
		op32map2.put(0b1000111, DecodeType.decodeTypeR4);
		op32map2.put(0b1001011, DecodeType.decodeTypeR4);
		op32map2.put(0b1001111, DecodeType.decodeTypeR4);
		op32map2.put(0b1010011, DecodeType.decodeTypeFR);
		op32map2.put(0b0110111, DecodeType.decodeTypeU); // type U
		op32map2.put(0b0010111, DecodeType.decodeTypeU); // type U
		op32map2.put(0b1110011, DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU); // ecall ebreak mret sret wfi sfence csr
		op32map2.put(0b1111111, DecodeType.magic);
		op32map2.put(0b1010111, DecodeType.decodeTypeV); //OP-V

	}

	public void setCurrentOffset(long currentOffset) {
		this.currentOffset = currentOffset;
	}

	public RISCVDisassembler() {
	}

	public RISCVDisassembler(boolean verbose) {
		this.verbose = verbose;
	}

	public RISCVDisassembler(String arch) {
		this.arch = arch;
	}

	public RISCVDisassembler(String arch, boolean verbose) {
		this.arch = arch;
		this.verbose = verbose;
	}

	public DisasmStructure disasm(InputStream inputStream) throws IOException, WrongInstructionException, NoOfByteException {
		return this.disasm(inputStream, 0, null);
	}

	public DisasmStructure disasm(InputStream inputStream, long offset) throws IOException, WrongInstructionException, NoOfByteException {
		return this.disasm(inputStream, offset, null);
	}

	public DisasmStructure disasm(InputStream inputStream, Dwarf dwarf) throws IOException, WrongInstructionException, NoOfByteException {
		return this.disasm(inputStream, 0, null);
	}

	public DisasmStructure disasm(InputStream inputStream, long offset, Dwarf dwarf) throws IOException, WrongInstructionException, NoOfByteException {
		disasmStructure.lines.clear();
		currentOffset = offset;
		decode(inputStream, dwarf);
		return disasmStructure;
	}

	public DisasmStructure disasm(File file) throws IOException, WrongInstructionException, NoOfByteException {
		return disasm(file, ".text");
	}

	public DisasmStructure disasm(File file, String sectionName) throws IOException, WrongInstructionException, NoOfByteException {
		return disasm(file, new String[]{sectionName});
	}

	public DisasmStructure disasm(File file, String sectionNames[]) throws IOException, WrongInstructionException, NoOfByteException {
		Dwarf dwarf = new Dwarf();
		int r = dwarf.initElf(file, file.getName(), 0, true);
		if (r != 0) {
			return null;
		}

		DisasmStructure structure = new DisasmStructure();

		for (String sectionName : sectionNames) {
			Elf_Shdr section = SectionFinder.getSection(file, sectionName, SectionFinder.getElf32_Ehdr(file).is32Bits() ? 32 : 64);

			ByteBuffer bf = SectionFinder.findSectionByte(dwarf.ehdr, file, sectionName);
			byte bytes[] = bf.array();
			DisasmStructure temp = disasm(new ByteArrayInputStream(bytes), section.getSh_addr().longValue(), dwarf);
			structure.addAll(temp);
		}
		return structure;
	}

	public void decode(InputStream iStream, Dwarf dwarf) throws NoOfByteException, IOException {
		int b;
		while ((b = iStream.read()) != -1) {
			int noOfByte = getNoOfByte(b);
//			MessageHandler.println(noOfByte);
			int[] ins;
			if (noOfByte == 2) {
				ins = new int[2];
				ins[0] = b;
				ins[1] = iStream.read();
			} else if (noOfByte == 4) {
				ins = new int[4];
				ins[0] = b;
				ins[1] = iStream.read();
				ins[2] = iStream.read();
				ins[3] = iStream.read();
			} else {
				ins = new int[2];
				ins[0] = b;
				ins[1] = iStream.read();
				//MessageHandler.println(Integer.toBinaryString(ins[1] )+ " " + Integer.toBinaryString(ins[0] ));
				MessageHandler.println("Wrong no of byte at offset 0x" + Long.toHexString(currentOffset) + " = " + noOfByte);
				currentOffset += 2;
				continue;
			}
			try {
//			MessageHandler.println(Long.toHexString((int) currentOffset));
				DecodeType decodeType;
				if (ins[0] == 0 && ins[1] == 0) {
					decodeType = DecodeType.unimp;
				} else {
					decodeType = getDecodeType(b & 0x7f);
				}
//			MessageHandler.println(instructionType);
				Line line = null;
				if (decodeType == DecodeType.decodeTypeB) {
					line = decodeTypeB(ins);
				} else if (decodeType == DecodeType.decodeTypeI) {
					line = decodeTypeI(ins);
				} else if (decodeType == DecodeType.decodeTypeS) {
					line = decodeTypeS(ins);
				} else if (decodeType == DecodeType.decodeTypeFS) {
					line = decodeTypeFS(ins);
				} else if (decodeType == DecodeType.decodeTypeR) {
					line = decodeTypeR(ins);
				} else if (decodeType == DecodeType.decodeTypeR4) {
					line = decodeTypeR4(ins);
				} else if (decodeType == DecodeType.decodeTypeFR) {
					line = decodeTypeFR(ins);
				} else if (decodeType == DecodeType.decodeTypeU) {
					line = decodeTypeU(ins);
				} else if (decodeType == DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU) {
					line = ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(ins);
				} else if (decodeType == DecodeType.magic) {
					line = decodeMagic(ins);
				} else if (decodeType == DecodeType.jal) {
					line = jal(ins);
				} else if (decodeType == DecodeType.jalr) {
					line = jalr(ins);
				} else if (decodeType == DecodeType.fence) {
					line = fence(ins);
				} else if (decodeType == DecodeType.decodeTypeV) {
					line = decodeTypeV(ins);
				} else if (decodeType == DecodeType.compressed) {
					line = decodeCompressed(ins);
				} else if (decodeType == DecodeType.unimp) {
					line = new Line();
					line.sourceFile = null;
					line.lineNo = -1;
					line.offset = currentOffset;
					line.bytes = ins;
					line.code = "unimp";
					line.instructionType = InstructionType.unimp;
//				} else if (decodeType==DecodeType.WrongType) {
//					line = decodeWrongType(ins);
				} else {
					MessageHandler.errorPrintln("unsupport " + decodeType);
					return;
				}

				if (verbose) {
					MessageHandler.println(disasmStructure.lines.get(disasmStructure.lines.size() - 1).toString());
				}

//			// Invoke can't single step
//			Method m = this.getClass().getDeclaredMethod(instructionType, int[].class);
//			try {
//				m.invoke(this, ins);
//			} catch (InvocationTargetException ex) {
//				ex.getCause().printStackTrace();
//				MessageHandler.errorPrintln(instructionType + " > " + ex.getMessage());
//			}
				if (dwarf != null) {
					line.symbol = dwarf.findSectionByValue((int) currentOffset);
				}
				disasmStructure.lines.add(line);
			} catch (Exception ex) {
				Line line = new Line();
				line.sourceFile = null;
				line.lineNo = -1;
				line.offset = currentOffset;
				line.bytes = ins;
				line.code = CommonLib.arrayToHexString(ins);
				line.instructionType = InstructionType.unimp;
				disasmStructure.lines.add(line);
				MessageHandler.errorPrintln(ex);
			}
			currentOffset += noOfByte;
		}
	}

	public static int getNoOfByte(int b) throws NoOfByteException {
//		MessageHandler.println(b);
		b = b & 0x7f;
		if (op32map2.containsKey(b & 0b1111111)) {
			return 4;
		} else if (op16map.containsKey(b & 0b11)) {
			return 2;
		}
		throw new NoOfByteException("byte = 0x" + Integer.toHexString(b & 0xff));
	}

	public static DecodeType getDecodeType(int b) {
		b = b & 0x7f;
		if (op32map2.containsKey(b & 0b1111111)) {
			return op32map2.get(b);
		} else if (op16map.containsKey(b & 0b11)) {
			return op16map.get(b & 0b11);
		} else {
			return DecodeType.unimp;
		}

		//MessageHandler.println("Wrong getType : " + Integer.toBinaryString(b));
		//return null;
	}

	public static InstructionType getTypeRInstruction(int opcode, int funct3, int funct7) throws WrongInstructionException {
		if (opcode == 0b0110011) {
			if (funct3 == 0 && funct7 == 0) {
				return InstructionType.add;
			} else if (funct3 == 0 && funct7 == 32) {
				return InstructionType.sub;
			} else if (funct3 == 1 && funct7 == 0) {
				return InstructionType.sll;
			} else if (funct3 == 2 && funct7 == 0) {
				return InstructionType.slt;
			} else if (funct3 == 3 && funct7 == 0) {
				return InstructionType.sltu;
			} else if (funct3 == 4 && funct7 == 0) {
				return InstructionType.xor;
			} else if (funct3 == 5 && funct7 == 0) {
				return InstructionType.srl;
			} else if (funct3 == 5 && funct7 == 32) {
				return InstructionType.sra;
			} else if (funct3 == 6 && funct7 == 0) {
				return InstructionType.or;
			} else if (funct3 == 7 && funct7 == 0) {
				return InstructionType.and;
			} else if (funct3 == 0 && funct7 == 1) {
				return InstructionType.mul;
			} else if (funct3 == 1 && funct7 == 1) {
				return InstructionType.mulh;
			} else if (funct3 == 2 && funct7 == 1) {
				return InstructionType.mulhsu;
			} else if (funct3 == 3 && funct7 == 1) {
				return InstructionType.mulhu;
			} else if (funct3 == 4 && funct7 == 1) {
				return InstructionType.div;
			} else if (funct3 == 5 && funct7 == 1) {
				return InstructionType.divu;
			} else if (funct3 == 6 && funct7 == 1) {
				return InstructionType.rem;
			} else if (funct3 == 7 && funct7 == 1) {
				return InstructionType.remu;
			}
		} else if (opcode == 0b0011011) {
			if (funct3 == 1 && funct7 == 0) {
				return InstructionType.slliw;
			} else if (opcode == 0b0011011 && funct3 == 5 && funct7 == 0) {
				return InstructionType.srliw;
			} else if (opcode == 0b0011011 && funct3 == 5 && funct7 == 0b0100000) {
				return InstructionType.sraiw;
			}
		} else if (opcode == 0b0111011) {
			if (funct3 == 0 && funct7 == 0) {
				return InstructionType.addw;
			} else if (funct3 == 0 && funct7 == 0b0100000) {
				return InstructionType.subw;
			} else if (funct3 == 1 && funct7 == 0) {
				return InstructionType.sllw;
			} else if (funct3 == 5 && funct7 == 0) {
				return InstructionType.srlw;
			} else if (funct3 == 5 && funct7 == 0b0100000) {
				return InstructionType.sraw;
			} else if (funct3 == 0) {
				return InstructionType.mulw;
			} else if (funct3 == 4) {
				return InstructionType.divw;
			} else if (funct3 == 5) {
				return InstructionType.divuw;
			} else if (funct3 == 6) {
				return InstructionType.remw;
			} else if (funct3 == 7) {
				return InstructionType.remuw;
			}
		} else if (opcode == 0b0101111) {
			if (funct3 == 0b010) {
				int funct8 = funct7 & 0b0011; //check .aq/.rl/.aqrl
				if (funct7 >> 2 == 0b00010) {
					if (funct8 == 0b00011) {
						return InstructionType.lr_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.lr_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.lr_w_rl;
					} else {
						return InstructionType.lr_w;
					}
				} else if (funct7 >> 2 == 0b00011) {
					if (funct8 == 0b00011) {
						return InstructionType.sc_w_aqrl;
					} else if (funct8 == 0b00010) {
						return InstructionType.sc_w_aq;
					} else if (funct8 == 0b00001) {
						return InstructionType.sc_w_rl;
					} else {
						return InstructionType.sc_w;
					}
				} else if (funct7 >> 2 == 0b00000) {
					if (funct8 == 0b00011) {
						return InstructionType.amoadd_w_aqrl;
					} else if (funct8 == 0b00010) {
						return InstructionType.amoadd_w_aq;
					} else if (funct8 == 0b00001) {
						return InstructionType.amoadd_w_rl;
					} else {
						return InstructionType.amoadd_w;
					}
				} else if (funct7 >> 2 == 0b00001) {
					if (funct8 == 0b00011) {
						return InstructionType.amoswap_w_aqrl;
					} else if (funct8 == 0b00010) {
						return InstructionType.amoswap_w_aq;
					} else if (funct8 == 0b00001) {
						return InstructionType.amoswap_w_rl;
					} else {
						return InstructionType.amoswap_w;
					}
				} else if (funct7 >> 2 == 0b00100) {
					if (funct8 == 0b00011) {
						return InstructionType.amoxor_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoxor_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoxor_w_rl;
					} else {
						return InstructionType.amoxor_w;
					}
				} else if (funct7 >> 2 == 0b01100) {
					if (funct8 == 0b00011) {
						return InstructionType.amoand_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoand_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoand_w_rl;
					} else {
						return InstructionType.amoand_w;
					}
				} else if (funct7 >> 2 == 0b01000) {
					if (funct8 == 0b00011) {
						return InstructionType.amoor_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoor_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoor_w_rl;
					} else {
						return InstructionType.amoor_w;
					}
				} else if (funct7 >> 2 == 0b10000) {
					if (funct8 == 0b00011) {
						return InstructionType.amomin_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomin_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomin_w_rl;
					} else {
						return InstructionType.amomin_w;
					}
				} else if (funct7 >> 2 == 0b10100) {
					if (funct8 == 0b00011) {
						return InstructionType.amomax_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomax_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomax_w_rl;
					} else {
						return InstructionType.amomax_w;
					}
				} else if (funct7 >> 2 == 0b11000) {
					if (funct8 == 0b00011) {
						return InstructionType.amominu_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amominu_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amominu_w_rl;
					} else {
						return InstructionType.amominu_w;
					}
				} else if (funct7 >> 2 == 0b11100) {
					if (funct8 == 0b00011) {
						return InstructionType.amomaxu_w_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomaxu_w_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomaxu_w_rl;
					} else {
						return InstructionType.amomaxu_w;
					}
				}
			} else if (funct3 == 0b011) {
				int funct8 = funct7 & 0b0011; //check .aq/.rl/.aqrl
				if (funct7 >> 2 == 0b00010) {
					if (funct8 == 0b00011) {
						return InstructionType.lr_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.lr_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.lr_d_rl;
					} else {
						return InstructionType.lr_d;
					}
				} else if (funct7 >> 2 == 0b00011) {
					if (funct8 == 0b00011) {
						return InstructionType.sc_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.sc_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.sc_d_rl;
					} else {
						return InstructionType.sc_d;
					}
				} else if (funct7 >> 2 == 0b00000) {
					if (funct8 == 0b00011) {
						return InstructionType.amoadd_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoadd_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoadd_d_rl;
					} else {
						return InstructionType.amoadd_d;
					}
				} else if (funct7 >> 2 == 0b00001) {
					if (funct8 == 0b00011) {
						return InstructionType.amoswap_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoswap_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoswap_d_rl;
					} else {
						return InstructionType.amoswap_d;
					}
				} else if (funct7 >> 2 == 0b00100) {
					if (funct8 == 0b00011) {
						return InstructionType.amoxor_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoxor_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoxor_d_rl;
					} else {
						return InstructionType.amoxor_d;
					}
				} else if (funct7 >> 2 == 0b01100) {
					if (funct8 == 0b00011) {
						return InstructionType.amoand_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoand_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoand_d_rl;
					} else {
						return InstructionType.amoand_d;
					}
				} else if (funct7 >> 2 == 0b01000) {
					if (funct8 == 0b00011) {
						return InstructionType.amoor_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amoor_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amoor_d_rl;
					} else {
						return InstructionType.amoor_d;
					}
				} else if (funct7 >> 2 == 0b10000) {
					if (funct8 == 0b00011) {
						return InstructionType.amomin_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomin_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomin_d_rl;
					} else {
						return InstructionType.amomin_d;
					}
				} else if (funct7 >> 2 == 0b10100) {
					if (funct8 == 0b00011) {
						return InstructionType.amomax_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomax_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomax_d_rl;
					} else {
						return InstructionType.amomax_d;
					}
				} else if (funct7 >> 2 == 0b11000) {
					if (funct8 == 0b00011) {
						return InstructionType.amominu_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amominu_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amominu_d_rl;
					} else {
						return InstructionType.amominu_d;
					}
				} else if (funct7 >> 2 == 0b11100) {
					if (funct8 == 0b00011) {
						return InstructionType.amomaxu_d_aqrl;
					}
					if (funct8 == 0b00010) {
						return InstructionType.amomaxu_d_aq;
					}
					if (funct8 == 0b00001) {
						return InstructionType.amomaxu_d_rl;
					} else {
						return InstructionType.amomaxu_d;
					}
				}
			}
		}
		throw new WrongInstructionException("Wrong funct5 and funct7 : " + funct3 + ", " + funct7);
	}

	public static InstructionType getTypeR4Instruction(int opcode, int funct2) throws WrongInstructionException {
		if (funct2 == 0 && opcode == 0b1000011) {
			return InstructionType.fmadd_s;
		} else if (funct2 == 0 && opcode == 0b1000111) {
			return InstructionType.fmsub_s;
		} else if (funct2 == 0 && opcode == 0b1001011) {
			return InstructionType.fnmsub_s;
		} else if (funct2 == 0 && opcode == 0b1001111) {
			return InstructionType.fnmadd_s;
		} else if (funct2 == 0b01 && opcode == 0b1000011) {
			return InstructionType.fmadd_d;
		} else if (funct2 == 0b01 && opcode == 0b1000111) {
			return InstructionType.fmsub_d;
		} else if (funct2 == 0b01 && opcode == 0b1001011) {
			return InstructionType.fnmsub_d;
		} else if (funct2 == 0b01 && opcode == 0b1001111) {
			return InstructionType.fnmadd_d;
		} else if (funct2 == 0b11 && opcode == 0b1000011) {
			return InstructionType.fmadd_q;
		} else if (funct2 == 0b11 && opcode == 0b1000111) {
			return InstructionType.fmsub_q;
		} else if (funct2 == 0b11 && opcode == 0b1001011) {
			return InstructionType.fnmsub_q;
		} else if (funct2 == 0b11 && opcode == 0b1001111) {
			return InstructionType.fnmadd_q;
		} else {
			throw new WrongInstructionException("Wrong opcode and funct2 : " + opcode + ", " + funct2);
		}
	}

	public static InstructionType getTypeFRInstruction(int funct3, int funct7, int frs1, int frs2) throws WrongInstructionException {
		if (funct7 == 0) {
			return InstructionType.fadd_s;
		} else if (funct7 == 4) {
			return InstructionType.fsub_s;
		} else if (funct7 == 8) {
			return InstructionType.fmul_s;
		} else if (funct7 == 10) {
			return InstructionType.fdiv_s;
		} else if (funct7 == 0b0101100 && frs2 == 0) {
			return InstructionType.fsqrt_s;
		} else if (funct7 == 0b0010000 && funct3 == 0) {
			return InstructionType.fsgnj_s;
		} else if (funct7 == 0b0010000 && funct3 == 0 && frs2 == frs1) {
			return InstructionType.fmv_s;
		} else if (funct7 == 0b0010000 && funct3 == 1) {
			return InstructionType.fsgnjn_s;
		} else if (funct7 == 0b0010000 && funct3 == 1 && frs2 == frs1) {
			return InstructionType.fneg_s;
		} else if (funct7 == 0b0010000 && funct3 == 2) {
			return InstructionType.fsgnjx_s;
		} else if (funct7 == 0b0010000 && funct3 == 2 && frs2 == frs1) {
			return InstructionType.fabs_s;
		} else if (funct7 == 0b0010100 && funct3 == 0) {
			return InstructionType.fmin_s;
		} else if (funct7 == 0b0010100 && funct3 == 1) {
			return InstructionType.fmax_s;
		} else if (funct7 == 0b1100000 && frs2 == 0) {
			return InstructionType.fcvt_w_s;
		} else if (funct7 == 0b1100000 && frs2 == 1) {
			return InstructionType.fcvt_wu_s;
		} else if (funct7 == 0b1110000 && funct3 == 0) {
			return InstructionType.fmv_x_w;
		} else if (funct7 == 0b1010000 && funct3 == 2) {
			return InstructionType.feq_s;
		} else if (funct7 == 0b1010000 && funct3 == 1) {
			return InstructionType.flt_s;
		} else if (funct7 == 0b1010000 && funct3 == 0) {
			return InstructionType.fle_s;
		} else if (funct7 == 0b1110000 && funct3 == 1 && frs2 == 0) {
			return InstructionType.fclass_s;
		} else if (funct7 == 0b1101000 && frs2 == 0) {
			return InstructionType.fcvt_s_w;
		} else if (funct7 == 0b1101000 && frs2 == 1) {
			return InstructionType.fcvt_s_wu;
		} else if (funct7 == 0b1111000 && funct3 == 0 && frs2 == 0) {
			return InstructionType.fmv_w_x;
		} else if (funct7 == 0b1100000 && frs2 == 2) {
			return InstructionType.fcvt_l_s;
		} else if (funct7 == 0b1100000 && frs2 == 3) {
			return InstructionType.fcvt_lu_s;
		} else if (funct7 == 0b1101000 && frs2 == 2) {
			return InstructionType.fcvt_s_l;
		} else if (funct7 == 0b1101000 && frs2 == 3) {
			return InstructionType.fcvt_s_lu;
		} else if (funct7 == 1) {
			return InstructionType.fadd_d;
		} else if (funct7 == 5) {
			return InstructionType.fsub_d;
		} else if (funct7 == 9) {
			return InstructionType.fmul_d;
		} else if (funct7 == 13) {
			return InstructionType.fdiv_d;
		} else if (funct7 == 0b0101101 && frs2 == 0) {
			return InstructionType.fsqrt_d;
		} else if (funct7 == 0b0010001 && funct3 == 0) {
			return InstructionType.fsgnj_d;
		} else if (funct7 == 0b0010001 && funct3 == 0 && frs2 == frs1) {
			return InstructionType.fmv_d;
		} else if (funct7 == 0b0010001 && funct3 == 1) {
			return InstructionType.fsgnjn_d;
		} else if (funct7 == 0b0010001 && funct3 == 1 && frs2 == frs1) {
			return InstructionType.fneg_d;
		} else if (funct7 == 0b0010001 && funct3 == 2) {
			return InstructionType.fsgnjx_d;
		} else if (funct7 == 0b0010001 && funct3 == 2 && frs2 == frs1) {
			return InstructionType.fabs_d;
		} else if (funct7 == 0b0010101 && funct3 == 0) {
			return InstructionType.fmin_d;
		} else if (funct7 == 0b0010101 && funct3 == 1) {
			return InstructionType.fmax_d;
		} else if (funct7 == 0b1000000 && frs2 == 1) {
			return InstructionType.fcvt_s_d;
		} else if (funct7 == 0b0100001 && frs2 == 0) {
			return InstructionType.fcvt_d_s;
		} else if (funct7 == 0b1110001 && funct3 == 0 && frs2 == 0) {
			return InstructionType.fmv_x_d;
		} else if (funct7 == 0b1010001 && funct3 == 2) {
			return InstructionType.feq_d;
		} else if (funct7 == 0b1010001 && funct3 == 1) {
			return InstructionType.flt_d;
		} else if (funct7 == 0b1010001 && funct3 == 0) {
			return InstructionType.fle_d;
		} else if (funct7 == 0b1110001 && funct3 == 1 && frs2 == 0) {
			return InstructionType.fclass_d;
		} else if (funct7 == 0b1100001 && frs2 == 0) {
			return InstructionType.fcvt_w_d;
		} else if (funct7 == 0b1100001 && frs2 == 1) {
			return InstructionType.fcvt_wu_d;
		} else if (funct7 == 0b1111001 && funct3 == 0 && frs2 == 0) {
			return InstructionType.fmv_d_x;
		} else if (funct7 == 0b1100001 && frs2 == 2) {
			return InstructionType.fcvt_l_d;
		} else if (funct7 == 0b1100001 && frs2 == 3) {
			return InstructionType.fcvt_lu_d;
		} else if (funct7 == 0b1101001 && frs2 == 2) {
			return InstructionType.fcvt_d_l;
		} else if (funct7 == 0b1101001 && frs2 == 3) {
			return InstructionType.fcvt_d_lu;
		} //32Q start here
		else if (funct7 == 3) {
			return InstructionType.fadd_q;
		} else if (funct7 == 7) {
			return InstructionType.fsub_q;
		} else if (funct7 == 11) {
			return InstructionType.fmul_q;
		} else if (funct7 == 15) {
			return InstructionType.fdiv_q;
		} else if (funct7 == 0b0101111 && frs2 == 0) {
			return InstructionType.fsqrt_q;
		} else if (funct7 == 0b0010011 && funct3 == 0) {
			return InstructionType.fsgnj_q;
		} else if (funct7 == 0b0010011 && funct3 == 1) {
			return InstructionType.fsgnjn_q;
		} else if (funct7 == 0b0010011 && funct3 == 2) {
			return InstructionType.fsgnjx_q;
		} else if (funct7 == 0b0010111 && funct3 == 0) {
			return InstructionType.fmin_q;
		} else if (funct7 == 0b0010111 && funct3 == 1) {
			return InstructionType.fmax_q;
		} else if (funct7 == 0b0100000 && frs2 == 3) {
			return InstructionType.fcvt_s_q;
		} else if (funct7 == 0b0100011 && frs2 == 0) {
			return InstructionType.fcvt_q_s;
		} else if (funct7 == 0b0100001 && frs2 == 3) {
			return InstructionType.fcvt_d_q;
		} else if (funct7 == 0b0100011 && frs2 == 1) {
			return InstructionType.fcvt_q_d;
		} else if (funct7 == 0b1010011 && funct3 == 2) {
			return InstructionType.feq_q;
		} else if (funct7 == 0b1010011 && funct3 == 1) {
			return InstructionType.flt_q;
		} else if (funct7 == 0b1010011 && funct3 == 0) {
			return InstructionType.fle_q;
		} else if (funct7 == 0b1110011 && funct3 == 1 && frs2 == 0) {
			return InstructionType.fclass_q;
		} else if (funct7 == 0b1100011 && frs2 == 0) {
			return InstructionType.fcvt_w_q;
		} else if (funct7 == 0b1100011 && frs2 == 1) {
			return InstructionType.fcvt_wu_q;
		} else if (funct7 == 0b1101011 && frs2 == 0) {
			return InstructionType.fcvt_q_w;
		} else if (funct7 == 0b1101011 && frs2 == 1) {
			return InstructionType.fcvt_q_wu;
		} else if (funct7 == 0b1100011 && frs2 == 2) {
			return InstructionType.fcvt_l_q;
		} else if (funct7 == 0b1100011 && frs2 == 3) {
			return InstructionType.fcvt_lu_q;
		} else if (funct7 == 0b1101011 && frs2 == 2) {
			return InstructionType.fcvt_q_l;
		} else if (funct7 == 0b1101011 && frs2 == 3) {
			return InstructionType.fcvt_q_lu;
		} else {
			throw new WrongInstructionException("Wrong funct5 and funct7 : " + funct3 + ", " + funct7);
		}
	}

	public static InstructionType getTypeBInstruction(int funct3, int rs1, int rs2) throws WrongInstructionException {
		if (/*rs1 != 0 && */rs2 != 0) {
			if (funct3 == 0) {
				return InstructionType.beq;
			} else if (funct3 == 1) {
				return InstructionType.bne;
			} else if (funct3 == 4) {
				return InstructionType.blt;
			} else if (funct3 == 5) {
				return InstructionType.bge;
			} else if (funct3 == 6) {
				return InstructionType.bltu;
			} else if (funct3 == 7) {
				return InstructionType.bgeu;
			} else {
				throw new WrongInstructionException("Wrong getTypeBInstruction, funct3 : " + funct3);
			}
		} else if (funct3 == 0 && rs2 == 0) {
			return InstructionType.beqz;
		} else if (funct3 == 1 && rs2 == 0) {
			return InstructionType.bnez;
		} else if (funct3 == 4 && rs2 == 0) {
			return InstructionType.bltz;
		} else if (funct3 == 4 && rs1 == 0) {
			return InstructionType.bgtz;
		} else if (funct3 == 5 && rs1 == 0) {
			return InstructionType.blez;
		} else if (funct3 == 5 && rs2 == 0) {
			return InstructionType.bgez;
		} else {
			throw new WrongInstructionException("Wrong getTypeBInstruction, funct3 : " + funct3);
		}
	}

	public static int isShamtInstruction(int opcode, int fun3) {
		if (opcode == 0b0010011 && (fun3 == 1 || fun3 == 5)) {
			return 2;
		} else if (opcode == 0b0011011 && (fun3 == 1 || fun3 == 5)) {
			return 1;
		} else {
			return 0;
		}
	}

	public static InstructionType getTypeIInstruction(int opcode, int funct3, int funct7) throws WrongInstructionException {
		if (opcode == 0b0010011 && funct7 != -1) {
			if (funct7 == 0 && funct3 == 1) {
				return InstructionType.slli;
			} else if (funct7 == 0 && funct3 == 5) {
				return InstructionType.srli;
			} else if (funct7 == 16 && funct3 == 5) {
				return InstructionType.srai;
			} else {
				throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3 + ", funct7 : " + funct7);
			}
		} else if (opcode == 0b0000011 && funct7 == -1) {
			if (funct3 == 0) {
				return InstructionType.lb;
			} else if (funct3 == 1) {
				return InstructionType.lh;
			} else if (funct3 == 2) {
				return InstructionType.lw;
			} else if (funct3 == 4) {
				return InstructionType.lbu;
			} else if (funct3 == 5) {
				return InstructionType.lhu;
			} else if (funct3 == 6) {
				return InstructionType.lwu;
			} else if (funct3 == 3) {
				return InstructionType.ld;
			} else {
				throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3);
			}
		} else if (opcode == 0b0010011 && funct7 == -1) {
			if (funct3 == 0) {
				return InstructionType.addi;
			} else if (funct3 == 2) {
				return InstructionType.slti;
			} else if (funct3 == 3) {
				return InstructionType.sltiu;
			} else if (funct3 == 4) {
				return InstructionType.xori;
			} else if (funct3 == 6) {
				return InstructionType.ori;
			} else if (funct3 == 7) {
				return InstructionType.andi;
			} else {
				throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3);
			}
		} else if (opcode == 0b0000111 && funct7 == -1) {
			if (funct3 == 2) {
				return InstructionType.flw;
			} else if (funct3 == 3) {
				return InstructionType.fld;
			} else if (funct3 == 4) {
				return InstructionType.flq;
			} else {
				throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3);
			}
		} else if (opcode == 0b0011011 && funct7 == -1) {
			if (funct3 == 0) {
				return InstructionType.addiw;
			} else {
				throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3);
			}
		} else {
			MessageHandler.println("FUCK");
		}
		throw new WrongInstructionException("Wrong TypeI, funct3 : " + funct3 + ", funct7 : " + funct7);
	}

	public static InstructionType getTypeSInstruction(int opcode, int funct3) throws WrongInstructionException {
		if (opcode == 0b0100011) {
			if (funct3 == 0) {
				return InstructionType.sb;
			} else if (funct3 == 1) {
				return InstructionType.sh;
			} else if (funct3 == 2) {
				return InstructionType.sw;
			} else if (funct3 == 3) {
				return InstructionType.sd;
			} else {
				throw new WrongInstructionException("Wrong getTypeSInstruction, funct3 : " + funct3);
			}
		}
		throw new WrongInstructionException("Wrong getTypeSInstruction, funct3 : " + funct3);
	}

	public static InstructionType getTypeFSInstruction(int opcode, int funct3) throws WrongInstructionException {
		if (opcode == 0b0100111 && funct3 == 2) {
			return InstructionType.fsw;
		} else if (opcode == 0b0100111 && funct3 == 3) {
			return InstructionType.fsd;
		} else if (opcode == 0b0100111 && funct3 == 3) {
			return InstructionType.fsq;
		} else {
			throw new WrongInstructionException("Wrong getTypeSInstruction, funct3 : " + funct3);
		}

	}

//	public static String getTypeRVCQ0_instruction(int funct3)  {
//		if (funct3 == 0) {
//			return "c.addi4spn";
//		} else if (funct3 == 1) {
//			return "c.fld";
//		} else if (funct3 == 2) {
//			return "c.lw";
//		} else if (funct3 == 3) {
//			return "c.flw";
//		} else if (funct3 == 5) {
//			return "c.fsd";
//		} else if (funct3 == 6) {
//			return "c.sw";
//		} else if (funct3 == 7) {
//			return "c.fsw";
//		} else {
//			throw new Exception("Wrong getTypeRVCQ0_instruction, funct3 : " + funct3);
//		}
//	}
	public static InstructionType getTypeUInstruction(int opcode) {
		if (opcode == 0b0110111) {
			return InstructionType.lui;
		} else {
			return InstructionType.auipc;
		}
	}

//	public static String getTypeRVCQ2_instruction(int funct3, int bit12, int bit2_6, int bit7_11)  {
//		if (funct3 == 0) {
//			return "c.slli";
//		} else if (funct3 == 1) {
//			return "c.fldsp";
//		} else if (funct3 == 2) {
//			return "c.lwsp";
//		} else if (funct3 == 3) {
//			return "c.flwsp";
//		} else if (funct3 == 4) {
//			if (bit12 == 0) {
//				return bit2_6 == 0 ? "c.jr" : "c.mv";
//			} else {
//				if (bit2_6 == 0 && bit7_11 == 0) {
//					return "c.ebreak";
//				} else {
//					return bit2_6 == 0 ? "c.jalr" : "c.add";
//				}
//			}
//		} else if (funct3 == 5) {
//			return "c.fsdsp";
//		} else if (funct3 == 6) {
//			return "c.swsp";
//		} else if (funct3 == 7) {
//			return "c.fswsp";
//		} else {
//			throw new Exception("Wrong getTypeRVCQ2_instruction, funct3 : " + funct3);
//		}
//	}
	public static InstructionType getTypeVInstruction(int opcode, int funct3, int funct6) {
		if (opcode == 0b1010111) {
			//is OP-V major opcode
//            MessageHandler.println("is OP-V major opcode");

			if (funct6 == 0b000000) {
				//vadd
//                MessageHandler.println("is vadd");
				if (funct3 == 0b000) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vadd_vv;
				} else if (funct3 == 0b100) {
					return InstructionType.vadd_vx;
				} else if (funct3 == 0b011) {
					return InstructionType.vadd_vi;
				}
			} else if (funct6 == 0b000010) {
				//vsub
				MessageHandler.println("is vsub");
				if (funct3 == 0b000) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vsub_vv;
				} else if (funct3 == 0b100) {
					return InstructionType.vsub_vx;
				}
			} else if (funct6 == 0b000011) {
				//vrsub
//                MessageHandler.println("is vrsub");
				if (funct3 == 0b100) {
					return InstructionType.vrsub_vx;
				} else if (funct3 == 0b011) {
					return InstructionType.vrsub_vi;
				}
			} else if (funct6 == 0b110000) {
				//vwaddu
//                MessageHandler.println("is vwaddu");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwaddu_vv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwaddu_vx;
				}
			} else if (funct6 == 0b110001) {
				//vwadd
//                MessageHandler.println("is vwadd");
				if (funct3 == 010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwadd_vv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwadd_vx;
				}
			} else if (funct6 == 0b110010) {
				//vwsubu
//                MessageHandler.println("is vwsubu");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwsubu_vv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwsubu_vx;
				}
			} else if (funct6 == 0b110011) {
				//vwsub
//                MessageHandler.println("is vwsub");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwsub_vv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwsub_vx;
				}
			} else if (funct6 == 0b110100) {
				//vwadduw
//                MessageHandler.println("is vwaddu.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwaddu_wv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwaddu_wx;
				}
			} else if (funct6 == 0b110110) {
				//vwsubuw
//                MessageHandler.println("is vwsubu.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwsubu_wv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwsubu_wx;
				}
			} else if (funct6 == 0b110101) {
				//vxaddw
//                MessageHandler.println("is vwadd.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwadd_wv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwadd_wx;
				}
			} else if (funct6 == 0b110111) {
//                MessageHandler.println("is vwsub.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
//                    MessageHandler.println("is OPIVV vector-vector");
					return InstructionType.vwsub_wv;
				} else if (funct3 == 0b110) {
					return InstructionType.vwsub_wx;
				}
			}
		} else {
			//opcode wrong
			MessageHandler.println("wrong opcode");
			return InstructionType.unimp;
		}
		MessageHandler.println("wrong but not opcode");
		return InstructionType.unimp;
	}

	public Line decodeTypeU(int arr[]) {
		//0-7 8-15 16-23 24-31
		int rd = (arr[1] & 0x0f) << 1 | (arr[0] >> 7);
		int imm = arr[3] << 12 | arr[2] << 4 | ((arr[1] & 0xf0) >> 4);

		InstructionType insName = getTypeUInstruction(arr[0] & 0x7f);
		String ins = String.format("%s %s,0x%s", insName, Registers.getRegXNum32(rd), Integer.toString(imm, 16));

//		MessageHandler.println(ins);
		Line line = new Line();
		line.type = "decodeTypeU";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = insName;
		line.rs1 = 0;
		line.rs2 = 0;
		line.imm = imm;
		line.rd = rd;
		//full_code = ins;

//			disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeUnimp(int arr[]) {
		//0-7 8-15 16-23 24-31
//		MessageHandler.println(ins);
		String code = "0x";
		//String code = Arrays.toString(arr).replaceAll("\\[|\\]|,|\\s", "");
		for (int i = 0; i < arr.length; i++) {
			String temp = Integer.toHexString(arr[i]).replaceAll("0x", "");
			code += temp;

		}
		Line line = new Line();
		line.code = code;
		line.type = "unimp";
		line.instructionType = InstructionType.unimp;
		line.bytes = arr.clone();
		//full_code = ins;

//			disasmStructure.lines.add(line);
		return line;
	}

	public Line jal(int arr[]) {
		String ins = "jal";
		String code = null;
		int bit_11_7 = ((arr[1] & 0b1111) << 1) | ((arr[0] & 10000000) >> 7);
		//0-7 8-15
		long value = arrTo32Binary(arr);
		int rd = (int) AssemblerLib.getBitsDense(value, new int[]{11, 10, 9, 8, 7});
		long imm = (long) AssemblerLib.getTwosComplement(value, new int[]{31, 19, 18, 17, 16, 15, 14, 13, 12, 20, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21}, 20) << 1;

		code = ins + " ";

		if (bit_11_7 != 0) {
			code = code + Registers.getRegXNum32(rd) + ",";
		}

		code = code + "0x" + Long.toHexString(imm);

		Line line = new Line();
		line.type = "jal";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = InstructionType.jal;
		line.rd = rd;
		line.imm = imm;

//		disasmStructure.lines.add(line);
		return line;
	}

	public long arrTo32Binary(int arr[]) {
		long result = 0;

		for (int i = arr.length - 1; i >= 0; i--) {
			result |= arr[i];

			if (i != 0) {
				result = result << 8;
			}

		}

		return result;
	}

	public Line jalr(int arr[]) {
		String ins = "jalr";
		String code = null;

		long value = arrTo32Binary(arr);
		int rd = (int) AssemblerLib.getBitsDense(value, new int[]{11, 10, 9, 8, 7});
		int rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{19, 18, 17, 16, 15});
		long imm = (long) AssemblerLib.getTwosComplement(value, new int[]{31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20}, 12);

		String register_rd = Registers.getRegXNum32(rd);
		String register_rs1 = Registers.getRegXNum32(rs1);

		code = ins + " ";
		InstructionType instructionType;
		if (register_rd.equals("zero") && register_rs1.equals("ra") && imm == 0) {
			code = "ret";
			instructionType = InstructionType.ret;
		} else if (register_rd.equals("zero") && imm == 0) {
			code = "jr" + " " + Registers.getRegXNum32(rs1);
			instructionType = InstructionType.jr;
		} else {
			instructionType = InstructionType.jalr;
			if (register_rd.equals("ra") && imm == 0) {
				code = code + imm + "(" + Registers.getRegXNum32(rs1) + ")";
			} else {
				code = code + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rs1) + "," + imm;
			}
		}

		Line line = new Line();
		line.type = "jalr";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = instructionType;
		line.imm = imm;
		line.rd = rd;
		line.rs1 = rs1;

		//full_code = code;
//		disasmStructure.lines.add(line);
		return line;
	}

	public Line fence(int arr[]) {
		String ins;
		int funct3 = (arr[1] & 0x70) >> 4;

		InstructionType instructionType;
		if (funct3 == 1) {
			ins = "fence.i";
			instructionType = InstructionType.fence_i;
		} else {
			instructionType = InstructionType.fence;

			int successor = arr[2] >> 4;
			int predecessor = arr[3];
			char[] io = {'w', 'r', 'o', 'i'};

			String rs1 = "";
			for (int i = 0; i < 4 && successor != '0'; i++) {
				if ((successor & 1) == 1) {
					rs1 += io[i];
				}

				successor = successor >> 1;
			}

			String rd = "";
			for (int i = 0; i < 4; i++) {
				if ((predecessor & 1) == 1) {
					rd += io[i];
				}

				predecessor = predecessor >> 1;
			}

			if (rs1.isEmpty() || rd.isEmpty()) {
				ins = "fence";
			} else {
				ins = String.format("fence %s,%s", new StringBuilder(rd).reverse().toString(), new StringBuilder(rs1).reverse().toString());
			}
		}

		Line line = new Line();
		line.type = "fence";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = instructionType;

//		disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeTypeV(int arr[]) {
		//for varith
		int opcode = arr[0] & 0b01111111;
		int funct3 = (arr[1] & 0b01110000) >> 4;
		int funct6 = (arr[3] & 0b11111100) >> 2;
		int vd = (arr[1] & 0b00001111) << 1 | (arr[0] & 0b10000000) >> 7;
		int vs1 = (arr[2] & 0b00001111) << 1 | (arr[1] & 0b10000000) >> 7;
		int vs2 = (arr[3] & 0b00000001) << 4 | (arr[2] & 0b11110000) >> 4;
//        MessageHandler.println("vd is " + vd);
//        MessageHandler.println("vs1 is " + vs1);
//        MessageHandler.println("vs2 is " + vs2);
		//rs1 and imm are not done properly and are just using vs2 as placeholder in order to function, pending fix

		//for vset
		int xlenminone = (arr[3] & 0b10000000) >> 7;
		int xlenmintwo = (arr[3] & 0b01000000) >> 6;
//        MessageHandler.println("opcode is " + opcode);
//        MessageHandler.println("xlenminone is " + xlenminone);
//        MessageHandler.println("xlenmintwo is " + xlenmintwo);

		int rd = (arr[1] & 0b00001111) << 1 | (arr[0] & 0b10000000) >> 7;
		int rs1 = (arr[2] & 0b00001111) << 1 | (arr[1] & 0b10000000) >> 7;
		int rs2 = (arr[3] & 0b00000001) << 4 | (arr[2] & 0b11110000) >> 4;
		int uimm = (arr[2] & 0b00001111) << 1 | (arr[1] & 0b10000000) >> 7;
		int zimmeleven = (arr[3] & 0b01111111) << 4 | (arr[2] & 0b11110000) >> 4;
		int zimmten = (arr[3] & 0b00111111) << 4 | (arr[2] & 0b11110000) >> 4;

		int vsew = 0;
		int vlmul = 0;
		int vta = 0;
		int vma = 0;

		InstructionType instructionType = null;
		String ins = null;

		Line line = new Line();
		line.type = "decodeTypeV";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();

		if (opcode == 0b1010111 && funct3 != 0b111) {
			//varith
//            MessageHandler.println("varith");
			instructionType = getTypeVInstruction(opcode, funct3, funct6);
			if (funct6 == 0b000000 && funct3 != 0b111) {
				//vadd
//                MessageHandler.println("is vadd");
				if (funct3 == 0b000) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vadd.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b100) {
					//return "vadd.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				} else if (funct3 == 0b011) {
					//return "vadd.vi";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), vs1);
				}
			} else if (funct6 == 0b000010) {
				//vsub
//                MessageHandler.println("is vsub");
				if (funct3 == 0b000) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vsub.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b100) {
					//return "vsub.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b000011) {
				//vrsub
//                MessageHandler.println("is vrsub");
				if (funct3 == 0b100) {
					//return "vrsub.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				} else if (funct3 == 0b011) {
					//return "vrsub.vi";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), vs1);
				}
			} else if (funct6 == 0b110000) {
				//vwaddu
//                MessageHandler.println("is vwaddu");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwaddu.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwaddu.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110001) {
				//vwadd
//                MessageHandler.println("is vwadd");
				if (funct3 == 010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwadd.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwadd.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110010) {
				//vwsubu
//                MessageHandler.println("is vwsubu");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwsubu.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwsubu.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110011) {
				//vwsub
//                MessageHandler.println("is vwsub");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwsub.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwsub.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110100) {
				//vwaddu.w
//                MessageHandler.println("is vwaddu.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwaddu.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwaddu.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110101) {
				//vwadd.w
//                MessageHandler.println("is vwadd.w");
				if (funct3 == 010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwadd.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwadd.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110110) {
				//vwsubu.w
//                MessageHandler.println("is vwsubu.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwsubu.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwsubu.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			} else if (funct6 == 0b110111) {
				//vwsub.w
//                MessageHandler.println("is vwsub.w");
				if (funct3 == 0b010) {
					//is OPIVV vector-vector
					//MessageHandler.println("is OPIVV vector-vector");
					//return "vwsub.vv";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getVregNum32(vs1));
				} else if (funct3 == 0b110) {
					//return "vwsub.vx";
					ins = String.format("%s %s,%s,%s", instructionType, Registers.getVregNum32(vd), Registers.getVregNum32(vs2), Registers.getRegXNum32(vs1));
				}
			}
			if (funct3 == 0b000 || funct3 == 0b001 || funct3 == 0b010) {
				line.vd = vd;
				line.vs1 = vs1;
				line.vs2 = vs2;
				line.rs1 = 0;
				line.imm = 0;
			} else if (funct3 == 0b011) {
				line.vd = vd;
				line.vs1 = vs1;
				line.vs2 = 0;
				line.rs1 = 0;
				line.imm = vs2;
			} else if (funct3 == 0b100 || funct3 == 0b101 || funct3 == 0b110) {
				line.vd = vd;
				line.vs1 = vs1;
				line.vs2 = 0;
				line.rs1 = vs2;
				line.imm = 0;
			}
//            MessageHandler.println("Successfully attempted OP-V Arith Instruction");
		} else if (funct3 == 0b111) {
			//vset
			if (xlenminone == 0) {
				//vsetvli
//                MessageHandler.println("is vsetvli");
				instructionType = InstructionType.vsetvli;
				vma = (zimmeleven & 0b00010000000) >> 5;
				vta = (zimmeleven & 0b00001000000) >> 4;
				vsew = (zimmeleven & 0b00000111000) >> 3;
				vlmul = zimmeleven & 0b00000000111;
				ins = String.format("%s %s,%s,%s,%s,%s", instructionType, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs1), Registers.getVcsrNum32(vsew), Registers.getVcsrNum32(vlmul), Registers.getVcsrNum32(vta), Registers.getVcsrNum32(vma));
			} else if (xlenminone == 1 && xlenmintwo == 1) {
				//vsetivli
//                MessageHandler.println("is vsetivli");
				instructionType = InstructionType.vsetivli;
				vma = (zimmten & 0b0010000000) >> 5;
				vta = (zimmten & 0b0001000000) >> 4;
				vsew = (zimmten & 0b0000111000) >> 3;
				vlmul = zimmten & 0b0000000111;
				ins = String.format("%s %s,%s,%s,%s,%s", instructionType, Registers.getRegXNum32(rd), Integer.toHexString(uimm), Registers.getVcsrNum32(vsew), Registers.getVcsrNum32(vlmul), Registers.getVcsrNum32(vta), Registers.getVcsrNum32(vma));
			} else if (xlenminone == 1 && xlenmintwo == 0) {
				//vsetvl
//                MessageHandler.println("is vsetvl");
				instructionType = InstructionType.vsetvl;
				ins = String.format("%s %s,%s,%s,%s,%s", instructionType, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs1), Registers.getRegXNum32(rs2));
			}
			line.vlmul = vlmul;
			line.vsew = vsew;
			line.vta = vta;
			line.vma = vma;
//            MessageHandler.println("Successfully attempted OP-V Config Instruction");
		} else {
			line.rd = rd;
			line.rs1 = rs1;
			line.rs2 = rs2;
			line.imm = 0;
			ins = "Wrong";
			instructionType = InstructionType.unimp;
			MessageHandler.println("Failed OP-V Instruction");
		}
		line.code = ins;
		line.instructionType = instructionType;
		return line;
	}

	public void decodeRVCQ0(int arr[]) {

	}

	public void decodeRVCQ1(int arr[]) {

	}

	public void decodeRVCQ2(int arr[]) {

	}

	public Line decodeCompressed(int arr[]) throws WrongInstructionException {
		int opcode = arr[0] & 0b11;
		int funct3 = (arr[1] & 0b11100000) >> 5;
		int value = arr[1] << 8 | arr[0];

		String code = null;
		InstructionType ins = null;
		int rs1 = 0;
		int rs2 = 0;
		int rd = 0;
		long imm = 0;
		int bit_12 = (arr[1] & 0b10000) >> 4;
		int bit_11_10 = (arr[1] & 0b1100) >> 2;
		int bit_11_7 = ((arr[1] << 1) & 0b11110) | ((arr[0] & 0b10000000) >> 7);
		int bit_6_5 = (arr[0] & 0b1100000) >> 5;
		int bit_6_2 = (arr[0] & 0b1111100) >> 2;

		HashMap<String, Object> reg_imm = new HashMap<>();

		if (opcode == 0 && funct3 == 0) {
			ins = InstructionType.c_addi4spn;

			reg_imm = getCIW(arr, 2);//according the shift of uimm

			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum16(rd) + ",sp," + imm;
		} else if (opcode == 0 && funct3 == 1) {
			if (arch.equals("rv32") || arch.equals("rv64")) {
				ins = InstructionType.c_fld;
			} else {
				ins = InstructionType.c_lq;
			}

			reg_imm = getCL(arr, 3);//according the shift of uimm

			rs1 = (int) reg_imm.get("rs1");
			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegFNum16(rd) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";
		} else if (opcode == 0 && funct3 == 2) {
			ins = InstructionType.c_lw;

			reg_imm = getCL(arr, 2);//according the shift of uimm

			rs1 = (int) reg_imm.get("rs1");
			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum16(rd) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";
		} else if (opcode == 0 && funct3 == 3) {
			if (arch.equals("rv32")) {
				ins = InstructionType.c_flw;
			} else {
				ins = InstructionType.c_ld;
			}

			reg_imm = getCL(arr, 3);//according the shift of uimm
			rd = (int) reg_imm.get("rd");
			rs1 = (int) reg_imm.get("rs1");
			imm = (long) reg_imm.get("imm");
			code = ins + " " + Registers.getRegFNum16(rd) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";
		} else if (opcode == 0 && funct3 == 4) {
			ins = InstructionType.reserved;
		} else if (opcode == 0 && funct3 == 5) {
			if (arch.equals("rv32") || arch.equals("rv64")) {
				ins = InstructionType.c_fsd;
			} else {
				ins = InstructionType.c_sq;
			}

			reg_imm = getCS(arr, 3);//according the shift of uimm

			rs1 = (int) reg_imm.get("rs1");
			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegFNum16(rs2) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";  //waiting to test
		} else if (opcode == 0 && funct3 == 6) {
			ins = InstructionType.c_sw;

			reg_imm = getCS(arr, 2);//according the shift of uimm

			rs1 = (int) reg_imm.get("rs1");
			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum16(rs2) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";  //waiting to test
		} else if (opcode == 0 && funct3 == 7) {
			if (arch.equals("rv32")) {
				ins = InstructionType.c_fsw;
			} else {
				ins = InstructionType.c_sd;
			}

			reg_imm = getCS(arr, 3);//according the shift of uimm

			rs1 = (int) reg_imm.get("rs1");
			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegFNum16(rs2) + "," + imm + "(" + Registers.getRegXNum16(rs1) + ")";  //waiting to test
		} else if (opcode == 1 && funct3 == 0) {
			if (bit_11_7 == 0) {
				ins = InstructionType.c_nop;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + "," + imm;
			} else {
				ins = InstructionType.c_addi;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + "," + imm;
			}
		} else if (opcode == 1 && funct3 == 1) {
			ins = InstructionType.c_addiw;

			reg_imm = getCI(arr, 0);
			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + "," + imm;
		} else if (opcode == 1 && funct3 == 2) {
			ins = InstructionType.c_li;

			reg_imm = getCI(arr, 0);//according the shift of imm

			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rd) + "," + imm;
		} else if (opcode == 1 && funct3 == 3) {
			if (bit_11_7 == 2) {
				ins = InstructionType.c_addi16sp;

				reg_imm = getCI(arr, 4);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + "," + imm;  //waiting to test
			} else {
				ins = InstructionType.c_lui;

				reg_imm = getCI(arr, 12);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + ",0x" + Long.toHexString(imm);  //waiting to test
			}
		} else if (opcode == 1 && funct3 == 4) {
			if (bit_11_10 == 0 && bit_6_2 >= 0) {
				ins = InstructionType.c_srli;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + ",0x" + Long.toHexString(imm);  //waiting to test
			} else if (bit_11_10 == 1 && bit_6_2 >= 0) {
				ins = InstructionType.c_srai;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + ",0x" + Long.toHexString(imm);  //waiting to test
			} else if (bit_11_10 == 2) {
				ins = InstructionType.c_andi;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + imm;  //waiting to test
			} else if (bit_12 == 0 && bit_11_10 == 3 && bit_6_5 == 0) {
				ins = InstructionType.c_sub;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 0 && bit_11_10 == 3 && bit_6_5 == 1) {
				ins = InstructionType.c_xor;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 0 && bit_11_10 == 3 && bit_6_5 == 2) {
				ins = InstructionType.c_or;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 0 && bit_11_10 == 3 && bit_6_5 == 3) {
				ins = InstructionType.c_and;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 1 && bit_11_10 == 3 && bit_6_5 == 0) {
				ins = InstructionType.c_subw;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 1 && bit_11_10 == 3 && bit_6_5 == 1) {
				ins = InstructionType.c_addw;

				reg_imm = getCA(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = rd;
				rs2 = (int) reg_imm.get("rs2");

				code = ins + " " + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rd) + "," + Registers.getRegXNum16(rs2);  //waiting to test
			} else if (bit_12 == 1 && bit_11_10 == 3 && bit_6_5 == 2) {
				ins = InstructionType.reserved;
			} else if (bit_12 == 1 && bit_11_10 == 3 && bit_6_5 == 3) {
				ins = InstructionType.reserved;
			} else {
				throw new WrongInstructionException("Unrecognized compressed instruction");
			}
		} else if (opcode == 1 && funct3 == 5) {
			ins = InstructionType.c_j;

			reg_imm = getCJ(arr, 1);

			imm = (long) reg_imm.get("imm");

			code = ins + " " + Long.toHexString(imm);  //waiting to test
		} else if (opcode == 1 && funct3 == 6) {
			ins = InstructionType.c_beqz;

			reg_imm = getCB(arr, 1);//according the shift of imm

			rs1 = (int) reg_imm.get("rs1");
			imm = (long) reg_imm.get("imm");
			//MessageHandler.println("Current Offset"+ currentOffset + "imm" + imm);
			code = ins + " " + Registers.getRegXNum16(rs1) + "," + Long.toString(imm);  //waiting to test
		} else if (opcode == 1 && funct3 == 7) {
			ins = InstructionType.c_bnez;

			reg_imm = getCB(arr, 1);//according the shift of imm

			rs1 = (int) reg_imm.get("rs1");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum16(rs1) + "," + Long.toString(imm);  //waiting to test
		} else if (opcode == 2 && funct3 == 0) {
			if (bit_12 == 0 && bit_6_2 == 0) {
				ins = InstructionType.c_slli64;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + ",0x" + Long.toHexString(imm);  //waiting to test
			} else {
				ins = InstructionType.c_slli;

				reg_imm = getCI(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + ",0x" + Long.toHexString(imm);  //waiting to test
			}
		} else if (opcode == 2 && funct3 == 1) {
			if (rd == 0) {
				ins = InstructionType.c_flwsp;

				reg_imm = getCI(arr, 3);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				imm = (long) reg_imm.get("imm");

				code = ins + " " + Registers.getRegFNum32(rd) + "," + imm + "(sp)";  //waiting to test
			}
		} else if (opcode == 2 && funct3 == 2) {
			ins = InstructionType.c_lwsp;

			reg_imm = getCI(arr, 2);//according the shift of imm

			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rd) + "," + imm + "(sp)";  //waiting to test
		} else if (opcode == 2 && funct3 == 3) {

			ins = InstructionType.c_ldsp;

			reg_imm = getCI(arr, 2);//according the shift of imm

			rd = (int) reg_imm.get("rd");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rd) + "," + imm + "(sp)";  //waiting to test

		} else if (opcode == 2 && funct3 == 4) {
			if (bit_11_7 == 0) {
				ins = InstructionType.c_ebreak;

				code = ins + " " + "ebreak";
			} else if (bit_12 == 0 && bit_6_2 == 0) {
				ins = InstructionType.c_jr;

				reg_imm = getCR(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");

				String register = Registers.getRegXNum32(rd);

				if (register.equals("ra")) {
					code = "ret";
				} else {
					code = ins + " " + register;
				}
			} else if (bit_12 == 0) {
				ins = InstructionType.c_mv;

				reg_imm = getCR(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = (int) reg_imm.get("rs1");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rs1);  //waiting to test
			} else if (bit_12 == 1 && bit_6_2 == 0) {
				ins = InstructionType.c_jalr;

				reg_imm = getCR(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");

				code = ins + " " + Registers.getRegXNum32(rd);
			} else if (bit_12 == 1) {
				ins = InstructionType.c_add;

				reg_imm = getCR(arr, 0);//according the shift of imm

				rd = (int) reg_imm.get("rd");
				rs1 = (int) reg_imm.get("rs1");

				code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rd) + "," + Registers.getRegXNum32(rs1);  //waiting to test
			}
		} else if (opcode == 2 && funct3 == 5) {
			ins = InstructionType.c_fsdsp;

			reg_imm = getCSS(arr, 3);//according the shift of uimm

			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rs2) + "," + imm + "(sp)";
		} else if (opcode == 2 && funct3 == 6) {
			ins = InstructionType.c_swsp;

			reg_imm = getCSS(arr, 2);//according the shift of uimm

			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rs2) + "," + imm + "(sp)";
		} else if (opcode == 2 && funct3 == 7) {
			ins = InstructionType.c_sdsp;

			reg_imm = getCSS(arr, 3);//according the shift of uimm

			rs2 = (int) reg_imm.get("rs2");
			imm = (long) reg_imm.get("imm");

			code = ins + " " + Registers.getRegXNum32(rs2) + "," + imm + "(sp)";

		} else {
			ins = InstructionType.unimp;
			throw new WrongInstructionException("Unrecognized compressed instruction");
		}

		Line line = new Line();
		line.type = "decodeCompressed";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = ins;
		line.rs1 = rs1;
		line.rs2 = rs2;
		line.imm = imm;
		line.rd = rd;

//		disasmStructure.lines.add(line);
		return line;
	}

	public HashMap<String, Object> getCSS(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object rs2 = (int) AssemblerLib.getBitsDense(value, new int[]{6, 5, 4, 3, 2});
		Object imm = null;

		if (shift == 3) {//c.fsdsp
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7, 12, 11, 10}) << shift;
		} else if (shift == 2) {//c.swsp c.fswsp
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{8, 7, 12, 11, 10, 9}) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CSS instruction");
		}

		hm.put("rs2", rs2);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCB(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object rs1 = null;
		Object imm = null;

		if (shift == 1) {//c..beqz.bnez
			rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});
			imm = (long) AssemblerLib.getTwosComplement(value, new int[]{12, 6, 5, 2, 11, 10, 4, 3}, 8) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CB instruction");
		}

		hm.put("rs1", rs1);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCS(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});
		Object rs2 = (int) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2});
		Object imm = null;

		if (shift == 2) {//c.sw,c.fsw
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{5, 12, 11, 10, 6}) << shift;
		} else if (shift == 3) {//c.fsd
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{6, 5, 12, 11, 10}) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CS instruction");
		}

		hm.put("rs1", rs1);
		hm.put("rs2", rs2);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCIW(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object rd = null;
		Object imm = null;

		if (shift == 2) {//c.addi4spn
			rd = (int) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2});
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{10, 9, 8, 7, 12, 11, 5, 6}) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CIW instruction");
		}

		hm.put("rd", rd);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCL(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});
		Object rd = (int) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2});
		Object imm = null;

		if (shift == 2) {//c.lw
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{5, 12, 11, 10, 6}) << shift;
		} else if (shift == 3) {//c.flw c.fld
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{6, 5, 12, 11, 10}) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CL instruction");
		}

		hm.put("rs1", rs1);
		hm.put("rd", rd);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCI(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];
		int funct3 = (arr[1] & 0b11100000) >> 5;
		int opcode = arr[0] & 0b11;
		int bit_11_10 = (arr[1] & 0b1100) >> 2;
		int bit_17 = (arr[1] & 0b10000) >> 4;

		Object rd = (int) AssemblerLib.getBitsDense(value, new int[]{11, 10, 9, 8, 7});
		Object imm = null;

		if (shift == 0) {//c.slli/c.slli64/c.ebreak/c.li/c.addi/c.srli64/c,srai64/c.srli/c.srai/c.andi
			int[] arr_imm_nzuimm = {12, 6, 5, 4, 3, 2};

			if (funct3 == 4 && opcode == 1) {
				rd = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});

				if (bit_11_10 == 2) {//c.andi
					imm = (long) AssemblerLib.getTwosComplement(value, arr_imm_nzuimm, arr_imm_nzuimm.length) << shift;
				} else {//c.srli64,c.scrli,scrai64,scrai
					imm = (long) AssemblerLib.getBitsDense(value, arr_imm_nzuimm) << shift;
				}
			} else {//c.slli,c.slli64,c.li,c.addi				
				if (opcode == 1) {//c.li,c.addi
					imm = (long) AssemblerLib.getTwosComplement(value, arr_imm_nzuimm, arr_imm_nzuimm.length) << shift;
				} else {//c.slli,c.slli64
					imm = (long) AssemblerLib.getBitsDense(value, arr_imm_nzuimm) << shift;
				}
			}
		} else if (shift == 2) {//c.lwsp,c.flwsp
			imm = (long) AssemblerLib.getBitsDense(value, new int[]{3, 2, 12, 6, 5, 4}) << shift;
		} else if (shift == 3) {//c.fldsp,c.ldsp
			if (funct3 == 3) {//c.ldsp
				imm = (long) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2, 12, 6, 5}) << shift;
			} else {
				imm = (long) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2, 12, 6, 5}) << shift;
			}
		} else if (shift == 4) {//c.addi16sp
			imm = (long) AssemblerLib.getTwosComplement(value, new int[]{12, 4, 3, 5, 2, 6}, 6) << shift;
		} else if (shift == 12) {//c.lui//special
			long a = (long) AssemblerLib.getBitsDense(value, new int[]{12, 6, 5, 4, 3, 2});

			if (bit_17 == 1) {
				a = a | 0b11000000;
				imm = (long) (a + 0xfff00);
			} else {
				imm = (long) a + 0;
			}
		} else {
			throw new WrongInstructionException("Unrecognized CI instruction");
		}

		hm.put("rd", rd);
		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCJ(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];

		Object imm = null;

		if (shift == 1) {
			imm = (long) AssemblerLib.getTwosComplement(value, new int[]{12, 8, 10, 9, 6, 7, 2, 11, 5, 4, 3}, 11) << shift;
		} else {
			throw new WrongInstructionException("Unrecognized CJ instruction");
		}

		hm.put("imm", imm);

		return hm;
	}

	public HashMap<String, Object> getCR(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];
		int funct3 = (arr[1] & 0b11100000) >> 5;
		int opcode = arr[0] & 0b11;

		Object rd = null;
		Object rs1 = null;

		if (shift == 0) {
			if (opcode == 1 && funct3 == 4) {
				rd = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});
				rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2});
			} else if (opcode == 2 && funct3 == 4) {
				rd = (int) AssemblerLib.getBitsDense(value, new int[]{11, 10, 9, 8, 7});
				rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{6, 5, 4, 3, 2});
			}
		} else {
			throw new WrongInstructionException("Unrecognized CR instruction");
		}

		hm.put("rd", rd);
		hm.put("rs1", rs1);

		return hm;
	}

	public HashMap<String, Object> getCA(int arr[], int shift) throws WrongInstructionException {
		HashMap<String, Object> hm = new HashMap<>();

		int value = arr[1] << 8 | arr[0];
		int funct6 = (arr[1] & 0b11111100) >> 2;
		int funct2 = (arr[0] & 0b1100000) >> 5;
		int opcode = arr[0] & 0b11;

		Object rd = null;
		Object rs1 = null;
		Object rs2 = null;
		if (shift == 0) {
			rd = (int) AssemblerLib.getBitsDense(value, new int[]{9, 8, 7});
			rs1 = rd;
			rs2 = (int) AssemblerLib.getBitsDense(value, new int[]{4, 3, 2});
		} else {
			throw new WrongInstructionException("Unrecognized CA instruction");
		}

		hm.put("rd", rd);
		hm.put("rs1", rs1);
		hm.put("rs2", rs2);
		return hm;
	}

	public Line decodeTypeR(int arr[]) throws WrongInstructionException {
		// 5-bits reg
		// [00000000, 11111111, 22222222, 33333333]
		int rd = (arr[1] & 0x0f) << 1 | (arr[0] >> 7);
		int rs1 = (arr[2] & 0x0f) << 1 | (arr[1] >> 7);
		int rs2 = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		// 3-bits reg
		int funct3 = (arr[1] & 0x70) >> 4;
		int funct7 = (arr[3] >> 1);
		int opcode = arr[0] & 0x7f;
		String ins = null;
		InstructionType insName = null;

		insName = getTypeRInstruction(opcode, funct3, funct7);
		ins = String.format("%s %s,%s,%s", insName, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs2), Registers.getRegXNum32(rs1));
//		MessageHandler.println(ins);
		Line line = new Line();
		line.type = "decodeTypeR";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = insName;
		line.rs1 = rs1;
		line.rs2 = rs2;
		line.imm = 0;
		line.rd = rd;
//			disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeTypeR4(int arr[]) throws WrongInstructionException {
		// 5-bits reg
		// [00000000, 11111111, 22222222, 33333333]
		int frd = (arr[1] & 0x0f) << 1 | (arr[0] >> 7);
		int frs1 = (arr[2] & 0x0f) << 1 | (arr[1] >> 7);
		int frs2 = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		int frs3 = (arr[3] & 0xf8) >> 3;

		// 3-bits reg
		int funct3 = (arr[1] & 0x70) >> 4;
		int funct2 = (arr[3] & 0x06) >> 1;
		int opcode = arr[0] & 0x7f;

		InstructionType insName = getTypeR4Instruction(opcode, funct2);
		String ins = String.format("%s %s,%s,%s,%s,%s", insName, Registers.getRegFNum32(frd), Registers.getRegFNum32(frs1), Registers.getRegFNum32(frs2), Registers.getRegFNum32(frs3), Registers.getRegRm(funct3));

//		MessageHandler.println(ins);
		Line line = new Line();
		line.type = "decodeTypeR4";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = insName;
		line.frs1 = frs1;
		line.frs2 = frs2;
		line.frs3 = frs3;
		line.imm = 0;
		line.frd = frd;

//			disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeTypeFR(int arr[]) throws WrongInstructionException {
		int frd = (arr[1] & 0x0f) << 1 | (arr[0] >> 7);
		int frs1 = (arr[2] & 0x0f) << 1 | (arr[1] >> 7);
		int frs2 = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;

		// 3-bits reg
		int funct3 = (arr[1] & 0x70) >> 4;
		int funct7 = (arr[3] >> 1);

		InstructionType insName = getTypeFRInstruction(funct3, funct7, frs1, frs2);
		String ins;

		if (funct7 == 0 || funct7 == 0b0000100 || funct7 == 0b0001000 || funct7 == 0b0001100) {
			ins = String.format("%s %s,%s,%s,%s", insName, Registers.getRegFNum32(frd), Registers.getRegFNum32(frs1), Registers.getRegFNum32(frs2), Registers.getRegRm(funct3));
		} else if (funct7 == 0b0101100 || funct7 == 0b1100000 || funct7 == 0b1101000) {
			ins = String.format("%s %s,%s,%s", insName, Registers.getRegFNum32(frd), Registers.getRegFNum32(frs1), Registers.getRegRm(funct3));
		} else if (funct7 == 0b1110000) {
			ins = String.format("%s %s,%s", insName, Registers.getRegXNum32(frd), Registers.getRegFNum32(frs1));
		} else {
			ins = String.format("%s %s,%s,%s", insName, Registers.getRegFNum32(frd), Registers.getRegFNum32(frs1), Registers.getRegFNum32(frs2));
		}
//		MessageHandler.println(ins);
		Line line = new Line();
		line.type = "decodeTypeFR";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = insName;
		line.frs1 = frs1;
		line.frs2 = frs2;
		line.imm = 0;
		line.frd = frd;

//			disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeTypeI(int arr[]) throws WrongInstructionException {
		//0-7 8-15
		int rd = (arr[1] & 0x0f) << 1 | (arr[0] >> 7);
		int rs1 = (arr[2] & 0x0f) << 1 | (arr[1] >> 7);
		int shamt5 = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		int shamt6 = (arr[3] & 0b11) << 4 | (arr[2] & 0xf0) >> 4;
		int funct3 = (arr[1] & 0x70) >> 4;
		int funct6 = (arr[3] >> 2);
		int funct7 = (arr[3] >> 1);
		int imm = (arr[3] << 4) | ((arr[2] & 0xf0) >> 4);
		long value = arrTo32Binary(arr);
		int opcode = arr[0] & 0b1111111;

		InstructionType insName;
		String ins = null;

		if (isShamtInstruction(opcode, funct3) == 2) {
			insName = getTypeIInstruction(opcode, funct3, funct6);
			ins = String.format("%s %s,%s,0x%s", insName, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs1), Integer.toString(shamt6, 16));
		} else if (isShamtInstruction(opcode, funct3) == 1) {
			insName = getTypeRInstruction(opcode, funct3, funct7);
			ins = String.format("%s %s,%s,0x%s", insName, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs1), Integer.toString(shamt5, 16));
		} else {
			imm = (int) AssemblerLib.getTwosComplement(value, new int[]{31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20}, 12);
			insName = getTypeIInstruction(opcode, funct3, -1);
			if (opcode == 3) {// lb - lhu
				ins = String.format("%s %s,%s(%s)", insName, Registers.getRegXNum32(rd), imm, Registers.getRegXNum32(rs1));
			} else if (opcode == 0b0000111) { //floating
				ins = String.format("%s %s,%s,%s", insName, Registers.getRegFNum32(rd), Registers.getRegXNum32(rs1), imm);
				System.out.println("HI");
			} else {
				ins = String.format("%s %s,%s,%s", insName, Registers.getRegXNum32(rd), Registers.getRegXNum32(rs1), imm);
			}
		}
//		MessageHandler.println(ins);
		Line line = new Line();
		line.type = "decodeTypeI";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = ins;
		line.instructionType = insName;
		line.rs1 = rs1;
		line.rs2 = 0;
		line.imm = imm;
		line.rd = rd;
		return line;
	}

	public Line decodeTypeFS(int arr[]) throws WrongInstructionException {
		int funct3 = (arr[1] >> 4) & 0b0111;
		int frs1 = ((arr[2] & 0b11110000) >> 4) | ((arr[3] & 0b1) << 4);
		int rs2 = ((arr[2] & 0b1111) << 1) | (arr[1] >> 7);
		long value = arrTo32Binary(arr);
		long imm = (long) AssemblerLib.getTwosComplement(value, new int[]{31, 30, 29, 28, 27, 26, 25, 11, 10, 9, 8, 7}, 12);
		int opcode = arr[0] & 0b1111111;;

		InstructionType insName = getTypeFSInstruction(opcode, funct3);

		String code = insName + " " + Registers.getRegFNum32(frs1) + "," + imm + "(" + Registers.getRegXNum32(rs2) + ")";

		//String ins = String.format("%s %s,%s(%s)", insName, Registers.getRegXNum32(rs1), imm, Registers.getRegXNum32(rs2));
		Line line = new Line();
		line.type = "decodeTypeFS";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;

		line.instructionType = insName;
		//full_code = code;
		line.frs1 = frs1;
		line.rs2 = rs2;
		line.imm = imm;
		line.rd = 0;

//		disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeTypeS(int arr[]) throws WrongInstructionException {
		int funct3 = (arr[1] >> 4) & 0b0111;
		int rs1 = ((arr[2] & 0b11110000) >> 4) | ((arr[3] & 0b1) << 4);
		int rs2 = ((arr[2] & 0b1111) << 1) | (arr[1] >> 7);
		long value = arrTo32Binary(arr);
		long imm = (long) AssemblerLib.getTwosComplement(value, new int[]{31, 30, 29, 28, 27, 26, 25, 11, 10, 9, 8, 7}, 12);
		int opcode = arr[0] & 0b1111111;

		InstructionType ins = getTypeSInstruction(opcode, funct3);
		String code = ins + " " + Registers.getRegXNum32(rs1) + "," + imm + "(" + Registers.getRegXNum32(rs2) + ")";

		Line line = new Line();
		line.type = "decodeTypeS";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = ins;
		//full_code = code;
		line.rs1 = rs1;
		line.rs2 = rs2;
		line.imm = imm;
		line.rd = 0;

//		disasmStructure.lines.add(line);
		return line;
	}

	public Line decodeMagic(int arr[]) {
		Line line = new Line();
		line.type = "decodeMagic";
		int funct = (arr[1]) & 0b0111;
		if (funct == 0) {
			line.instructionType = InstructionType.pause_sim;
			line.code = "pause_sim";
		} else {
			//line.code = " ";
			line = decodeUnimp(arr);
		}
		return line;

	}

	public Line decodeTypeB(int arr[]) throws WrongInstructionException {
		int funct3 = (arr[1] >> 4) & 0b0111;
		int rs2 = ((arr[2] & 0b11110000) >> 4) | ((arr[3] & 0b1) << 4);
		int rs1 = ((arr[2] & 0b1111) << 1) | (arr[1] >> 7);
		int bit_14_12 = (arr[1] & 0b1110000) >> 4;
		long value = arrTo32Binary(arr);
		long imm = (long) AssemblerLib.getTwosComplement(value, new int[]{31, 7, 30, 29, 28, 27, 26, 25, 11, 10, 9, 8}, 12) << 1;

		InstructionType ins = getTypeBInstruction(funct3, rs1, rs2);
		String register_rs1 = Registers.getRegXNum32(rs1);
		String register_rs2 = Registers.getRegXNum32(rs2);
		String offset = Long.toHexString(currentOffset + imm);
		String code = ins + " ";

		if (register_rs2.equals("zero")) {
			code = code + register_rs1 + "," + imm;
		} else if (register_rs1.equals("zero")) {
			code = code + register_rs2 + "," + imm;
		} else {
			code = code + register_rs1 + "," + register_rs2 + "," + imm;
		}

		Line line = new Line();
		line.type = "decodeTypeB";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = ins;
		line.rs1 = rs1;
		line.rs2 = rs2;
		line.imm = imm;
		line.rd = 0;
		//full_code = code;

//		disasmStructure.lines.add(line);
		return line;
	}

	public Line ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(int arr[]) throws WrongInstructionException {
		/*0-7 8-15 16-23 24-31*/
		int funct3 = (arr[1] >> 4) & 0b111;
		int csr = (arr[3] << 4) | (arr[2] >> 4);
		int bit_20_31 = (arr[3] << 4) | (arr[2] >> 4);
		int rs1 = ((arr[2] & 0b1111) << 1) | (arr[1] >> 7);
		int rs2;
		int rd = ((arr[1] & 0b1111) << 1) | (arr[0] >> 7);
		int uimm = ((arr[2] & 0b1111) << 1) | (arr[1] >> 7);
		int funct7 = (arr[3] >> 1);
		InstructionType ins;
		String code = null;

		if (funct3 == 0) {
			if (bit_20_31 == 0 && rd == 0 && rs1 == 0) {
				/*	Line line = new Line();
				
				line.sourceFile = null;
				line.lineNo = -1;
				line.offset = currentOffset;
				line.bytes = arr.clone();
				line.code = "ecall";
				line.instruction = "ecall";
				//full_code = "ecall";

				disasmStructure.lines.add(line);
				return line;*/
				ins = InstructionType.ecall;
				code = "ecall";
			} else if (bit_20_31 == 1 && rd == 0 && rs1 == 0) {
				ins = InstructionType.ebreak;
				code = "ebreak";
			} else if (bit_20_31 == 258 && rd == 0 && rs1 == 0) {
				ins = InstructionType.sret;
				code = "sret";
			} else if (bit_20_31 == 770 && rd == 0 && rs1 == 0) {
				ins = InstructionType.mret;
				code = "mret";
			} else if (bit_20_31 == 261) {
				ins = InstructionType.wfi;
				code = "wfi";
			} else if (funct7 == 9) {
				ins = InstructionType.sfence_vm;

				int value = (int) arrTo32Binary(arr);
				rs1 = (int) AssemblerLib.getBitsDense(value, new int[]{24, 23, 22, 21, 20});
				rs2 = (int) AssemblerLib.getBitsDense(value, new int[]{19, 18, 17, 16, 15});

				code = ins + " " + Registers.getRegXNum32(rs1) + "," + Registers.getRegXNum32(rs2);
			} else {
				ins = InstructionType.unimp;//CommonLib.arrayToHexString(arr);
				code = CommonLib.arrayToHexString(arr);
			}
		} else if (funct3 == 1) {
			ins = InstructionType.csrrw;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + Registers.getRegXNum32(rs1);
		} else if (funct3 == 2) {
			ins = InstructionType.csrrs;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + Registers.getRegXNum32(rs1);
		} else if (funct3 == 3) {
			ins = InstructionType.csrrc;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + Registers.getRegXNum32(rs1);
		} else if (funct3 == 5) {
			ins = InstructionType.csrrwi;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + uimm;
		} else if (funct3 == 6) {
			ins = InstructionType.csrrsi;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + uimm;
		} else if (funct3 == 7) {
			ins = InstructionType.csrrci;
			code = ins + " " + Registers.getRegXNum32(rd) + "," + Registers.getCsrNum12(csr) + "," + uimm;
		} else {
			throw new WrongInstructionException("Wrong ecallOrEbreakOrCsr, arr[2] : " + Integer.toHexString(arr[2]));
		}

		Line line = new Line();
		line.type = "ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU";
		line.sourceFile = null;
		line.lineNo = -1;
		line.offset = currentOffset;
		line.bytes = arr.clone();
		line.code = code;
		line.instructionType = ins;
		line.rs1 = rs1;
		line.rd = rd;
		line.csr = csr;
		line.imm = uimm;

//		disasmStructure.lines.add(line);
		return line;
	}
}
