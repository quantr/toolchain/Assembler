package hk.quantr.assembler.print;

import com.thoughtworks.xstream.XStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class MessageHandler {

	private static final Logger logger = Logger.getLogger(MessageHandler.class.getName());
	public static boolean isXML;


	public static MessageError ErrorMessage = new MessageError();

	static {
		InputStream stream = MessageHandler.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void println(String str) {
		logger.log(Level.INFO, "{0}", str);
	}
	
	public static void errorPrintln(String type, String msg, int lineNo, int start, int stop, String line) {
		if (isXML) {
			ErrorMessage.Messages.add(new Message(type, msg, lineNo, start, stop, line));
		} else {
			msg = "Error at line " + lineNo + " [" + line + "]. " + msg;
			logger.log(Level.WARNING, msg);
		}
	}
	
	public static void errorPrintln(String type, String msg) {
		if (isXML) {
			ErrorMessage.Messages.add(new Message(type, msg, 0, 0, 0, "null"));
		} else {
			logger.log(Level.WARNING, "Error: {0}", msg);
		}
	}
	
	public static void errorPrintln(String type, String msg, int lineNo, String line) {
		if (isXML) {
			ErrorMessage.Messages.add(new Message(type, msg, lineNo, 0, 0, line));
		} else {
			logger.log(Level.WARNING, "Error: {0}", msg);
		}
	}
	
	public static void errorPrintln(String str) {
		if (isXML) {
			ErrorMessage.Messages.add(new Message("error", str, 0, 0, 0, null));
		} else {
			logger.log(Level.WARNING, "Error: {0}", str);
		}
	}

	public static void errorPrintln(Object obj) {
		if (isXML) {
			ErrorMessage.Messages.add(new Message("error", obj.toString(), 0, 0, 0, "null"));
		} else {
			logger.log(Level.WARNING, "Error: {0}", obj.toString());
		}
	}

	public static void flushXML() {
		try {
			if (isXML && !ErrorMessage.Messages.isEmpty()) {
				XStream xstream = new XStream();
				xstream.processAnnotations(MessageError.class);
				String xml = xstream.toXML(ErrorMessage);
				System.out.println(xml);
			}
		} catch (Exception e) {
			System.exit(1000);
		}
	}
}
