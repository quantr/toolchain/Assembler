package hk.quantr.assembler.exception;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class NoOfByteException extends Exception {

	public NoOfByteException(String errorMessage) {
		super(errorMessage);
	}
}
