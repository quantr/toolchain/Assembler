package hk.quantr.assembler.exception;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class WrongInstructionException extends Exception {

	public WrongInstructionException(String message) {
		super(message);
	}

}
