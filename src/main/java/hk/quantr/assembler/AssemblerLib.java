package hk.quantr.assembler;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.listener.IncludeListener;
import hk.quantr.assembler.riscv.listener.DefineListener;
import hk.quantr.assembler.riscv.listener.RISCVErrorListener;
//import hk.quantr.assembler.riscv.listener.TimesListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class AssemblerLib {

	public static long getBits(long imm, int offsets[]) {
		String immb = Long.toBinaryString(imm);
		return getBits(gen32StringBit() + immb, offsets);
	}

	public static long getBits(String binary, int offsets[]) {
		long result = 0;
		for (int offset : offsets) {
			int strIndex = binary.length() - offset - 1;
			long temp = Long.parseLong(binary.substring(strIndex, strIndex + 1)) << offset;
			result |= temp;
		}
		return result;
	}

	public static long getBitsDense(long imm, int offsets[]) {
		String immb = Long.toBinaryString(imm);
		//MessageHandler.println("beofre 8087"+" " +"length of immn" + immn.length());
		return getBitsDense(gen32StringBit() + immb, offsets);//2
	}

	public static long getBitsDense(String binary, int offsets[]) {//binary.length=35
		//MessageHandler.println(binary + " " + binary.length());
		long result = 0;
		int shift = 0;
		for (int x = offsets.length - 1; x >= 0; x--) {
			int offset = offsets[x];
			int strIndex = binary.length() - offset - 1;
			//MessageHandler.println(binary.length() + "-" + offset + "-1" +"=" +strIndex + " ");
			long temp = Long.parseLong(binary.substring(strIndex, strIndex + 1)) << shift;
			result |= temp;
			//MessageHandler.println("x:" + " " +Long.toBinaryString(result));
			shift++;
		}
		//MessageHandler.println();
		return result;
	}

	public static long signExtend64(long val, int signbit) { //Input the value, and the bit to sign extend. Let's say 12-bit imm, signbit = 11
		return (((val >> signbit) & 1) == 1) ? val | (0xffffffffffffffffL << signbit) : val & (0xffffffffffffffffL >>> (63 - signbit));
	}

	public static long getTwosComplement(long imm, int offsets[], int noOfBit) {
		String immb = Long.toBinaryString(imm);
		long l = getBitsDense(gen32StringBit() + immb, offsets);
		long v = 1 << (noOfBit - 1);
		if ((l & v) == v) {
			long temp = ~l & ((int) Math.pow(2, noOfBit - 1) - 1);
			temp = temp + 1;
			temp = temp * -1;
			return temp;
		} else {
			return l;
		}
	}

	public static String gen32StringBit() {
		return "0".repeat(32);
	}

	//preProcessor
	public static ArrayList<String> path = new ArrayList<>();
	public static HashSet<String> fileRecorder = new HashSet<>();

	public static String preProcessIncludeFile(String content, File from, String to) throws IOException {
		if ((!path.contains(from + " -> " + to)) && (!path.contains(to + " -> " + from))) {
			try {
				fileRecorder.add(to);
				String includeFileContent = IOUtils.toString(new FileInputStream("testbench/" + to), "utf-8");
				content = content.replaceFirst(("%include \"" + to + "\""), includeFileContent);

				path.add(from + " -> " + to);

			} catch (FileNotFoundException ex) {

				MessageHandler.errorPrintln("Pre-Process Error", String.format("File: %s not found", to), 0, 0, 0, "null");
				MessageHandler.flushXML();
				System.exit(1000);

			}

			if (content.contains("%include")) {
				RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
				parser.encoder.arch = "rv64";
				parser.removeErrorListeners();
				parser.addErrorListener(new RISCVErrorListener(content));
				IncludeListener includeListener = new IncludeListener();
				DefineListener defineListener = new DefineListener();
				parser.addParseListener(defineListener);
				parser.addParseListener(includeListener);
				RISCVAssemblerParser.AssembleContext context = parser.assemble();
				for (String filename : includeListener.includes) {
					return preProcessIncludeFile(content, from, filename);
				}
			}
		} else {
			String[] contentSplit = content.split("\n");
			MessageHandler.errorPrintln(String.format("recursive include: %s", from.getAbsolutePath() + " -> " + (new File(to)).getAbsolutePath()));
			MessageHandler.errorPrintln(String.format("file: %s, line Num: %d", from.getAbsolutePath(), Arrays.asList(contentSplit).indexOf("%include \"" + to + "\"") + 1));
			MessageHandler.errorPrintln("line: " + contentSplit[Arrays.asList(contentSplit).indexOf("%include \"" + to + "\"")]);
			MessageHandler.flushXML();
			System.exit(2);
		}
		return content;
	}

	public static String preProcessDefineVariable(String file, String duplicateElement) throws IOException {
		String fileContent = IOUtils.toString(new FileInputStream(file), "utf-8");
		String[] contentSplit = fileContent.split("\n");
		File fileFile = new File(file);
		for (String line : contentSplit) {
			if (line.contains("%define") && line.contains(String.format(" %s ", duplicateElement))) {
				MessageHandler.errorPrintln(String.format("file: %s, line num: %d", fileFile.getAbsolutePath(), Arrays.asList(contentSplit).indexOf(line) + 1));
			}
		}
		return null;
	}

	public static String preProcess(File file, String arch) throws FileNotFoundException, IOException {
//
		ArrayList<String> duplicateDefine = new ArrayList<>();
		String currentFile = file.toString();
//
		String content = IOUtils.toString(new FileInputStream(currentFile), "utf-8");
		RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
		parser.encoder.arch = arch;
		parser.removeErrorListeners();
		IncludeListener includeListener = new IncludeListener();
		DefineListener defineListener = new DefineListener();
//		TimesListener timesListener = new TimesListener();
		parser.addParseListener(includeListener);
		parser.addParseListener(defineListener);
//		parser.addParseListener(timesListener);
		parser.assemble();
		MessageHandler.ErrorMessage.Messages.clear();

		if (content.contains("%include")) {
			for (String filename : includeListener.includes) {
				content = preProcessIncludeFile(content, new File(currentFile), filename);
			}
		}
		if (content.contains("%define")) {
			if (DefineListener.duplicateElements.isEmpty()) {
				for (String i : DefineListener.lines) {
					content = content.replaceAll(i, "");
				}
				for (String h : DefineListener.map.keySet()) {
					content = content.replaceAll(String.format("\\b%s\\b", h), DefineListener.map.get(h));
				}
			} else {
				String[] lines = content.split("\n");
				File fileCurrentFile = new File(currentFile);
				for (String duplicateElement : DefineListener.duplicateElements) {
					for (int k = 0; k < lines.length; k++) {
						if (lines[k].contains("%define") && lines[k].contains(String.format(" %s ", duplicateElement))) {
//					   MessageHandler.errorPrintln(String.format("(%s) file: %s, line num: %d ", duplicateElement, fileCurrentFile.getAbsolutePath(), k + 1));
							MessageHandler.errorPrintln("Pre-process Error: Re-define", String.format("(%s) file: %s, line num: %d ", duplicateElement, fileCurrentFile.getAbsolutePath(), k + 1), k + 1, lines[k]);
						}
						for (String files : fileRecorder) {
							preProcessDefineVariable(files, duplicateElement);
						}
					}
				}
				MessageHandler.flushXML();
				System.exit(1000);
			}
		}

		if (content.contains("%times")) {

		}
		return content;
	}

}
