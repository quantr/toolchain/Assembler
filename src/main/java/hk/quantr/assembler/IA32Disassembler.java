package hk.quantr.assembler;

import hk.quantr.assembler.print.MessageHandler;
import java.io.InputStream;

public class IA32Disassembler {

	public static void disassemble(InputStream inputStream) {
		try {
			int b;
			int count = 0;

//			while ((b = inputStream.read()) != 0xcc && b != -1) {
//				MessageHandler.println(Integer.toHexString(b));
//			}
			while ((b = inputStream.read()) != 0xcc && b != -1) {
				int operandSize = 2;
				int FPU = 0;
				while (b == 0xF0
						|| b == 0xF2
						|| b == 0xF3
						|| (b & 0xFC) == 0x64
						|| (b & 0xF8) == 0xD8
						|| (b & 0x7E) == 0x62) {
					if (b == 0x66) {
						operandSize = 2;
					} else if ((b & 0xF8) == 0xD8) {
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
						FPU = b;
						break;
					}

					b = inputStream.read();
					MessageHandler.println("	> " + Integer.toHexString(b));
				}

				// Skip two-byte opcode byte 
				boolean twoByte = false;
				if (b == 0x0F) {
					twoByte = true;
					b = inputStream.read();
					MessageHandler.println("	> " + Integer.toHexString(b));
				}
				// Skip opcode byte 
				int opcode = b;
				MessageHandler.println(Integer.toHexString(opcode));

				// Skip mod R/M byte 
				int modRM = 0xFF;
				if (FPU > 0) {
					if ((opcode & 0xC0) != 0xC0) {
						modRM = opcode;
					}
				} else if (!twoByte) {
					if ((opcode & 0xC4) == 0x00
							|| (opcode & 0xF4) == 0x60 && ((opcode & 0x0A) == 0x02 || (opcode & 0x09) == 0x9)
							|| (opcode & 0xF0) == 0x80
							|| (opcode & 0xF8) == 0xC0 && (opcode & 0x0E) != 0x02
							|| (opcode & 0xFC) == 0xD0
							|| (opcode & 0xF6) == 0xF6) {
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
						modRM = b;
					}
				} else {
					if ((opcode & 0xF0) == 0x00 && (opcode & 0x0F) >= 0x04 && (opcode & 0x0D) != 0x0D
							|| (opcode & 0xF0) == 0x30
							|| opcode == 0x77
							|| (opcode & 0xF0) == 0x80
							|| (opcode & 0xF0) == 0xA0 && (opcode & 0x07) <= 0x02
							|| (opcode & 0xF8) == 0xC8) {
						// No mod R/M byte 
					} else {
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
						modRM = b;
					}
				}

				// Skip SIB
				if ((modRM & 0x07) == 0x04
						&& (modRM & 0xC0) != 0xC0) {
					b = inputStream.read();
					MessageHandler.println("	> " + Integer.toHexString(b));
					//b += 1;   // SIB
				}

				// Skip displacement
				if ((modRM & 0xC5) == 0x05) {
					//func += 4;   // Dword displacement, no base 
					inputStream.readNBytes(new byte[4], 0, 4);
					MessageHandler.println("	> '4'");
				}
				if ((modRM & 0xC0) == 0x40) {
					//func += 1;   // Byte displacement 
					inputStream.readNBytes(new byte[1], 0, 1);
					MessageHandler.println("	> '1'");
				}
				if ((modRM & 0xC0) == 0x80) {
					//func += 4;   // Dword displacement 
					inputStream.readNBytes(new byte[4], 0, 4);
					MessageHandler.println("	> '4'");
				}
				// Skip immediate 
				if (FPU > 0) {
					// Can't have immediate operand 
				} else if (!twoByte) {
					if ((opcode & 0xC7) == 0x04
							|| (opcode & 0xFE) == 0x6A
							|| // PUSH/POP/IMUL 
							(opcode & 0xF0) == 0x70
							|| // Jcc 
							opcode == 0x80
							|| opcode == 0x83
							|| (opcode & 0xFD) == 0xA0
							|| // MOV 
							opcode == 0xA8
							|| // TEST 
							(opcode & 0xF8) == 0xB0
							|| // MOV
							(opcode & 0xFE) == 0xC0
							|| // RCL 
							opcode == 0xC6
							|| // MOV 
							opcode == 0xCD
							|| // INT 
							(opcode & 0xFE) == 0xD4
							|| // AAD/AAM 
							(opcode & 0xF8) == 0xE0
							|| // LOOP/JCXZ 
							opcode == 0xEB
							|| opcode == 0xF6 && (modRM & 0x30) == 0x00) // TEST 
					{
//						func += 1;
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
					} else if ((opcode & 0xF7) == 0xC2) {
//						func += 2;   // RET 
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
					} else if ((opcode & 0xFC) == 0x80
							|| (opcode & 0xC7) == 0x05
							|| (opcode & 0xF8) == 0xB8
							|| (opcode & 0xFE) == 0xE8
							|| // CALL/Jcc 
							(opcode & 0xFE) == 0x68
							|| (opcode & 0xFC) == 0xA0
							|| (opcode & 0xEE) == 0xA8
							|| opcode == 0xC7
							|| opcode == 0xF7 && (modRM & 0x30) == 0x00) {
//						func += operandSize;
						inputStream.readNBytes(new byte[operandSize], 0, operandSize);
					}
				} else {
					if (opcode == 0xBA
							|| // BT 
							opcode == 0x0F
							|| // 3DNow! 
							(opcode & 0xFC) == 0x70
							|| // PSLLW 
							(opcode & 0xF7) == 0xA4
							|| // SHLD 
							opcode == 0xC2
							|| opcode == 0xC4
							|| opcode == 0xC5
							|| opcode == 0xC6) {
//						func += 1;
						b = inputStream.read();
						MessageHandler.println("	> " + Integer.toHexString(b));
					} else if ((opcode & 0xF0) == 0x80) {
//						func += operandSize;   // Jcc -i
						inputStream.readNBytes(new byte[operandSize], 0, operandSize);
					}
				}

				count++;
			}
			MessageHandler.println("count=" + count);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
