/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.assembler.riscv;

import java.util.ArrayList;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyTableModel implements TableModel {

	String[] columnNames = {"ID", "Code", "Quantr", "Quantr Error"};
	ArrayList<Data> data = new ArrayList<Data>();

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			Data d = data.get(rowIndex);

			switch (columnIndex) {
				case 0:
					return "";
				case 1:
					return d.quantrCode;
				case 2:
					return bytesToString(d.quantr32Bytes.getValue());
				case 3:
					return d.quantr32Bytes.getKey();
				default:
					return d;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public Data getData(int row) {
		return data.get(row);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void add(String functionName, String quantrCode, String gasCode, Pair<String, byte[]> quantr32Bytes, Pair<String, byte[]> quantr64Bytes) {
		data.add(new Data(functionName, quantrCode, gasCode, quantr32Bytes, false, quantr64Bytes, false));
	}

	class Data {

		String functionName;
		String quantrCode;
		String gasCode;
		Pair<String, byte[]> quantr32Bytes;
		Pair<String, byte[]> quantr64Bytes;
		boolean pass32;
		boolean pass64;

		public Data(String functionName, String quantrCode, String gasCode, Pair<String, byte[]> quantr32Bytes, boolean pass32, Pair<String, byte[]> quantr64Bytes, boolean pass64) {
			this.functionName = functionName;
			this.quantrCode = quantrCode;
			this.gasCode = gasCode;
			this.quantr32Bytes = quantr32Bytes;
			this.pass32 = pass32;
			this.quantr64Bytes = quantr64Bytes;
		}

	}

	String bytesToString(byte[] bytes) {
		if (bytes == null) {
			return "";
		}
		String str = "";
		for (byte b : bytes) {
			str += Integer.toHexString(b & 0xff) + " ";
		}
		return str;
	}
}
