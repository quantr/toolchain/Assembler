package hk.quantr.assembler.riscv;

import hk.quantr.antlrcalculatorlibrary.CalculatorLibrary;
import hk.quantr.assembler.AssemblerLib;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.listing.Line;
import hk.quantr.assembler.riscv.listing.Listing;
import java.io.ByteArrayOutputStream;
import java.util.List;
import org.antlr.v4.runtime.Token;

public class RISCVEncoder {

	public Listing listing = new Listing();
	public boolean flag = false;
	public String arch;
	public int time = 0;
	public ByteArrayOutputStream out = new ByteArrayOutputStream();
	String err;

	public void addListingNonCode(int lineNo, String text) {
		listing.lines.add(new Line(lineNo, 0L, null, null, text, null));
	}

	public int encodeTypeI(String code, int lineNo, String section, Long address, int opcode, int funct3, int rd, int rs1, long imm) {
		if (opcode == 0b0011011 || opcode == 0b0111011) { //all w case
			if (arch.equals("rv32")) {
				err = "This function is not allowed in rv32.";
				int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
				MessageHandler.errorPrintln("Encoder Error", err, lineNo, positions[0], positions[1], code);
				return 0;
			}
		}
		try {
			if (imm > 0x7ff || imm < -0x7ff) { //imm more than 12 bit, fuck but do
				err = "imm has to be 12 bits signed";
				int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "imm");
				MessageHandler.errorPrintln("Encoder Error", err, lineNo, positions[0], positions[1], code);
			}
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | ((funct3 & 0x7) << 4) | (rd >> 1));
			bytes[2] = (byte) ((imm & 0b1111) << 4 | (rs1 >> 1));
			bytes[3] = (byte) (imm >> 4);

			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeI(String code, int lineNo, String section, Long address, int opcode, int funct3, int funct7, int rd, int rs1, long imm) {
		try {
			if (imm > 0b11111) {
				err = "Improper shift amount, returned as maximum allowed value";
				int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "imm");
				MessageHandler.errorPrintln("Encoder Error", err, lineNo, positions[0], positions[1], code);
			}

			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | ((funct3 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) ((int) ((imm & 0b1111) << 4 | (rs1 >> 1)));
			bytes[3] = (byte) ((int) ((funct7 << 1) | (imm >> 4)));

			out.write(bytes);
			System.out.println("hi");

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}

	}

	public int encodeTypeR(String code, int lineNo, String section, Long address, int opcode, int funct3, int funct7, int rd, int rs1, int rs2) {
		if ((opcode == 0b0111011 || opcode == 0b0101111) && arch.equals("rv32")) {
			err = "This function is not allowed in rv32.";
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, positions[0], positions[1], code);
		} else try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | ((funct3 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) (((rs2 & 0b1111) << 4) | (rs1 >> 1));
			bytes[3] = (byte) ((funct7 << 1) | (rs2 >> 4));
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
		return 0;
	}

	public int encodeTypeLi(String code, int lineNo, String section, Long address, int rd, long imm) {
		int b = 0;
		long imm32to64 = imm >>> 32;
		long imm0to31 = (imm << 32) >> 32;

		if (imm32to64 != 0 && arch.equals("rv32")) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "imm");
			MessageHandler.errorPrintln("Encoder Error", "Immediate exceeded 32 bits in rv32.", lineNo, positions[0], positions[1], code);
		}

		long ADD_imm, LUI_imm;
		if (imm32to64 != 0 && arch.equals("rv64")) {
			ADD_imm = imm32to64 & 0xfff;
			LUI_imm = (imm32to64 >> 12) + (ADD_imm >> 11);
			if (LUI_imm != 0) {
				b += encodeTypeU("lui x" + String.valueOf(rd) + ", 0x" + Long.toHexString(LUI_imm), lineNo, section, address + b, 0b0110111, rd, Long.toString(LUI_imm)); //lui rd, LUI_imm
				b += encodeTypeI("addiw x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0011011, 0, rd, rd, ADD_imm); //addiw rd, rd, ADD_imm
			} else {
				b += encodeTypeI("addiw x" + String.valueOf(rd) + ", x0, 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0011011, 0, rd, 0, ADD_imm); //addiw rd, x0, ADD_imm
			}
			ADD_imm = imm0to31 >> 21;
			b += encodeTypeI("slli x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 11", lineNo, section, address + b, 0b0010011, 0b001, 0, rd, rd, 11); //slli rd, rd, 11
			b += encodeTypeI("addi x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0010011, 0, rd, rd, ADD_imm); //addi rd, rd, ADD_imm
			ADD_imm = (imm0to31 >> 10) - (ADD_imm << 11);
			b += encodeTypeI("slli x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 11", lineNo, section, address + b, 0b0010011, 0b001, 0, rd, rd, 11); //slli rd, rd, 11
			b += encodeTypeI("addi x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0010011, 0, rd, rd, ADD_imm); //addi rd, rd, ADD_imm
			ADD_imm = (imm0to31 & 0xfff) - ((ADD_imm << 10) & 0xfff);
			b += encodeTypeI("slli x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 10", lineNo, section, address + b, 0b0010011, 0b001, 0, rd, rd, 10); //slli rd, rd, 10
			b += encodeTypeI("addi x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0010011, 0, rd, rd, ADD_imm); //addi rd, rd, ADD_imm
		} else {
			ADD_imm = imm0to31 & 0xfff;
			LUI_imm = (imm0to31 >> 12) + (ADD_imm >> 11);
			if (LUI_imm != 0) {
				b += encodeTypeU("lui x" + String.valueOf(rd) + ", 0x" + Long.toHexString(LUI_imm), lineNo, section, address + b, 0b0110111, rd, Long.toString(LUI_imm)); //lui rd, LUI_imm
				b += encodeTypeI("addi x" + String.valueOf(rd) + ", x" + String.valueOf(rd) + ", 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0010011, 0, rd, rd, ADD_imm); //addi rd, rd, ADD_imm
			} else {
				b += encodeTypeI("addi x" + String.valueOf(rd) + ", x0, 0x" + Long.toHexString(ADD_imm), lineNo, section, address + b, 0b0010011, 0, rd, 0, ADD_imm); //addi rd, x0, ADD_imm
			}
		}
		return b;
	}

	public int encodeTypeS(String code, int lineNo, String section, Long address, int opcode, int funct3, int rs1, int rs2, String imm) {
		int tmp = Integer.parseInt(imm);
		if (tmp > 0xFFF || tmp < -0xFFF) {
			err = "out of range! imm must be within 12 bits.";
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "imm");
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, positions[0], positions[1], code);
		} else if (arch.equals("rv32") && opcode == 0b0100011 && funct3 == 0b011) {
			err = "This function is not allowed in rv32.";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
		} else if (((tmp & 0x1) == 1) & (funct3 != 0)) {
			err = "Improper store address, bit 0 can't be 1";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
		} else {
			try {
				byte bytes[] = new byte[4];
				int imm1 = tmp & 0x1f;
				int imm2 = (tmp & 0xfe0) >> 5;
				bytes[0] = (byte) (((tmp & 0b1) << 7) | opcode);
				bytes[1] = (byte) (((rs1 & 0b1) << 7) | (funct3 & 0x7) << 4 | (imm1 >> 1));
				bytes[2] = (byte) (((rs2 & 0b1111) << 4) | (rs1 >> 1));
				bytes[3] = (byte) (imm2 << 1 | (rs2 >> 4));
				out.write(bytes);

				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 4;
			} catch (Exception ex) {
				int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
				MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
				return 0;
			}
		}
		return 0;
	}

	public int encodeTypeI_memOrder(String code, int lineNo, String section, Long address, int opcode, int funct3, int rd, int rs1, String memOrder1, String memOrder2) {
		int fm = 0b0000;
		int p = 0;
		int s = 0;
		if (memOrder1 == null && memOrder2 == null) {
			p = 0b1111;
			s = 0b1111;
		} else {
			if (memOrder1.contains("i")) {
				p = p | 0b1000;
			}
			if (memOrder1.contains("o")) {
				p = p | 0b0100;
			}
			if (memOrder1.contains("r")) {
				p = p | 0b0010;
			}
			if (memOrder1.contains("w")) {
				p = p | 0b0001;
			}
			if (memOrder2.contains("i")) {
				s = s | 0b1000;
			}
			if (memOrder2.contains("o")) {
				s = s | 0b0100;
			}
			if (memOrder2.contains("r")) {
				s = s | 0b0010;
			}
			if (memOrder2.contains("w")) {
				s = s | 0b0001;
			}
		}

		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | ((funct3 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) ((int) (s << 4) | (rs1 >> 1));
			bytes[3] = (byte) ((int) (fm << 4) | p);
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeB(String code, int lineNo, String section, Long address, int opcode, int funct3, int rs1, int rs2, String imm) {
		int tmp = Integer.parseInt(imm);

		if ((tmp & 1) == 1) {
			err = "Improper branch address, bit 0 can't be 1";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			byte bytes[] = new byte[4];
			int tmp11 = (tmp & 0x800) >> 11;
			int tmp1To4 = (tmp & 0x1e) >> 1; // 0b11110
			int tmp5To10 = (tmp & 0x7e0) >> 5;  // 0b111 1110 0000
			int tmp12 = (tmp & 0x1000) >> 12;
			int imm1 = (tmp1To4 << 1) | tmp11;
			int imm2 = (tmp12 << 6) | tmp5To10;
			bytes[0] = (byte) (((imm1 & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | (funct3 << 4) | imm1 >> 1);
			bytes[2] = (byte) (((rs2 & 0xf) << 4) | rs1 >> 1);
			bytes[3] = (byte) ((imm2 << 1) | (rs2 & 0x10) >> 4);
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeJ(String code, int lineNo, String section, Long address, int opcode, int rd, String imm) {
		int tmp = Integer.parseInt(imm);
		if ((tmp & 0x1) == 1) {
			err = "bit 0 of imm should be 0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (tmp > 0xfffff) {
			err = "imm cannot larger than 20 bits";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			byte bytes[] = new byte[4];
			int tmp20 = tmp & 0x100000 >> 20;
			int tmp1To10 = (tmp & 0x7fe) >> 1; //0b:111-1111-1110
			int tmp11 = (tmp & 0x800) >> 11;
			int tmp12To19 = (tmp & 0xff000) >> 12;// 0b: 111-1111-1000-0000-0000

			int imm1 = tmp12To19 | (tmp11 << 8) | (tmp1To10 << 9) | (tmp20 << 19);
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((imm1 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) ((imm1 & 0xFF0) >> 4);
			bytes[3] = (byte) (imm1 >> 12);
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeU(String code, int lineNo, String section, Long address, int opcode, int rd, String imm) {
		long tmp = Long.parseLong(imm); // imm should be 19 bits
		if (tmp > 0xfffff) {
			err = "imm cannot large than 20 bits";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
		}
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | opcode); // rd:1, opcode: 7
			bytes[1] = (byte) ((tmp << 4) | (rd >> 1)); // tmp: 4, rd: 4
			bytes[2] = (byte) (tmp >> 4); // tmp: 8
			bytes[3] = (byte) (tmp >> 12); // tmp: 5
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeCL(String code, int lineNo, String section, Long address, int opcode, int rd, int rs1, long imm, int funct3) {
		if (imm > 0b1111111) {
			err = "imm cannot large than 7 bits";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if ((imm & 0b11) != 0b00) {
			err = "imm last two bits must be zero";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			if (funct3 == 0b010) { //c.lw
				long uimm_5_3 = (imm >> 3) & 0b111;
				long uimm_2_6 = (((imm >> 2) & 1) << 1) | ((imm >> 6) & 1);

				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((rs1 & 1) << 7) | (uimm_2_6 << 5) | (rd << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_3 << 2) | (rs1 >> 1));
				out.write(bytes);

				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			} else if (funct3 == 0b011 || funct3 == 0b001) { //c.ld & c.fld
				long uimm_5_3 = (imm >> 3) & 0b111;

				long uimm_7_6 = (imm >> 6) & 0b11;

				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((rs1 & 1) << 7) | (uimm_7_6 << 5) | (rd << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_3 << 2) | (rs1 >> 1));
				out.write(bytes);

				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			}

		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
		return 0;
	}

	public int encodeTypeCI(String code, int lineNo, String section, Long address, int opcode, int rd, long imm, int funct3, boolean signed, boolean ignoreCheckRDEqualX0) {
		if ((funct3 == 0b010 || funct3 == 0b11) && rd == 0) {
			err = "rd cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (funct3 == 0b000 && code.contains("c.addi") && rd == 0) {
			err = "rd cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (funct3 == 0b001 && code.contains("c.addiw") && rd == 0) {
			err = "rd cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (funct3 == 0b010 && code.contains("c.li") && rd == 0) {
			err = "rd cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (funct3 == 0b011 && code.contains("c.lui") && (rd == 0 | rd == 2)) {
			err = "rd cannot be x0 or x2";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}
		if (signed == false && imm < 0) {
			err = "imm cannot be negative";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			if (opcode == 0b10) {
				if (funct3 == 0) {//c.slli
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_0_4 = ((imm) & 0b1111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_0_4 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;
				} else if (funct3 == 1) {//c.fldsp
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_4_3_8_6 = (imm & 0b11000) | ((imm >> 6) & 0b111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_4_3_8_6 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;
				} else if (funct3 == 2) {//c.lwsp
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_4_2_7_6 = (imm & 0b11100) | ((imm & 0b11000000) >> 6);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_4_2_7_6 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;
				} else {
					//c.flwsp | c.ldsp
					if (arch.equals("rv64")) { //c.ldsp
						long uimm_5 = (imm >> 5) & 0b1;
						long uimm_4_3_8_6 = (imm & 0b11000) | ((imm >> 6) & 0b111);
						byte bytes[] = new byte[2];
						bytes[0] = (byte) (((rd & 1) << 7) | (uimm_4_3_8_6 << 2) | opcode);
						bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
						out.write(bytes);
						listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					} else { //c.flwsp
						long uimm_5 = (imm >> 5) & 0b1;
						long uimm_4_2_7_6 = (imm & 0b11100) | ((imm & 0b11000000) >> 6);
						byte bytes[] = new byte[2];
						bytes[0] = (byte) (((rd & 1) << 7) | (uimm_4_2_7_6 << 2) | opcode);
						bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
						out.write(bytes);
						listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					}
					return 2;
				}
			} else if (opcode == 0b01) {
				if (funct3 == 0) { //c.nop ,c.addi

					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_0_4 = ((imm) & 0b1111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_0_4 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;

				} else if (funct3 == 0b001 && rd != 0) { //c.addiw
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_0_4 = ((imm) & 0b1111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_0_4 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;
				} else if (funct3 == 0b010 && rd != 0) { //c.li
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_0_4 = ((imm) & 0b1111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 1) << 7) | (uimm_0_4 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
					return 2;
				} else if (funct3 == 0b011) { //c.lui c.addi16sp
					if (rd == 2) {
						//c.addi16sp
						if (imm % 16 != 0) {
							err = "imm must be multiple of 16";
							MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
							return 0;
						}
						long uimm_5 = (imm >> 5) & 0b1;
						long uimm_4_6_8_7_5 = (imm & 0b10000) | ((imm & 0b1000000) >> 3) | ((imm & 0b100000000) >> 6) | ((imm & 0b10000000) >> 6) | ((imm & 0b100000) >> 5);

						byte bytes[] = new byte[2];
						bytes[0] = (byte) (((rd & 1) << 7) | (uimm_4_6_8_7_5 << 2) | opcode);
						bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 4) | (rd >> 1));
						out.write(bytes);

						listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
						return 2;
					} else {
						//c.lui
						long uimm_17 = (imm >> 17) & 0b1;
						long uimm_16_12 = ((imm << 12) & 0b11111000000000000) >> 12;
						byte bytes[] = new byte[2];
						bytes[0] = (byte) (((rd & 1) << 7) | (uimm_16_12 << 2) | opcode);
						bytes[1] = (byte) ((funct3 << 5) | (uimm_17 << 4) | (rd >> 1));
						out.write(bytes);
						listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
						return 2;
					}
				} else if (funct3 == 0b100) { //c.andi , c.srli, c.srai ****
					long uimm_5 = (imm >> 5) & 0b1;
					long uimm_0_4 = ((imm) & 0b1111);
					byte bytes[] = new byte[2];
					bytes[0] = (byte) (((rd & 0b1) << 7) | (uimm_0_4 << 2) | opcode);
					bytes[1] = (byte) ((funct3 << 5) | (uimm_5 << 2) | rd >> 1);
					out.write(bytes);
					listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				}

				System.out.println("code=" + code);
			}
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}

		return 0;
	}

	public int encodeTypeCS(String code, int lineNo, String section, Long address, int opcode, int rs1, int rs2, long imm, int funct3, boolean signed) {
		if (imm > 0b11111111) {
			err = "imm cannot large than 8 bits";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if ((imm & 0b11) != 0b00) {
			err = "imm last two bits must be zero";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (signed == false && imm < 0) {
			err = "imm cannot be negative";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			if (funct3 == 0b110) { //c.sw
				long uimm_5_3 = (imm >> 3) & 0b111;
				long uimm_2_6 = (((imm >> 2) & 1) << 1) | ((imm >> 6) & 1);
				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((rs1 & 1) << 7) | (uimm_2_6 << 5) | (rs2 << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_3 << 2) | (rs1 >> 1));
				out.write(bytes);
				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			} else if (funct3 == 0b111 || funct3 == 0b101) { //c.sd & c.fsd
				long uimm_5_3 = (imm >> 3) & 0b111;
				long uimm_7_6 = (imm >> 6) & 0b11;
				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((rs1 & 1) << 7) | (uimm_7_6 << 5) | (rs2 << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_3 << 2) | (rs1 >> 1));
				out.write(bytes);
				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			}
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
		return 0;
	}

	public int encodeTypeCSS(String code, int lineNo, String section, Long address, int opcode, int rs2, long imm, int funct3, boolean signed) {
		if (imm > 0b11111111) {
			err = "imm cannot large than 8 bits";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if ((imm & 0b11) != 0b00) {
			err = "imm last two bits must be zero";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (signed == false && imm < 0) {
			err = "imm cannot be negative";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			if (funct3 == 0b110) { //c.swsp
				long uimm_5_2_7_6 = (imm & 0b111100) | ((imm >> 6) & 0b11);
				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((uimm_5_2_7_6 & 1) << 7) | (rs2 << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_2_7_6 >> 1));
				out.write(bytes);
				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			} else if (funct3 == 0b101 || funct3 == 0b111) { //c.sdsp & c.fsdsp
				long uimm_5_3_8_6 = (imm & 0b111000) | ((imm >> 6) & 0b111);
				byte bytes[] = new byte[2];
				bytes[0] = (byte) (((uimm_5_3_8_6 & 1) << 7) | (rs2 << 2) | opcode);
				bytes[1] = (byte) ((funct3 << 5) | (uimm_5_3_8_6 >> 1));
				out.write(bytes);
				listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
				return 2;
			}

		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
		return 0;
	}

	public int encodeTypeCR(String code, int lineNo, String section, Long address, int opcode, int rd, int rs2, int funct4, boolean checkRS2IsX0) {
		if (checkRS2IsX0 && rs2 == 0) {
			err = "rs2 cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}
		try {
			byte bytes[] = new byte[2];
			bytes[0] = (byte) (((rd & 1) << 7) | (rs2 << 2) | opcode);
			bytes[1] = (byte) ((funct4 << 4) | (rd >> 1));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeCA(String code, int lineNo, String section, Long address, int opcode, int rd, int rs2, int funct6, int funct2, boolean checkRS2IsX0) {
		if (checkRS2IsX0 && rs2 == 0) {
			err = "rs2 cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}
		try {
			byte bytes[] = new byte[2];
			bytes[0] = (byte) (((rd & 1) << 7) | (funct2 << 5) | (rs2 << 2) | opcode);
			bytes[1] = (byte) ((funct6 << 2) | (rd >> 1));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeCIW(String code, int lineNo, String section, Long address, int opcode, int rs2, long imm, int funct3) {
		if (rs2 == 0) {
			err = "rs2 cannot be x0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (imm == 0) {
			err = "imm cannot be 0";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		} else if (imm < 0) {
			err = "imm cannot be negative";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			long uimm_5_4_9_6_2_3 = ((imm & 0b110000) << 2) | ((imm & 0b1111000000) >> 4) | ((imm & 0b100) >> 1) | ((imm & 0b1000) >> 3);
			byte bytes[] = new byte[2];
			bytes[0] = (byte) (((uimm_5_4_9_6_2_3 & 0b111) << 5) | (rs2 << 2) | opcode);
			bytes[1] = (byte) ((funct3 << 5) | ((uimm_5_4_9_6_2_3 >> 3) & 0b111111));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeCB(String code, int lineNo, String section, Long address, int opcode, int rs2, long imm, int funct3) {

		try {
			long imm_8_4_3 = AssemblerLib.getBitsDense(imm, new int[]{8, 4, 3});
			long imm_7_6_2_1_5 = AssemblerLib.getBitsDense(imm, new int[]{7, 6, 2, 1, 5});
			byte bytes[] = new byte[2];
			bytes[0] = (byte) (((rs2 & 0b1) << 7) | (imm_7_6_2_1_5 << 2) | opcode);
			bytes[1] = (byte) ((funct3 << 5) | (imm_8_4_3 << 2) | rs2 >> 1);
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeCJ(String code, int lineNo, String section, Long address, int opcode, long imm, int funct3) {
		try {
			long imm_11_4_9_8_10_6_7_3_1_5 = AssemblerLib.getBitsDense(imm, new int[]{11, 4, 9, 8, 10, 6, 7, 3, 2, 1, 5});

			byte bytes[] = new byte[2];
			bytes[0] = (byte) (((imm_11_4_9_8_10_6_7_3_1_5 & 0b111111) << 2) | opcode);
			bytes[1] = (byte) ((funct3 << 5) | ((imm_11_4_9_8_10_6_7_3_1_5 & 0b11111000000) >> 6));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

	public int encodeTypeCSR(String code, int lineNo, String section, Long address, int opcode, int rd, int funct3, int rs1, int csr) {
		int csr_value;
		if (csr == 0x1000) {
			csr_value = 0x000;
		} else {
			csr_value = csr;
		}

		if (csr == 0) {
			err = "Unknown CSR registers";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) ((rd << 7) | opcode);
			bytes[1] = (byte) (((rs1 & 0b1) << 7) | ((funct3 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) ((csr_value & 0x00f) << 4 | rs1 >> 1);
			bytes[3] = (byte) (csr_value >> 4);
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

	public int encodeTypeCSR(String code, int lineNo, String section, Long address, int opcode, int rd, int funct3, String imm, int csr) {
		int csr_value;
		if (csr == 0x1000) {
			csr_value = 0x000;
		} else {
			csr_value = csr;
		}

		int tmp = Integer.parseInt(imm);
		if (csr == 0) {
			err = "Unknown CSR registers";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}

		if (tmp > 31) {
			err = "Improper imm value";
			MessageHandler.errorPrintln("Encoder Error", err, lineNo, 0, 0, code);
			return 0;
		}
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) ((rd << 7) | opcode);
			bytes[1] = (byte) (((tmp & 0x1) << 7) | ((funct3 & 0xf) << 4) | (rd >> 1));
			bytes[2] = (byte) ((csr_value & 0x00f) << 4 | tmp >> 1);
			bytes[3] = (byte) (csr_value >> 4);
			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

	public int encodeTypePause(String code, int lineNo, String section, Long address, int opcode, int funct) {
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (opcode & 0x7f);
			bytes[1] = (byte) (0xf0 | (0xf & funct));
			bytes[2] = (byte) (0xa0);
			bytes[3] = (byte) (0xa0);
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

	public int encodeTypeCO(String code, int lineNo, String section, Long address, long imm) {
		long tmp1 = (imm >> 12 + imm >> 11);
		String imm1 = Long.toString(tmp1);
		int b = encodeTypeU(code, lineNo, section, address, 0b0010111, 1, imm1);
		long tmp2 = ((1 << 12) - 1 & 0);
		b += encodeTypeI(code, lineNo, section, address, 0b1100111, 0b000, 1, 1, tmp2);
		return b;
	}

	public int encodeTypeTO(String code, int lineNo, String section, Long address, long imm) {
		long tmp1 = (imm >> 12 + imm >> 11);
		String imm1 = Long.toString(tmp1);
		int b = encodeTypeU(code, lineNo, section, address, 0b0010111, 6, imm1);
		long tmp2 = ((1 << 12) - 1 & 0);
		b += encodeTypeI(code, lineNo, section, address, 0b1100111, 0b000, 0, 6, tmp2);
		return b;
	}

	public int encodeTypeR4(String code, int lineNo, String section, Long address, int opcode, int rm, int funct2, int frd, int frs1, int frs2, int frs3) {
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((frd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((frs1 & 0b1) << 7) | ((rm & 0xf) << 4) | (frd >> 1));
			bytes[2] = (byte) (((frs2 & 0b1111) << 4) | (frs1 >> 1));
			bytes[3] = (byte) ((funct2 << 1) | (frs3 << 3) | (frs2 >> 4));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

	public int encodeTypeFR(String code, int lineNo, String section, Long address, int opcode, int rm, int funct7, int frd, int frs1, int frs2) {
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((frd & 0b1) << 7) | opcode);
			bytes[1] = (byte) (((frs1 & 0b1) << 7) | ((rm & 0xf) << 4) | (frd >> 1));
			bytes[2] = (byte) (((frs2 & 0b1111) << 4) | (frs1 >> 1));
			bytes[3] = (byte) ((funct7 << 1) | (frs2 >> 4));
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 2;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeVArith(String code, int lineNo, String section, Long address, int operation, int funct6, int funct3, int vd, int vs1, int vs2, int rs1, int vm, long imm) {
		try {
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((vd & 0b1) << 7) | operation);
			if (funct3 >= 0b000 && funct3 <= 0b010 /*vector-vector*/) {
				bytes[1] = (byte) (((vs1 & 0b11) << 7) | (funct3 << 4) | (vd >> 1));
				bytes[2] = (byte) (((vs2 & 0xf) << 4 | (vs1 >> 1)));
				bytes[3] = (byte) ((funct6 << 2) | vm << 1 | vs2 >> 4);
			} else if (funct3 == 0b011 /*vector-immediate*/) {
				bytes[1] = (byte) (((imm & 0b11) << 7 | (funct3 << 4) | (vd >> 1)));
				bytes[2] = (byte) (((vs2 & 0xf) << 4 | ((imm & 0x1f) >> 1)));
				bytes[3] = (byte) ((funct6 << 2) | vm << 1 | vs2 >> 4);
			} else if (funct3 >= 0b100 && funct3 <= 0b110 /*vector-scalar*/) {
				bytes[1] = (byte) (((rs1 & 0b1) << 7) | (funct3 << 4) | (vd >> 1));
				bytes[2] = (byte) (((vs2 & 0xf) << 4 | (rs1 >> 1)));
				bytes[3] = (byte) ((funct6 << 2) | vm << 1 | vs2 >> 4);
			} else if (funct3 == 0b111 /*scalars--immediates*/) {
			}
			out.write(bytes);
			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeVConfig(String code, int lineNo, String section, Long address, int operation, int xlenminone, int xlenmintwo, int vma, int vta, int vsew, int vlmul, int rs2, int rs1, int rd, int vm, long imm) {
		try {
			//has not implemented constraints for whether vl, avl, or vtype are legal value
			//very specifically for a vset{i}vli rd, rs1, e*, m*, t*, m* format. Pending: changed emtm order, implicit defaults for emtm
			byte bytes[] = new byte[4];
			bytes[0] = (byte) (((rd & 0b1) << 7) | operation);

			if (xlenminone == 0) {
				//vsetvli
				//MessageHandler.println(" xlenminone: " + xlenminone + " xlenmintwo: " + xlenmintwo + " vma: " + vma + " vta: " + vta + " vsew: " + vsew + " vlmul: " + vlmul);
				if (vma == 24) {
					vma = 0b1;
				} else if (vma == 25) {
					vma = 0b0;
				}
				if (vta == 22) {
					vta = 0b1;
				} else if (vta == 23) {
					vta = 0b0;
				}
				if (vsew == 11) {
					vsew = 0b000;
				} else if (vsew == 12) {
					vsew = 0b001;
				} else if (vsew == 13) {
					vsew = 0b010;
				} else if (vsew == 14) {
					vsew = 0b011;
				}
				if (vlmul == 15) {
					vlmul = 0b101;
				} else if (vlmul == 16) {
					vlmul = 0b110;
				} else if (vlmul == 17) {
					vlmul = 0b111;
				} else if (vlmul == 18) {
					vlmul = 0b000;
				} else if (vlmul == 19) {
					vlmul = 0b001;
				} else if (vlmul == 20) {
					vlmul = 0b010;
				} else if (vlmul == 21) {
					vlmul = 0b011;
				}
				//MessageHandler.println(" xlenminone: " + xlenminone + " xlenmintwo: " + xlenmintwo + " vma: " + vma + " vta: " + vta + " vsew: " + vsew + " vlmul: " + vlmul);
				bytes[1] = (byte) ((rs1 & 0b1) << 7 | 0b111 << 4 | (rd & 0b11110) >> 1);
				bytes[2] = (byte) ((vsew & 0b1) << 7 | vlmul << 4 | (rs1 & 0b11110) >> 1);
				bytes[3] = (byte) (xlenminone << 7 | vma << 3 | vta << 2 | (vsew & 0b110) >> 1);
			} else if (xlenminone == 1 && xlenmintwo == 1) {
				//vsetivli
				//MessageHandler.println(" xlenminone: " + xlenminone + " xlenmintwo: " + xlenmintwo + " vma: " + vma + " vta: " + vta + " vsew: " + vsew + " vlmul: " + vlmul);
				if (vma == 24) {
					vma = 0b1;
				} else if (vma == 25) {
					vma = 0b0;
				}
				if (vta == 22) {
					vta = 0b1;
				} else if (vta == 23) {
					vta = 0b0;
				}
				if (vsew == 11) {
					vsew = 0b000;
				} else if (vsew == 12) {
					vsew = 0b001;
				} else if (vsew == 13) {
					vsew = 0b010;
				} else if (vsew == 14) {
					vsew = 0b011;
				}
				if (vlmul == 15) {
					vlmul = 0b101;
				} else if (vlmul == 16) {
					vlmul = 0b110;
				} else if (vlmul == 17) {
					vlmul = 0b111;
				} else if (vlmul == 18) {
					vlmul = 0b000;
				} else if (vlmul == 19) {
					vlmul = 0b001;
				} else if (vlmul == 20) {
					vlmul = 0b010;
				} else if (vlmul == 21) {
					vlmul = 0b011;
				}
				//MessageHandler.println(" xlenminone: " + xlenminone + " xlenmintwo: " + xlenmintwo + " vma: " + vma + " vta: " + vta + " vsew: " + vsew + " vlmul: " + vlmul);
				bytes[1] = (byte) ((imm & 0b1) << 7 | 0b111 << 4 | (rd & 0b11110) >> 1);
				bytes[2] = (byte) ((vsew & 0b1) << 7 | vlmul << 4 | (imm & 0b11110) >> 1);
				bytes[3] = (byte) (xlenminone << 7 | xlenmintwo << 6 | vma << 3 | vta << 2 | (vsew & 0b110) >> 1);
			} else if (xlenminone == 1 && xlenmintwo == 0) {
				//vsetvl
				bytes[1] = (byte) ((rs1 & 0b1) << 7 | 0b111 << 4 | (rd & 0b11110) >> 1);
				bytes[2] = (byte) ((rs2 & 0b01111) << 4 | (rs1 & 0b11110) >> 1);
				bytes[3] = (byte) (xlenminone << 7 | 0b000000 << 1 | (rs2 & 0b10000) >> 4);
			}

			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int encodeTypeVLoadStore(String code, int lineNo, String section, Long address, int operation, int rs1, int rs2, int vs2, int vs3, int vd, int vm, int width, int mew, int mop, int nf, int lumop, int sumop, long imm) {
		try {
			byte bytes[] = new byte[4];

			if (operation == 0b0000111) {
				//load-fp
				bytes[0] = (byte) (((vd & 0b10000) << 7) | operation);
				bytes[1] = (byte) (((rs1 & 0b1) << 7) | (width << 4) | ((vd & 0b11110) >> 1));
				if (mop == 0b00) {
					//unit stride, VLE<EEW>
					bytes[2] = (byte) (((lumop & 0b01111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((lumop & 0b00001) >> 4));
				} else if (mop == 0b01) {
					//index-unordered, VLUXEI<EEW>
					bytes[2] = (byte) (((vs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((vs2 & 0b1) >> 4));
				} else if (mop == 0b10) {
					//strided, VLSE<EEW>
					bytes[2] = (byte) (((rs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((rs2 & 0b1) >> 4));
				} else if (mop == 0b11) {
					//index-ordered, VLOXEI
					bytes[2] = (byte) (((vs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((vs2 & 0b1) >> 4));
				}
			} else if (operation == 0b0100111) {
				//store-fp
				MessageHandler.println(Integer.toString(vs3));
				bytes[0] = (byte) (((vs3 & 0b00001) << 7) | operation);
				bytes[1] = (byte) (((rs1 & 0b1) << 7) | (width << 4) | ((vs3 & 0b11110) >> 1));
				if (mop == 0b00) {
					//unit stride, VSE<EEW>
//					MessageHandler.println("ok");
					bytes[2] = (byte) (((sumop & 0b01111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((sumop & 0b00001) >> 4));
				} else if (mop == 0b01) {
					//index-unordered, VSUXEI<EEW>
					bytes[2] = (byte) (((vs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((vs2 & 0b1) >> 4));
				} else if (mop == 0b10) {
					//strided, VSSE<EEW>
					bytes[2] = (byte) (((rs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((rs2 & 0b1) >> 4));
				} else if (mop == 0b11) {
					//index-ordered, VSOXEI
					bytes[2] = (byte) (((vs2 & 0b1111) << 4) | ((rs1 & 0b11110) >> 1));
					bytes[3] = (byte) ((nf << 5) | (mew << 4) | (mop << 2) | (vm << 1) | ((vs2 & 0b1) >> 4));
				}
			}

			out.write(bytes);

			listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
			return 4;
		} catch (Exception ex) {
			int[] positions = RISCVEncoderErrorLib.getPosition(lineNo, "all");
			MessageHandler.errorPrintln("Encoder Error", ex.toString(), lineNo, positions[0], positions[1], code);
			return 0;
		}
	}

	public int ejectByte(String code, int lineNo, String section, Long address, String expression, String dollar, List<Token> list) {
		int intAddress = address.intValue();
		byte bytes[] = new byte[list.size()];
		for (int j = 0; j < bytes.length; j++) {
			bytes[j] = (byte) (CalculatorLibrary.calLong(list.get(j).getText()));
		}

		listing.lines.add(new Line(lineNo, address, bytes, section, code, null));
		return (expression == null) ? list.size() : ((dollar == null) ? Integer.parseInt(expression) * list.size() : (Integer.parseInt(expression) - intAddress) * list.size());
	}

	public int ejectHalfByte(String code, int lineNo, String section, Long address, String expression, String dollar, List<Token> list) {
//		
		int intAddress = address.intValue();
		for (Token token : list) {
			MessageHandler.println(token.getText());
		}
//		MessageHandler.println("no. of number: "+list.size());
//		MessageHandler.println("---------------------");
//	

		byte bytes2[] = new byte[list.size() * 2];
		for (int j = 0; j < bytes2.length; j += 2) {
			long number = CalculatorLibrary.calLong(list.get(j / 2).getText());
			bytes2[j] = (byte) (number);
			bytes2[j + 1] = (byte) (number >> 8);

		}
		listing.lines.add(new Line(lineNo, address, bytes2, section, code, null));
		return (expression == null) ? list.size() * 2 : ((dollar == null) ? Integer.parseInt(expression) * list.size() * 2 : (Integer.parseInt(expression) - intAddress) * list.size() * 2);
	}

	public int ejectWordByte(String code, int lineNo, String section, Long address, String expression, String dollar, List<Token> list) {
//		
		int intAddress = address.intValue();
		for (Token token : list) {
			MessageHandler.println(token.getText());
		}
//		MessageHandler.println("no. of number: "+list.size());
//		MessageHandler.println("---------------------");
//	

		byte bytes2[] = new byte[list.size() * 4];
		for (int j = 0; j < bytes2.length; j += 4) {
			long number = CalculatorLibrary.calLong(list.get(j / 4).getText());
			for (int a = 0, b = 0; a < 4; a++, b += 8) {
				bytes2[j + a] = (byte) (number >> b);

			}
		}
		listing.lines.add(new Line(lineNo, address, bytes2, section, code, null));
		return (expression == null) ? list.size() * 4 : ((dollar == null) ? Integer.parseInt(expression) * list.size() * 4 : (Integer.parseInt(expression) - intAddress) * list.size() * 4);
	}

	public int ejectDwordByte(String code, int lineNo, String section, Long address, String expression, String dollar, List<Token> list) {
//		
		int intAddress = address.intValue();
		for (Token token : list) {
			MessageHandler.println(token.getText());
		}
//		MessageHandler.println("no. of number: "+list.size());
//		MessageHandler.println("---------------------");
//	

		byte bytes2[] = new byte[list.size() * 8];
		for (int j = 0; j < bytes2.length; j += 8) {
			long number = CalculatorLibrary.calLong(list.get(j / 8).getText());
			for (int a = 0, b = 0; a < 8; a++, b += 8) {
				bytes2[j + a] = (byte) (number >> b);

			}
		}
		listing.lines.add(new Line(lineNo, address, bytes2, section, code, null));
		return (expression == null) ? list.size() * 8 : ((dollar == null) ? Integer.parseInt(expression) * list.size() * 8 : (Integer.parseInt(expression) - intAddress) * list.size() * 8);
	}

	public int ejectString(String code, int lineNo, String section, String expression, String dollar, Long address) {
		int intAddress = address.intValue();
		String str = code.substring(8);
//		System.out.println(str);

		try {
			Long l = address;

			int[] b = new int[str.length()];
			for (int m = 0; m < str.length(); m++) {
				b[m] = str.charAt(m);
			}
			int x = 0;
			byte[] c = new byte[str.length() > 4 ? 4 : str.length()];
			for (int m = 0; m < str.length(); m++) {

				c[x] = (byte) b[m];
//				System.out.println(c[x]);
				x++;
				if (x == 4) {
//					System.out.println("hi shit" + c[0] + c[1] + c[2] + c[3]);
//					out.write(c);
					listing.lines.add(new Line(lineNo, l, c, section, code, null));
					l += 4;
					x = 0;
					c = new byte[4];
				}
			}
			if (x != 0) {
				byte[] d = new byte[x];
				System.arraycopy(c, 0, d, 0, x);
//				out.write(d);
				listing.lines.add(new Line(lineNo, l, d, section, code, null));
			}

			return (expression == null) ? str.length() : ((dollar == null) ? Integer.parseInt(expression) * str.length() : (Integer.parseInt(expression) - intAddress) * str.length());

		} catch (Exception ex) {
			MessageHandler.errorPrintln(ex);
			return 0;
		}
	}

//	public int times(String code, int lineNo, String section, Long address, int i, List<Token> list, int type, String expression) {
//		int returnAddress = 0;
//		String stringAddress = String.valueOf(address);
//		expression = expression.replaceAll("(?<!\\S)$(?<!\\S)", stringAddress);
//		ScriptEngineManager mgr = new ScriptEngineManager();
//		ScriptEngine engine = mgr.getEngineByName("Java");
//		int time = Integer.parseInt(expression);
//		switch (type) {
//			case 0:
//				for (int j = 0; j < time; j++) {
//					returnAddress += ejectByte(code, lineNo, section, address, i, list);
//				}
//			case 1:
//				for (int j = 0; j < time; j++) {
//					returnAddress += ejectHalfByte(code, lineNo, section, address, i, list);
//				}
//			case 2:
//				for (int j = 0; j < time; j++) {
//					returnAddress += ejectWordByte(code, lineNo, section, address, i, list);
//				}
//			case 3:
//				for (int j = 0; j < time; j++) {
//					returnAddress += ejectDwordByte(code, lineNo, section, address, i, list);
//				}
//			case 4:
//				for (int j = 0; j < time; j++) {
//					returnAddress += ejectString(code, lineNo, section, address);
//				}
//		}
//		return returnAddress;
//	}
}
