package hk.quantr.assembler.riscv;

import hk.quantr.assembler.Assembler;
import org.antlr.v4.runtime.Token;

public class RISCVEncoderErrorLib {

	public static int[] getPosition(int line, String type) { //type can be imm, all, add any other token trace if u like
		int[] position = {0, 0};
		Token current = null;
		if (type.equals("all")) {
			Assembler.allTokens.add(null);
			for (Token next : Assembler.allTokens) {
				if (current == null) {
					current = next;
					continue;
				}
				if (current.getLine() == line && position[0] == 0) { //will enter condition on the first token on correct line
					position[0] = current.getStartIndex();
				}
				if (next == null || next.getLine() > line || (position[0] != 0 && next.getType() == 3)) { //will enter condition if next token is on next line
					position[1] = current.getStopIndex();
					break;
				}
				if (next.getChannel() != 1) {
					current = next;
				}
			}
			Assembler.allTokens.remove(Assembler.allTokens.size()-1);
		} else if (type.equals("imm")) {
			for (Token t : Assembler.allTokens) {
//				System.out.println("line: " + t.getLine() + ", type: " + t.getType());
				if (t.getLine() == line && t.getType() == 23) {
					position[0] = t.getStartIndex();
					position[1] = t.getStopIndex();
					break;
				}
			}
		} else if (type.equals("registers")) {
			for (Token t : Assembler.allTokens) {
//				System.out.println("line: " + t.getLine() + ", type: " + t.getType());
				if (t.getLine() == line && (t.getType() >= 33 && t.getType() <= 442)) {
					position[0] = t.getStartIndex();
					position[1] = t.getStopIndex();
					break;
				}
			}
		}
		return position;
	}
}
