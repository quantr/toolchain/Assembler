package hk.quantr.assembler.riscv.il;

import java.util.HashMap;

public final class Registers {

	public static final HashMap<Integer, String> regXMap32 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> regRm = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> regFMap32 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> regXMap16 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> regFMap16 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> csrMap12 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> vregMap32 = new HashMap<Integer, String>();
	public static final HashMap<Integer, String> vcsrMap32 = new HashMap<Integer, String>();

	static {
		// initialize the 16-bit integer register map
		regXMap16.put(0, "s0");
		regXMap16.put(1, "s1");
		regXMap16.put(2, "a0");
		regXMap16.put(3, "a1");
		regXMap16.put(4, "a2");
		regXMap16.put(5, "a3");
		regXMap16.put(6, "a4");
		regXMap16.put(7, "a5");

		// initialize the 16-bit floating point register map
		regFMap16.put(0, "fs0");
		regFMap16.put(1, "fs1");
		regFMap16.put(2, "fa0");
		regFMap16.put(3, "fa1");
		regFMap16.put(4, "fa2");
		regFMap16.put(5, "fa3");
		regFMap16.put(6, "fa4");
		regFMap16.put(7, "fa5");

		// initialize the 32-bit x  register map
		regXMap32.put(0, "zero");
		regXMap32.put(1, "ra");
		regXMap32.put(2, "sp");
		regXMap32.put(3, "gp");
		regXMap32.put(4, "tp");
		regXMap32.put(5, "t0");
		regXMap32.put(6, "t1");
		regXMap32.put(7, "t2");
		regXMap32.put(8, "s0");
		regXMap32.put(9, "s1");
		regXMap32.put(10, "a0");
		regXMap32.put(11, "a1");
		regXMap32.put(12, "a2");
		regXMap32.put(13, "a3");
		regXMap32.put(14, "a4");
		regXMap32.put(15, "a5");
		regXMap32.put(16, "a6");
		regXMap32.put(17, "a7");
		regXMap32.put(18, "s2");
		regXMap32.put(19, "s3");
		regXMap32.put(20, "s4");
		regXMap32.put(21, "s5");
		regXMap32.put(22, "s6");
		regXMap32.put(23, "s7");
		regXMap32.put(24, "s8");
		regXMap32.put(25, "s9");
		regXMap32.put(26, "s10");
		regXMap32.put(27, "s11");
		regXMap32.put(28, "t3");
		regXMap32.put(29, "t4");
		regXMap32.put(30, "t5");
		regXMap32.put(31, "t6");

		//initialize the rm register map
		regRm.put(0, "rne");
		regRm.put(1, "rtz");
		regRm.put(2, "rdn");
		regRm.put(3, "rup");
		regRm.put(4, "rmm");
		regRm.put(7, "dyn");

		//initialize the 32-bit f  register map
		regFMap32.put(0, "ft0");
		regFMap32.put(1, "ft1");
		regFMap32.put(2, "ft2");
		regFMap32.put(3, "ft3");
		regFMap32.put(4, "ft4");
		regFMap32.put(5, "ft5");
		regFMap32.put(6, "ft6");
		regFMap32.put(7, "ft7");

		regFMap32.put(8, "fs0");
		regFMap32.put(9, "fs1");

		regFMap32.put(10, "fa0");
		regFMap32.put(11, "fa1");

		regFMap32.put(12, "fa2");
		regFMap32.put(13, "fa3");
		regFMap32.put(14, "fa4");
		regFMap32.put(15, "fa5");
		regFMap32.put(16, "fa6");
		regFMap32.put(17, "fa7");

		regFMap32.put(18, "fs2");
		regFMap32.put(19, "fs3");
		regFMap32.put(20, "fs4");
		regFMap32.put(21, "fs5");
		regFMap32.put(22, "fs6");
		regFMap32.put(23, "fs7");
		regFMap32.put(24, "fs8");
		regFMap32.put(25, "fs9");
		regFMap32.put(26, "fs10");
		regFMap32.put(27, "fs11");

		regFMap32.put(28, "ft8");
		regFMap32.put(29, "ft9");
		regFMap32.put(30, "ft10");
		regFMap32.put(31, "ft11");

		//https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html
		// initialize the 12-bit control and status register map
		// user trap setup
		csrMap12.put(0x000, "ustatus");
		csrMap12.put(0x004, "uie");
		csrMap12.put(0x005, "utvec");

		//User Trap Handling
		csrMap12.put(0x040, "uscratch");
		csrMap12.put(0x041, "uepc");
		csrMap12.put(0x042, "ucause");
		csrMap12.put(0x043, "ubadaddr");
		csrMap12.put(0x044, "uip");

		//User Floating-Point CSRs
		csrMap12.put(0x001, "fflags");
		csrMap12.put(0x002, "frm");
		csrMap12.put(0x003, "fcsr");

		//User Counter/Timers
		csrMap12.put(0xC00, "cycle");
		csrMap12.put(0xC01, "time");
		csrMap12.put(0xC02, "instret");
		for (int i = 3, j = 0xc03; i < 32; i++, j++) {
			csrMap12.put(j, "hpmcounter" + i);
		}
		csrMap12.put(0xC80, "cycleh");
		csrMap12.put(0xC81, "timeh");
		csrMap12.put(0xC82, "instreth");
		for (int i = 3, j = 0xc83; i < 32; i++, j++) {
			csrMap12.put(j, "hpmcounter" + i + "h");
		}

		//Supervisor trap setup
		csrMap12.put(0x100, "sstatus");
		csrMap12.put(0x102, "sedeleg");
		csrMap12.put(0x103, "sideleg");
		csrMap12.put(0x104, "sie");
		csrMap12.put(0x105, "stvec");
		csrMap12.put(0x106, "scounteren");

		//Supervisor Trap Handling
		csrMap12.put(0x140, "sscratch");
		csrMap12.put(0x141, "sepc");
		csrMap12.put(0x142, "scause");
		csrMap12.put(0x143, "stval");
		csrMap12.put(0x144, "sip");

		//Supervisor Protection and Translation
		csrMap12.put(0x180, "satp");

		//Debug/Trace Registers
		csrMap12.put(0x5A8, "scontext");

		//Hypervisor Trap Setup
		csrMap12.put(0x600, "hstatus");
		csrMap12.put(0x602, "hedeleg");
		csrMap12.put(0x603, "hideleg");
		csrMap12.put(0x604, "hie");
		csrMap12.put(0x606, "hcounteren");
		csrMap12.put(0x607, "hgeie");

		//Hypervisor Trap Handling
		csrMap12.put(0x643, "htval");
		csrMap12.put(0x644, "hip");
		csrMap12.put(0x645, "hvip");
		csrMap12.put(0x64A, "htinst");
		csrMap12.put(0xE12, "hgeip");

		//Hypervisor Configuration
		csrMap12.put(0x60A, "henvcfg");
		csrMap12.put(0x61A, "henvcfgh");

		//Hypervisor Protection and Translation
		csrMap12.put(0x680, "hgatp");

		//Debug/Trace Registers
		csrMap12.put(0x6A8, "hcontext");

		//Hypervisor Counter/Timer Virtualization Registers
		csrMap12.put(0x605, "htimedelta");
		csrMap12.put(0x615, "htimedeltah");

		//Virtual Supervisor Registers
		csrMap12.put(0x200, "vsstatus");
		csrMap12.put(0x204, "vsie");
		csrMap12.put(0x205, "vstvec");
		csrMap12.put(0x240, "vsscratch");
		csrMap12.put(0x241, "vsepc");
		csrMap12.put(0x242, "vscause");
		csrMap12.put(0x243, "vstval");
		csrMap12.put(0x244, "vsip");
		csrMap12.put(0x280, "vsatp");

		//Machine Information Registers
		csrMap12.put(0xF11, "mvendorid");
		csrMap12.put(0xF12, "marchid");
		csrMap12.put(0xF13, "mimpid");
		csrMap12.put(0xF14, "mhartid");
		csrMap12.put(0xF15, "mconfigptr");

		//Machine Trap Setup
		csrMap12.put(0x300, "mstatus");
		csrMap12.put(0x301, "misa");
		csrMap12.put(0x302, "medeleg");
		csrMap12.put(0x303, "mideleg");
		csrMap12.put(0x304, "mie");
		csrMap12.put(0x305, "mtvec");
		csrMap12.put(0x306, "mcounteren");
		csrMap12.put(0x310, "mstatush");

		//Machine Trap Handling
		csrMap12.put(0x340, "mscratch");
		csrMap12.put(0x341, "mepc");
		csrMap12.put(0x342, "mcause");
		csrMap12.put(0x343, "mtval");
		csrMap12.put(0x344, "mip");
		csrMap12.put(0x34A, "mtinst");
		csrMap12.put(0x34B, "mtval2");

		//Machine Configuration
		csrMap12.put(0x30A, "menvcfg");
		csrMap12.put(0x31A, "menvcfgh");
		csrMap12.put(0x747, "mseccfg");
		csrMap12.put(0x757, "mseccfgh");

		//Machine Memory Protection
		for (int i = 0, j = 0x3a0; i < 16; i++, j++) {
			csrMap12.put(j, "pmpcfg" + i);
		}
		for (int i = 0, j = 0x3b0; i < 64; i++, j++) {
			csrMap12.put(j, "pmpaddr" + i);
		}

		//Machine Counter/Timers
		csrMap12.put(0xB00, "mcycle");
		csrMap12.put(0xB02, "minstret");
		for (int i = 3, j = 0xb03; i < 32; i++, j++) {
			csrMap12.put(j, "mhpmcounter" + i);
		}
		csrMap12.put(0xB80, "mcycleh");
		csrMap12.put(0xB82, "minstreth");
		for (int i = 3, j = 0xB83; i < 32; i++, j++) {
			csrMap12.put(j, "mhpmcounter" + i + "h");
		}
		//Machine Counter Setup
		csrMap12.put(0x320, "mcountinhibit");
		for (int i = 3, j = 0x323; i < 32; i++, j++) {
			csrMap12.put(j, "mhpmevent" + i);
		}

		csrMap12.put(0x740, "mnscratch");
		csrMap12.put(0x741, "mnepc");
		csrMap12.put(0x742, "mncause");
		csrMap12.put(0x744, "mnstatus");

		//Debug/Trace Registers (shared with Debug Mode)
		csrMap12.put(0x7A0, "tselect");
		csrMap12.put(0x7A1, "tdata1");
		csrMap12.put(0x7A2, "tdata2");
		csrMap12.put(0x7A3, "tdata3");
		csrMap12.put(0x7A8, "mcontext");

		//Debug Mode Registers
		csrMap12.put(0x7B0, "dcsr");
		csrMap12.put(0x7B1, "dpc");
		csrMap12.put(0x7B2, "dscratch0");
		csrMap12.put(0x7B3, "dscratch1");

		//Vector Registers
		for (int i = 0; i < 32; i++) {
			vregMap32.put(i, "v" + i);
		}

		//VCSR Registers
		vcsrMap32.put(0, "vtype");
		vcsrMap32.put(1, "vsew");
		vcsrMap32.put(2, "vlmul");
		vcsrMap32.put(3, "vta");
		vcsrMap32.put(4, "vma");
		vcsrMap32.put(5, "vl");
		vcsrMap32.put(6, "vlemb");
		vcsrMap32.put(7, "vstart");
		vcsrMap32.put(8, "vxrm");
		vcsrMap32.put(9, "vxsat");
		vcsrMap32.put(10, "vcsr");
	}

	public static String getRegFNum16(int i) {
		String reg = regFMap16.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return regXMap16.get(i);
	}

	public static String getRegXNum16(int i) {
		String reg = regXMap16.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return regXMap16.get(i);
	}

	public static String getRegXNum32(int i) {
		String reg = regXMap32.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return reg;
	}

	public static String getRegRm(int i) {
		String reg = regRm.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return reg;
	}

	public static String getRegFNum32(int i) {
		String reg = regFMap32.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return reg;
	}

	public static String getCsrNum12(int i) {
		String reg = csrMap12.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return reg;
	}

	public static String getVregNum32(int i) {
		String reg = vregMap32.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return vregMap32.get(i);
	}

	public static String getVcsrNum32(int i) {
		String reg = vcsrMap32.get(i);
		if (reg == null) {
			return "0x" + Integer.toHexString(i);
		}
		return vcsrMap32.get(i);
	}
}
