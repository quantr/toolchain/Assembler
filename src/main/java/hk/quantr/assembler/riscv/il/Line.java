package hk.quantr.assembler.riscv.il;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.dwarf.elf.Elf_Sym;
import hk.quantr.javalib.CommonLib;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Line {

	public String type;
	public int lineNo;
	public long offset;
	public String sourceFile;
	public int[] bytes;
	public String code;
//	public String label;
	public RISCVDisassembler.InstructionType instructionType;
	public int rs1;
	public int rs2;
	public int frd;
	public int frs1;
	public int frs2;
	public int frs3;
	public int rd;
	public int csr;
	public long imm;
	public Elf_Sym symbol;
    public int vd;
    public int vs1;
    public int vs2;
    public int vs3;
    public int vlmul;
    public int vsew;
    public int vta;
    public int vma;

	@Override
	public String toString() {
		String s = "";
		if (symbol != null) {
			s = String.format("%8x <%s>:\n", symbol.getST_value(), symbol.getName());
		}
		return s + String.format("%s:%d:%8x\t[%-19s]\t%-19s\t%s", sourceFile, lineNo, offset, CommonLib.arrayToHexString(bytes), instructionType, code);
	}
}
