package hk.quantr.assembler.riscv.il;

import java.util.ArrayList;

/**
 * @author Peter (peter@quantr.hk)
 */
public class DisasmStructure {

	public ArrayList<Line> lines = new ArrayList<>();

	public Line findLine(long baseOffset, long address) {
		for (Line line : lines) {
			if (baseOffset + line.offset == address || line.offset == address) {
				return line;
			}
		}
		return null;
	}

	public void addAll(DisasmStructure structure) {
		lines.addAll(structure.lines);
	}

}
