/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>, Quantr License
 *
 */
package hk.quantr.assembler.riscv;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.ia32.UnderlineListener;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class TestUtil {

	public static final Logger logger = Logger.getLogger(TestUtil.class.getName());
	static Runtime rt = Runtime.getRuntime();
	static final HashMap<String, ArrayList<String>> translatedMap = new HashMap<String, ArrayList<String>>();
//	static final HashMap<String, ArrayList<String>> translatedMapGas = new HashMap<String, ArrayList<String>>();

	static {
//		translatedMap.put("${rd}", concatArraysToList(new String[]{"x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30", "f31", "zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0", "fp", "s1", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6", "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7", "fs0", "fs1", "fa0", "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7", "fs2", "fs3", "fs4", "fs5", "fs6", "fs7", "fs8", "fs9", "fs10", "fs11", "ft8", "ft9", "ft10", "ft11"}));
//		translatedMap.put("${rs1}", concatArraysToList(new String[]{"x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30", "f31", "zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0", "fp", "s1", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6", "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7", "fs0", "fs1", "fa0", "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7", "fs2", "fs3", "fs4", "fs5", "fs6", "fs7", "fs8", "fs9", "fs10", "fs11", "ft8", "ft9", "ft10", "ft11"}));
//		translatedMap.put("${rs2}", concatArraysToList(new String[]{"x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30", "f31", "zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0", "fp", "s1", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6", "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7", "fs0", "fs1", "fa0", "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7", "fs2", "fs3", "fs4", "fs5", "fs6", "fs7", "fs8", "fs9", "fs10", "fs11", "ft8", "ft9", "ft10", "ft11"}));
		translatedMap.put("${rd}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMap.put("${rm}", concatArraysToList(new String[]{"dyn", "rne", "rdn"}));
		translatedMap.put("${rs1}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMap.put("${rs2}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMap.put("${rt}", concatArraysToList(new String[]{"x0", "x1", "x2"}));
		translatedMap.put("${shamt}", concatArraysToList(new String[]{"0x00", "0x1f", "0x3f", "0x1234", "0xffff", "0x12345678"}));
		translatedMap.put("${imm}", concatArraysToList(new String[]{"0x00", "0x20", "0x1f", "0x3f", "0x20", "0x40", "0x1234", "0xffff", "0x12345678"}));
		translatedMap.put("${offset}", concatArraysToList(new String[]{"0x00", "0x10", "0x20", "0x1f", "0x3f", "0x4f", "0x80", "0xff", "0x1ff", "0xfffe", "0xffff", "0x1fffe", "0x1ffff", "0x1ffffe", "0x1fffff", "0x1fffffe", "0x1ffffff", "0x1ffffffe", "0x1fffffff", "0xfffffff"}));
		translatedMap.put("${label}", concatArraysToList(new String[]{"0x00", "0x10", "0x20", "0x1f", "0x3f", "0x4f", "0x80", "0xff", "0x1ff", "0xfffe", "0xffff", "0x1fffe", "0x1ffff", "0x1ffffe", "0x1fffff", "0x1fffffe", "0x1ffffff", "0x1ffffffe", "0x1fffffff", "0xfffffff"}));
		translatedMap.put("${rwio}", concatArraysToList(new String[]{"r", "w", "rw", "i", "o", "io", "ior", "iow", "iorw"}));
		translatedMap.put("${rvc}", concatArraysToList(new String[]{"x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "s0", "s1", "a0", "a1", "a2", "a3", "a4", "a5"}));
		translatedMap.put("${frd}", concatArraysToList(new String[]{"f1", "ft1", "f2", "ft2"}));
		translatedMap.put("${frs1}", concatArraysToList(new String[]{"f1", "ft1", "f2", "ft2"}));
		translatedMap.put("${frs2}", concatArraysToList(new String[]{"f1", "ft1", "f2", "ft2"}));
		translatedMap.put("${frs3}", concatArraysToList(new String[]{"f1", "ft1", "f2", "ft2"}));
		translatedMap.put("${csr}", concatArraysToList(new String[]{"ustatus",
			"fflags",
			"frm",
			"fcsr",
			"uie",
			"utvec",
			"mstatus",
			"misa",
			"mie",
			"mtvec",
			"mscratch",
			"mepc",
			"mcause",
			"mtrval",
			"mip",
			"mcycle",
			"mcycleh",
			"minstret",
			"minstreth",
			"mvendorid",
			"marchid",
			"mimpid",
			"mhartid",
			"uscratch",
			"uepc",
			"ucause",
			"uscratch",
			"uip",
			"cycle",
			"time",
			"instret",
			"hpmcounter3",
			"hpmcounter4",
			"hpmcounter5",
			"hpmcounter6",
			"hpmcounter7",
			"hpmcounter8",
			"hpmcounter9",
			"hpmcounter10",
			"hpmcounter11",
			"hpmcounter12",
			"hpmcounter13",
			"hpmcounter14",
			"hpmcounter15",
			"hpmcounter16",
			"hpmcounter17",
			"hpmcounter18",
			"hpmcounter19",
			"hpmcounter20",
			"hpmcounter21",
			"hpmcounter22",
			"hpmcounter23",
			"hpmcounter24",
			"hpmcounter25",
			"hpmcounter26",
			"hpmcounter27",
			"hpmcounter28",
			"hpmcounter29",
			"hpmcounter30",
			"hpmcounter31",
			"cycleh",
			"timeh",
			"instreth",
			"hpmcounter3h",
			"hpmcounter4h",
			"hpmcounter5h",
			"hpmcounter6h",
			"hpmcounter7h",
			"hpmcounter8h",
			"hpmcounter9h",
			"hpmcounter10h",
			"hpmcounter11h",
			"hpmcounter12h",
			"hpmcounter13h",
			"hpmcounter14h",
			"hpmcounter15h",
			"hpmcounter16h",
			"hpmcounter17h",
			"hpmcounter18h",
			"hpmcounter19h",
			"hpmcounter20h",
			"hpmcounter21h",
			"hpmcounter22h",
			"hpmcounter23h",
			"hpmcounter24h",
			"hpmcounter25h",
			"hpmcounter26h",
			"hpmcounter27h",
			"hpmcounter28h",
			"hpmcounter29h",
			"hpmcounter30h",
			"hpmcounter31h",
			"sstatus",
			"sedeleg",
			"sideleg",
			"sie",
			"stvec",
			"sscratch",
			"sepc",
			"scause",
			"sbadaddr",
			"sip",
			"satp",
			"hstatus",
			"hedeleg",
			"hideleg",
			"hie",
			"htvec",
			"hscratch",
			"hepc",
			"hcause",
			"hbadaddr",
			"hip",
			"tbd",
			"mbase",
			"mbound",
			"mibase",
			"mibound",
			"mdbase",
			"mdbound",
			"mhpmcounter3",
			"mhpmcounter4",
			"mhpmcounter5",
			"mhpmcounter6",
			"mhpmcounter7",
			"mhpmcounter8",
			"mhpmcounter9",
			"mhpmcounter10",
			"mhpmcounter11",
			"mhpmcounter12",
			"mhpmcounter13",
			"mhpmcounter14",
			"mhpmcounter15",
			"mhpmcounter16",
			"mhpmcounter17",
			"mhpmcounter18",
			"mhpmcounter19",
			"mhpmcounter20",
			"mhpmcounter21",
			"mhpmcounter22",
			"mhpmcounter23",
			"mhpmcounter24",
			"mhpmcounter25",
			"mhpmcounter26",
			"mhpmcounter27",
			"mhpmcounter28",
			"mhpmcounter29",
			"mhpmcounter30",
			"mhpmcounter31",
			"hpmcounter3h",
			"hpmcounter4h",
			"hpmcounter5h",
			"hpmcounter6h",
			"hpmcounter7h",
			"hpmcounter8h",
			"hpmcounter9h",
			"hpmcounter10h",
			"hpmcounter11h",
			"hpmcounter12h",
			"hpmcounter13h",
			"hpmcounter14h",
			"hpmcounter15h",
			"hpmcounter16h",
			"hpmcounter17h",
			"hpmcounter18h",
			"hpmcounter19h",
			"hpmcounter20h",
			"hpmcounter21h",
			"hpmcounter22h",
			"hpmcounter23h",
			"hpmcounter24h",
			"hpmcounter25h",
			"hpmcounter26h",
			"hpmcounter27h",
			"hpmcounter28h",
			"hpmcounter29h",
			"hpmcounter30h",
			"hpmcounter31h",
			"mucounteren",
			"mscounteren",
			"mhcounteren",
			"tselect",
			"tdata1",
			"tdata2",
			"tdata3",
			"dcsr",
			"dpc",
			"dscratch",
			"mhpmevent3",
			"mhpmevent4",
			"mhpmevent5",
			"mhpmevent6",
			"mhpmevent7",
			"mhpmevent8",
			"mhpmevent9",
			"mhpmevent10",
			"mhpmevent11",
			"mhpmevent12",
			"mhpmevent13",
			"mhpmevent14",
			"mhpmevent15",
			"mhpmevent16",
			"mhpmevent17",
			"mhpmevent18",
			"mhpmevent19",
			"mhpmevent20",
			"mhpmevent21",
			"mhpmevent22",
			"mhpmevent23",
			"mhpmevent24",
			"mhpmevent25",
			"mhpmevent26",
			"mhpmevent27",
			"mhpmevent28",
			"mhpmevent29",
			"mhpmevent30",
			"mhpmevent31",
			"mtime",
			"mtimecmp",
			"msip"}));

//		translatedMapGas.putAll(translatedMap);
//		translatedMapGas.remove("${label}");
//		List<String> temp = translatedMap.get("${label}").stream().map(s -> "label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s) - 4) + "\nlabel1:").collect(Collectors.toList());
//		translatedMapGas.put("${label}", (ArrayList<String>) temp);

		/*
		ArrayList<String> temp = new ArrayList<>();
		for (String s : translatedMap.get("${label}")) {
//			if (s.startsWith("-")) {
			temp.add("label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s)) + "\nlabel1:");
//			} else {
//				temp.add("label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(s) - 4) + "\nlabel1:");
//			}
		}
		translatedMap.put("${label_gas}", temp);
		 */
	}

	public static Pair<String, byte[]> compileRISCV(String instruction, int bits) {
		try {
			RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(instruction));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
			parser.removeErrorListeners();
			parser.addErrorListener(new UnderlineListener());
			RISCVAssemblerParser.AssembleContext context = parser.assemble();
			byte bytes[] = parser.encoder.out.toByteArray();
			if (bytes.length == 0) {
				return new ImmutablePair<>(parser.encoder.err, null);
			} else {
				return new ImmutablePair<>(null, bytes);
			}
		} catch (RecognitionException ex) {
			MessageHandler.errorPrintln(ex);
			return null;
		}
	}

	public static Pair<String, byte[]> compileGas32(String instruction) {
		try {
			ProcessBuilder builder;
			if (System.getProperty("os.name").contains("Windows")) {
				String r = RandomStringUtils.random(20, true, true);
				String filename = "temp_" + r + ".s";
				String s = IOUtils.toString(new FileInputStream("tool/compileRISCV/a.template"), "utf8");
				s = s.replace("{instruction}", instruction);
				FileOutputStream fo = new FileOutputStream("tool/compileRISCV/" + filename);
				IOUtils.write(s, fo, "UTF-8");
				fo.close();
				builder = new ProcessBuilder(new File("tool/compileRISCV/asm32.bat").getAbsolutePath(), filename, r);
			} else {
				builder = new ProcessBuilder("bash", "-l", new File("tool/compileRISCV/asm32.sh").getAbsolutePath(), instruction.replaceAll("\n", "\\\\n"));
//				MessageHandler.println("SED INS=" + instruction.replaceAll("\n", "\\\\n"));
			}
			builder.directory(new File("tool/compileRISCV").getAbsoluteFile());
			builder.redirectErrorStream(true);
			Process process = builder.start();
			byte bytes[] = IOUtils.toByteArray(process.getInputStream());
//			ArrayUtils.reverse(bytes);
			int result = process.waitFor();
			if (new String(bytes).contains(":")) {
				MessageHandler.println("GAS ERROR:" + new String(bytes));
				return new ImmutablePair<>(new String(bytes), null);
			}
			return new ImmutablePair<>(null, bytes);
		} catch (IOException | InterruptedException ex) {
			MessageHandler.errorPrintln(ex);
		}
		return null;
	}

	public static Pair<String, byte[]> compileGas64(String instruction) {
		try {
			ProcessBuilder builder;
			if (System.getProperty("os.name").contains("Windows")) {
				String r = RandomStringUtils.random(20, true, true);
				String filename = "temp_" + r + ".s";
				String s = IOUtils.toString(new FileInputStream("tool/compileRISCV/a.template"), "utf8");
				s = s.replace("{instruction}", instruction);
				FileOutputStream fo = new FileOutputStream("tool/compileRISCV/" + filename);
				IOUtils.write(s, fo, "UTF-8");
				fo.close();
				builder = new ProcessBuilder(new File("tool/compileRISCV/asm64.bat").getAbsolutePath(), filename, r);
			} else {
				builder = new ProcessBuilder("bash", "-l", new File("tool/compileRISCV/asm64.sh").getAbsolutePath(), instruction.replaceAll("\n", "\\\\n"));
//				MessageHandler.println("SED INS=" + instruction.replaceAll("\n", "\\\\n"));
			}
			builder.directory(new File("tool/compileRISCV").getAbsoluteFile());
			builder.redirectErrorStream(true);
			Process process = builder.start();
			byte bytes[] = IOUtils.toByteArray(process.getInputStream());
//			ArrayUtils.reverse(bytes);
			int result = process.waitFor();
			if (new String(bytes).contains(":")) {
				MessageHandler.println("GAS ERROR:" + new String(bytes));
				return new ImmutablePair<>(new String(bytes), null);
			}
			return new ImmutablePair<>(null, bytes);
		} catch (IOException | InterruptedException ex) {
			MessageHandler.errorPrintln(ex);
		}
		return null;
	}

	public static Pair<String, byte[]> compileVector(String instruction) {
		try {
			ProcessBuilder builder = null;
			if (System.getProperty("os.name").contains("Windows")) {
				String r = RandomStringUtils.random(20, true, true);
				String filename = "temp_" + r + ".s";
				String s = IOUtils.toString(new FileInputStream("tool/compileRISCV/a.template"), "utf8");
				s = s.replace("{instruction}", instruction);
				FileOutputStream fo = new FileOutputStream("tool/compileRISCV/" + filename);
				IOUtils.write(s, fo, "UTF-8");
				fo.close();
				builder = new ProcessBuilder(new File("tool/compileRISCV/asm64Vector.bat").getAbsolutePath(), filename, r);
			} else {
//				builder = new ProcessBuilder("bash", "-l", new File("tool/compileRISCV/asm64.sh").getAbsolutePath(), instruction.replaceAll("\n", "\\\\n"));
//				MessageHandler.println("SED INS=" + instruction.replaceAll("\n", "\\\\n"));
			}
			builder.directory(new File("tool/compileRISCV").getAbsoluteFile());
			builder.redirectErrorStream(true);
			Process process = builder.start();
			byte bytes[] = IOUtils.toByteArray(process.getInputStream());
//			ArrayUtils.reverse(bytes);
			int result = process.waitFor();
			if (new String(bytes).contains(":")) {
				MessageHandler.println("GAS ERROR:" + new String(bytes));
				return new ImmutablePair<>(new String(bytes), null);
			}
			return new ImmutablePair<>(null, bytes);
		} catch (IOException | InterruptedException ex) {
			MessageHandler.errorPrintln(ex);
		}
		return null;
	}

	public static String[] concatArrays(String[]... args) {
		return Stream.of(args).flatMap(Stream::of).toArray(String[]::new);
	}

	public static ArrayList<String> concatArraysToList(String[]... args) {
		return new ArrayList<String>(Arrays.asList(concatArrays(args)));
	}

//	public static Pair<String[], String[]> translateInstruction(String testingInstruction) {
//		if (testingInstruction.contains("---")) {
//			return new ImmutablePair(translateInstruction(translatedMap, testingInstruction.split("---")[0]), translateInstruction(translatedMapGas, testingInstruction.split("---")[1]));
//		} else {
	//return new ImmutablePair(translateInstruction(translatedMap, testingInstruction), translateInstruction(translatedMapGas, testingInstruction));
//		}
//	}
	public static Pair<String[], String[]> translateInstruction(String testingInstruction) {
		String[] translateInstructions = translateInstruction(translatedMap, testingInstruction);

		ArrayList<String> quantr = new ArrayList<>();
		ArrayList<String> gas = new ArrayList<>();
		for (String instruction : translateInstructions) {
			if (instruction.contains("---")) {
				quantr.add(instruction.split("---")[0].trim());
				gas.add(instruction.split("---")[1].trim());
			} else {
				quantr.add(instruction);
				gas.add(instruction);
			}
		}

		return new ImmutablePair<>(quantr.toArray(new String[0]), gas.toArray(new String[0]));
	}

	public static String[] translateInstruction(HashMap<String, ArrayList<String>> translatedMap, String instruction) {
//		MessageHandler.println("\t\t\t" + testingInstruction);
		String patternStr = "\\$\\{[^\\}]+\\}";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(instruction);

		ArrayList<String> groups = new ArrayList<>();
		ArrayList<Integer> starts = new ArrayList<>();
		ArrayList<Integer> ends = new ArrayList<>();
		while (matcher.find()) {
//			MessageHandler.println("matcher.group() : " + matcher.group() + ", " + matcher.start() + " > " + matcher.end());
			if (!groups.contains(matcher.group())) {
				groups.add(matcher.group());
				starts.add(matcher.start());
				ends.add(matcher.end());
			}
		}

		if (groups.isEmpty()) {
			return new String[]{instruction};
		} else {
			LinkedHashSet<String> instructions = new LinkedHashSet<>();
			for (int x = 0; x < groups.size(); x++) {
				String group = groups.get(x);
//				int start = starts.get(x);
//				int end = ends.get(x);
				ArrayList<String> elements = translatedMap.get(group);
				if (elements == null) {
					MessageHandler.errorPrintln(String.format("Wrong test case : %s, this %s not exist in myTranslatedMap", instruction, group));
					System.exit(10);
				}
//				if (instructions.isEmpty()) {
				for (String element : elements) {
					if (instruction.contains("---")) {
						String quantr_instruction = instruction.split("---")[0].trim();
						String gas_instruction = instruction.split("---")[1].trim();
						quantr_instruction = quantr_instruction.replaceFirst(Pattern.quote(group), element);

						if (group.equals("${label}")) {
							gas_instruction = gas_instruction.replaceFirst(Pattern.quote(group), "label1\n" + ".skip 0x" + Integer.toHexString(CommonLib.string2int(element)) + "\nlabel1:");
						} else {
							gas_instruction = gas_instruction.replaceFirst(Pattern.quote(group), element);
						}
						String translatedInstruction = quantr_instruction + " --- " + gas_instruction;
						instructions.addAll(Arrays.asList(translateInstruction(translatedMap, translatedInstruction)));
					} else {
						String translatedInstruction = instruction.replaceFirst(Pattern.quote(group), element);
						instructions.addAll(Arrays.asList(translateInstruction(translatedMap, translatedInstruction)));
					}
				}
//				}
			}

			return instructions.toArray(new String[0]);
		}
	}
}
