package hk.quantr.assembler.riscv.listener;

import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
import java.util.ArrayList;

public class IncludeListener extends RISCVAssemblerParserBaseListener {

    public int recursionLineNumber = 0;
    public String recursionLine = null;
    public ArrayList<String> includes = new ArrayList<>();

    @Override
    public void exitInclude(RISCVAssemblerParser.IncludeContext ctx) {
		  recursionLine = "%include \"" + ctx.FILENAME().getText() + "\"";
		  includes.add(ctx.FILENAME().getText());
    }

}
