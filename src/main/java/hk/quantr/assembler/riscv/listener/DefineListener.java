package hk.quantr.assembler.riscv.listener;

import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
import java.util.ArrayList;
import java.util.HashMap;

public class DefineListener extends RISCVAssemblerParserBaseListener {

    public static HashMap<String, String> map = new HashMap<>();
    public static ArrayList<String> lines = new ArrayList<>();
    public static ArrayList<String> variableName = new ArrayList<>();
    public static ArrayList<String> duplicateElements = new ArrayList<>();
//    public static HashMap<String, String> record = new HashMap<>();
    @Override
    public void exitDefine(RISCVAssemblerParser.DefineContext ctx) {
	   map.put(ctx.variable.getText(), ctx.assignValue.getText());
	   lines.add("%define " + ctx.variable.getText() + " " + ctx.assignValue.getText());
	   if (!variableName.contains(ctx.variable.getText())) {
		  variableName.add(ctx.variable.getText());
	   } else {
		  duplicateElements.add(ctx.variable.getText());
	   }
    }
}
