package hk.quantr.assembler.riscv.listener;

import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ByteListener extends RISCVAssemblerParserBaseListener {

	public ArrayList<Integer> bytelist = new ArrayList<>();

	@Override
	public void exitDotbyte(RISCVAssemblerParser.DotbyteContext ctx) {
		int i = ctx.MATH_EXPRESSION().size();
		for(int j=0;j<i-1;j++)
		bytelist.add(Integer.parseInt(ctx.MATH_EXPRESSION(j).getText()));

	}
}
