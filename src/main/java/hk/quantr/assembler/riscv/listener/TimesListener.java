///*
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
// */
//package hk.quantr.assembler.riscv.listener;
//
//import hk.quantr.assembler.antlr.RISCVAssemblerParser;
//import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.script.ScriptEngine;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//
///**
// *
// * @author walter
// */
//public class TimesListener extends RISCVAssemblerParserBaseListener{
//	
//	public int times;
//	public String instruction;
//	
//	@Override
//	public void exitTimes(RISCVAssemblerParser.TimesContext ctx) {
//		ScriptEngineManager mgr = new ScriptEngineManager();
//		ScriptEngine engine = mgr.getEngineByName("JavaScript");
//		try {
//			times = Integer.parseInt(engine.eval(ctx.MATH_EXPRESSION().getText()).toString());
//		} catch (ScriptException ex) {
//			Logger.getLogger(TimesListener.class.getName()).log(Level.SEVERE, null, ex);
//		}
//	}
//	
//}
