package hk.quantr.assembler.riscv.listener;

import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
import hk.quantr.assembler.print.MessageHandler;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class LabelListener extends RISCVAssemblerParserBaseListener {

	@Override
	public void exitLabel(RISCVAssemblerParser.LabelContext ctx) {
		MessageHandler.println(ctx.IDENTIFIER().getSymbol().getLine() + " : " + ctx.IDENTIFIER().getText());
	}

	@Override
	public void exitSection(RISCVAssemblerParser.SectionContext ctx) {
		MessageHandler.println(ctx.DOT().getSymbol().getLine() + " : section = " + ctx.IDENTIFIER(0).getText() + " -> " + ctx.IDENTIFIER(1).getText());
	}
}
