package hk.quantr.assembler.riscv.listing;

import hk.quantr.javalib.CommonLib;


/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Line {

	public int lineNo;
	public long address;
	public byte bytes[];
	//public String label;
	public String section;
	public String code;
	public String comment;

	public Line(int lineNo, long address, byte[] bytes, String section, String code, String comment) {
		this.lineNo = lineNo;
		this.address = address;
		this.bytes = bytes;
		this.section = section;
		this.code = code;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return String.format("%d\t%s\t%08d\t%s\t%s\t%s\n", lineNo, section, address, CommonLib.arrayToHexString(bytes).replaceAll("0x",""), code, comment);
	}
}
