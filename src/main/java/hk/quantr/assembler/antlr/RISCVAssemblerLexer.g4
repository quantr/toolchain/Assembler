lexer grammar RISCVAssemblerLexer;

WS			:	(' '|'\t')+ -> channel(1);
NL			:	'\r'? '\n' -> skip;
LINE_COMMENT:	';' ~[\r\n]*;

ADD_		:	'+';
MIN_		:	'-';
MUL_		:	'*';
DIV_		:	'/';
MOD_		:	'%';
SQU_		:	'^';
DOLLAR		:	'$';
DOUBLE_QUOTATION : '"';
DEFINE		:	'%define';
IFDEF		:	'%ifdef';
ELIF		:	'%elif';
ELSE		:	'%else';
ENDIF		:	'%endif';
DOTBYTE		:	'.byte';
INCLUDE		:	'%include';
DOTHALF		:	'.half';
DOTWORD		:	'.word';
DOTDWORD	:	'.dword';
DOTSTRING	:	'.string';
TIMES		:	'times';

DOT			:	'.';

ARITHMETIC_SYMBOL	:	ADD_
					|	MIN_
					|	MUL_
					|	DIV_
					;

MATH_EXPRESSION	:	'-'? ('0x' [0-9a-zA-Z]+ | [0-9]+ | '0b' [01]+) (
					'0x' [0-9a-zA-Z]+
				|	[0-9]+
				|	ADD_
				|	MIN_
				|	MUL_
				|	DIV_
				|	OPEN_SMALL_BRACKET MATH_EXPRESSION CLOSE_SMALL_BRACKET
				)*
				;

COMMA		:	',';
COLON		:	':';
DB_SYMBOL	:	'db';

OPEN_BIG_BRACKET	:	'{';
CLOSE_BIG_BRACKET	:	'}';

OPEN_SMALL_BRACKET	:	'(';
CLOSE_SMALL_BRACKET	:	')';

OPEN_MID_BRACKET	:	'[';
CLOSE_MID_BRACKET	:	']';

RNE			:   'rne';
RTZ			:   'rtz';
RDN			:	'rdn';
RUP			:	'rup';
RMM			:	'rmm';
DYN			:	'dyn';
X0			:	'x0';
X1			:	'x1';
X2			:	'x2';
X3			:	'x3';
X4			:	'x4';
X5			:	'x5';
X6			:	'x6';
X7			:	'x7';
X8			:	'x8';
X9			:	'x9';
X10			:	'x10';
X11			:	'x11';
X12			:	'x12';
X13			:	'x13';
X14			:	'x14';
X15			:	'x15';
X16			:	'x16';
X17			:	'x17';
X18			:	'x18';
X19			:	'x19';
X20			:	'x20';
X21			:	'x21';
X22			:	'x22';
X23			:	'x23';
X24			:	'x24';
X25			:	'x25';
X26			:	'x26';
X27			:	'x27';
X28			:	'x28';
X29			:	'x29';
X30			:	'x30';
X31			:	'x31';
F0			:	'f0';
F1			:	'f1';
F2			:	'f2';
F3			:	'f3';
F4			:	'f4';
F5			:	'f5';
F6			:	'f6';
F7			:	'f7';
F8			:	'f8';
F9			:	'f9';
F10			:	'f10';
F11			:	'f11';
F12			:	'f12';
F13			:	'f13';
F14			:	'f14';
F15			:	'f15';
F16			:	'f16';
F17			:	'f17';
F18			:	'f18';
F19			:	'f19';
F20			:	'f20';
F21			:	'f21';
F22			:	'f22';
F23			:	'f23';
F24			:	'f24';
F25			:	'f25';
F26			:	'f26';
F27			:	'f27';
F28			:	'f28';
F29			:	'f29';
F30			:	'f30';
F31			:	'f31';
ZERO		:	'zero';
RA			:	'ra';
SP			:	'sp';
GP			:	'gp';
TP			:	'tp';
T0			:	't0';
T1			:	't1';
T2			:	't2';
S0			:	's0';
FP			:	'fp';
S1			:	's1';
A0			:	'a0';
A1			:	'a1';
A2			:	'a2';
A3			:	'a3';
A4			:	'a4';
A5			:	'a5';
A6			:	'a6';
A7			:	'a7';
S2			:	's2';
S3			:	's3';
S4			:	's4';
S5			:	's5';
S6			:	's6';
S7			:	's7';
S8			:	's8';
S9			:	's9';
S10			:	's10';
S11			:	's11';
T3			:	't3';
T4			:	't4';
T5			:	't5';
T6			:	't6';
FT0			:	'ft0';
FT1			:	'ft1';
FT2			:	'ft2';
FT3			:	'ft3';
FT4			:	'ft4';
FT5			:	'ft5';
FT6			:	'ft6';
FT7			:	'ft7';
FS0			:	'fs0';
FS1			:	'fs1';
FA0			:	'fa0';
FA1			:	'fa1';
FA2			:	'fa2';
FA3			:	'fa3';
FA4			:	'fa4';
FA5			:	'fa5';
FA6			:	'fa6';
FA7			:	'fa7';
FS2			:	'fs2';
FS3			:	'fs3';
FS4			:	'fs4';
FS5			:	'fs5';
FS6			:	'fs6';
FS7			:	'fs7';
FS8			:	'fs8';
FS9			:	'fs9';
FS10		:	'fs10';
FS11		:	'fs11';
FT8			:	'ft8';
FT9			:	'ft9';
FT10		:	'ft10';
FT11		:	'ft11';

V0          :	'v0';
V1          :	'v1';
V2          :	'v2';
V3          :	'v3';
V4          :	'v4';
V5          :	'v5';
V6          :	'v6';
V7          :	'v7';
V8          :	'v8';
V9          :	'v9';
V10         :	'v10';
V11         :	'v11';
V12         :	'v12';
V13         :	'v13';
V14         :	'v14';
V15         :	'v15';
V16         :	'v16';
V17         :	'v17';
V18         :	'v18';
V19         :	'v19';
V20         :	'v20';
V21         :	'v21';
V22         :	'v22';
V23         :	'v23';
V24         :	'v24';
V25         :	'v25';
V26         :	'v26';
V27         :	'v27';
V28         :	'v28';
V29         :	'v29';
V30         :	'v30';
V31         :	'v31';

VTYPE       :	'vtype';
VSEW        :	'vsew';
VLMUL       :	'vlmul';
VTA         :	'vta';
VMA         :	'vma';
VL          :	'vl';
VLENB       :	'vlenb';
VSTART      :	'vstart';
VXRM        :	'vxrm';
VXSAT       :	'vxsat';
VCSR        :	'vcsr';
E8          :	'e8';
E16         :	'e16';
E32         :	'e32';
E64         :	'e64';
MF8         :	'mf8';
MF4         :	'mf4';
MF2         :	'mf2';
M1          :	'm1';
M2          :	'm2';
M4          :	'm4';
M8          :	'm8';
TA          :	'ta';
TU          :	'tu';
MA          :	'ma';
MU          :	'mu';

USTATUS		:	'ustatus';
UIE			:	'uie';
UTVEC		:	'utvec';
FFLAGS		:	'fflags';
FRM			:	'frm';
FCSR		:	'fcsr';
MSTATUS		:	'mstatus';
MISA		:	'misa';
MIE			:	'mie';
MTVEC		:	'mtvec';
MSCRATCH	:	'mscratch';
MEPC		:	'mepc';
MCAUSE		:	'mcause';

MIP			:	'mip';
MCYCLE		:	'mcycle';
MCYCLEH		:	'mcycleh';
MINSTRET	:	'minstret';
MINSTRETH	:	'minstreth';
MVENDORID	:	'mvendorid';
MARCHID		:	'marchid';
MIMPID		:	'mimpid';
MHARTID		:	'mhartid';
/*
MTIME		:	'mtime';
MTIMECMP	:	'mtimecmp';
*/
MSIP		:	'msip';
USCRATCH	:	'uscratch';
UEPC		:	'uepc';
UCAUSE		:	'ucause';
UBADADDR	:	'ubadaddr';
UIP			:	'uip';
CYCLE		:	'cycle';
TIME		:	'time';
INSTRET		:	'instret';

HPMCOUNTER3	:	'hpmcounter3';
HPMCOUNTER4	:	'hpmcounter4';
HPMCOUNTER5	:   'hpmcounter5';
HPMCOUNTER6	:	'hpmcounter6';
HPMCOUNTER7	:	'hpmcounter7';
HPMCOUNTER8	:	'hpmcounter8';
HPMCOUNTER9	:	'hpmcounter9';
HPMCOUNTER10:	'hpmcounter10';
HPMCOUNTER11:	'hpmcounter11';
HPMCOUNTER12:	'hpmcounter12';
HPMCOUNTER13:	'hpmcounter13';
HPMCOUNTER14:	'hpmcounter14';
HPMCOUNTER15:	'hpmcounter15';
HPMCOUNTER16:	'hpmcounter16';
HPMCOUNTER17:	'hpmcounter17';
HPMCOUNTER18:	'hpmcounter18';
HPMCOUNTER19:	'hpmcounter19';
HPMCOUNTER20:	'hpmcounter20';
HPMCOUNTER21:	'hpmcounter21';
HPMCOUNTER22:	'hpmcounter22';
HPMCOUNTER23:	'hpmcounter23';
HPMCOUNTER24:	'hpmcounter24';
HPMCOUNTER25:	'hpmcounter25';
HPMCOUNTER26:	'hpmcounter26';
HPMCOUNTER27:	'hpmcounter27';
HPMCOUNTER28:	'hpmcounter28';
HPMCOUNTER29:	'hpmcounter29';
HPMCOUNTER30:	'hpmcounter30';
HPMCOUNTER31:	'hpmcounter31';

CYCLEH		:	'cycleh';
TIMEH		:	'timeh';
INSTRETH	:	'instreth';

HPMCOUNTER3H:	'hpmcounter3h';
HPMCOUNTER4H:	'hpmcounter4h';
HPMCOUNTER5H:   'hpmcounter5h';
HPMCOUNTER6H:	'hpmcounter6h';
HPMCOUNTER7H:	'hpmcounter7h';
HPMCOUNTER8H:	'hpmcounter8h';
HPMCOUNTER9H:	'hpmcounter9h';
HPMCOUNTER10H:	'hpmcounter10h';
HPMCOUNTER11H:	'hpmcounter11h';
HPMCOUNTER12H:	'hpmcounter12h';
HPMCOUNTER13H:	'hpmcounter13h';
HPMCOUNTER14H:	'hpmcounter14h';
HPMCOUNTER15H:	'hpmcounter15h';
HPMCOUNTER16H:	'hpmcounter16h';
HPMCOUNTER17H:	'hpmcounter17h';
HPMCOUNTER18H:	'hpmcounter18h';
HPMCOUNTER19H:	'hpmcounter19h';
HPMCOUNTER20H:	'hpmcounter20h';
HPMCOUNTER21H:	'hpmcounter21h';
HPMCOUNTER22H:	'hpmcounter22h';
HPMCOUNTER23H:	'hpmcounter23h';
HPMCOUNTER24H:	'hpmcounter24h';
HPMCOUNTER25H:	'hpmcounter25h';
HPMCOUNTER26H:	'hpmcounter26h';
HPMCOUNTER27H:	'hpmcounter27h';
HPMCOUNTER28H:	'hpmcounter28h';
HPMCOUNTER29H:	'hpmcounter29h';
HPMCOUNTER30H:	'hpmcounter30h';
HPMCOUNTER31H:	'hpmcounter31h';

SSTATUS		:	'sstatus';
SEDELEG		:	'sedeleg';
SIDELEG		:	'sideleg';
SIE			:	'sie';
STVEC		:	'stvec';
SSCRATCH	:	'sscratch';
SEPC		:	'sepc';
SCAUSE		:	'scause';
SBADADDR	:	'sbadaddr';
SIP			:	'sip';
SATP		:	'satp';
HSTATUS		:	'hstatus';
HEDELEG		:	'hedeleg';
HIDELEG		:	'hideleg';
HIE			:	'hie';
HTVEC		:	'htvec';
HSCRATCH	:	'hscratch';
HEPC		:	'hepc';
HCAUSE		:	'hcause';
HBADADDR	:	'hbadaddr';
HIP			:	'hip';
//TBD		:	'tbd';
MBASE		:	'mbase';
MBOUND		:	'mbound';
MIBASE		:	'mibase';
MIBOUND		:	'mibound';
MDBASE		:	'mdbase';
MDBOUND		:	'mdbound';
MBADADDR	:	'mbadaddr';

MHPMCOUNTER3:	'mhpmcounter3';
MHPMCOUNTER4:	'mhpmcounter4';
MHPMCOUNTER5:   'mhpmcounter5';
MHPMCOUNTER6:	'mhpmcounter6';
MHPMCOUNTER7:	'mhpmcounter7';
MHPMCOUNTER8:	'mhpmcounter8';
MHPMCOUNTER9:	'mhpmcounter9';
MHPMCOUNTER10:	'mhpmcounter10';
MHPMCOUNTER11:	'mhpmcounter11';
MHPMCOUNTER12:	'mhpmcounter12';
MHPMCOUNTER13:	'mhpmcounter13';
MHPMCOUNTER14:	'mhpmcounter14';
MHPMCOUNTER15:	'mhpmcounter15';
MHPMCOUNTER16:	'mhpmcounter16';
MHPMCOUNTER17:	'mhpmcounter17';
MHPMCOUNTER18:	'mhpmcounter18';
MHPMCOUNTER19:	'mhpmcounter19';
MHPMCOUNTER20:	'mhpmcounter20';
MHPMCOUNTER21:	'mhpmcounter21';
MHPMCOUNTER22:	'mhpmcounter22';
MHPMCOUNTER23:	'mhpmcounter23';
MHPMCOUNTER24:	'mhpmcounter24';
MHPMCOUNTER25:	'mhpmcounter25';
MHPMCOUNTER26:	'mhpmcounter26';
MHPMCOUNTER27:	'mhpmcounter27';
MHPMCOUNTER28:	'mhpmcounter28';
MHPMCOUNTER29:	'mhpmcounter29';
MHPMCOUNTER30:	'mhpmcounter30';
MHPMCOUNTER31:	'mhpmcounter31';

MHPMCOUNTER3H:	'mhpmcounter3h';
MHPMCOUNTER4H:	'mhpmcounter4h';
MHPMCOUNTER5H:	'mhpmcounter5h';
MHPMCOUNTER6H:	'mhpmcounter6h';
MHPMCOUNTER7H:	'mhpmcounter7h';
MHPMCOUNTER8H:	'mhpmcounter8h';
MHPMCOUNTER9H:	'mhpmcounter9h';
MHPMCOUNTER10H:	'mhpmcounter10h';
MHPMCOUNTER11H:	'mhpmcounter11h';
MHPMCOUNTER12H:	'mhpmcounter12h';
MHPMCOUNTER13H:	'mhpmcounter13h';
MHPMCOUNTER14H:	'mhpmcounter14h';
MHPMCOUNTER15H:	'mhpmcounter15h';
MHPMCOUNTER16H:	'mhpmcounter16h';
MHPMCOUNTER17H:	'mhpmcounter17h';
MHPMCOUNTER18H:	'mhpmcounter18h';
MHPMCOUNTER19H:	'mhpmcounter19h';
MHPMCOUNTER20H:	'mhpmcounter20h';
MHPMCOUNTER21H:	'mhpmcounter21h';
MHPMCOUNTER22H:	'mhpmcounter22h';
MHPMCOUNTER23H:	'mhpmcounter23h';
MHPMCOUNTER24H:	'mhpmcounter24h';
MHPMCOUNTER25H:	'mhpmcounter25h';
MHPMCOUNTER26H:	'mhpmcounter26h';
MHPMCOUNTER27H:	'mhpmcounter27h';
MHPMCOUNTER28H:	'mhpmcounter28h';
MHPMCOUNTER29H:	'mhpmcounter29h';
MHPMCOUNTER30H:	'mhpmcounter30h';
MHPMCOUNTER31H:	'mhpmcounter31h';

MUCOUNTEREN	:	'mucounteren';
MSCOUNTEREN	:	'mscounteren';
MHCOUNTEREN	:	'mhcounteren';
TSELECT		:	'tselect';
TDATA1		:	'tdata1';
TDATA2		:	'tdata2';
TDATA3		:	'tdata3';
DCSR		:	'dcsr';
DPC			:	'dpc';
DSCRATCH	:	'dscratch';

MHPMEVENT3	:	'mhpmevent3';
MHPMEVENT4	:	'mhpmevent4';
MHPMEVENT5	:	'mhpmevent5';
MHPMEVENT6	:	'mhpmevent6';
MHPMEVENT7	:	'mhpmevent7';
MHPMEVENT8	:	'mhpmevent8';
MHPMEVENT9	:	'mhpmevent9';
MHPMEVENT10	:	'mhpmevent10';
MHPMEVENT11	:	'mhpmevent11';
MHPMEVENT12	:	'mhpmevent12';
MHPMEVENT13	:	'mhpmevent13';
MHPMEVENT14	:	'mhpmevent14';
MHPMEVENT15	:	'mhpmevent15';
MHPMEVENT16	:	'mhpmevent16';
MHPMEVENT17	:	'mhpmevent17';
MHPMEVENT18	:	'mhpmevent18';
MHPMEVENT19	:	'mhpmevent19';
MHPMEVENT20	:	'mhpmevent20';
MHPMEVENT21	:	'mhpmevent21';
MHPMEVENT22	:	'mhpmevent22';
MHPMEVENT23	:	'mhpmevent23';
MHPMEVENT24	:	'mhpmevent24';
MHPMEVENT25	:	'mhpmevent25';
MHPMEVENT26	:	'mhpmevent26';
MHPMEVENT27	:	'mhpmevent27';
MHPMEVENT28	:	'mhpmevent28';
MHPMEVENT29	:	'mhpmevent29';
MHPMEVENT30	:	'mhpmevent30';
MHPMEVENT31	:	'mhpmevent31';
PMPCFG0		:	'pmpcfg0';
PMPCFG1		:	'pmpcfg1';
PMPCFG2		:	'pmpcfg2';
PMPCFG3		:	'pmpcfg3';
PMPADDR0	:	'pmpaddr0';
PMPADDR1	:	'pmpaddr1';
PMPADDR2	:	'pmpaddr2';
PMPADDR3	:	'pmpaddr3';
PMPADDR4	:	'pmpaddr4';
PMPADDR5	:	'pmpaddr5';
PMPADDR6	:	'pmpaddr6';
PMPADDR7	:	'pmpaddr7';
PMPADDR8	:	'pmpaddr8';
PMPADDR9	:	'pmpaddr9';
PMPADDR10	:	'pmpaddr10';
PMPADDR11	:	'pmpaddr11';
PMPADDR12	:	'pmpaddr12';
PMPADDR13	:	'pmpaddr13';
PMPADDR14	:	'pmpaddr14';
PMPADDR15	:	'pmpaddr15';


SLL			:   'sll';
SLLI		:   'slli';
SRL			:   'srl';
SRLI		:   'srli';
SRA			:   'sra';
SRAI		:   'srai';
ADD			:   'add';
ADDI		:   'addi';
SUB			:   'sub';
LUI			:   'lui';
AUIPC		:   'auipc';
XOR			:   'xor';
XORI		:   'xori';
OR			:   'or';
ORI			:   'ori';
AND			:   'and';
ANDI		:   'andi';
SLT			:   'slt';
SLTI		:   'slti';
SLTU		:   'sltu';
SLTIU		:   'sltiu';
BEQ			:   'beq';
BNE			:	'bne';
BLT			:	'blt';
BGE			:	'bge';
BLTU		:	'bltu';
BGEU		:	'bgeu';
JAL			:	'jal';
JALR		:	'jalr';
FENCE		:	'fence';
FENCEI		:	'fence.i';
ECALL		:	'ecall';
EBREAK		:	'ebreak';
CLW			:	'c.lw';
CLWSP		:	'c.lwsp';
CSW			:	'c.sw';
CSWSP		:	'c.swsp';
CADD		:	'c.add';
CADDI		:	'c.addi';
CADDIW		:	'c.addiw';
CADDI16SP	:	'c.addi16sp';
CADDI4SPN	:	'c.addi4spn';
CADDW		:	'c.addw';
CSUB		:	'c.sub';
CSUBW		:	'c.subw';
CAND		:	'c.and';
CANDI		:	'c.andi';
COR			:	'c.or';
CXOR		:	'c.xor';
CMV			:	'c.mv';
CLI			:	'c.li';
CLUI		:	'c.lui';
CSLLI		:	'c.slli';
CSRAI		:	'c.srai';
CSRLI		:	'c.srli';
CBEQZ		:	'c.beqz';
CBNEZ		:	'c.bnez';
CJ			:	'c.j';
CJR			:	'c.jr';
CJAL		:	'c.jal';
CJALR		:	'c.jalr';
CEBREAK		:	'c.ebreak';
CLDSP		:   'c.ldsp';
CFLDSP		:   'c.fldsp';
CFSDSP		:	'c.fsdsp';
CFSD		:	'c.fsd';
CSDSP		:	'c.sdsp';
CSD			:	'c.sd';
CLD			:	'c.ld';
CFLD		:	'c.fld';
  
CSRRW		:	'csrrw';
CSRRS		:	'csrrs';
CSRRC		:	'csrrc';
CSRRWI		:	'csrrwi';
CSRRSI		:	'csrrsi';
CSRRCI		:	'csrrci';

CSRC		:	'csrc';
CSRCI		:	'csrci';
CSRR		:	'csrr';
CSRW		:	'csrw';
CSRWI		:	'csrwi';
CSRS		:	'csrs';
CSRSI		:	'csrsi';


MUL			:	'mul';
MULH		:	'mulh';
MULHSU		:	'mulhsu';
MULHU		:	'mulhu';
DIV			:	'div';
DIVU		:	'divu';
REM			:	'rem';
REMU		:	'remu';

LB			:	'lb';
LH			:	'lh';
LW			:	'lw';
LD			:	'ld';
LBU			:	'lbu';
LHU			:	'lhu';
LWU			:	'lwu';

LI			:	'li';

SB			:	'sb';
SH			:	'sh';
SW			:	'sw';
SD			:	'sd';

ERET		:	'eret';

MRTS		:	'mrts';
MRTH		:	'mrth';
HRTS		:	'hrts';

WFI			:	'wfi';
//magic breakpoint type instructions

PAUSE		: 'pause_sim';
SFENCEVM	:	'sfence.vm';

URET		:	'uret';
SRET		:	'sret';
HRET		:	'hret';
MRET		:	'mret';

BEQZ		:	'beqz';
BNEZ		:	'bnez';
BLEZ		:	'blez';
BGEZ		:	'bgez';
BLTZ		:	'bltz';
BGTZ		:	'bgtz';
BGT			:	'bgt';
BLE			:	'ble';
BGTU		:	'bgtu';
BLEU		:	'bleu';

J			:	'j';
JR			:	'jr';
MV			:	'mv';
RET			:	'ret';
CALL		:	'call';
TAIL		:	'tail';

ADDW		:	'addw';
ADDIW		:	'addiw';
SUBW		:	'subw';
MULW		:	'mulw';
DIVW		:	'divw';
DIVUW		:	'divuw';

NOP			:	'nop';
NOT			:	'not';
CNOP		:	'c.nop';

NEG			:	'neg';
NEGW		:	'negw';

SRLW		:	'srlw';
SLLW		:	'sllw';


SLLIW		:	'slliw';
SRLIW		:	'srliw';
SRAIW		:	'sraiw';
SRAW		:	'sraw';

SLTZ		:	'sltz';
SGTZ		:   'sgtz';
SEQZ		:	'seqz';

REMW		:	'remw';
REMUW		:	'remuw';
SNEZ		:	'snez';
SEXTW		:	'sextw';

LRW			:	'lr.w' ;
LRWAQ		:	'lr.w.aq' ;
LRWRL		:	'lr.w.rl' ;
LRWAQRL		:	'lr.w.aqrl' ;

SCW			:	'sc.w';
SCWAQ		:	'sc.w.aq' ;
SCWRL		:	'sc.w.rl' ;
SCWAQRL		:	'sc.w.aqrl' ;

AMOSWAPW	:	'amoswap.w';
AMOSWAPWAQ	:	'amoswap.w.aq';
AMOSWAPWRL	:	'amoswap.w.rl';
AMOSWAPWAQRL:	'amoswap.w.aqrl';

AMOADDW		:	'amoadd.w';
AMOADDWAQ	:	'amoadd.w.aq';
AMOADDWRL	:	'amoadd.w.rl';
AMOADDWAQRL	:	'amoadd.w.aqrl';

AMOXORW		:	'amoxor.w';
AMOXORWAQ	:	'amoxor.w.aq';
AMOXORWRL	:	'amoxor.w.rl';
AMOXORWAQRL	:	'amoxor.w.aqrl';

AMOANDW		:	'amoand.w';
AMOANDWAQ	:	'amoand.w.aq';
AMOANDWRL	:	'amoand.w.rl';
AMOANDWAQRL	:	'amoand.w.aqrl';

AMOORW		:	'amoor.w';
AMOORWAQ	:	'amoor.w.aq';
AMOORWRL	:	'amoor.w.rl';
AMOORWAQRL	:	'amoor.w.aqrl';

AMOMINW		:	'amomin.w';
AMOMINWAQ	:	'amomin.w.aq';
AMOMINWRL	:	'amomin.w.rl';
AMOMINWAQRL	:	'amomin.w.aqrl';

AMOMAXW		:	'amomax.w';
AMOMAXWAQ	:	'amomax.w.aq';
AMOMAXWRL	:	'amomax.w.rl';
AMOMAXWAQRL	:	'amomax.w.aqrl';

AMOMINUW	:	'amominu.w';
AMOMINUWAQ	:	'amominu.w.aq';
AMOMINUWRL	:	'amominu.w.rl';
AMOMINUWAQRL:	'amominu.w.aqrl';

AMOMAXUW	:	'amomaxu.w';
AMOMAXUWAQ	:	'amomaxu.w.aq';
AMOMAXUWRL	:	'amomaxu.w.rl';
AMOMAXUWAQRL:	'amomaxu.w.aqrl';

LRD			:	'lr.d' ;
LRDAQ		:	'lr.d.aq' ;
LRDRL		:	'lr.d.rl' ;
LRDAQRL		:	'lr.d.aqrl' ;

SCD			:	'sc.d';
SCDAQ		:	'sc.d.aq' ;
SCDRL		:	'sc.d.rl' ;
SCDAQRL		:	'sc.d.aqrl' ;

AMOSWAPD	:	'amoswap.d';
AMOSWAPDAQ	:	'amoswap.d.aq';
AMOSWAPDRL	:	'amoswap.d.rl';
AMOSWAPDAQRL:	'amoswap.d.aqrl';

AMOADDD		:	'amoadd.d';
AMOADDDAQ	:	'amoadd.d.aq';
AMOADDDRL	:	'amoadd.d.rl';
AMOADDDAQRL	:	'amoadd.d.aqrl';

AMOXORD		:	'amoxor.d';
AMOXORDAQ	:	'amoxor.d.aq';
AMOXORDRL	:	'amoxor.d.rl';
AMOXORDAQRL	:	'amoxor.d.aqrl';

AMOANDD		:	'amoand.d';
AMOANDDAQ	:	'amoand.d.aq';
AMOANDDRL	:	'amoand.d.rl';
AMOANDDAQRL	:	'amoand.d.aqrl';

AMOORD		:	'amoor.d';
AMOORDAQ	:	'amoor.d.aq';
AMOORDRL	:	'amoor.d.rl';
AMOORDAQRL	:	'amoor.d.aqrl';

AMOMIND		:	'amomin.d';
AMOMINDAQ	:	'amomin.d.aq';
AMOMINDRL	:	'amomin.d.rl';
AMOMINDAQRL	:	'amomin.d.aqrl';

AMOMAXD		:	'amomax.d';
AMOMAXDAQ	:	'amomax.d.aq';
AMOMAXDRL	:	'amomax.d.rl';
AMOMAXDAQRL	:	'amomax.d.aqrl';

AMOMINUD	:	'amominu.d';
AMOMINUDAQ	:	'amominu.d.aq';
AMOMINUDRL	:	'amominu.d.rl';
AMOMINUDAQRL:	'amominu.d.aqrl';

AMOMAXUD	:	'amomaxu.d';
AMOMAXUDAQ	:	'amomaxu.d.aq';
AMOMAXUDRL	:	'amomaxu.d.rl';
AMOMAXUDAQRL:	'amomaxu.d.aqrl';

FLW			:	'flw';
FSW			:   'fsw';
FMADDS		:	'fmadd.s';
FMSUBS		:   'fmsub.s';
FNMSUBS		:   'fnmsub.s';
FNMADDS		:	'fnmadd.s';
FADDS		:   'fadd.s';
FSUBS		:	'fsub.s';
FMULS		:	'fmul.s';
FDIVS		:   'fdiv.s';
FSQRTS		:	'fsqrt.s';
FSGNJS		:	'fsgnj.s';
FSGNJNS		:	'fsgnjn.s';
FSGNJXS		:	'fsgnjx.s';
FMINS		:	'fmin.s';
FMAXS		:	'fmax.s';
FCVTWS		:	'fcvt.w.s';
FCVTWUS		:	'fcvt.wu.s';
FMVXW		:   'fmv.x.w';
FEQS		:   'feq.s';
FLTS		:	'flt.s';
FLES		:	'fle.s';
FCLASSS		:   'fclass.s';
FCVTSW		:   'fcvt.s.w';
FCVTSWU		:	'fcvt.s.wu';
FMVWX		:	'fmv.w.x';

FLD			:	'fld';
FSD			:   'fsd';
FMADDD		:	'fmadd.d';
FMSUBD		:   'fmsub.d';
FNMSUBD		:   'fnmsub.d';
FNMADDD		:	'fnmadd.d';
FADDD		:   'fadd.d';
FSUBD		:	'fsub.d';
FMULD		:	'fmul.d';
FDIVD		:   'fdiv.d';
FSQRTD		:	'fsqrt.d';
FSGNJD		:	'fsgnj.d';
FSGNJND		:	'fsgnjn.d';
FSGNJXD		:	'fsgnjx.d';
FMIND		:	'fmin.d';
FMAXD		:	'fmax.d';
FCVTSD		:	'fcvt.s.d';
FCVTDS		:	'fcvt.d.s';
FEQD		:   'feq.d';
FLTD		:	'flt.d';
FLED		:	'fle.d';
FCLASSD		:   'fclass.d';
FCVTWD		:   'fcvt.w.d';
FCVTWUD		:	'fcvt.wu.d';
FCVTDW		:	'fcvt.d.w';
FCVTDWU		:	'fcvt.d.wu';

FMVS		:	'fmv.s';
FABSS		:	'fabs.s';
FNEGS		:	'fneg.s';
FMVD		:	'fmv.d';
FABSD		:   'fabs.d';
FNEGD		:   'fneg.d';
FRCSR		:   'frcsr';
FSCSR		:	'fscsr';
FRRM		:	'frrm';
FSRM		:	'fsrm';
FRFLAGS		:	'frflags';
FSFLAGS		:	'fsflags';



FCVTLS		:   'fcvt.l.s';
FCVTLUS		:	'fcvt.lu.s';
FCVTSL		:	'fcvt.s.l';
FCVTSLU		:	'fcvt.s.lu';

FCVTLD		:	'fcvt.l.d';
FCVTLUD		:	'fcvt.lu.d';
FMVXD		:   'fmv.x.d';
FCVTDL		:	'fcvt.d.l';
FCVTDLU		:	'fcvt.d.lu';
FMVDX		:   'fmv.d.x';
RDINSTRET	:   'rdinstret';
RDCYCLE		:	'rdcycle';
RDTIME		:	'rdtime';
RDINSTRETH	:   'rdinstreth';
RDCYCLEH	:	'rdcycleh';
RDTIMEH		:	'rdtimeh';

VSETVLI			:   'vsetvli';
VSETIVLI		:   'vsetivli';
VSETVL			:   'vsetvl';

VLE8V			:   'vle8.v';
VLE16V			:   'vle16.v';
VLE32V			:   'vle32.v';
VLE64V			:   'vle64.v';

VSE8V			:   'vse8.v';
VSE16V			:   'vse16.v';
VSE32V			:   'vse32.v';
VSE64V			:   'vse64.v';

VLMV			:   'vlm.v';
VSMV			:   'vsm.v';

VLSE8V			:   'vlse8.v';
VLSE16V			:   'vlse16.v';
VLSE32V			:   'vlse32.v';
VLSE64V			:   'vlse64.v';
	
VSSE8V			:   'vsse8.v';
VSSE16V			:   'vsse16.v';
VSSE32V			:   'vsse32.v';
VSSE64V			:   'vsse64.v';

VLUXEI8V		:   'vluxei8.v';
VLUXEI16V		:   'vluxei16.v';
VLUXEI32V		:   'vluxei32.v';
VLUXEI64V		:   'vluxei64.v';

VLOXEI8V		:   'vloxei8.v';
VLOXEI16V		:   'vloxei16.v';
VLOXEI32V		:   'vloxei32.v';
VLOXEI64V		:   'vloxei64.v';

VSUXEI8V		:   'vsuxei8.v';
VSUXEI16V		:   'vsuxei16.v';
VSUXEI32V		:   'vsuxei32.v';
VSUXEI64V		:   'vsuxei64.v';

VSUXEIVSOXEI8V	:   'vsuxeivsoxei8.v';
VSUXEIVSOXEI16V	:   'vsuxeivsoxei16.v';
VSUXEIVSOXEI32V	:   'vsuxeivsoxei32.v';
VSUXEIVSOXEI64V :   'vsuxeivsoxei64.v';

VLE8FFV			:   'vle8ff.v';
VLE16FFV		:   'vle16ff.v';
VLE32FFV		:   'vle32ff.v';
VLE64FFV		:   'vle64ff.v';

VLSEG1E8V		:   'vlseg1e8.v';
VLSEG2E8V		:   'vlseg2e8.v';
VLSEG3E8V		:   'vlseg3e8.v';
VLSEG4E8V		:   'vlseg4e8.v';
VLSEG5E8V		:   'vlseg5e8.v';
VLSEG6E8V		:   'vlseg6e8.v';
VLSEG7E8V		:   'vlseg7e8.v';
VLSEG8E8V		:   'vlseg8e8.v';

VSSEG1E8V		:   'vsseg1e8.v';
VSSEG2E8V		:   'vsseg2e8.v';
VSSEG3E8V		:   'vsseg3e8.v';
VSSEG4E8V		:   'vsseg4e8.v';
VSSEG5E8V		:   'vsseg5e8.v';
VSSEG6E8V		:   'vsseg6e8.v';
VSSEG7E8V		:   'vsseg7e8.v';
VSSEG8E8V		:   'vsseg8e8.v';

VLSEG1E16V		:	'vlseg1e16.v';
VLSEG2E16V		:   'vlseg2e16.v';
VLSEG3E16V		:   'vlseg3e16.v';
VLSEG4E16V		:   'vlseg4e16.v';
VLSEG5E16V		:   'vlseg5e16.v';
VLSEG6E16V		:   'vlseg6e16.v';
VLSEG7E16V		:   'vlseg7e16.v';
VLSEG8E16V		:   'vlseg8e16.v';

VSSEG1E16V		:   'vsseg1e16.v';
VSSEG2E16V		:   'vsseg2e16.v';
VSSEG3E16V		:   'vsseg3e16.v';
VSSEG4E16V		:   'vsseg4e16.v';
VSSEG5E16V		:   'vsseg5e16.v';
VSSEG6E16V		:   'vsseg6e16.v';
VSSEG7E16V		:   'vsseg7e16.v';
VSSEG8E16V		:   'vsseg8e16.v';

VLSEG1E32V		:   'vlseg1e32.v';
VLSEG2E32V		:   'vlseg2e32.v';
VLSEG3E32V		:   'vlseg3e32.v';
VLSEG4E32V		:   'vlseg4e32.v';
VLSEG5E32V		:   'vlseg5e32.v';
VLSEG6E32V		:   'vlseg6e32.v';
VLSEG7E32V		:   'vlseg7e32.v';
VLSEG8E32V		:   'vlseg8e32.v';

VSSEG1E32V		:   'vsseg1e32.v';
VSSEG2E32V		:   'vsseg2e32.v';
VSSEG3E32V		:   'vsseg3e32.v';
VSSEG4E32V		:   'vsseg4e32.v';
VSSEG5E32V		:   'vsseg5e32.v';
VSSEG6E32V		:   'vsseg6e32.v';
VSSEG7E32V		:   'vsseg7e32.v';
VSSEG8E32V		:   'vsseg8e32.v';

VLSEG1E64V		:   'vlseg1e64.v';
VLSEG2E64V		:   'vlseg2e64.v';
VLSEG3E64V		:   'vlseg3e64.v';
VLSEG4E64V		:   'vlseg4e64.v';
VLSEG5E64V		:   'vlseg5e64.v';
VLSEG6E64V		:   'vlseg6e64.v';
VLSEG7E64V		:   'vlseg7e64.v';
VLSEG8E64V		:   'vlseg8e64.v';

VSSEG1E64V		:   'vsseg1e64.v';
VSSEG2E64V		:   'vsseg2e64.v';
VSSEG3E64V		:   'vsseg3e64.v';
VSSEG4E64V		:   'vsseg4e64.v';
VSSEG5E64V		:   'vsseg5e64.v';
VSSEG6E64V		:   'vsseg6e64.v';
VSSEG7E64V		:   'vsseg7e64.v';
VSSEG8E64V		:   'vsseg8e64.v';

VLSSEG1E8V		:   'vlsseg1e8.v';
VLSSEG2E8V		:   'vlsseg2e8.v';
VLSSEG3E8V		:   'vlsseg3e8.v';
VLSSEG4E8V		:   'vlsseg4e8.v';
VLSSEG5E8V		:   'vlsseg5e8.v';
VLSSEG6E8V		:   'vlsseg6e8.v';
VLSSEG7E8V		:   'vlsseg7e8.v';
VLSSEG8E8V		:   'vlsseg8e8.v';

VSSSEG1E8V		:   'vssseg1e8.v';
VSSSEG2E8V		:   'vssseg2e8.v';
VSSSEG3E8V		:   'vssseg3e8.v';
VSSSEG4E8V		:   'vssseg4e8.v';
VSSSEG5E8V		:   'vssseg5e8.v';
VSSSEG6E8V		:   'vssseg6e8.v';
VSSSEG7E8V		:   'vssseg7e8.v';
VSSSEG8E8V		:   'vssseg8e8.v';

VLSSEG1E16V		:   'vlsseg1e16.v';
VLSSEG2E16V     :   'vlsseg2e16.v';
VLSSEG3E16V     :   'vlsseg3e16.v';
VLSSEG4E16V     :   'vlsseg4e16.v';
VLSSEG5E16V     :   'vlsseg5e16.v';
VLSSEG6E16V     :   'vlsseg6e16.v';
VLSSEG7E16V     :   'vlsseg7e16.v';
VLSSEG8E16V     :   'vlsseg8e16.v';

VSSSEG1E16V     :   'vssseg1e16.v';
VSSSEG2E16V     :   'vssseg2e16.v';
VSSSEG3E16V     :   'vssseg3e16.v';
VSSSEG4E16V     :   'vssseg4e16.v';
VSSSEG5E16V     :   'vssseg5e16.v';
VSSSEG6E16V     :   'vssseg6e16.v';
VSSSEG7E16V     :   'vssseg7e16.v';
VSSSEG8E16V     :   'vssseg8e16.v';

VLSSEG1E32V     :   'vlsseg1e32.v';
VLSSEG2E32V     :   'vlsseg2e32.v';
VLSSEG3E32V     :   'vlsseg3e32.v';
VLSSEG4E32V     :   'vlsseg4e32.v';
VLSSEG5E32V     :   'vlsseg5e32.v';
VLSSEG6E32V     :   'vlsseg6e32.v';
VLSSEG7E32V     :   'vlsseg7e32.v';
VLSSEG8E32V     :   'vlsseg8e32.v';

VSSSEG1E32V     :   'vssseg1e32.v';
VSSSEG2E32V     :   'vssseg2e32.v';
VSSSEG3E32V     :   'vssseg3e32.v';
VSSSEG4E32V     :   'vssseg4e32.v';
VSSSEG5E32V     :   'vssseg5e32.v';
VSSSEG6E32V     :   'vssseg6e32.v';
VSSSEG7E32V     :   'vssseg7e32.v';
VSSSEG8E32V     :   'vssseg8e32.v';

VLSSEG1E64V     :   'vlsseg1e64.v';
VLSSEG2E64V     :   'vlsseg2e64.v';
VLSSEG3E64V     :   'vlsseg3e64.v';
VLSSEG4E64V     :   'vlsseg4e64.v';
VLSSEG5E64V     :   'vlsseg5e64.v';
VLSSEG6E64V     :   'vlsseg6e64.v';
VLSSEG7E64V     :   'vlsseg7e64.v';
VLSSEG8E64V     :   'vlsseg8e64.v';

VSSSEG1E64V     :   'vssseg1e64.v';
VSSSEG2E64V     :   'vssseg2e64.v';
VSSSEG3E64V     :   'vssseg3e64.v';
VSSSEG4E64V     :   'vssseg4e64.v';
VSSSEG5E64V     :   'vssseg5e64.v';
VSSSEG6E64V     :   'vssseg6e64.v';
VSSSEG7E64V     :   'vssseg7e64.v';
VSSSEG8E64V     :   'vssseg8e64.v';

VLUXSEG1EI8V    :   'vluxseg1ei8.v';
VLUXSEG2EI8V    :   'vluxseg2ei8.v';
VLUXSEG3EI8V    :   'vluxseg3ei8.v';
VLUXSEG4EI8V    :   'vluxseg4ei8.v';
VLUXSEG5EI8V    :   'vluxseg5ei8.v';
VLUXSEG6EI8V    :   'vluxseg6ei8.v';
VLUXSEG7EI8V    :   'vluxseg7ei8.v';
VLUXSEG8EI8V    :   'vluxseg8ei8.v';

VLOXSEG1EI8V    :   'vloxseg1ei8.v';
VLOXSEG2EI8V    :   'vloxseg2ei8.v';
VLOXSEG3EI8V    :   'vloxseg3ei8.v';
VLOXSEG4EI8V    :   'vloxseg4ei8.v';
VLOXSEG5EI8V    :   'vloxseg5ei8.v';
VLOXSEG6EI8V    :   'vloxseg6ei8.v';
VLOXSEG7EI8V    :   'vloxseg7ei8.v';
VLOXSEG8EI8V    :   'vloxseg8ei8.v';

VSUXSEG1EI8V    :   'vsuxseg1ei8.v';
VSUXSEG2EI8V    :   'vsuxseg2ei8.v';
VSUXSEG3EI8V    :   'vsuxseg3ei8.v';
VSUXSEG4EI8V    :   'vsuxseg4ei8.v';
VSUXSEG5EI8V    :   'vsuxseg5ei8.v';
VSUXSEG6EI8V    :   'vsuxseg6ei8.v';
VSUXSEG7EI8V    :   'vsuxseg7ei8.v';
VSUXSEG8EI8V    :   'vsuxseg8ei8.v';

VSOXSEG1EI8V    :   'vsoxseg1ei8.v';
VSOXSEG2EI8V    :   'vsoxseg2ei8.v';
VSOXSEG3EI8V    :   'vsoxseg3ei8.v';
VSOXSEG4EI8V    :   'vsoxseg4ei8.v';
VSOXSEG5EI8V    :   'vsoxseg5ei8.v';
VSOXSEG6EI8V    :   'vsoxseg6ei8.v';
VSOXSEG7EI8V    :   'vsoxseg7ei8.v';
VSOXSEG8EI8V    :   'vsoxseg8ei8.v';

VLUXSEG1EI16V   :   'vluxseg1ei16.v';
VLUXSEG2EI16V   :   'vluxseg2ei16.v';
VLUXSEG3EI16V   :   'vluxseg3ei16.v';
VLUXSEG4EI16V   :   'vluxseg4ei16.v';
VLUXSEG5EI16V   :   'vluxseg5ei16.v';
VLUXSEG6EI16V   :   'vluxseg6ei16.v';
VLUXSEG7EI16V   :   'vluxseg7ei16.v';
VLUXSEG8EI16V   :   'vluxseg8ei16.v';

VLOXSEG1EI16V   :   'vloxseg1ei16.v';
VLOXSEG2EI16V   :   'vloxseg2ei16.v';
VLOXSEG3EI16V   :   'vloxseg3ei16.v';
VLOXSEG4EI16V   :   'vloxseg4ei16.v';
VLOXSEG5EI16V   :   'vloxseg5ei16.v';
VLOXSEG6EI16V   :   'vloxseg6ei16.v';
VLOXSEG7EI16V   :   'vloxseg7ei16.v';
VLOXSEG8EI16V   :   'vloxseg8ei16.v';

VSUXSEG1EI16V   :   'vsuxseg1ei16.v';
VSUXSEG2EI16V   :   'vsuxseg2ei16.v';
VSUXSEG3EI16V   :   'vsuxseg3ei16.v';
VSUXSEG4EI16V   :   'vsuxseg4ei16.v';
VSUXSEG5EI16V   :   'vsuxseg5ei16.v';
VSUXSEG6EI16V   :   'vsuxseg6ei16.v';
VSUXSEG7EI16V   :   'vsuxseg7ei16.v';
VSUXSEG8EI16V   :   'vsuxseg8ei16.v';

VSOXSEG1EI16V   :   'vsoxseg1ei16.v';
VSOXSEG2EI16V   :   'vsoxseg2ei16.v';
VSOXSEG3EI16V   :   'vsoxseg3ei16.v';
VSOXSEG4EI16V   :   'vsoxseg4ei16.v';
VSOXSEG5EI16V   :   'vsoxseg5ei16.v';
VSOXSEG6EI16V   :   'vsoxseg6ei16.v';
VSOXSEG7EI16V   :   'vsoxseg7ei16.v';
VSOXSEG8EI16V   :   'vsoxseg8ei16.v';

VLUXSEG1EI32V   :   'vluxseg1ei32.v';
VLUXSEG2EI32V   :   'vluxseg2ei32.v';
VLUXSEG3EI32V   :   'vluxseg3ei32.v';
VLUXSEG4EI32V   :   'vluxseg4ei32.v';
VLUXSEG5EI32V   :   'vluxseg5ei32.v';
VLUXSEG6EI32V   :   'vluxseg6ei32.v';
VLUXSEG7EI32V   :   'vluxseg7ei32.v';
VLUXSEG8EI32V   :   'vluxseg8ei32.v';

VLOXSEG1EI32V   :   'vloxseg1ei32.v';
VLOXSEG2EI32V   :   'vloxseg2ei32.v';
VLOXSEG3EI32V   :   'vloxseg3ei32.v';
VLOXSEG4EI32V   :   'vloxseg4ei32.v';
VLOXSEG5EI32V   :   'vloxseg5ei32.v';
VLOXSEG6EI32V   :   'vloxseg6ei32.v';
VLOXSEG7EI32V   :   'vloxseg7ei32.v';
VLOXSEG8EI32V   :   'vloxseg8ei32.v';

VSUXSEG1EI32V   :   'vsuxseg1ei32.v';
VSUXSEG2EI32V   :   'vsuxseg2ei32.v';
VSUXSEG3EI32V   :   'vsuxseg3ei32.v';
VSUXSEG4EI32V   :   'vsuxseg4ei32.v';
VSUXSEG5EI32V   :   'vsuxseg5ei32.v';
VSUXSEG6EI32V   :   'vsuxseg6ei32.v';
VSUXSEG7EI32V   :   'vsuxseg7ei32.v';
VSUXSEG8EI32V   :   'vsuxseg8ei32.v';

VSOXSEG1EI32V   :   'vsoxseg1ei32.v';
VSOXSEG2EI32V   :   'vsoxseg2ei32.v';
VSOXSEG3EI32V   :   'vsoxseg3ei32.v';
VSOXSEG4EI32V   :   'vsoxseg4ei32.v';
VSOXSEG5EI32V   :   'vsoxseg5ei32.v';
VSOXSEG6EI32V   :   'vsoxseg6ei32.v';
VSOXSEG7EI32V   :   'vsoxseg7ei32.v';
VSOXSEG8EI32V   :   'vsoxseg8ei32.v';

VLUXSEG1EI64V   :   'vluxseg1ei64.v';
VLUXSEG2EI64V   :   'vluxseg2ei64.v';
VLUXSEG3EI64V   :   'vluxseg3ei64.v';
VLUXSEG4EI64V   :   'vluxseg4ei64.v';
VLUXSEG5EI64V   :   'vluxseg5ei64.v';
VLUXSEG6EI64V   :   'vluxseg6ei64.v';
VLUXSEG7EI64V   :   'vluxseg7ei64.v';
VLUXSEG8EI64V   :   'vluxseg8ei64.v';

VLOXSEG1EI64V   :   'vloxseg1ei64.v';
VLOXSEG2EI64V   :   'vloxseg2ei64.v';
VLOXSEG3EI64V   :   'vloxseg3ei64.v';
VLOXSEG4EI64V   :   'vloxseg4ei64.v';
VLOXSEG5EI64V   :   'vloxseg5ei64.v';
VLOXSEG6EI64V   :   'vloxseg6ei64.v';
VLOXSEG7EI64V   :   'vloxseg7ei64.v';
VLOXSEG8EI64V	:   'vloxseg8ei64.v';

VSUXSEG1EI64V   :   'vsuxseg1ei64.v';
VSUXSEG2EI64V   :   'vsuxseg2ei64.v';
VSUXSEG3EI64V   :   'vsuxseg3ei64.v';
VSUXSEG4EI64V   :   'vsuxseg4ei64.v';
VSUXSEG5EI64V   :   'vsuxseg5ei64.v';
VSUXSEG6EI64V   :   'vsuxseg6ei64.v';
VSUXSEG7EI64V   :   'vsuxseg7ei64.v';
VSUXSEG8EI64V   :   'vsuxseg8ei64.v';

VSOXSEG1EI64V   :   'vsoxseg1ei64.v';
VSOXSEG2EI64V   :   'vsoxseg2ei64.v';
VSOXSEG3EI64V   :   'vsoxseg3ei64.v';
VSOXSEG4EI64V   :   'vsoxseg4ei64.v';
VSOXSEG5EI64V   :   'vsoxseg5ei64.v';
VSOXSEG6EI64V   :   'vsoxseg6ei64.v';
VSOXSEG7EI64V   :   'vsoxseg7ei64.v';
VSOXSEG8EI64V	:   'vsoxseg8ei64.v';

VL1RV			:   'vl1r.v';
VL1RE8V			:   'vl1re8.v';
VL1RE16V		:   'vl1re16.v';
VL1RE32V		:   'vl1re32.v';
VL1RE64V		:   'vl1re64.v';

VL2RV			:   'vl2r.v';
VL2RE8V			:   'vl2re8.v';
VL2RE16V		:   'vl2re16.v';
VL2RE32V		:   'vl2re32.v';
VL2RE64V		:   'vl2re64.v';

VL4RV			:   'vl4r.v';
VL4RE8V			:   'vl4re8.v';
VL4RE16V		:   'vl4re16.v';
VL4RE32V		:   'vl4re32.v';
VL4RE64V		:   'vl4re64.v';

VL8RV			:   'vl8r.v';
VL8RE8V			:   'vl8re8.v';
VL8RE16V		:   'vl8re16.v';
VL8RE32V		:   'vl8re32.v';
VL8RE64V		:   'vl8re64.v';

VS1RV			:   'vs1r.v';
VS2RV			:   'vs2r.v';
VS4RV			:   'vs4r.v';
VS8RV			:   'vs8r.v';

VADDVV			:   'vadd.vv';
VADDVX			:   'vadd.vx';
VADDVI			:   'vadd.vi';

VSUBVV			:   'vsub.vv';
VSUBVX			:   'vsub.vx';

VRSUBVX			:   'vrsub.vx';
VRSUBVI			:   'vrsub.vi';
	
VWADDUVV		:   'vwaddu.vv';
VWADDUVX		:   'vwaddu.vx';

VWSUBUVV		:   'vwsubu.vv';
VWSUBUVX		:   'vwsubu.vx';

VWADDVV			:   'vwadd.vv';
VWADDVX			:   'vwadd.vx';

VWSUBVV			:   'vwsub.vv';
VWSUBVX			:   'vwsub.vx';

VWADDUWV		:   'vwaddu.wv';
VWADDUWX		:   'vwaddu.wx';

VWSUBUWV		:   'vwsubu.wv';
VWSUBUWX		:   'vwsubu.wx';

VWADDWV			:   'vwadd.wv';
VWADDWX			:   'vwadd.wx';

VWSUBWV			:   'vwsub.wv';
VWSUBWX			:   'vwsub.wx';

FLQ			:	'flq';
FSQ			:   'fsq';
FMADDQ		:	'fmadd.q';
FMSUBQ		:   'fmsub.q';
FNMSUBQ		:   'fnmsub.q';
FNMADDQ		:	'fnmadd.q';
FADDQ		:   'fadd.q';
FSUBQ		:	'fsub.q';
FMULQ		:	'fmul.q';
FDIVQ		:   'fdiv.q';
FSQRTQ		:	'fsqrt.q';
FSGNJQ		:	'fsgnj.q';
FSGNJNQ		:	'fsgnjn.q';
FSGNJXQ		:	'fsgnjx.q';
FMINQ		:	'fmin.q';
FMAXQ		:	'fmax.q';
FCVTSQ		:	'fcvt.s.q';
FCVTQS		:	'fcvt.q.s';
FCVTDQ		:	'fcvt.d.q';
FCVTQD		:	'fcvt.q.d';
FEQQ		:   'feq.q';
FLTQ		:	'flt.q';
FLEQ		:	'fle.q';
FCLASSQ		:   'fclass.q';
FCVTWQ		:   'fcvt.w.q';
FCVTWUQ		:	'fcvt.wu.q';
FCVTQW		:	'fcvt.q.w';
FCVTQWU		:	'fcvt.q.wu';
FCVTLQ		:	'fcvt.l.q';
FCVTLUQ		:	'fcvt.lu.q';
FCVTQL		:	'fcvt.q.l';
FCVTQLU		:	'fcvt.q.lu';

FILENAME		:	[a-zA-Z0-9]+ ('.s' | '.asm');

IORW			:	[iorw]+;
IDENTIFIER		:	[a-zA-Z0-9]+;

INSTRUCTION		:	.;
REGISTERS		:	.;
MACRO			:	.;
LINENUMBER		:	.;
ADDRESS			:	.;
BYTE			:	.;

