parser grammar RISCVAssemblerParser;
options { tokenVocab=RISCVAssemblerLexer; }

@header {
	import hk.quantr.javalib.CommonLib;
	import hk.quantr.assembler.riscv.RISCVEncoder;
	import hk.quantr.assembler.riscv.Label;
	import hk.quantr.antlrcalculatorlibrary.CalculatorLibrary;
	import hk.quantr.assembler.print.MessageHandler;
}

@parser::members{
	public String arch;
	public RISCVEncoder encoder = new RISCVEncoder();
	public ArrayList<Label> labels=new ArrayList<>();
	public long address=0l;
	private String section;
	private String label;
}


assemble				:	lines EOF
						;

lines					:	line*
						;

line					:	instructions
						|	section
						|	label
						|	comment
						|	macro
						|	error
						;

//Instructions Set
instructions			:	rv32i
						|	rv64i
						|	rva
						|	rvc
						|	rvm
						|	rv32f
						|	rv64f
						|	rv32q
						|	pause
						|	special
						|	system
						|	pseudoinstructions
						|	vector
						;

section					:	DOT sectionName=IDENTIFIER IDENTIFIER?	{section=$sectionName.getText(); encoder.addListingNonCode($sectionName.line, $text);};                                                                                                                                    

label					:	labelName=IDENTIFIER COLON {label=$labelName.getText(); encoder.addListingNonCode($labelName.line, $text);};

comment					:	LINE_COMMENT;

macro					:	define
						|	ifdef
						|	include
						|	dotbyte
						|	dothalf
						|	dotword
						|	dotdword
						|	dotstring
						;

dotInstructions			:	dotbyte
						|	dothalf
						|	dotword
						|	dotdword
						|	dotstring
						;

dotbyte					:   (TIMES expression=MATH_EXPRESSION (MIN_ DOLLAR)?)? DOTBYTE  eject_byte+=MATH_EXPRESSION (COMMA eject_byte+=MATH_EXPRESSION)* {address+=encoder.ejectByte(    $text,  $DOTBYTE.line,  section, address, ($expression.text != null)? $expression.text:null, ($DOLLAR.text != null)? $DOLLAR.text:null, $eject_byte);};
dothalf					:	(TIMES expression=MATH_EXPRESSION (MIN_ DOLLAR)?)? DOTHALF  eject_byte+=MATH_EXPRESSION (COMMA eject_byte+=MATH_EXPRESSION)* {address+=encoder.ejectHalfByte($text,  $DOTHALF.line,  section, address, ($expression.text != null)? $expression.text:null, ($DOLLAR.text != null)? $DOLLAR.text:null, $eject_byte);};
dotword					:	(TIMES expression=MATH_EXPRESSION (MIN_ DOLLAR)?)? DOTWORD  eject_byte+=MATH_EXPRESSION (COMMA eject_byte+=MATH_EXPRESSION)* {address+=encoder.ejectWordByte($text,  $DOTWORD.line,  section, address, ($expression.text != null)? $expression.text:null, ($DOLLAR.text != null)? $DOLLAR.text:null, $eject_byte);};
dotdword				:	(TIMES expression=MATH_EXPRESSION (MIN_ DOLLAR)?)? DOTDWORD eject_byte+=MATH_EXPRESSION (COMMA eject_byte+=MATH_EXPRESSION)* {address+=encoder.ejectDwordByte($text, $DOTDWORD.line, section, address, ($expression.text != null)? $expression.text:null, ($DOLLAR.text != null)? $DOLLAR.text:null, $eject_byte);};
dotstring				:	(TIMES expression=MATH_EXPRESSION (MIN_ DOLLAR)?)? DOTSTRING IDENTIFIER {address+=encoder.ejectString($text, $DOTSTRING.line, section, ($expression.text != null)? $expression.text:null, ($DOLLAR.text != null)? $DOLLAR.text:null, address);};

include					:	INCLUDE DOUBLE_QUOTATION fileName=FILENAME DOUBLE_QUOTATION;
define					:	DEFINE variable=IDENTIFIER assignValue=MATH_EXPRESSION;
ifdef					:	IFDEF macroName=IDENTIFIER lines (ELIF macroName=IDENTIFIER lines)? (ELSE lines)? ENDIF;

error					:	offendingtoken=IDENTIFIER (IDENTIFIER | registers | fp_r | v_registers | vcsr_registers | rmt | rvc_registers |frvc_registers| csr_registers | COMMA | MATH_EXPRESSION)* { MessageHandler.errorPrintln("Syntax Error", "Unrecognized Syntax", $offendingtoken.line, $offendingtoken.getStartIndex(), ($IDENTIFIER!=null)?$IDENTIFIER.getStopIndex():$offendingtoken.getStopIndex(), $text);};

rv32i					:	arithmetic32
						|	loads32
						|	stores32
						|	logical
						|	compare
						|	branches
						|	jump
						|	fence
						|	fencei
						|	environment
						|	csr_access
						|	trap_redirect
						|	interrupt
						|	sfencevm
						;

rv64i					:	shifts
						|	arithmetic64
						|	loads64
						|	stores64
						;

rva						:	atomic32
						|	atomic64
						;

rvc						:	compressed
						;

rvm						:	multiply_divide32
						|	multiply_divide64
						;

rv32f					:	flw
						|	fsw
						|	fmadds
						|	fmsubs
						|	fnmadds
						|	fnmsubs
						|	fadds		
						|	fsubs		
						|	fmuls		
						|	fdivs		
						|	fsqrts		
						|	fsgnjs		
						|	fsgnjns		
						|	fsgnjxs		
						|	fmins		
						|	fmaxs		
						|	fcvtws		
						|	fcvtwus		
						|	fmvxw		
						|	feqs	
						|	flts		
						|	fles		
						|	fclasss		
						|	fcvtsw		
						|	fcvtswu		
						|	fmvwx
						|	fcvtls		
						|	fcvtlus		
						|	fcvtsl	
						|	fcvtslu		
						;

rv64f					:	fld
						|	fsd
						|	fmaddd
						|	fmsubd
						|	fnmaddd
						|	fnmsubd
						|	faddd
						|	fsubd		
						|	fmuld		
						|	fdivd		
						|	fsqrtd		
						|	fsgnjd		
						|	fsgnjnd		
						|	fsgnjxd		
						|	fmind		
						|	fmaxd		
						|	fcvtsd		
						|	fcvtds
						|	feqd	
						|	fltd		
						|	fled		
						|	fclassd
						|	fcvtwd
						|	fcvtwud
						|	fcvtdw
						|	fcvtdwu
						|	fcvtld		
						|	fcvtlud		
						|	fcvtdl	
						|	fcvtdlu
						|	fmvxd
						|	fmvdx
						;

rv32q					:	flq
						|	fsq
						|	fmaddq
						|	fmsubq
						|	fnmaddq
						|	fnmsubq
						|	faddq
						|	fsubq		
						|	fmulq		
						|	fdivq		
						|	fsqrtq		
						|	fsgnjq		
						|	fsgnjnq		
						|	fsgnjxq		
						|	fminq		
						|	fmaxq		
						|	fcvtsq		
						|	fcvtqs
						|	fcvtdq
						|	fcvtqd
						|	feqq	
						|	fltq		
						|	fleq		
						|	fclassq
						|	fcvtwq
						|	fcvtwuq
						|	fcvtqw
						|	fcvtqwu
						|	fcvtlq		
						|	fcvtluq		
						|	fcvtql	
						|	fcvtqlu
						;
						

shifts					:	sll
						|	slli
						|	slliw
						|	sllw
						|	srl
						|	srli
						|	srliw
						|	srlw
						|	sra
						|	srai
						|	sraiw
						|	sraw
						;

atomic32				:	lrww
						|	scww
						|	amoswapww
						|	amoaddww
						|	amoxorww
						|	amoandww
						|	amoorww
						|	amominww		
						|	amomaxww
						|	amominuww
						|	amomaxuww
						;

atomic64				:	lrdd
						|	scdd
						|	amoswapdd
						|	amoadddd
						|	amoxordd
						|	amoanddd
						|	amoordd
						|	amomindd		
						|	amomaxdd
						|	amominudd	
						|	amomaxudd
						;

arithmetic32			:	add
						|	addi
						|	sub
						|	neg
						|	lui
						|	auipc
						;

arithmetic64			:	addw
						|	addiw
						|	sextw
						|	subw
						|	negw
						;

logical					:	xor
						|	xori
						|	or
						|	ori
						|	and
						|	andi
						;

compare					:	slt
						|	slti
						|	sltu
						|	snez
						|	sltiu
						|	sltz
						|	seqz
						|	sgtz
						;

branches				:	beq
						|	bne
						|	blt
						|	bge
						|	bltu
						|	bgeu
						|	beqz
						|	bnez		
						|	blez		
						|	bgez		
						|	bltz		
						|	bgtz	
						|	bgt
						|	ble
						|	bgtu
						|	bleu
						;

jump					:	jal
						|	jalr
						;

environment				:	ecall
						|	ebreak
						|	eret
						;

compressed				:	cloads
						|	cstores
						|	carithmetic
						|	cshifts
						|	cbranches
						|	cjump
						|	cjumpAndLink
						|	csystem
						;

csr_access				:	csrrw
						|	csrrs
						|	csrrc
						|	csrrwi
						|	csrrsi
						|	csrrci
						|	csrc
						|	csrci
						|	csrr
						|	csrw
						|	csrwi
						|	csrs
						|	csrsi
						;

trap_redirect			:	uret
						|	sret
						|	hret
						|	mret
						;

interrupt				:	wfi
						;

loads32					:	lb
						|	lh
						|	lw
						|	li
						|	lbu
						|	lhu
						;

loads64					:	lwu
						|	ld
						;

stores32				:	sb
						|	sh
						|	sw
						|	sd
						;

stores64				:	sd
						;

lrww					:	lrw
						|	lrwaq
						|	lrwrl
						|	lrwaqrl
						;

scww					:	scw
						|	scwaq
						|	scwrl
						|	scwaqrl
						;

pseudoinstructions		:	j
						|	jr
						|	mv
						|	ret
						|	call
						|	tail
						|	fmvs
						|	fabss
						|	fnegs
						|	fmvd
						|	fabsd
						|	fnegd
						|	frcsr
						|	fscsr
						|	frrm
						|	fsrm
						|	frflags
						|	fsflags
						;

special					:	rdinstret
						|	rdcycle
						|	rdtime
						|	rdinstreth
						|	rdcycleh
						|	rdtimeh
						;

vector					:	vle
						|	vse
						|	vadd
						|	vsub
						|	vrsub
						|	vwaddu
						|	vwsubu
						|	vwadd
						|	vwsub
						|	vset
						;


vset					:	vsetvli
						|	vsetivli
						|	vsetvl
						;

vle						:	vle8v
						|	vle16v
						|	vle32v
						|	vle64v
						;

vse						:	vse8v
						|	vse16v
						|	vse32v
						|	vse64v
						;      

vadd					:	vaddvv
						|	vaddvx
						|	vaddvi
						;

vsub					:	vsubvv
						|	vsubvx
						;

vrsub					:	vrsubvx
						|	vrsubvi
						;

vwaddu					:	vwadduvv
						|	vwadduvx
						|	vwadduwv
						|	vwadduwx
						;

vwsubu					:	vwsubuvv
						|	vwsubuvx
						|	vwsubuwv
						|	vwsubuwx
						;

vwadd					:	vwaddvv
						|	vwaddvx
						|	vwaddwv
						|	vwaddwx
						;

vwsub					:	vwsubvv
						|	vwsubvx
						|	vwsubwv
						|	vwsubwx
						;

cloads					:	clw
						|	clwsp
						|	cld
						|	cldsp
						|	cfld
						|	cfldsp
						;

cstores					:	csw
						|	cswsp
						|	csd
						|	csdsp
						|	cfsd
						|	cfsdsp
						;

carithmetic				:	cadd
						|	caddi
						|	caddiw
						|	caddi16sp
						|	caddi4spn
                        |	caddw
						|	csub
						|	csubw
						|	cand
						|	candi
						|	cor
						|	cxor
						|	cmv
						|	cli
						|	clui
						;

cshifts					:	cslli
						|	csrai
						|	csrli
						;

cbranches				:	cbeqz
						|	cbnez
						;

cjump					:	cj
						|	cjr
						;

cjumpAndLink			:	cjal
						|	cjalr
						;

csystem					:	cebreak
						|	cnop;

multiply_divide32		:	mul
						|	mulh
						|	mulhsu
						|	mulhu
						|	div
						|	divu
						|	rem
						|	remu
						;

multiply_divide64		:	mulw
						|	divuw
						|	divw
						|	remuw
						|	remw
						;

system					:	not
						|	nop
						;

amoswapww				:	amoswapw
						|	amoswapwaq
						|	amoswapwrl
						|	amoswapwaqrl
						;

amoaddww				:	amoaddw
						|	amoaddwaq
						|	amoaddwrl
						|	amoaddwaqrl
						;

amoxorww				:	amoxorw
						|	amoxorwaq
						|	amoxorwrl
						|	amoxorwaqrl
						;

amoandww				:	amoandw
						|	amoandwaq
						|	amoandwrl
						|	amoandwaqrl
						;

amoorww					:	amoorw
						|	amoorwaq
						|	amoorwrl
						|	amoorwaqrl
						;

amominww				:	amominw
						|	amominwaq
						|	amominwrl
						|	amominwaqrl
						;

amomaxww				:	amomaxw
						|	amomaxwaq
						|	amomaxwrl
						|	amomaxwaqrl
						;

amominuww				:	amominuw
						|	amominuwaq
						|	amominuwrl
						|	amominuwaqrl
						;

amomaxuww				:	amomaxuw
						|	amomaxuwaq
						|	amomaxuwrl
						|	amomaxuwaqrl
						;

lrdd					:	lrd
						|	lrdaq
						|	lrdrl
						|	lrdaqrl
						;

scdd					:	scd
						|	scdaq
						|	scdrl
						|	scdaqrl
						;

amoswapdd				:	amoswapd
						|	amoswapdaq
						|	amoswapdrl
						|	amoswapdaqrl
						;

amoadddd				:	amoaddd
						|	amoadddaq
						|	amoadddrl
						|	amoadddaqrl
						;

amoxordd				:	amoxord
						|	amoxordaq
						|	amoxordrl
						|	amoxordaqrl
						;

amoanddd				:	amoandd
						|	amoanddaq
						|	amoanddrl
						|	amoanddaqrl
						;

amoordd					:	amoord
						|	amoordaq
						|	amoordrl
						|	amoordaqrl
						;

amomindd				:	amomind
						|	amomindaq
						|	amomindrl
						|	amomindaqrl
						;

amomaxdd				:	amomaxd
						|	amomaxdaq
						|	amomaxdrl
						|	amomaxdaqrl
						;

amominudd				:	amominud
						|	amominudaq
						|	amominudrl
						|	amominudaqrl
						;

amomaxudd				:	amomaxud
						|	amomaxudaq
						|	amomaxudrl
						|	amomaxudaqrl
						;

//Registers Set
rd						returns[int type]:	registers		{$type=$registers.type;};
rs1						returns[int type]:	registers		{$type=$registers.type;};
rs2						returns[int type]:	registers		{$type=$registers.type;};

frd						returns[int type]:	fp_r			{$type=$fp_r.type;};
frs1					returns[int type]:	fp_r			{$type=$fp_r.type;};
frs2					returns[int type]:	fp_r			{$type=$fp_r.type;};
frs3					returns[int type]:	fp_r			{$type=$fp_r.type;};

vd						returns[int type]:	v_registers		{$type=$v_registers.type;};
vs1						returns[int type]:	v_registers		{$type=$v_registers.type;};
vs2						returns[int type]:	v_registers		{$type=$v_registers.type;};
vs3						returns[int type]:	v_registers		{$type=$v_registers.type;};
vcsr					returns[int type]:	vcsr_registers	{$type=$vcsr_registers.type;};
vsew					returns[int type]:	vcsr_registers	{$type=$vcsr_registers.type;};
vlmul					returns[int type]:	vcsr_registers	{$type=$vcsr_registers.type;};
vta						returns[int type]:	vcsr_registers	{$type=$vcsr_registers.type;};
vma						returns[int type]:	vcsr_registers	{$type=$vcsr_registers.type;};

rm						returns[int type]:  rmt             {$type=$rmt.type;};
rd_apos					returns[int type]:	rvc_registers	{$type=$rvc_registers.type;};
frd_apos				returns[int type]:	frvc_registers	{$type=$frvc_registers.type;};

rs1_apos				returns[int type]:	rvc_registers	{$type=$rvc_registers.type;};
rs2_apos				returns[int type]:	rvc_registers	{$type=$rvc_registers.type;};
frs2_apos				returns[int type]:	frvc_registers	{$type=$frvc_registers.type;};

csr						returns[int type]:	csr_registers	{$type=$csr_registers.type;};

rvc_registers			returns[int type]
						:	S0				{$type=0;}
						|	S1				{$type=1;}
						|	A0				{$type=2;}
						|	A1				{$type=3;}
						|	A2				{$type=4;}
						|	A3				{$type=5;}
						|	A4				{$type=6;}
						|	A5				{$type=7;}
						|	X8				{$type=0;}
						|	X9				{$type=1;}
						|	X10				{$type=2;}
						|	X11				{$type=3;}
						|	X12				{$type=4;}
						|	X13				{$type=5;}
						|	X14				{$type=6;}
						|	X15				{$type=7;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

frvc_registers				returns[int type]
						:	F8				{$type=0;}
						|	F9				{$type=1;}
						|	F10				{$type=2;}
						|	F11				{$type=3;}
						|	F12				{$type=4;}
						|	F13				{$type=5;}
						|	F14				{$type=6;}
						|	F15				{$type=7;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

registers				returns[int type]
						:	X0				{$type=0;}
						|	X1				{$type=1;}
						|	X2				{$type=2;}
						|	X3				{$type=3;}
						|	X4				{$type=4;}
						|	X5				{$type=5;}
						|	X6				{$type=6;}
						|	X7				{$type=7;}
						|	X8				{$type=8;}
						|	X9				{$type=9;}
						|	X10				{$type=10;}
						|	X11				{$type=11;}
						|	X12				{$type=12;}
						|	X13				{$type=13;}
						|	X14				{$type=14;}
						|	X15				{$type=15;}
						|	X16				{$type=16;}
						|	X17				{$type=17;}
						|	X18				{$type=18;}
						|	X19				{$type=19;}
						|	X20				{$type=20;}
						|	X21				{$type=21;}
						|	X22				{$type=22;}
						|	X23				{$type=23;}
						|	X24				{$type=24;}
						|	X25				{$type=25;}
						|	X26				{$type=26;}
						|	X27				{$type=27;}
						|	X28				{$type=28;}
						|	X29				{$type=29;}
						|	X30				{$type=30;}
						|	X31				{$type=31;}
						|	ZERO			{$type=0;}
						|	RA				{$type=1;}
						|	SP				{$type=2;}
						|	GP				{$type=3;}
						|	TP				{$type=4;}
						|	T0				{$type=5;}
						|	T1				{$type=6;}
						|	T2				{$type=7;}
						|	S0				{$type=8;}
						|	FP				{$type=8;}
						|	S1				{$type=9;}
						|	A0				{$type=10;}
						|	A1				{$type=11;}
						|	A2				{$type=12;}
						|	A3				{$type=13;}
						|	A4				{$type=14;}
						|	A5				{$type=15;}
						|	A6				{$type=16;}
						|	A7				{$type=17;}
						|	S2				{$type=18;}
						|	S3				{$type=19;}
						|	S4				{$type=20;}
						|	S5				{$type=21;}
						|	S6				{$type=22;}
						|	S7				{$type=23;}
						|	S8				{$type=24;}
						|	S9				{$type=25;}
						|	S10				{$type=26;}
						|	S11				{$type=27;}
						|	T3				{$type=28;}
						|	T4				{$type=29;}
						|	T5				{$type=30;}
						|	T6				{$type=31;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

v_registers				returns[int type]
						:	V0				{$type=0;}
						|	V1				{$type=1;}
						|	V2				{$type=2;}
						|	V3				{$type=3;}
						|	V4				{$type=4;}
						|	V5				{$type=5;}
						|	V6				{$type=6;}
						|	V7				{$type=7;}
						|	V8				{$type=8;}
						|	V9				{$type=9;}
						|	V10				{$type=10;}
						|	V11				{$type=11;}
						|	V12				{$type=12;}
						|	V13				{$type=13;}
						|	V14				{$type=14;}
						|	V15				{$type=15;}
						|	V16				{$type=16;}
						|	V17				{$type=17;}
						|	V18				{$type=18;}
						|	V19				{$type=19;}
						|	V20				{$type=20;}
						|	V21				{$type=21;}
						|	V22				{$type=22;}
						|	V23				{$type=23;}
						|	V24				{$type=24;}
						|	V25				{$type=25;}
						|	V26				{$type=26;}
						|	V27				{$type=27;}
						|	V28				{$type=28;}
						|	V29				{$type=29;}
						|	V30				{$type=30;}
						|	V31				{$type=31;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

vcsr_registers			returns[int type]
						:   VTYPE			{$type=0;}
						|   VSEW			{$type=1;}
						|   VLMUL			{$type=2;}
						|   VTA				{$type=3;}
						|   VMA				{$type=4;}
						|   VL				{$type=5;}
						|   VLENB			{$type=6;}
						|   VSTART			{$type=7;}
						|   VXRM			{$type=8;}
						|   VXSAT			{$type=9;}
						|   VCSR			{$type=10;}
						|   E8				{$type=11;}
						|   E16				{$type=12;}
						|   E32				{$type=13;}
						|   E64				{$type=14;}
						|   MF8				{$type=15;}
						|   MF4				{$type=16;}
						|   MF2				{$type=17;}
						|   M1				{$type=18;}
						|   M2				{$type=19;}
						|   M4				{$type=20;}
						|   M8				{$type=21;}
						|   TA				{$type=22;}
						|   TU				{$type=23;}
						|   MA				{$type=24;}
						|   MU				{$type=25;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

rmt						returns[int type]
						:   RNE				{$type=0;}
						|	RTZ				{$type=1;}
						|   RDN				{$type=2;}
						|	RUP				{$type=3;}
						|   RMM				{$type=4;}
						|	DYN				{$type=7;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

fp_r					returns[int type]
						:	F0				{$type=0;}
						|	F1				{$type=1;}
						|	F2				{$type=2;}
						|	F3				{$type=3;}
						|	F4				{$type=4;}
						|	F5				{$type=5;}
						|	F6				{$type=6;}
						|	F7				{$type=7;}
						|	F8				{$type=8;}
						|	F9				{$type=9;}
						|	F10				{$type=10;}
						|	F11				{$type=11;}
						|	F12				{$type=12;}
						|	F13				{$type=13;}
						|	F14				{$type=14;}
						|	F15				{$type=15;}
						|	F16				{$type=16;}
						|	F17				{$type=17;}
						|	F18				{$type=18;}
						|	F19				{$type=19;}
						|	F20				{$type=20;}
						|	F21				{$type=21;}
						|	F22				{$type=22;}
						|	F23				{$type=23;}
						|	F24				{$type=24;}
						|	F25				{$type=25;}
						|	F26				{$type=26;}
						|	F27				{$type=27;}
						|	F28				{$type=28;}
						|	F29				{$type=29;}
						|	F30				{$type=30;}
						|	F31				{$type=31;}
						|	FT0				{$type=0;}
						|	FT1				{$type=1;}
						|	FT2				{$type=2;}
						|	FT3				{$type=3;}
						|	FT4				{$type=4;}
						|	FT5				{$type=5;}
						|	FT6				{$type=6;}
						|	FT7				{$type=7;}
						|	FS0				{$type=8;}
						|	FS1				{$type=9;}
						|	FA0				{$type=10;}
						|	FA1				{$type=11;}
						|	FA2				{$type=12;}
						|	FA3				{$type=13;}
						|	FA4				{$type=14;}
						|	FA5				{$type=15;}
						|	FA6				{$type=16;}
						|	FA7				{$type=17;}
						|	FS2				{$type=18;}
						|	FS3				{$type=19;}
						|	FS4				{$type=20;}
						|	FS5				{$type=21;}
						|	FS6				{$type=22;}
						|	FS7				{$type=23;}
						|	FS8				{$type=24;}
						|	FS9				{$type=25;}
						|	FS10			{$type=26;}
						|	FS11			{$type=27;}
						|	FT8				{$type=28;}
						|	FT9				{$type=29;}
						|	FT10			{$type=30;}
						|	FT11			{$type=31;}
						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

csr_registers			returns[int type]
						:	USTATUS			{$type =0x1000;}
						|	FFLAGS			{$type = 0x001;}
						|	FRM				{$type = 0x002;}
						|	FCSR			{$type = 0x003;}
						|	UIE				{$type = 0x004;}
						|	UTVEC			{$type = 0x005;}
						|	MSTATUS			{$type = 0x300;}
						|	MISA			{$type = 0x301;}
						|	MIE				{$type = 0x304;}
						|	MTVEC			{$type = 0x305;}
						|	MSCRATCH		{$type = 0x340;}
						|	MEPC			{$type = 0x341;}
						|	MCAUSE			{$type = 0x342;}
						|	MBADADDR		{$type = 0x343;}
						|	MIP				{$type = 0x344;}
						|	MCYCLE			{$type = 0xb00;}
						|	MCYCLEH			{$type = 0xb80;}
						|	MINSTRET		{$type = 0xb02;}
						|	MINSTRETH		{$type = 0xb82;}
						|	MVENDORID		{$type = 0xf11;}
						|	MARCHID			{$type = 0xf12;}
						|	MIMPID			{$type = 0xf13;}
						|	MHARTID			{$type = 0xf14;}
						|	USCRATCH		{$type = 0x040;}
						|	UEPC			{$type = 0x041;}
						|	UCAUSE			{$type = 0x042;}
						|	USCRATCH		{$type = 0x043;}
						|	UIP				{$type = 0x044;}
						|	CYCLE			{$type = 0xc00;}
						|	TIME			{$type = 0xc01;}
						|	INSTRET			{$type = 0xc02;}
						|	HPMCOUNTER3		{$type = 0xc03;}
						|	HPMCOUNTER4		{$type = 0xc04;}
						|	HPMCOUNTER5		{$type = 0xc05;}
						|	HPMCOUNTER6		{$type = 0xc06;}
						|	HPMCOUNTER7		{$type = 0xc07;}
						|	HPMCOUNTER8		{$type = 0xc08;}
						|	HPMCOUNTER9		{$type = 0xc09;}
						|	HPMCOUNTER10	{$type = 0xc0a;}
						|	HPMCOUNTER11	{$type = 0xc0b;}
						|	HPMCOUNTER12	{$type = 0xc0c;}
						|	HPMCOUNTER13	{$type = 0xc0d;}
						|	HPMCOUNTER14	{$type = 0xc0e;}
						|	HPMCOUNTER15	{$type = 0xc0f;}
						|	HPMCOUNTER16	{$type = 0xc10;}
						|	HPMCOUNTER17	{$type = 0xc11;}
						|	HPMCOUNTER18	{$type = 0xc12;}
						|	HPMCOUNTER19	{$type = 0xc13;}
						|	HPMCOUNTER20	{$type = 0xc14;}
						|	HPMCOUNTER21	{$type = 0xc15;}
						|	HPMCOUNTER22	{$type = 0xc16;}
						|	HPMCOUNTER23	{$type = 0xc17;}
						|	HPMCOUNTER24	{$type = 0xc18;}
						|	HPMCOUNTER25	{$type = 0xc19;}
						|	HPMCOUNTER26	{$type = 0xc1a;}
						|	HPMCOUNTER27	{$type = 0xc1b;}
						|	HPMCOUNTER28	{$type = 0xc1c;}
						|	HPMCOUNTER29	{$type = 0xc1d;}
						|	HPMCOUNTER30	{$type = 0xc1e;}
						|	HPMCOUNTER31	{$type = 0xc1f;}
						|	CYCLEH			{$type = 0xc80;}
						|	TIMEH			{$type = 0xc81;}
						|	INSTRETH		{$type = 0xc82;}
						|	HPMCOUNTER3H	{$type = 0xc83;}
						|	HPMCOUNTER4H	{$type = 0xc84;}
						|	HPMCOUNTER5H	{$type = 0xc85;}
						|	HPMCOUNTER6H	{$type = 0xc86;}
						|	HPMCOUNTER7H	{$type = 0xc87;}
						|	HPMCOUNTER8H	{$type = 0xc88;}
						|	HPMCOUNTER9H	{$type = 0xc89;}
						|	HPMCOUNTER10H	{$type = 0xc8a;}
						|	HPMCOUNTER11H	{$type = 0xc8b;}
						|	HPMCOUNTER12H	{$type = 0xc8c;}
						|	HPMCOUNTER13H	{$type = 0xc8d;}
						|	HPMCOUNTER14H	{$type = 0xc8e;}
						|	HPMCOUNTER15H	{$type = 0xc8f;}
						|	HPMCOUNTER16H	{$type = 0xc90;}
						|	HPMCOUNTER17H	{$type = 0xc91;}
						|	HPMCOUNTER18H	{$type = 0xc92;}
						|	HPMCOUNTER19H	{$type = 0xc93;}
						|	HPMCOUNTER20H	{$type = 0xc94;}
						|	HPMCOUNTER21H	{$type = 0xc95;}
						|	HPMCOUNTER22H	{$type = 0xc96;}
						|	HPMCOUNTER23H	{$type = 0xc97;}
						|	HPMCOUNTER24H	{$type = 0xc98;}
						|	HPMCOUNTER25H	{$type = 0xc99;}
						|	HPMCOUNTER26H	{$type = 0xc9a;}
						|	HPMCOUNTER27H	{$type = 0xc9b;}
						|	HPMCOUNTER28H	{$type = 0xc9c;}
						|	HPMCOUNTER29H	{$type = 0xc9d;}
						|	HPMCOUNTER30H	{$type = 0xc9e;}
						|	HPMCOUNTER31H	{$type = 0xc9f;}
						|	SSTATUS			{$type = 0x100;}
						|	SEDELEG			{$type = 0x102;}
						|	SIDELEG			{$type = 0x103;}
						|	SIE				{$type = 0x104;}
						|	STVEC			{$type = 0x105;}
						|	SSCRATCH		{$type = 0x140;}
						|	SEPC			{$type = 0x141;}
						|	SCAUSE			{$type = 0x142;}
						|	SBADADDR		{$type = 0x143;}
						|	SIP				{$type = 0x144;}
						|	SATP			{$type = 0x180;}
						|	HSTATUS			{$type = 0x200;}
						|	HEDELEG			{$type = 0x202;}
						|	HIDELEG			{$type = 0x203;}
						|	HIE				{$type = 0x204;}
						|	HTVEC			{$type = 0x205;}
						|	HSCRATCH		{$type = 0x240;}
						|	HEPC			{$type = 0x241;}
						|	HCAUSE			{$type = 0x242;}
						|	HBADADDR		{$type = 0x243;}
						|	HIP				{$type = 0x244;}
						//|	TBD				{$type = 0x280;}
						|	MBASE			{$type = 0x380;}
						|	MBOUND			{$type = 0x381;}
						|	MIBASE			{$type = 0x382;}
						|	MIBOUND			{$type = 0x383;}
						|	MDBASE			{$type = 0x384;}
						|	MDBOUND			{$type = 0x385;}
						|	MHPMCOUNTER3	{$type = 0xb03;}
						|	MHPMCOUNTER4	{$type = 0xb04;}
						|	MHPMCOUNTER5	{$type = 0xb05;}
						|	MHPMCOUNTER6	{$type = 0xb06;}
						|	MHPMCOUNTER7	{$type = 0xb07;}
						|	MHPMCOUNTER8	{$type = 0xb08;}
						|	MHPMCOUNTER9	{$type = 0xb09;}
						|	MHPMCOUNTER10	{$type = 0xb0a;}
						|	MHPMCOUNTER11	{$type = 0xb0b;}
						|	MHPMCOUNTER12	{$type = 0xb0c;}
						|	MHPMCOUNTER13	{$type = 0xb0d;}
						|	MHPMCOUNTER14	{$type = 0xb0e;}
						|	MHPMCOUNTER15	{$type = 0xb0f;}
						|	MHPMCOUNTER16	{$type = 0xb10;}
						|	MHPMCOUNTER17	{$type = 0xb11;}
						|	MHPMCOUNTER18	{$type = 0xb12;}
						|	MHPMCOUNTER19	{$type = 0xb13;}
						|	MHPMCOUNTER20	{$type = 0xb14;}
						|	MHPMCOUNTER21	{$type = 0xb15;}
						|	MHPMCOUNTER22	{$type = 0xb16;}
						|	MHPMCOUNTER23	{$type = 0xb17;}
						|	MHPMCOUNTER24	{$type = 0xb18;}
						|	MHPMCOUNTER25	{$type = 0xb19;}
						|	MHPMCOUNTER26	{$type = 0xb1a;}
						|	MHPMCOUNTER27	{$type = 0xb1b;}
						|	MHPMCOUNTER28	{$type = 0xb1c;}
						|	MHPMCOUNTER29	{$type = 0xb1d;}
						|	MHPMCOUNTER30	{$type = 0xb1e;}
						|	MHPMCOUNTER31	{$type = 0xb1f;}
						|	HPMCOUNTER3H	{$type = 0xb83;}
						|	HPMCOUNTER4H	{$type = 0xb84;}
						|	HPMCOUNTER5H	{$type = 0xb85;}
						|	HPMCOUNTER6H	{$type = 0xb86;}
						|	HPMCOUNTER7H	{$type = 0xb87;}
						|	HPMCOUNTER8H	{$type = 0xb88;}
						|	HPMCOUNTER9H	{$type = 0xb89;}
						|	HPMCOUNTER10H	{$type = 0xb8a;}
						|	HPMCOUNTER11H	{$type = 0xb8b;}
						|	HPMCOUNTER12H	{$type = 0xb8c;}
						|	HPMCOUNTER13H	{$type = 0xb8d;}
						|	HPMCOUNTER14H	{$type = 0xb8e;}
						|	HPMCOUNTER15H	{$type = 0xb8f;}
						|	HPMCOUNTER16H	{$type = 0xb90;}
						|	HPMCOUNTER17H	{$type = 0xb91;}
						|	HPMCOUNTER18H	{$type = 0xb92;}
						|	HPMCOUNTER19H	{$type = 0xb93;}
						|	HPMCOUNTER20H	{$type = 0xb94;}
						|	HPMCOUNTER21H	{$type = 0xb95;}
						|	HPMCOUNTER22H	{$type = 0xb96;}
						|	HPMCOUNTER23H	{$type = 0xb97;}
						|	HPMCOUNTER24H	{$type = 0xb98;}
						|	HPMCOUNTER25H	{$type = 0xb99;}
						|	HPMCOUNTER26H	{$type = 0xb9a;}
						|	HPMCOUNTER27H	{$type = 0xb9b;}
						|	HPMCOUNTER28H	{$type = 0xb9c;}
						|	HPMCOUNTER29H	{$type = 0xb9d;}
						|	HPMCOUNTER30H	{$type = 0xb9e;}
						|	HPMCOUNTER31H	{$type = 0xb9f;}
						|	MUCOUNTEREN		{$type = 0x320;}
						|	MSCOUNTEREN		{$type = 0x321;}
						|	MHCOUNTEREN		{$type = 0x322;}
						|	TSELECT			{$type = 0x7a0;}
						|	TDATA1			{$type = 0x7a1;}
						|	TDATA2			{$type = 0x7a2;}
						|	TDATA3			{$type = 0x7a3;}
						|	DCSR			{$type = 0x7b0;}
						|	DPC				{$type = 0x7b1;}
						|	DSCRATCH		{$type = 0x7b2;}
						|	MHPMEVENT3		{$type = 0x323;}
						|	MHPMEVENT4		{$type = 0x324;}
						|	MHPMEVENT5		{$type = 0x325;}
						|	MHPMEVENT6		{$type = 0x326;}
						|	MHPMEVENT7		{$type = 0x327;}
						|	MHPMEVENT8		{$type = 0x328;}
						|	MHPMEVENT9		{$type = 0x329;}
						|	MHPMEVENT10		{$type = 0x32a;}
						|	MHPMEVENT11		{$type = 0x32b;}
						|	MHPMEVENT12		{$type = 0x32c;}
						|	MHPMEVENT13		{$type = 0x32d;}
						|	MHPMEVENT14		{$type = 0x32e;}
						|	MHPMEVENT15		{$type = 0x32f;}
						|	MHPMEVENT16		{$type = 0x330;}
						|	MHPMEVENT17		{$type = 0x331;}
						|	MHPMEVENT18		{$type = 0x332;}
						|	MHPMEVENT19		{$type = 0x333;}
						|	MHPMEVENT20		{$type = 0x334;}
						|	MHPMEVENT21		{$type = 0x335;}
						|	MHPMEVENT22		{$type = 0x336;}
						|	MHPMEVENT23		{$type = 0x337;}
						|	MHPMEVENT24		{$type = 0x338;}
						|	MHPMEVENT25		{$type = 0x339;}
						|	MHPMEVENT26		{$type = 0x33a;}
						|	MHPMEVENT27		{$type = 0x33b;}
						|	MHPMEVENT28		{$type = 0x33c;}
						|	MHPMEVENT29		{$type = 0x33d;}
						|	MHPMEVENT30		{$type = 0x33e;}	
						|	MHPMEVENT31		{$type = 0x33f;}
						|	PMPCFG0			{$type = 0x3A0;}
						|	PMPCFG1			{$type = 0x3A1;}
						|	PMPCFG2			{$type = 0x3A2;}
						|	PMPCFG3			{$type = 0x3A3;}
						|	PMPADDR0		{$type = 0x3B0;}
						|	PMPADDR1		{$type = 0x3B1;}
						|	PMPADDR2		{$type = 0x3B2;}
						|	PMPADDR3		{$type = 0x3B3;}
						|	PMPADDR4		{$type = 0x3B4;}
						|	PMPADDR5		{$type = 0x3B5;}
						|	PMPADDR6		{$type = 0x3B6;}
						|	PMPADDR7		{$type = 0x3B7;}
						|	PMPADDR8		{$type = 0x3B8;}
						|	PMPADDR9		{$type = 0x3B9;}
						|	PMPADDR10		{$type = 0x3B10;}
						|	PMPADDR11		{$type = 0x3B11;}
						|	PMPADDR12		{$type = 0x3B12;}
						|	PMPADDR13		{$type = 0x3B13;}
						|	PMPADDR14		{$type = 0x3B14;}
						|	PMPADDR15		{$type = 0x3B15;}




						|	IDENTIFIER		{MessageHandler.errorPrintln("Syntax Error", "Unrecognized Register.", $IDENTIFIER.line, $IDENTIFIER.getStartIndex(), $IDENTIFIER.getStopIndex(), $text);}
						;

imm						:	MATH_EXPRESSION
						;

memory_order1			:	IORW;
memory_order2			:	IORW;

shamt					:	imm
						;

//RISCV set manual, page 130 | RV32I Base Instruction Set
lui						:	LUI		rd COMMA imm							{address+=encoder.encodeTypeU($text, $rd.ctx.start.getLine(), section, address, 0b0110111,$rd.ctx.type, CalculatorLibrary.cal($imm.text));};
auipc					:	AUIPC	rd COMMA imm							{address+=encoder.encodeTypeU($text, $rd.ctx.start.getLine(), section, address, 0b0010111,$rd.ctx.type, CalculatorLibrary.cal($imm.text));};

jal						:	JAL		rd COMMA imm							{address+=encoder.encodeTypeJ($text, $rd.ctx.start.getLine(), section, address, 0b1101111,$rd.ctx.type, CalculatorLibrary.cal($imm.text));}
						|	JAL		imm										{address+=encoder.encodeTypeJ($text, $imm.ctx.start.getLine(), section, address, 0b1101111,0b001, CalculatorLibrary.cal($imm.text));};
jalr					:	JALR	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b1100111,0b000, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));}
						|	JALR	rs1										{address+=encoder.encodeTypeI($text, $rs1.ctx.start.getLine(), section, address, 0b1100111,0b000, 1, $rs1.ctx.type, 0);};

beq						:	BEQ		rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b000, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
bne						:	BNE		rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b001, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
blt						:	BLT		rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b100, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
bge						:	BGE		rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b101, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
bltu					:	BLTU	rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b110, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
bgeu					:	BGEU	rs1 COMMA rs2 COMMA imm					{address+=encoder.encodeTypeB($text, $rs1.ctx.start.getLine(), section, address, 0b1100011,0b111, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};

lb						:	LB		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET					{address+=encoder.encodeTypeI($text, $LB.line, section, address, 0b0000011,0b000, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
lh						:	LH		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET				{address+=encoder.encodeTypeI($text, $LH.line, section, address, 0b0000011,0b001, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
lw						:	LW		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET					{address+=encoder.encodeTypeI($text, $LW.line, section, address, 0b0000011,0b010, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
lbu						:	LBU		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET					{address+=encoder.encodeTypeI($text, $LBU.line, section, address, 0b0000011,0b100, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
lhu						:	LHU		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET					{address+=encoder.encodeTypeI($text, $LHU.line, section, address, 0b0000011,0b101, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};	

sb						:	SB		rs2 COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET				{address+=encoder.encodeTypeS($text, $SB.line, section, address, 0b0100011,0b000, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
sh						:	SH		rs2 COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET			{address+=encoder.encodeTypeS($text, $SH.line, section, address, 0b0100011,0b001, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
sw						:	SW		rs2 COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeS($text, $SW.line, section, address, 0b0100011,0b010, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};

addi					:	ADDI	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b000, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));} ;

slti					:	SLTI	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b010, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
sltiu					:	SLTIU	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b011, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
xori					:	XORI	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b100, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
ori						:	ORI		rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b110, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
andi					:	ANDI	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b111, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};

add						:	ADD		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b000,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
sub						:	SUB		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b000,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
sll						:	SLL		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b001,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
slt						:	SLT		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b010,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
sltu					:	SLTU	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b011,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
xor						:	XOR		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b100,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
srl						:	SRL		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b101,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
sra						:	SRA		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b101,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
or						:	OR		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b110,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
and						:	AND		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b111,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

fence					:	FENCE	(memory_order1 COMMA memory_order2)?	{address+=encoder.encodeTypeI_memOrder($text, $memory_order1.ctx.start.getLine(), section, address, 0b0001111,0b000,0, 0, $memory_order1.text, $memory_order2.text);};
ecall					:	ECALL											{address+=encoder.encodeTypeI($text, $ECALL.line, section, address, 0b1110011,0b000, 0, 0, 0);};
ebreak					:	EBREAK											{address+=encoder.encodeTypeI($text, $EBREAK.line, section, address, 0b1110011,0b000, 0, 0, 1);};

//RISCV set manual, page 131 | RV64I Base Instruction Set (in addition to RV32I)
lwu						:	LWU		rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $LWU.line, section, address, 0b0000011,0b110, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
ld						:	LD		rd COMMA imm OPEN_SMALL_BRACKET rs1 CLOSE_SMALL_BRACKET					{address+=encoder.encodeTypeI($text, $LD.line, section, address, 0b0000011,0b011, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
sd						:	SD		rs2 COMMA rs1 COMMA imm					{address+=encoder.encodeTypeS($text, $SD.line, section, address, 0b0100011,0b011, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
slli					:	SLLI	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b001,0, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($shamt.text));};
srli					:	SRLI	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b101,0, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($shamt.text));};
srai					:	SRAI	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b101, 0b0100000,$rd.ctx.type, $rs1.ctx.type,  CalculatorLibrary.calLong($shamt.text));};
addiw					:	ADDIW	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0011011,0b000, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};	
slliw					:	SLLIW	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0011011,0b001,0, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($shamt.text));};
srliw					:	SRLIW	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0011011,0b101,0, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($shamt.text)); };
sraiw					:	SRAIW	rd COMMA rs1 COMMA shamt				{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0011011,0b101, 0b0100000,$rd.ctx.type, $rs1.ctx.type,  CalculatorLibrary.calLong($shamt.text));};
//for slli, srli and srai, we made it a RV64I instruction directly

addw					:	ADDW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b000,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
subw					:	SUBW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b000,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
sllw					:	SLLW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b001,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
srlw					:	SRLW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b101,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);}; 
sraw					:	SRAW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b101,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

//RISCV set manual, page 131 | RV32/RV64 Zifencei Standard Extension
fencei					:	FENCEI											{address+=encoder.encodeTypeI($text, $FENCEI.line, section, address, 0b0001111,0b001,0, 0, 0, 0);};

//RISCV set manual, page 139 | Pseudo Instructions
li						:	LI		rd COMMA imm							{address+=encoder.encodeTypeLi($text, $LI.line, section, address, $rd.ctx.type, CalculatorLibrary.calLong($imm.text));};
mv						:	MV		rd COMMA rs1							{address+=encoder.encodeTypeI($text, $MV.line, section, address, 0b0010011,0b000, $rd.ctx.type, $rs1.ctx.type, 0);};
not						:	NOT		rd COMMA rs1							{address+=encoder.encodeTypeI($text, $NOT.line, section, address, 0b0010011,0b100, $rd.ctx.type, $rs1.ctx.type, -1);}; //need further implement
neg						:	NEG		rd COMMA rs1							{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b000,0b0100000,$rd.ctx.type,0,$rs1.ctx.type);};
negw					:	NEGW	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0111011,0b000,0b0100000,$rd.ctx.type,0,$rs1.ctx.type);};
sextw					:	SEXTW	rd COMMA rs1							{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0011011,0b000, $rd.ctx.type, $rs1.ctx.type, 0);};
seqz					:	SEQZ	rd COMMA rs1							{address+=encoder.encodeTypeI($text, $rd.ctx.start.getLine(), section, address, 0b0010011,0b011, $rd.ctx.type, $rs1.ctx.type, 1);};
snez					:	SNEZ	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b011,0b0000000,$rd.ctx.type,0,$rs1.ctx.type);};
sltz					:	SLTZ	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b010,0b0000000,$rd.ctx.type,$rs1.ctx.type,0);};
sgtz					:	SGTZ	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0110011,0b010,0b0000000,$rd.ctx.type,0,$rs1.ctx.type);};
// ---------------------------------------------
beqz					:	BEQZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BEQZ.line, section, address, 0b1100011,0b000, $rs1.ctx.type, 0, CalculatorLibrary.cal($imm.text));};
bnez					:	BNEZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BNEZ.line, section, address, 0b1100011,0b001, $rs1.ctx.type, 0, CalculatorLibrary.cal($imm.text));};
blez					:	BLEZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BLEZ.line, section, address, 0b1100011,0b101, 0, $rs1.ctx.type, CalculatorLibrary.cal($imm.text));};
bgez					:	BGEZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BGEZ.line, section, address, 0b1100011,0b101, $rs1.ctx.type, 0, CalculatorLibrary.cal($imm.text));};
bltz					:	BLTZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BLTZ.line, section, address, 0b1100011,0b100, $rs1.ctx.type, 0, CalculatorLibrary.cal($imm.text));};
bgtz					:	BGTZ	rs1 COMMA imm							{address+=encoder.encodeTypeB($text, $BGTZ.line, section, address, 0b1100011,0b100, 0,$rs1.ctx.type, CalculatorLibrary.cal($imm.text));};
// ---------------------------------------------
bgt						:	BGT		rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeB($text, $BGT.line, section, address, 0b1100011,0b100, $rs1.ctx.type, $rd.ctx.type, CalculatorLibrary.cal($imm.text));}; 
ble						:	BLE		rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeB($text, $BLE.line, section, address, 0b1100011,0b101, $rs1.ctx.type, $rd.ctx.type, CalculatorLibrary.cal($imm.text));}; 
bgtu					:	BGTU	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeB($text, $BGTU.line, section, address, 0b1100011,0b110, $rs1.ctx.type, $rd.ctx.type, CalculatorLibrary.cal($imm.text));}; 
bleu					:	BLEU	rd COMMA rs1 COMMA imm					{address+=encoder.encodeTypeB($text, $BLEU.line, section, address, 0b1100011,0b111, $rs1.ctx.type, $rd.ctx.type, CalculatorLibrary.cal($imm.text));}; 
// ---------------------------------------------
j						:	J		imm										{address+=encoder.encodeTypeJ($text, $J.line, section, address, 0b1101111,0, CalculatorLibrary.cal($imm.text));};
jr						:	JR		rs1										{address+=encoder.encodeTypeI($text, $JR.line, section, address, 0b1100111,0b000, 0b00000, $rs1.ctx.type, 0);}; 
ret						:	RET												{address+=encoder.encodeTypeI($text, $RET.line, section, address, 0b1100111,0b000, 0, 1, 0);} ; 
call					:	CALL	imm										{address+=encoder.encodeTypeCO($text, $CALL.line, section, address, CalculatorLibrary.calLong($imm.text));};  
tail					:	TAIL	imm										{address+=encoder.encodeTypeTO($text, $TAIL.line, section, address, CalculatorLibrary.calLong($imm.text));};
// ---------------------------------------------
csrr					:	CSRR	rd COMMA csr							{address+=encoder.encodeTypeCSR($text, $CSRR.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,$csr.ctx.type);};
csrw					:	CSRW	csr COMMA rs1							{address+=encoder.encodeTypeCSR($text, $CSRW.line, section, address, 0b1110011,0,0b001,$rs1.ctx.type,$csr.ctx.type);};
csrs					:	CSRS	csr COMMA rs1							{address+=encoder.encodeTypeCSR($text, $CSRS.line, section, address, 0b1110011,0,0b010,$rs1.ctx.type,$csr.ctx.type);};
csrc					:	CSRC	csr COMMA rs1							{address+=encoder.encodeTypeCSR($text, $CSRC.line, section, address, 0b1110011,0,0b011,$rs1.ctx.type,$csr.ctx.type);};
// ---------------------------------------------
csrwi					:	CSRWI	csr COMMA imm							{address+=encoder.encodeTypeCSR($text, $CSRWI.line, section, address, 0b1110011,0,0b101,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};
csrsi					:	CSRSI	csr COMMA imm							{address+=encoder.encodeTypeCSR($text, $CSRSI.line, section, address, 0b1110011,0,0b111,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};
csrci					:	CSRCI	csr COMMA imm							{address+=encoder.encodeTypeCSR($text, $CSRCI.line, section, address, 0b1110011,0,0b111,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};
// ---------------------------------------------
eret					:	ERET											{address+=encoder.encodeTypeI($text, $ERET.line, section, address, 0b1110011,0b000,0,0, 258);};
// ---------------------------------------------

//RISCV set manual, page 131 | RV32/RV64 Zicsr Standard Extension
csrrw					:	CSRRW	rd COMMA csr COMMA rs1					{address+=encoder.encodeTypeCSR($text, $CSRRW.line, section, address, 0b1110011,$rd.ctx.type,0b001,$rs1.ctx.type,$csr.ctx.type);};
csrrs					:	CSRRS	rd COMMA csr COMMA rs1					{address+=encoder.encodeTypeCSR($text, $CSRRS.line, section, address, 0b1110011,$rd.ctx.type,0b010,$rs1.ctx.type,$csr.ctx.type);};
csrrc					:	CSRRC	rd COMMA csr COMMA rs1					{address+=encoder.encodeTypeCSR($text, $CSRRC.line, section, address, 0b1110011,$rd.ctx.type,0b011,$rs1.ctx.type,$csr.ctx.type);};
csrrwi					:	CSRRWI	rd COMMA csr COMMA imm					{address+=encoder.encodeTypeCSR($text, $CSRRWI.line, section, address, 0b1110011,$rd.ctx.type,0b101,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};
csrrsi					:	CSRRSI	rd COMMA csr COMMA imm					{address+=encoder.encodeTypeCSR($text, $CSRRSI.line, section, address, 0b1110011,$rd.ctx.type,0b110,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};
csrrci					:	CSRRCI	rd COMMA csr COMMA imm					{address+=encoder.encodeTypeCSR($text, $CSRRCI.line, section, address, 0b1110011,$rd.ctx.type,0b111,CalculatorLibrary.cal($imm.text),$csr.ctx.type);};

//RISCV set manual, page 131 | RV32M Standard Extension
mul						:	MUL		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $MUL.line, section, address, 0b0110011,0b000,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
mulh					:	MULH	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $MULH.line, section, address, 0b0110011,0b001,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
mulhsu					:	MULHSU	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $MULHSU.line, section, address, 0b0110011,0b010,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
mulhu					:	MULHU	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $MULHU.line, section, address, 0b0110011,0b011,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
div						:	DIV		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $DIV.line, section, address, 0b0110011,0b100,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
divu					:	DIVU	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $DIVU.line, section, address, 0b0110011,0b101,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
rem						:	REM		rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $REM.line, section, address, 0b0110011,0b110,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
remu					:	REMU	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $REMU.line, section, address, 0b0110011,0b111,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

//RISCV set manual, page 131 | RV64M Standard Extension (in addition to RV32M)
mulw					:	MULW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $MULW.line, section, address, 0b0111011,0b000,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
divw					:	DIVW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $DIVW.line, section, address, 0b0111011,0b100,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
divuw					:	DIVUW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $DIVUW.line, section, address, 0b0111011,0b101,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
remw					:	REMW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $REMW.line, section, address, 0b0111011,0b110,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
remuw					:	REMUW	rd COMMA rs1 COMMA rs2					{address+=encoder.encodeTypeR($text, $REMUW.line, section, address, 0b0111011,0b111,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

//RISCV set manual, page 132 | RV32A Standard Extension (in addition to sub instructions)
lrw						:	LRW		rd COMMA rs1							{address+=encoder.encodeTypeR($text, $LRW.line, section, address, 0b0101111,0b010,0b0001000,$rd.ctx.type,$rs1.ctx.type,0);};
lrwaq					:	LRWAQ	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $LRWAQ.line, section, address, 0b0101111,0b010,0b0001010,$rd.ctx.type,$rs1.ctx.type,0);};
lrwrl					:	LRWRL	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $LRWRL.line, section, address, 0b0101111,0b010,0b0001001,$rd.ctx.type,$rs1.ctx.type,0);};
lrwaqrl					:	LRWAQRL	rd COMMA rs1							{address+=encoder.encodeTypeR($text, $LRWAQRL.line, section, address, 0b0101111,0b010,0b0001011,$rd.ctx.type,$rs1.ctx.type,0);};
	
scw						:	SCW			rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $SCW.line, section, address, 0b0101111,0b010,0b0001100,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scwaq					:	SCWAQ		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $SCWAQ.line, section, address, 0b0101111,0b010,0b0001110,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scwrl					:	SCWRL		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $SCWRL.line, section, address, 0b0101111,0b010,0b0001101,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scwaqrl					:	SCWAQRL		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $SCWAQRL.line, section, address, 0b0101111,0b010,0b0001111,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoswapw				:	AMOSWAPW	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $AMOSWAPW.line, section, address, 0b0101111,0b010,0b0000100,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapwaq				:	AMOSWAPWAQ	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $AMOSWAPWAQ.line, section, address, 0b0101111,0b010,0b0000110,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapwrl				:	AMOSWAPWRL	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $AMOSWAPWRL.line, section, address, 0b0101111,0b010,0b0000101,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapwaqrl			:	AMOSWAPWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $AMOSWAPWAQRL.line, section, address, 0b0101111,0b010,0b0000111,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoaddw					:	AMOADDW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoaddwaq				:	AMOADDWAQ	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0000010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoaddwrl				:	AMOADDWRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoaddwaqrl				:	AMOADDWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0000011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoxorw					:	AMOXORW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0010000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxorwaq				:	AMOXORWAQ	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0010010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxorwrl				:	AMOXORWRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0010001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxorwaqrl				:	AMOXORWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0010011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoandw					:	AMOANDW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0110000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoandwaq				:	AMOANDW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0110010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoandwrl				:	AMOANDW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0110001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoandwaqrl				:	AMOANDW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0110011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoorw					:	AMOORW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoorwaq				:	AMOORWAQ	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0100010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoorwrl				:	AMOORWRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0100001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoorwaqrl				:	AMOORWAQRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b0100011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amominw					:	AMOMINW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominwaq				:	AMOMINWAQ	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1000010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominwrl				:	AMOMINWRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominwaqrl				:	AMOMINWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1000011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amomaxw					:	AMOMAXW		rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1010000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxwaq				:	AMOMAXWAQ	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1010010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxwrl				:	AMOMAXWRL	rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1010001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxwaqrl				:	AMOMAXWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1010011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amominuw				:	AMOMINUW	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominuwaq				:	AMOMINUWAQ	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1100010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominuwrl				:	AMOMINUWRL	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1100001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominuwaqrl			:	AMOMINUWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1100011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amomaxuw				:	AMOMAXUW	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1110000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxuwaq				:	AMOMAXUWAQ	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1110010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxuwrl				:	AMOMAXUWRL	 rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1110001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxuwaqrl			:	AMOMAXUWAQRL rd COMMA rs2 COMMA rs1				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b010,0b1110011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

//RISCV set manual, page 132 | RV64A Standard Extension (in addition to sub instructions)
lrd						:	LRD			rd COMMA rs1						{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001000,$rd.ctx.type,$rs1.ctx.type,0);};
lrdaq					:	LRDAQ		rd COMMA rs1						{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001010,$rd.ctx.type,$rs1.ctx.type,0);};
lrdrl					:	LRDRL		rd COMMA rs1						{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001001,$rd.ctx.type,$rs1.ctx.type,0);};
lrdaqrl					:	LRDAQRL		rd COMMA rs1						{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001011,$rd.ctx.type,$rs1.ctx.type,0);};

scd						:	SCD			rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001100,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scdaq					:	SCDAQ		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001110,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scdrl					:	SCDRL		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001101,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
scdaqrl					:	SCDAQRL		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0001111,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoswapd				:	AMOSWAPD	 rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000100,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapdaq				:	AMOSWAPDAQ	 rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000110,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapdrl				:	AMOSWAPDRL	 rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000101,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoswapdaqrl			:	AMOSWAPDAQRL rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000111,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoaddd					:	AMOADDD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoadddaq				:	AMOADDDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoadddrl				:	AMOADDDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoadddaqrl				:	AMOADDDAQRL rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0000011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoxord					:	AMOXORD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0010000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxordaq				:	AMOXORDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0010010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxordrl				:	AMOXORDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0010001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoxordaqrl				:	AMOXORDAQRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0010011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoandd					:	AMOANDD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0110000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoanddaq				:	AMOANDD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0110010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoanddrl				:	AMOANDD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0110001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoanddaqrl				:	AMOANDD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0110011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amoord					:	AMOORD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoordaq				:	AMOORDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0100010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoordrl				:	AMOORDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0100001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amoordaqrl				:	AMOORDAQRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b0100011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amomind					:	AMOMIND		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomindaq				:	AMOMINDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1000010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomindrl				:	AMOMINDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1000001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomindaqrl				:	AMOMINDAQRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1000011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amomaxd					:	AMOMAXD		rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1010000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxdaq				:	AMOMAXDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1010010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxdrl				:	AMOMAXDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1010001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxdaqrl				:	AMOMAXDAQRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1010011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

amominud				:	AMOMINUD	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominudaq				:	AMOMINUDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1100010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominudrl				:	AMOMINUDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1100001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amominudaqrl			:	AMOMINUDAQRL rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1100011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
	
amomaxud				:	AMOMAXUD	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1110000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxudaq				:	AMOMAXUDAQ	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1110010,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxudrl				:	AMOMAXUDRL	rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1110001,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
amomaxudaqrl			:	AMOMAXUDAQRL rd COMMA rs1 COMMA rs2				{address+=encoder.encodeTypeR($text, $rd.ctx.start.getLine(), section, address, 0b0101111,0b011,0b1110011,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};

//RISCV set manual, page 133 | RV32F Standard Extension
flw						:	FLW			frd COMMA rs1 COMMA imm							{address+=encoder.encodeTypeI($text, $FLW.line, section, address, 0b0000111, 0b010, $frd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};	
fsw						:	FSW			frs1 COMMA rs2 COMMA imm						{address+=encoder.encodeTypeS($text, $FSW.line, section, address, 0b0100111, 0b010, $frs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
fmadds					:	FMADDS		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $FMADDS.line, section, address, 0b1000011,$rm.ctx.type,0,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fmsubs					:	FMSUBS		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $FMSUBS.line, section, address, 0b1000111,$rm.ctx.type,0,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fnmadds					:	FNMADDS		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $FNMADDS.line, section, address, 0b1001111,$rm.ctx.type,0,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);}; 
fnmsubs					:	FNMSUBS		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $FNMSUBS.line, section, address, 0b1001011,$rm.ctx.type,0,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);}; 
fadds					:	FADDS		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000000,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsubs					:	FSUBS		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000100,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};   
fmuls					:	FMULS		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001000,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fdivs					:	FDIVS		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001100,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsqrts					:	FSQRTS		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0101100,$frd.ctx.type,$frs1.ctx.type,0b00000);};
fsgnjs					:	FSGNJS		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjns					:	FSGNJNS		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjxs					:	FSGNJXS		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmins					:	FMINS		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010100,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmaxs					:	FMAXS		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010100,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};

fcvtws					:	FCVTWS		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100000,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtwus					:	FCVTWUS		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100000,$rd.ctx.type,$frs1.ctx.type,0b00001);};
fmvxw					:	FMVXW		rd COMMA frs1									{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1110000,$rd.ctx.type,$frs1.ctx.type,0b00000);};
feqs					:	FEQS		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b1010000,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
flts					:	FLTS		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1010000,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fles					:	FLES		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1010000,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fclasss					:	FCLASSS		rd COMMA frs1									{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1110000,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtsw					:	FCVTSW		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101000,$frd.ctx.type,$rs1.ctx.type,0b00000);};
fcvtswu					:	FCVTSWU		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101000,$frd.ctx.type,$rs1.ctx.type,0b00001);};
fmvwx					:	FMVWX		frd COMMA rs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1111000,$frd.ctx.type,$rs1.ctx.type,0b00000);};

//RISCV set manual, page 133 | RV64F Standard Extension (in addition to RV32F)
fcvtls					:	FCVTLS		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1110000,$rd.ctx.type,$frs1.ctx.type,0b00010);};
fcvtlus					:	FCVTLUS		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1110000,$rd.ctx.type,$frs1.ctx.type,0b00011);};
fcvtsl					:	FCVTSL		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101000,$frd.ctx.type,$rs1.ctx.type,0b00010);};
fcvtslu					:	FCVTSLU		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101000,$frd.ctx.type,$rs1.ctx.type,0b00011);};

//RISCV set manual, page 134 | RV32D Standard Extension
fld						:	FLD			frd COMMA rs1 COMMA imm							{address+=encoder.encodeTypeI($text, $frd.ctx.start.getLine(), section, address, 0b0000111, 0b011, $frd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
fsd						:	FSD			rs1 COMMA frs2 COMMA imm						{address+=encoder.encodeTypeS($text, $rs1.ctx.start.getLine(), section, address, 0b0100111, 0b011, $rs1.ctx.type, $frs2.ctx.type, CalculatorLibrary.cal($imm.text));};
fmaddd					:	FMADDD		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1000011,$rm.ctx.type,1,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fmsubd					:	FMSUBD		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1000111,$rm.ctx.type,1,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fnmaddd					:	FNMADDD		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1001111,$rm.ctx.type,1,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fnmsubd					:	FNMSUBD		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1001011,$rm.ctx.type,1,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
faddd					:	FADDD		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000001,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsubd					:	FSUBD		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000101,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmuld					:	FMULD		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001001,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fdivd					:	FDIVD		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001101,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsqrtd					:	FSQRTD		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0101101,$frd.ctx.type,$frs1.ctx.type,0b00000);};
fsgnjd					:	FSGNJD		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjnd					:	FSGNJND		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjxd					:	FSGNJXD		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmind					:	FMIND		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010101,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmaxd					:	FMAXD		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010101,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fcvtsd					:	FCVTSD		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100000,$frd.ctx.type,$frs1.ctx.type,0b00001);};
fcvtds					:	FCVTDS		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100001,$frd.ctx.type,$frs1.ctx.type,0b00000);};
feqd					:	FEQD		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b1010001,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fltd					:	FLTD		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1010001,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fled					:	FLED		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1010001,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fclassd					:	FCLASSD		rd COMMA frs1									{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1110001,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtwd					:	FCVTWD		rd COMMA frs1  COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100001,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtwud					:	FCVTWUD		rd COMMA frs1  COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100001,$rd.ctx.type,$frs1.ctx.type,0b00001);};
fcvtdw					:	FCVTDW		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101001,$frd.ctx.type,$rs1.ctx.type,0b00000);};
fcvtdwu					:	FCVTDWU		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101001,$frd.ctx.type,$rs1.ctx.type,0b00001);};

//RISCV set manual, page 134 | RV64D Standard Extension (in addition to RV32D)
fcvtld					:	FCVTLD		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100001,$rd.ctx.type,$frs1.ctx.type,0b00010);};
fcvtlud					:	FCVTLUD		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100001,$rd.ctx.type,$frs1.ctx.type,0b00011);};
fmvxd					:	FMVXD		rd COMMA frs1									{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1110001,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtdl					:	FCVTDL		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101001,$frd.ctx.type,$rs1.ctx.type,0b00010);};
fcvtdlu					:	FCVTDLU		frd COMMA rs1  COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101001,$frd.ctx.type,$rs1.ctx.type,0b00011);};
fmvdx					:	FMVDX		frd COMMA rs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1111001,$frd.ctx.type,$rs1.ctx.type,0b00000);};

//RISCV set manual, page 135 | RV32Q Standard Extension 

flq						:	FLQ			frd COMMA rs1 COMMA imm							{address+=encoder.encodeTypeI($text, $frd.ctx.start.getLine(), section, address, 0b0000111, 0b100, $frd.ctx.type, $rs1.ctx.type, CalculatorLibrary.calLong($imm.text));};
fsq						:	FSQ			rs1 COMMA frs2 COMMA imm						{address+=encoder.encodeTypeS($text, $rs1.ctx.start.getLine(), section, address, 0b0100111, 0b100, $rs1.ctx.type, $frs2.ctx.type, CalculatorLibrary.cal($imm.text));};
fmaddq					:	FMADDQ		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1000011,$rm.ctx.type,0b11,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fmsubq					:	FMSUBQ		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1000111,$rm.ctx.type,0b11,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fnmaddq					:	FNMADDQ		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1001111,$rm.ctx.type,0b11,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
fnmsubq					:	FNMSUBQ		frd COMMA frs1 COMMA frs2 COMMA frs3 COMMA rm	{address+=encoder.encodeTypeR4($text, $frd.ctx.start.getLine(), section, address, 0b1001011,$rm.ctx.type,0b11,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type,$frs3.ctx.type);};
faddq					:	FADDQ		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000011,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsubq					:	FSUBQ		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0000111,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmulq					:	FMULQ		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001011,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fdivq					:	FDIVQ		frd COMMA frs1 COMMA frs2 COMMA rm				{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0001111,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsqrtq					:	FSQRTQ		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0101111,$frd.ctx.type,$frs1.ctx.type,0b00000);};
fsgnjq					:	FSGNJQ		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010011,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjnq					:	FSGNJNQ		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010011,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fsgnjxq					:	FSGNJXQ		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b0010011,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fminq					:	FMINQ		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010111,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fmaxq					:	FMAXQ		frd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010111,$frd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fcvtsq					:	FCVTSQ		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100000,$frd.ctx.type,$frs1.ctx.type,0b00011);};
fcvtqs					:	FCVTQS		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100011,$frd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtdq					:	FCVTDQ		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100001,$frd.ctx.type,$frs1.ctx.type,0b00011);};
fcvtqd					:	FCVTQD		frd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b0100011,$frd.ctx.type,$frs1.ctx.type,0b00001);};
feqq					:	FEQQ		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b1010011,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fltq					:	FLTQ		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1010011,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fleq					:	FLEQ		rd COMMA frs1 COMMA frs2						{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b1010011,$rd.ctx.type,$frs1.ctx.type,$frs2.ctx.type);};
fclassq					:	FCLASSQ		rd COMMA frs1									{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b1110011,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtwq					:	FCVTWQ		rd COMMA frs1  COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100011,$rd.ctx.type,$frs1.ctx.type,0b00000);};
fcvtwuq					:	FCVTWUQ		rd COMMA frs1  COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100011,$rd.ctx.type,$frs1.ctx.type,0b00001);};
fcvtqw					:	FCVTQW		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101011,$frd.ctx.type,$rs1.ctx.type,0b00000);};
fcvtqwu					:	FCVTQWU		frd COMMA rs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101011,$frd.ctx.type,$rs1.ctx.type,0b00001);};

//RISCV set manual, page 135 | RV64Q Standard Extension (in addition to RV32Q)
fcvtlq					:	FCVTLQ		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100011,$rd.ctx.type,$frs1.ctx.type,0b00010);};
fcvtluq					:	FCVTLUQ		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1100011,$rd.ctx.type,$frs1.ctx.type,0b00011);};
fcvtql					:	FCVTQL		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101011,$rd.ctx.type,$frs1.ctx.type,0b00010);};
fcvtqlu					:	FCVTQLU		rd COMMA frs1 COMMA rm							{address+=encoder.encodeTypeFR($text, $rd.ctx.start.getLine(), section, address, 0b1010011,$rm.ctx.type,0b1101011,$rd.ctx.type,$frs1.ctx.type,0b00011);};


fmvs					:	FMVS		frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};
fabss					:	FABSS	 	frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};
fnegs					:	FNEGS		frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010000,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};
fmvd					:	FMVD		frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b000,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};
fabsd					:	FABSD		frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b010,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};
fnegd					:	FNEGD		frd COMMA frs1									{address+=encoder.encodeTypeFR($text, $frd.ctx.start.getLine(), section, address, 0b1010011,0b001,0b0010001,$frd.ctx.type,$frs1.ctx.type,$frs1.ctx.type);};

rdinstret				:	RDINSTRET	rd												{address+=encoder.encodeTypeCSR($text, $RDINSTRET.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc02);};
rdcycle					:	RDCYCLE		rd												{address+=encoder.encodeTypeCSR($text, $RDCYCLE.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc00);};
rdtime					:	RDTIME		rd												{address+=encoder.encodeTypeCSR($text, $RDTIME.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc01);};
rdinstreth				:	RDINSTRETH	rd												{address+=encoder.encodeTypeCSR($text, $RDINSTRETH.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc82);};
rdtimeh					:	RDTIMEH		rd												{address+=encoder.encodeTypeCSR($text, $RDTIMEH.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc81);};	
rdcycleh				:	RDCYCLEH	rd												{address+=encoder.encodeTypeCSR($text, $RDCYCLEH.line, section, address, 0b1110011,$rd.ctx.type,0b010,0,0xc80);};

frcsr					:	FRCSR		rd												{address+=encoder.encodeTypeCSR($text, $FRCSR.line, section, address, 0b1110011,$rd.ctx.type,0b010,0b00000, 0x003);};
fscsr					:	FSCSR		rd COMMA rs1									{address+=encoder.encodeTypeCSR($text, $FSCSR.line, section, address, 0b1110011,$rd.ctx.type,0b001,$rs1.ctx.type, 0x003);}
						|	FSCSR		rs1												{address+=encoder.encodeTypeCSR($text, $FSCSR.line, section, address, 0b1110011,0b00000,0b001,$rs1.ctx.type, 0x003);};
frrm					:	FRRM		rd												{address+=encoder.encodeTypeCSR($text, $FRRM.line, section, address, 0b1110011,$rd.ctx.type,0b010,0b00000, 0x002);};
fsrm					:	FSRM		rd COMMA rs1									{address+=encoder.encodeTypeCSR($text, $FSRM.line, section, address, 0b1110011,$rd.ctx.type,0b001,$rs1.ctx.type, 0x002);}
						|	FSRM		rs1												{address+=encoder.encodeTypeCSR($text, $FSRM.line, section, address, 0b1110011,0b00000,0b001,$rs1.ctx.type, 0x002);};
frflags					:	FRFLAGS		rd												{address+=encoder.encodeTypeCSR($text, $FRFLAGS.line, section, address, 0b1110011,$rd.ctx.type,0b010,0b00000, 0x001);};
fsflags					:	FSFLAGS		rd COMMA rs1									{address+=encoder.encodeTypeCSR($text, $FSFLAGS.line, section, address, 0b1110011,$rd.ctx.type,0b001,$rs1.ctx.type, 0x001);}
						|	FSFLAGS		rs1												{address+=encoder.encodeTypeCSR($text, $FSFLAGS.line, section, address, 0b1110011,0b00000,0b001,$rs1.ctx.type, 0x001);};

//Type C Load
clw						:	CLW		rd_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCL($text, $CLW.line, section, address, 0b00, $rd_apos.ctx.type, $rs1_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b010);};
cld						:	CLD		rd_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCL($text, $CLD.line, section, address, 0b00, $rd_apos.ctx.type, $rs1_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b011);};
cfld					:	CFLD		frd_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeCL($text, $CFLD.line, section, address, 0b00, $frd_apos.ctx.type, $rs1_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b001);};
//Type C Load with sp
clwsp					:	CLWSP	rd		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCI($text, $CLWSP.line, section, address, 0b10, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b010, false, false);};
cldsp					:	CLDSP	rd		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCI($text, $CLDSP.line, section, address, 0b10, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b011 , false, false);};
cfldsp					:	CFLDSP	frd		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCI($text, $CFLDSP.line, section, address, 0b10, $frd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b001 , false, true);};
//Type C Store with sp
cswsp					:	CSWSP	rs2		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCSS($text, $CSWSP.line, section, address, 0b10, $rs2.ctx.type, CalculatorLibrary.calLong($imm.text), 0b110, false);};
csdsp					:	CSDSP	rs2		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET		{address+=encoder.encodeTypeCSS($text, $CSDSP.line, section, address, 0b10, $rs2.ctx.type, CalculatorLibrary.calLong($imm.text), 0b111 , false);};
cfsdsp					:	CFSDSP	frs2		COMMA imm OPEN_SMALL_BRACKET (X2|SP) CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeCSS($text, $CFSDSP.line, section, address, 0b10, $frs2.ctx.type, CalculatorLibrary.calLong($imm.text), 0b101 , false);};
//Type C Store
csw						:	CSW		rs2_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeCS($text, $CSW.line, section, address, 0b00, $rs1_apos.ctx.type, $rs2_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b110, false);};
csd						:	CSD		rs2_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeCS($text, $CSD.line, section, address, 0b00, $rs1_apos.ctx.type, $rs2_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b111, false);};
cfsd					:	CFSD	frs2_apos	COMMA imm OPEN_SMALL_BRACKET rs1_apos CLOSE_SMALL_BRACKET	{address+=encoder.encodeTypeCS($text, $CFSD.line, section, address, 0b00, $rs1_apos.ctx.type, $frs2_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b101, false);};

cadd					:	CADD	rd	COMMA rs2										{address+=encoder.encodeTypeCR($text, $rd.ctx.start.getLine(), section, address, 0b10, $rd.ctx.type, $rs2.ctx.type, 0b1001, true);};
caddi					:	CADDI	rd	COMMA imm										{address+=encoder.encodeTypeCI($text, $rd.ctx.start.getLine(), section, address, 0b01, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b000, true, false);};
caddiw					:	CADDIW	rd	COMMA imm										{address+=encoder.encodeTypeCI($text, $rd.ctx.start.getLine(), section, address, 0b01, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b001, true, false);};
	
caddi16sp				:	CADDI16SP (X2|SP)	COMMA imm								{address+=encoder.encodeTypeCI($text, $CADDI16SP.line, section, address, 0b01, 2, CalculatorLibrary.calLong($imm.text), 0b011, true, true);};
caddi4spn				:	CADDI4SPN rd_apos	COMMA (X2|SP) COMMA imm					{address+=encoder.encodeTypeCIW($text, $CADDI4SPN.line, section, address, 0b00,  $rd_apos.ctx.type,  CalculatorLibrary.calLong($imm.text), 0b000);};

candi					:	CANDI	rd_apos	COMMA imm									{address+=encoder.encodeTypeCI($text, $CANDI.line, section, address, 0b01, (0b10<<3) | $rd_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b100, true, true);};

//Type C Arithmetic
caddw					:	CADDW	rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $CADDW.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100111, 0b01, true);};
csub					:	CSUB	rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $CSUB.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100011, 0b00, true);};
csubw					:	CSUBW	rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $CSUBW.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100111, 0b00, true);};
cand					:	CAND	rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $CAND.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100011, 0b11, true);};
cor						:	COR		rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $COR.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100011, 0b10, true);};
cxor					:	CXOR	rd COMMA rd COMMA rs2								{address+=encoder.encodeTypeCA($text, $CXOR.line, section, address, 0b01, $rd.ctx.type, $rs2.ctx.type, 0b100011, 0b01, true);};

cmv						:	CMV		rd		COMMA rs1									{address+=encoder.encodeTypeCR($text, $CMV.line, section, address, 0b10, $rd.ctx.type, $rs1.ctx.type, 0b1000, true);};
cli						:	CLI		rd		COMMA imm									{address+=encoder.encodeTypeCI($text, $CLI.line, section, address, 0b01, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b010, true, true);};
clui					:	CLUI	rd		COMMA imm									{address+=encoder.encodeTypeCI($text, $CLUI.line, section, address, 0b01, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b011, true, true);};

cslli					:	CSLLI	rd		COMMA imm									{address+=encoder.encodeTypeCI($text, $CSLLI.line, section, address, 0b10, $rd.ctx.type, CalculatorLibrary.calLong($imm.text), 0b000, false, true);};
csrai					:	CSRAI	rd_apos	COMMA imm									{address+=encoder.encodeTypeCI($text, $CSRAI.line, section, address, 0b01, (0b01<<3) | $rd_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b100, false, true);};
csrli					:	CSRLI	rd_apos	COMMA imm									{address+=encoder.encodeTypeCI($text, $CSRLI.line, section, address, 0b01, (0b00<<3) | $rd_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b100, false, true);};

cbeqz					:	CBEQZ	rs1_apos	COMMA imm								{address+=encoder.encodeTypeCB($text, $CBEQZ.line, section, address, 0b01, $rs1_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b110);};
cbnez					:	CBNEZ	rs1_apos	COMMA imm								{address+=encoder.encodeTypeCB($text, $CBNEZ.line, section, address, 0b01, $rs1_apos.ctx.type, CalculatorLibrary.calLong($imm.text), 0b111);};

cj						:	CJ		imm													{address+=encoder.encodeTypeCJ($text, $CJ.line, section, address, 0b01, CalculatorLibrary.calLong($imm.text), 0b101);};
cjr						:	CJR		rs1													{address+=encoder.encodeTypeCR($text, $CJR.line, section, address, 0b10, $rs1.ctx.type, 0, 0b1000, false);};
  
cjal					:	CJAL	imm													{address+=encoder.encodeTypeCJ($text, $CJAL.line, section, address, 0b01, CalculatorLibrary.calLong($imm.text), 0b001);};
cjalr					:	CJALR	rs1													{address+=encoder.encodeTypeCR($text, $CJALR.line, section, address, 0b10, $rs1.ctx.type, 0, 0b1001, false);};
	
cebreak					:	CEBREAK														{address+=encoder.encodeTypeCI($text, $CEBREAK.line, section, address, 0b10, 0, 0b100000, 0b100, false, true);};
cnop					:	CNOP														{address+=encoder.encodeTypeCI($text, $CNOP.line, section, address, 0b01, 0, 0, 0b000, true, false);};																							

uret					:	URET														{address+=encoder.encodeTypeI($text, $URET.line, section, address, 0b1110011,0b000,0,0, 2);};
sret					:	SRET														{address+=encoder.encodeTypeI($text, $SRET.line, section, address, 0b1110011,0b000,0,0, 258);};
hret					:	HRET														{address+=encoder.encodeTypeI($text, $HRET.line, section, address, 0b1110011,0b000,0,0, 514);};
mret					:	MRET														{address+=encoder.encodeTypeI($text, $MRET.line, section, address, 0b1110011,0b000,0,0, 770);};

wfi						:	WFI															{address+=encoder.encodeTypeR($text, $WFI.line, section, address, 0b1110011,0b000,0b0001000,0,0,5);};
sfencevm				:	SFENCEVM rs1 COMMA rs2										{address+=encoder.encodeTypeR($text, $SFENCEVM.line, section, address, 0b1110011,0b000,9,0,$rs1.ctx.type,$rs2.ctx.type);};
pause					:	PAUSE														{address+=encoder.encodeTypePause($text, $PAUSE.line, section, address, 0b1111111,0);} ;
nop						:	NOP															{address+=encoder.encodeTypeI($text, $NOP.line, section, address, 0b0010011,0b000, 0, 0,0);};

vsetvli					:	VSETVLI rd COMMA rs1 COMMA vsew COMMA vlmul COMMA vta COMMA vma		{address+=encoder.encodeTypeVConfig($text, $VSETVLI.line, section, address, 0b1010111, 0, 0, $vma.ctx.type, $vta.ctx.type, $vsew.ctx.type, $vlmul.ctx.type, 0, $rs1.ctx.type, $rd.ctx.type, 0, 0);}; 
vsetivli				:	VSETIVLI rd COMMA imm COMMA vsew COMMA vlmul COMMA vta COMMA vma	{address+=encoder.encodeTypeVConfig($text, $VSETIVLI.line, section, address, 0b1010111, 1, 1, $vma.ctx.type, $vta.ctx.type, $vsew.ctx.type, $vlmul.ctx.type, 0, 0, $rd.ctx.type, 0, CalculatorLibrary.calLong($imm.text));}; 
vsetvl					:	VSETVL rd COMMA rs1 COMMA rs2										{address+=encoder.encodeTypeVConfig($text, $VSETVL.line, section, address, 0b1010111, 1, 0, 0, 0, 0, 0, $rs2.ctx.type, $rs1.ctx.type, $rd.ctx.type, 0, 0);}; 

vle8v					:	VLE8V vd COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VLE8V.line, section, address, 0b0000111, $rs1.ctx.type, 0, 0, 0, $vd.ctx.type, 1, 0b000, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vle16v					:	VLE16V vd COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VLE16V.line, section, address, 0b0000111, $rs1.ctx.type, 0, 0, 0, $vd.ctx.type, 1, 0b101, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vle32v					:	VLE32V vd COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VLE32V.line, section, address, 0b0000111, $rs1.ctx.type, 0, 0, 0, $vd.ctx.type, 1, 0b110, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vle64v					:	VLE64V vd COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VLE64V.line, section, address, 0b0000111, $rs1.ctx.type, 0, 0, 0, $vd.ctx.type, 1, 0b111, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};

vse8v					:	VSE8V vs3 COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VSE8V.line, section, address, 0b0100111, $rs1.ctx.type, 0, 0, $vs3.ctx.type, 0, 1, 0b000, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vse16v					:	VSE16V vs3 COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VSE16V.line, section, address, 0b0100111, $rs1.ctx.type, 0, 0, $vs3.ctx.type, 0, 1, 0b101, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vse32v					:	VSE32V vs3 COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VSE32V.line, section, address, 0b0100111, $rs1.ctx.type, 0, 0, $vs3.ctx.type, 0, 1, 0b110, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
vse64v					:	VSE64V vs3 COMMA rs1 COMMA imm								{address+=encoder.encodeTypeVLoadStore($text, $VSE64V.line, section, address, 0b0100111, $rs1.ctx.type, 0, 0, $vs3.ctx.type, 0, 1, 0b111, 0b0, 0b00, 0b000, 0b00000, 0, CalculatorLibrary.calLong($imm.text));};
					
vaddvv					:	VADDVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VADDVV.line, section, address, 0b1010111, 0b000000, 0b000,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vaddvx					:	VADDVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VADDVX.line, section, address, 0b1010111, 0b000000, 0b100,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};
vaddvi					:	VADDVI vd COMMA vs2 COMMA imm								{address+=encoder.encodeTypeVArith($text, $VADDVI.line, section, address, 0b1010111, 0b000000, 0b011,$vd.ctx.type, 0, $vs2.ctx.type, 0, 1, CalculatorLibrary.calLong($imm.text));};

vsubvv					:	VSUBVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VSUBVV.line, section, address, 0b1010111, 0b000010, 0b000,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vsubvx					:	VSUBVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VSUBVX.line, section, address, 0b1010111, 0b000010, 0b100,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};
	
vrsubvx					:	VRSUBVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VRSUBVX.line, section, address, 0b1010111, 0b000011, 0b100,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};
vrsubvi					:	VRSUBVI vd COMMA vs2 COMMA imm								{address+=encoder.encodeTypeVArith($text, $VRSUBVI.line, section, address, 0b1010111, 0b000011, 0b011,$vd.ctx.type, 0, $vs2.ctx.type, 0, 1, CalculatorLibrary.calLong($imm.text));};

vwadduvv				:	VWADDUVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWADDUVV.line, section, address, 0b1010111, 0b110000, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwadduvx				:	VWADDUVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWADDUVX.line, section, address, 0b1010111, 0b110000, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwsubuvv				:	VWSUBUVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWSUBUVV.line, section, address, 0b1010111, 0b110010, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwsubuvx				:	VWSUBUVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWSUBUVX.line, section, address, 0b1010111, 0b110010, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwaddvv					:	VWADDVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWADDVV.line, section, address, 0b1010111, 0b110001, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwaddvx					:	VWADDVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWADDVX.line, section, address, 0b1010111, 0b110001, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwsubvv					:	VWSUBVV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWSUBVV.line, section, address, 0b1010111, 0b110011, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwsubvx					:	VWSUBVX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWSUBVX.line, section, address, 0b1010111, 0b110011, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwadduwv				:	VWADDUWV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWADDUWV.line, section, address, 0b1010111, 0b110100, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwadduwx				:	VWADDUWX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWADDUWX.line, section, address, 0b1010111, 0b110100, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwsubuwv				:	VWSUBUWV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWSUBUWV.line, section, address, 0b1010111, 0b110110, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwsubuwx				:	VWSUBUWX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWSUBUWX.line, section, address, 0b1010111, 0b110110, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};

vwaddwv					:	VWADDWV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWADDWV.line, section, address, 0b1010111, 0b110101, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwaddwx					:	VWADDWX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWADDWX.line, section, address, 0b1010111, 0b110101, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};
		
vwsubwv					:	VWSUBWV vd COMMA vs2 COMMA vs1								{address+=encoder.encodeTypeVArith($text, $VWSUBWV.line, section, address, 0b1010111, 0b110111, 0b010,$vd.ctx.type, $vs1.ctx.type, $vs2.ctx.type, 0, 1, 0);};
vwsubwx					:	VWSUBWX vd COMMA vs2 COMMA rs1								{address+=encoder.encodeTypeVArith($text, $VWSUBWX.line, section, address, 0b1010111, 0b110111, 0b110,$vd.ctx.type, 0, $vs2.ctx.type, $rs1.ctx.type, 1, 0);};
