/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.assembler.ia32;

import hk.quantr.assembler.antlr.AssemblerLexer;
import hk.quantr.assembler.antlr.AssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestUtil {

	public static Logger logger = Logger.getLogger(TestUtil.class.getName());

	static Runtime rt = Runtime.getRuntime();

	static HashMap<String, ArrayList<String>> translatedMap = new HashMap<String, ArrayList<String>>();
	static String allCombinations[] = {"${r/m8}", "${r/m16}", "${r/m32}", "${r/m64}", "${imm8}", "${imm16}", "${imm32}", "${imm64}", "${r8}", "${r16}", "${r32}", "${r64}"};

	static {
		translatedMap.put("${address}", concatArraysToList(getAddressSize()));
		translatedMap.put("${r/m8}", concatArraysToList(get8BitsRegisters(), get8BitsEffectiveAddress()));
		translatedMap.put("${r/m16}", concatArraysToList(get16BitsRegisters(), get16BitsEffectiveAddress()));
		translatedMap.put("${r/m32}", concatArraysToList(get32BitsRegisters(), get32BitsEffectiveAddress()));
		translatedMap.put("${r/m64}", concatArraysToList(get64BitsRegisters(), get64BitsEffectiveAddress()));

		translatedMap.put("${r/m8_prefix}", concatArraysToList(get8BitsRegisters(), get8BitsEffectiveAddress_prefix()));
		translatedMap.put("${r/m16_prefix}", concatArraysToList(get16BitsRegisters(), get16BitsEffectiveAddress_prefix()));
		translatedMap.put("${r/m32_prefix}", concatArraysToList(get32BitsRegisters(), get32BitsEffectiveAddress_prefix()));
		translatedMap.put("${r/m64_prefix}", concatArraysToList(get64BitsRegisters(), get64BitsEffectiveAddress_prefix()));

		translatedMap.put("${m8}", concatArraysToList(get8BitsEffectiveAddress()));
		translatedMap.put("${m16}", concatArraysToList(get16BitsEffectiveAddress()));
		translatedMap.put("${m32}", concatArraysToList(get32BitsEffectiveAddress()));
		translatedMap.put("${m64}", concatArraysToList(get64BitsEffectiveAddress()));

		translatedMap.put("${m16&32}", concatArraysToList(get32BitsEffectiveAddress()));
		translatedMap.put("${m16&64}", concatArraysToList(get64BitsEffectiveAddress()));

		translatedMap.put("${m8_prefix}", concatArraysToList(get8BitsEffectiveAddress_prefix()));
		translatedMap.put("${m16_prefix}", concatArraysToList(get16BitsEffectiveAddress_prefix()));
		translatedMap.put("${m32_prefix}", concatArraysToList(get32BitsEffectiveAddress_prefix()));
		translatedMap.put("${m64_prefix}", concatArraysToList(get64BitsEffectiveAddress_prefix()));

		translatedMap.put("${moffs8}", concatArraysToList(get8BitsEffectiveAddress()));
		translatedMap.put("${moffs16}", concatArraysToList(get16BitsEffectiveAddress()));
		translatedMap.put("${moffs32}", concatArraysToList(get32BitsEffectiveAddress()));
		translatedMap.put("${moffs64}", concatArraysToList(get64BitsEffectiveAddress()));

		translatedMap.put("${m}", concatArraysToList(get8BitsEffectiveAddress(), get16BitsEffectiveAddress(), get32BitsEffectiveAddress(), get64BitsEffectiveAddress()));
		translatedMap.put("${m_prefix}", concatArraysToList(get8BitsEffectiveAddress_prefix(), get16BitsEffectiveAddress_prefix(), get32BitsEffectiveAddress_prefix(), get64BitsEffectiveAddress_prefix()));

		translatedMap.put("${mem}", concatArraysToList(get8BitsEffectiveAddress(), get16BitsEffectiveAddress(), get32BitsEffectiveAddress(), get64BitsEffectiveAddress()));

		translatedMap.put("${rel8}", new ArrayList<String>(Arrays.asList("0x12", "0xaf", "0x1+0x2*0x3")));
		translatedMap.put("${rel16}", new ArrayList<String>(Arrays.asList("0x1234", "0x7fff")));
		translatedMap.put("${rel32}", new ArrayList<String>(Arrays.asList("0x12345678", "0xafc74321")));
		translatedMap.put("${rel64}", new ArrayList<String>(Arrays.asList("0x12345678abcdef12", "0xafc74321afc74321")));

		translatedMap.put("${imm8}", new ArrayList<String>(Arrays.asList("0x12", "0xaf", "0x1+0x2*0x3")));
		translatedMap.put("${imm16}", new ArrayList<String>(Arrays.asList("0x1234", "0x7fff")));
		translatedMap.put("${imm32}", new ArrayList<String>(Arrays.asList("0x12345678", "0xafc74321")));
		translatedMap.put("${imm64}", new ArrayList<String>(Arrays.asList("0x12345678abcdef12", "0xafc74321afc74321")));
		translatedMap.put("${r8}", new ArrayList<String>(Arrays.asList("al", "cl", "dl", "bl", "ah", "ch", "dh", "bh", "bpl", "spl", "dil", "sil", "r8l", "r9l", "r10l", "r11l", "r12l", "r13l", "r14l", "r15l")));
		translatedMap.put("${r16}", new ArrayList<String>(Arrays.asList("ax", "cx", "dx", "bx", "sp", "bp", "si", "di", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15")));
		translatedMap.put("${r32}", new ArrayList<String>(Arrays.asList("eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d", "r15d")));
		translatedMap.put("${r64}", new ArrayList<String>(Arrays.asList("rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15")));

		translatedMap.put("${sreg}", new ArrayList<String>(Arrays.asList("cs", "ds", "es", "fs")));

		translatedMap.put("${reg}", concatArraysToList(translatedMap.get("${r8}").toArray(new String[0]), translatedMap.get("${r16}").toArray(new String[0]), translatedMap.get("${r32}").toArray(new String[0]), translatedMap.get("${r64}").toArray(new String[0])));
		translatedMap.put("${r16/m16}", concatArraysToList(translatedMap.get("${r16}").toArray(new String[0]), translatedMap.get("${m16}").toArray(new String[0])));
		translatedMap.put("${r32/m16}", concatArraysToList(translatedMap.get("${r32}").toArray(new String[0]), translatedMap.get("${m16}").toArray(new String[0])));
		translatedMap.put("${r32/m32}", concatArraysToList(translatedMap.get("${r32}").toArray(new String[0]), translatedMap.get("${m32}").toArray(new String[0])));
		translatedMap.put("${r64/m64}", concatArraysToList(translatedMap.get("${r64}").toArray(new String[0]), translatedMap.get("${m64}").toArray(new String[0])));

		translatedMap.put("${r16/m16_prefix}", concatArraysToList(translatedMap.get("${r16}").toArray(new String[0]), translatedMap.get("${m16_prefix}").toArray(new String[0])));
		translatedMap.put("${r32/m16_prefix}", concatArraysToList(translatedMap.get("${r32}").toArray(new String[0]), translatedMap.get("${m16_prefix}").toArray(new String[0])));

		translatedMap.put("${rel16}", new ArrayList<String>(Arrays.asList("0x1234", "0xabcd")));
		translatedMap.put("${rel32}", new ArrayList<String>(Arrays.asList("0x12345678", "0xabcd1234")));
		translatedMap.put("${rel64}", new ArrayList<String>(Arrays.asList("0x12345678abcdefab", "abcdefab12345678")));
		translatedMap.put("${ptr16:16}", new ArrayList<String>(Arrays.asList("0x1234:0x1234", "0xabcd:0xefab")));
		translatedMap.put("${ptr16:32}", new ArrayList<String>(Arrays.asList("0x1234:0x12345678", "0xabcd:0xabcd1234")));
		translatedMap.put("${m16:16}", new ArrayList<String>(Arrays.asList("[cs:0x1234]", "[ds:0x1234]")));
		translatedMap.put("${m16:32}", new ArrayList<String>(Arrays.asList("[cs:0x12345678]", "[ds:0xabcdef12]")));
		translatedMap.put("${m16:64}", new ArrayList<String>(Arrays.asList("[cs:0x1234567812345678]", "[ds:abcdef12abcdef12]")));

		translatedMap.put("${cr0-cr7}", new ArrayList<String>(Arrays.asList("cr0", "cr1", "cr2", "cr3", "cr4", "cr5", "cr6", "cr7")));
		translatedMap.put("${dr0-dr7}", new ArrayList<String>(Arrays.asList("dr0", "dr1", "dr2", "dr3", "dr4", "dr5", "dr6", "dr7")));
	}

	@Test
	public void testTranslateInstruction() throws Exception {
		String str = "adc ${r/m8}, ${imm8}, ${r/m8}";
		String instructions[] = translateInstruction(str);
		for (String instruction : instructions) {
			MessageHandler.println("instructions＝" + instruction);
		}
	}

	static String[] getAddressSize() {
		return new String[]{
			"byte",
			"word",
			"dword"
		};
	}

	static String[] get8BitsEffectiveAddress_prefix() {
		return Arrays.stream(get8BitsEffectiveAddress()).map(s -> "byte " + s).toArray(String[]::new);
	}

	static String[] get8BitsEffectiveAddress() {
		return new String[]{
			"[bh+sil]",
			"[bh+dil]",
			"[bpl+sil]",
			"[bpl+dil]",
			"[sil]",
			"[dil]",
			"[0xab]",
			"[bh]",
			"[bh+sil+0x12]",
			"[bh+dil+0x12]",
			"[bpl+sil+0x12]",
			"[bpl+dil+0x12]",
			"[sil+0x12]",
			"[dil+0x12]",
			"[bpl+0x12]",
			"[bl+0x12]",
			"[bl+sil+0x12]",
			"[bl+dil+0x12]",
			"[bpl+sil+0x12]",
			"[bpl+dil+0x12]",
			"[sil+0x12]",
			"[dil+0x12]",
			"[bpl+0x12]",
			"[bl+0x12]",
			"[al]",
			"[cl]",
			"[dl]",
			"[bl]",
			"[spl]",
			"[bpl]",
			"[sil]",
			"[dil]"};
	}

	static String[] get16BitsEffectiveAddress_prefix() {
		return Arrays.stream(get16BitsEffectiveAddress()).map(s -> "word " + s).toArray(String[]::new);
	}

	static String[] get16BitsEffectiveAddress() {
		return new String[]{
			"[bx+si]",
			"[bx+di]",
			"[bp+si]",
			"[bp+di]",
			"[si]",
			"[di]",
			"[0x1234]",
			"[bx]",
			"[bx+si+0x12]",
			"[bx+di+0x12]",
			"[bp+si+0x12]",
			"[bp+di+0x12]",
			"[si+0x12]",
			"[di+0x12]",
			"[bp+0x12]",
			"[bx+0x12]",
			"[bx+si+0x1234]",
			"[bx+di+0x1234]",
			"[bp+si+0x1234]",
			"[bp+di+0x1234]",
			"[si+0x1234]",
			"[di+0x1234]",
			"[bp+0x1234]",
			"[bx+0x1234]",
			"[eax]",
			"[ax]",
			"[al]",
			"[mm0]",
			"[xmm0]",
			"[ecx]",
			"[cx]",
			"[cl]",
			"[mm1]",
			"[xmm1]",
			"[edx]",
			"[dx]",
			"[dl]",
			"[mm2]",
			"[xmm2]",
			"[ebx]",
			"[bx]",
			"[bl]",
			"[mm3]",
			"[xmm3]",
			"[esp]",
			"[sp]",
			"[ah]",
			"[mm4]",
			"[xmm4]",
			"[ebp]",
			"[bp]",
			"[ch]",
			"[mm5]",
			"[xmm5]",
			"[esi]",
			"[si]",
			"[dh]",
			"[mm6]",
			"[xmm6]",
			"[edi]",
			"[di]",
			"[bh]",
			"[mm7]",
			"[xmm7]",};
	}

	static String[] get32BitsEffectiveAddress_prefix() {
		return Arrays.stream(get32BitsEffectiveAddress()).map(s -> "dword " + s).toArray(String[]::new);
	}

	static String[] get32BitsEffectiveAddress() {
		return new String[]{
			"[eax]",
			"[ecx]",
			"[edx]",
			"[ebx]",
			"[0x12345678]",
			"[esi]",
			"[edi]",
			"[eax*2]",
			"[ecx*2]",
			"[edx*2]",
			"[ebx*2]",
			"[ebp*2]",
			"[esi*2]",
			"[edi*2]",
			"[eax*4]",
			"[ecx*4]",
			"[edx*4]",
			"[ebx*4]",
			"[ebp*4]",
			"[esi*4]",
			"[edi*4]",
			"[eax*8]",
			"[ecx*8]",
			"[edx*8]",
			"[ebx*8]",
			"[ebp*8]",
			"[esi*8]",
			"[edi*8]",};
	}

	static String[] get64BitsEffectiveAddress_prefix() {
		return Arrays.stream(get64BitsEffectiveAddress()).map(s -> "qword " + s).toArray(String[]::new);
	}

	static String[] get64BitsEffectiveAddress() {
		return new String[]{
			"[rax]",
			"[rcx]",
			"[rdx]",
			"[rbx]",
			"[0x1234567812345678]",
			"[rbp]",
			"[rsi]",
			"[rdi]",
			"[rax*2]",
			"[rcx*2]",
			"[rdx*2]",
			"[rbx*2]",
			"[rbp*2]",
			"[rsi*2]",
			"[rdi*2]",
			"[rax*4]",
			"[rcx*4]",
			"[rdx*4]",
			"[rbx*4]",
			"[rbp*4]",
			"[rsi*4]",
			"[rdi*4]",
			"[rax*8]",
			"[rcx*8]",
			"[rdx*8]",
			"[rbx*8]",
			"[rbp*8]",
			"[rsi*8]",
			"[rdi*8]",};
	}

	public static String[] get8BitsRegisters() {
		return new String[]{"al", "cl", "dl", "bl", "ah", "ch", "dh", "bh", "bpl", "spl", "dil", "sil"};
	}

	public static String[] get16BitsRegisters() {
		return new String[]{"ax", "cx", "dx", "bx", "bp", "sp", "di", "si"};
	}

	public static String[] get32BitsRegisters() {
		return new String[]{"eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d", "r15d"};
	}

	public static String[] get64BitsRegisters() {
		return new String[]{"rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15"};
	}

	public static String[] concatArrays(String[]... args) {
		return Stream.of(args).flatMap(Stream::of).toArray(String[]::new);
	}

	public static ArrayList<String> concatArraysToList(String[]... args) {
		return new ArrayList<String>(Arrays.asList(concatArrays(args)));
	}

	public static ArrayList<String> generateInstruction(String instruction) {
		ArrayList<String> list = new ArrayList<String>();
		for (String s1 : allCombinations) {
			for (String s2 : allCombinations) {
				list.add(instruction + " " + s1 + " " + s2);
			}
		}
		return list;
	}

	public static String[] translateInstruction(String testingInstruction) {
		String patternStr = "\\$\\{[^\\}]+\\}";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(testingInstruction);

		ArrayList<String> groups = new ArrayList<String>();
		ArrayList<Integer> starts = new ArrayList<Integer>();
		ArrayList<Integer> ends = new ArrayList<Integer>();
		while (matcher.find()) {
//			MessageHandler.println("matcher.group() : " + matcher.group() + ", " + matcher.start() + " > " + matcher.end());
			groups.add(matcher.group());
			starts.add(matcher.start());
			ends.add(matcher.end());
		}

		if (groups.isEmpty()) {
			return new String[]{testingInstruction};
		} else {
			TreeSet<String> instructions = new TreeSet<String>();
			for (int x = 0; x < groups.size(); x++) {
				String group = groups.get(x);
//				int start = starts.get(x);
//				int end = ends.get(x);
				ArrayList<String> elements = translatedMap.get(group);
				if (elements == null) {
					MessageHandler.errorPrintln("Wrong test case : " + testingInstruction);
					System.exit(10);
				}
				if (instructions.isEmpty()) {
					for (String element : elements) {
						String translatedInstruction = testingInstruction.replaceFirst(Pattern.quote(group), element);
						String temp2[] = translateInstruction(translatedInstruction);
						instructions.addAll(Arrays.asList(temp2));
					}
				}
			}

			return instructions.toArray(new String[0]);
		}
	}

	public static byte[] compileIA32(String instruction, int bits) {
		try {
			AssemblerLexer lexer = new AssemblerLexer(CharStreams.fromString("(bits " + bits + "){" + instruction + "}"));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			AssemblerParser parser = new AssemblerParser(tokenStream);
			parser.removeErrorListeners();
			parser.addErrorListener(new UnderlineListener());
//			parser.addErrorListener(new UnderlineListener());
//			ParseTreeWalker walker = new ParseTreeWalker();
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			MAL listener = new MAL(parser, out);
			parser.mal.errorStream = null;
			AssemblerParser.AssembleContext context = parser.assemble();
//			walker.walk(listener, context);
			byte bytes[] = parser.mal.encoder.out.toByteArray();
			if (bytes.length == 0) {
				return null;
			} else {
				return bytes;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void generateConsoleTable(MyTableModel model) {
		MyTableRenderer renderer = new MyTableRenderer();
		hk.quantr.javatexttable.TextTable table = new hk.quantr.javatexttable.TextTable(model, renderer);
		table.eraseScreen = true;
		table.showGridLine = true;
		table.margin = 1;
		table.sortColumn = 1;
		table.ascending = true;
		MessageHandler.println(table.render());
	}

	public static void generateHtmlTable(MyTableModel model, String filepath) {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>\n");
		sb.append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n");
		sb.append("<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n"
				+ "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n"
				+ "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n");
		sb.append("<script src=\"https://code.jquery.com/jquery-3.4.1.min.js\" async></script>\n"
				+ "<script>\n"
				+ "	var lastB;\n"
				+ "	function show(b){\n"
				+ "		lastB=b;\n"
				+ "		var ok=0;\n"
				+ "		var error=0;\n"
				+ "\n"
				+ "		var s=$('#drop1').val();\n"
				+ "		$('tr').each(function(index, value) {\n"
				+ "			if (index!=0){\n"
				+ "				tr=$(value);\n"
				+ "				td=tr.find('td.red');\n"
				+ "				td2=tr.find('td:nth-child(2)');\n"
				+ "				instruction=td2.html().split(' ')[0];\n"
				+ "				if (s!=null && !instruction.startsWith(s)){\n"
				+ "					console.log(instruction+ ' = ' +s);\n"
				+ "					tr.css('display','none');\n"
				+ "				}else{\n"
				+ "					if (b){\n"
				+ "						tr.css('display','');\n"
				+ "					}\n"
				+ "					if (td.length==0){\n"
				+ "						tr.css('display',b?'':'none');\n"
				+ "						ok++;\n"
				+ "					}else{\n"
				+ "						error++;\n"
				+ "					}\n"
				+ "				}\n"
				+ "			}\n"
				+ "			$('#infoDiv').html(error+ ' / '+(ok+error)+' in error');\n"
				+ "		});\n"
				+ "\n"
				+ "		$('#drop1').change(function(x){\n"
				+ "			show(lastB);\n"
				+ "		});\n"
				+ "	}\n"
				+ "	function showMisEncoded(){\n"
				+ "		var ok=0;\n"
				+ "		var error=0;\n"
				+ "\n"
				+ "		var s=$('#drop1').val();\n"
				+ "		$('tr').each(function(index, value) {\n"
				+ "			if (index!=0){\n"
				+ "				tr=$(value);\n"
				+ "				td=tr.find('td.red,td.blue');\n"
				+ "				td2=tr.find('td:nth-child(2)');\n"
				+ "				instruction=td2.html().split(' ')[0];\n"
				+ "				if ((s!=null && !instruction.startsWith(s)) || td.length){\n"
				+ "					console.log(instruction+ ' = ' +s);\n"
				+ "					tr.css('display','none');\n"
				+ "				}else{\n"
				+ "					tr.css('display','');\n"
				+ "					error++;\n"
				+ "				}\n"
				+ "			}\n"
				+ "			$('#infoDiv').html(error+ ' / in mis-encoded');\n"
				+ "		});\n"
				+ "\n"
				+ "		$('#drop1').change(function(x){\n"
				+ "			show(lastB);\n"
				+ "		});\n"
				+ "	}\n"
				+ "	$(function(){\n"
				+ "		var temp=[];\n"
				+ "		$('#drop1').append('<option value=\"\">All</option>');\n"
				+ "		$('tr').each(function(index, value) {\n"
				+ "			if (index!=0){\n"
				+ "				tr=$(value);\n"
				+ "				td=tr.find('td.red');\n"
				+ "				td2=tr.find('td:nth-child(2)');\n"
				+ "				instruction=td2.html().split(' ')[0];\n"
				+ "				// dropdown\n"
				+ "				bingo=true;\n"
				+ "				if (temp.includes(instruction)){\n"
				+ "					bingo=false;\n"
				+ "				}\n"
				+ "				if (bingo){\n"
				+ "					temp.push(instruction);\n"
				+ "					console.log(instruction);\n"
				+ "					$('#drop1').append('<option value=\"'+instruction+'\">'+instruction+'</option>');\n"
				+ "				}\n"
				+ "			}\n"
				+ "		});\n"
				+ "		show(true);\n"
				+ "	});\n"
				+ "</script>\n");
		sb.append("<style>\n"
				+ "  body {\n"
				+ "    font-family: Verdana, Geneva, Tahoma, sans-serif;\n"
				+ "    padding: 5px;\n"
				+ "  }\n"
				+ "\n"
				+ "  table {\n"
				+ "    border-collapse: collapse;\n"
				+ "    margin-top:5px;\n"
				+ "  }\n"
				+ "\n"
				+ "  tr:hover td{\n"
				+ "  		font-weight: bold;\n"
				+ "  }"
				+ "\n"
				+ "  table,\n"
				+ "  th,\n"
				+ "  td {\n"
				+ "    border: 1px solid black;\n"
				+ "  }\n"
				+ "\n"
				+ "  th, td {\n"
				+ "    padding: 2px;\n"
				+ "    font-size: 12px;\n"
				+ "  }\n"
				+ "\n"
				+ "  .red{\n"
				+ "	  background-color: pink;\n"
				+ "  }\n"
				+ "\n"
				+ "  .blue{\n"
				+ "	  background-color: #cbc9ff;\n"
				+ "  }"
				+ "\n"
				+ "  .shouldNotEncode{\n"
				+ "	  background-color: yellow;\n"
				+ "  }\n"
				+ "</style>\n");
		sb.append("<body>\n");
		sb.append("<button onclick=\"show(true);\" class=\"btn btn-primary\">Show All</button>\n"
				+ "<button onclick=\"show(false);\" class=\"btn btn-danger\">Show Error only</button>\n"
				+ "<button onclick=\"showMisEncoded();\" class=\"btn btn-warning\">Show Mis Encoded only</button>\n"
				+ "<span id=\"infoDiv\"></span>\n"
				+ "<select id=\"drop1\">\n"
				+ "</select>\n");
		sb.append("<table>\n");
		sb.append("<tr>\n");
		for (int x = 0; x < model.getColumnCount(); x++) {
			sb.append("<th>" + model.getColumnName(x) + "</th>\n");
		}
		sb.append("</tr>\n");

		JTable table = new JTable(model);
		TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
		table.setRowSorter(sorter);
		List<RowSorter.SortKey> sortKeys = new ArrayList<>();

		int columnIndexToSort = 1;
		sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));

		sorter.setSortKeys(sortKeys);
		sorter.sort();

		for (int row = 0; row < table.getRowCount(); row++) {
			sb.append("<tr>\n");
			for (int col = 0; col < table.getColumnCount(); col++) {
				String className = "";
				if (col == 2 || col == 3) {
					boolean ok = table.getValueAt(row, 2).equals(table.getValueAt(row, 3));
					if (ok) {
						className = "blue";
					} else {
						className = "red";
					}
				} else if (col == 4 || col == 5) {
					boolean ok = table.getValueAt(row, 4).equals(table.getValueAt(row, 5));
					if (ok) {
						className = "blue";
					} else {
						className = "red";
					}
				} else if (col == 6 || col == 7) {
					boolean ok = table.getValueAt(row, 6).equals(table.getValueAt(row, 7));
					if (ok) {
						className = "blue";
					} else {
						className = "red";
					}
				}
				if (col > 1 && table.getValueAt(row, 3).equals("") && table.getValueAt(row, 3).equals("") && table.getValueAt(row, 3).equals("")) {
					className = "shouldNotEncode";
				}
				if (col == 0) {
					sb.append("<td class=\"" + className + "\">" + row + "</td>\n");
				} else {
					sb.append("<td class=\"" + className + "\">" + table.getValueAt(row, col) + "</td>\n");
				}
			}
			sb.append("</tr>\n");
		}

		sb.append("</table>\n");
		sb.append("</body\n>");
		sb.append("</html>\n");

		File file = new File(filepath);
		try {
			FileUtils.writeStringToFile(file, sb.toString(), "utf-8");
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public static Pair<String, byte[]> compileNasm(String instruction, int bits) {
		try {
			File temp = File.createTempFile("temp", ".asm");
			File tempBin = File.createTempFile("temp", ".bin");
			String code = "bits " + bits + "\n";
			code += instruction + "\n";
			FileUtils.write(temp, code, "UTF-8");
			String line;

			if (System.getProperty("os.name").contains("Mac")) {
				line = "/opt/local/bin/nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			} else if (System.getProperty("os.name").toLowerCase().contains("linux")) {
				line = "nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			} else {
				line = "nasm -o " + tempBin.getAbsolutePath() + " " + temp.getAbsolutePath();
			}

			Process p = rt.exec(line.split(" "));
			p.waitFor();
			String err = IOUtils.toString(p.getErrorStream(), "utf8");
			err = err.replaceFirst(".*error:", "error:");

			if (temp.exists()) {
				temp.delete();
			}
			if (tempBin.exists()) {
				byte bytes[] = FileUtils.readFileToByteArray(tempBin);

				tempBin.delete();
				return new ImmutablePair<String, byte[]>(err, bytes);
			}
			return new ImmutablePair<String, byte[]>(err, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
