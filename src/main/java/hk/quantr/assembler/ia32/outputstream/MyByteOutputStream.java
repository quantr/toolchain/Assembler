//package hk.quantr.assembler.ia32.outputstream;
//
//import java.io.IOException;
//import java.io.OutputStream;
//
///**
// *
// * @author Peter (peter@quantr.hk)
// */
//public class MyByteOutputStream extends OutputStream {
//
//	String output = "";
//
//	@Override
//	public void write(int b) throws IOException {
//		String s = Integer.toHexString(b & 0xff);
//		if (s.length() % 2 == 1) {
//			s = "0" + s;
//		}
//		output += s;
//	}
//
//	@Override
//	public String toString() {
//		return output;
//	}
//}
