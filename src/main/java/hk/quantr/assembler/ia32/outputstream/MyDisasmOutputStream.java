package hk.quantr.assembler.ia32.outputstream;

import hk.quantr.assembler.ia32.MAL;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyDisasmOutputStream extends OutputStream {

	String output = "";

	@Override
	public void write(int b) throws IOException {
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String instruction = null;
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			if (stackTraceElement.getClassName().equals(MAL.class.getName())) {
				instruction = stackTraceElement.getMethodName().replace("enter", "");
			}
		}
		output += (instruction.toLowerCase() + "\t:\t" + Integer.toHexString(b)) + "\n";
	}

	@Override
	public void write(byte b[]) throws IOException {
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String instruction = null;
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			if (stackTraceElement.getClassName().equals(MAL.class.getName())) {
				instruction = stackTraceElement.getMethodName().replace("enter", "");
			}
		}
		output += (instruction.toLowerCase() + "\t:\t");
		for (byte temp : b) {
			output += (Integer.toHexString(temp & 0xFF) + " ");
		}
		output += ("\n");
	}

	@Override
	public String toString() {
		return output;
	}
}
