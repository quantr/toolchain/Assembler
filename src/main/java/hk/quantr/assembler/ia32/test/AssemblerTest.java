package hk.quantr.assembler.ia32.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AssemblerTest {

	public enum Architecture {
		ia32, arm
	}

	Architecture architecture() default Architecture.ia32;
	

//	String instruction() default "";
//
//	String instruction2() default "";
//
//	String instruction3() default "";
//
//	String instruction4() default "";
	
	String[] instructions();
	

//	String instruction16() default "";
//
//	String instruction32() default "";
//
//	String instruction64() default "";
}
