package hk.quantr.assembler.ia32;

/**
 *
 * @author Kelvinyu
 */
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.javalib.CommonLib;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.lang.Math;
import java.math.BigInteger;
import java.util.ArrayList;
import org.antlr.v4.runtime.Token;

public class Encoder {

	private int bits = 16;
	private boolean left_isReg;
	private boolean right_isReg;
	private boolean left_isIMM;
	private boolean right_isIMM;
	private int left_opSize;
	private int right_opSize;
	private int left_expSize;
	private int right_expSize;
	private int left_opCol;
	private int right_opCol;
	private int left_opRow;
	private int right_opRow;
	private String left_disp;
	private String right_disp;
	private boolean rex_x;
	private boolean left_rex_r;
	private boolean right_rex_r;
	private boolean left_rex_b;
	private boolean right_rex_b;
	private boolean is_sib;
	private boolean revert_leftright;
	private int sib_col;
	private int sib_row;
	private boolean table16;
	private boolean is64;
	private Token leftToken;
	private Token rightToken;
	private boolean do_nothing = false;
	private boolean startBit = false;
	private String cc_flag = "";
	private int numOfOp = 2;
	public int segType = 0;
	public String ins;
	public PrintStream errorStream = System.err;
	public ByteArrayOutputStream out = new ByteArrayOutputStream();
	public StringBuilder listFile = new StringBuilder();
	private boolean takeLeft = true;

	private class IMM {

		public String imm_data;
		public int imm_bits;

		IMM() {
			imm_data = "X";
			imm_bits = 0;
		}

		IMM(IMM other) {
			this.imm_data = other.imm_data;
			this.imm_bits = other.imm_bits;
		}

		IMM(String data, int bits) {
			this.imm_data = data;
			this.imm_bits = bits;
		}
	}

	public Encoder() {
		IA32.initConditionTable();
	}

	public void initCC(String cc) {
		cc_flag = cc;
	}

	public void initIns(String ins) {
		this.ins = ins;
	}

	public void initStartBit(boolean start) {
		startBit = start;
	}

	public void SetRDSide(boolean side) {
		takeLeft = side;
	}

	public void revertREX() {
		boolean temp = right_rex_r;
		right_rex_r = right_rex_b;
		right_rex_b = temp;
	}

	public void initSegment(int type) {
		this.segType = type;
	}

	public void initCondition(boolean table16, boolean is64, String leftTokenType) {
		this.table16 = table16;
		this.is64 = is64;

		ErrorMsgs error = new ErrorMsgs(ins, bits, left_isReg, right_isReg, left_opSize, right_opSize, left_expSize, right_expSize,
				left_opRow, right_opRow, table16, is64, right_isIMM, startBit, left_opRow, right_opRow, numOfOp, leftTokenType);

		if (error.isError()) {
			do_nothing = true;
			if (error.getLeftError()) {
				MessageHandler.errorPrintln("grammar error in line " + leftToken.getLine() + ": position " + leftToken.getCharPositionInLine() + " " + error.getErrorMsg());
			} else {
				MessageHandler.errorPrintln("grammar error " + leftToken.getLine() + ":" + rightToken.getCharPositionInLine() + " " + error.getErrorMsg());
			}
		}
	}

	public void initCondition(boolean table16, boolean is64) {
		this.table16 = table16;
		this.is64 = is64;
		this.revert_leftright = false;

		ErrorMsgs error = new ErrorMsgs(ins, bits, left_isReg, right_isReg, left_opSize, right_opSize, left_expSize, right_expSize,
				left_opRow, right_opRow, table16, is64, right_isIMM, startBit, left_opRow, right_opRow, numOfOp);

		if (error.isError()) {
			do_nothing = true;
			if (error.getLeftError()) {
				MessageHandler.errorPrintln("grammar error in line " + leftToken.getLine() + ": position " + leftToken.getCharPositionInLine() + " " + error.getErrorMsg());
			} else {
				MessageHandler.errorPrintln("grammar error " + leftToken.getLine() + ":" + rightToken.getCharPositionInLine() + " " + error.getErrorMsg());
			}
		}
	}

	public void initForOne(boolean isReg, int opSize, int expSize) {
		this.left_isReg = isReg;
		this.left_opSize = opSize;
		this.left_expSize = expSize;
	}

	public void setRight_opCol(int right_opCol) {
		this.right_opCol = right_opCol;
	}

	public void setRight_opRow(int right_opRow) {
		this.right_opRow = right_opRow;
	}

	public void setLeft_opRow(int left_opRow) {
		this.left_opRow = left_opRow;
	}

	public void setRevert_leftright(boolean revert_leftright) {
		this.revert_leftright = revert_leftright;
	}

	public void setLeftDisp(String leftDisp) {
		this.left_disp = leftDisp;
	}

	public void setRightDisp(String rightdisp) {
		this.right_disp = rightdisp;
	}

	public void setRightRex_rb(boolean right_rex_b) {
		this.right_rex_b = right_rex_b;
	}

	public void initLeftToken(Token leftToken) {
		this.leftToken = leftToken;
	}

	public void initRightToken(Token rightToken) {
		this.rightToken = rightToken;
	}

	public void initBit(int bit) {
		this.bits = bit;
	}

	public void initREX_X(boolean rex_x) {
		this.rex_x = rex_x;
	}

	public void init_SIB(boolean is_sib, int sib_col, int sib_row) {
		this.is_sib = is_sib;
		this.sib_col = sib_col;
		this.sib_row = sib_row;
	}

	//funciton below newly added
	public void rightInit(boolean right_isReg, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean right_rex_rb) {

		this.right_isReg = right_isReg;
		this.right_opSize = right_opSize;
		this.right_expSize = right_expSize;

		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		this.right_disp = right_disp;

		this.right_rex_b = right_rex_rb;
	}

	public void leftInit(boolean left_isReg, int left_opSize, int left_expSize,
			int left_opCol, int left_opRow, String left_disp, boolean left_rex_rb) {

		this.left_isReg = left_isReg;
		this.left_opSize = left_opSize;
		this.left_expSize = left_expSize;

		this.left_opCol = left_opCol;
		this.left_opRow = left_opRow;
		this.left_disp = left_disp;

		this.left_rex_b = left_rex_rb;
	}

	public void setNumofOp(int n) {
		numOfOp = n;
	}

	public int getNumofOp() {
		return numOfOp;
	}

	public void resetCol(int col) {
		right_opCol = col;
	}

	public void resetRightOpSize(int size) {
		right_opSize = size;
	}

	public void resetLeftOpSize(int size) {
		left_opSize = size;
	}

	public int getBits() {
		return this.bits;
	}

	public String getIns() {
		return this.ins;
	}

	public void rightInit(boolean right_isReg, boolean right_isIMM, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean rex_rb) {

		this.right_isReg = right_isReg;
		if (!startBit && !right_isReg) {
			this.right_opSize = left_opSize;
		} else if (!startBit && !left_isReg) {
			this.right_opSize = right_opSize;
			left_opSize = right_opSize;
		} else {
			this.right_opSize = right_opSize;
		}
		this.right_expSize = right_expSize;
		this.right_isIMM = right_isIMM;
		if (right_isIMM) {
			this.right_opCol = right_opCol;
			this.right_disp = left_disp;
		} else {
			this.right_opCol = right_isReg ? right_opCol : left_opCol;
			this.right_disp = right_isReg ? left_disp : right_disp;
		}
		this.right_opRow = right_opRow;

		if (IA32.changeType(ins)) {
			this.right_rex_r = left_rex_b;
			this.right_rex_b = rex_rb;
		} else {
			this.right_rex_r = right_isReg ? rex_rb : left_rex_b;
			this.right_rex_b = !right_isReg ? rex_rb : left_rex_b;
		}

	}
	//function below newly added

	public void leftInit(boolean left_isReg, boolean left_isIMM, int left_opSize, int left_expSize,
			int left_opCol, int left_opRow, String left_disp, boolean rex_rb) {

		this.left_isReg = left_isReg;
		if (!startBit && !left_isReg) {
			this.left_opSize = right_opSize;
		} else if (!startBit && !left_isReg) {
			this.left_opSize = left_opSize;
			right_opSize = left_opSize;
		} else {
			this.left_opSize = left_opSize;
		}
		this.left_expSize = left_expSize;
		this.left_isIMM = left_isIMM;
		if (left_isIMM) {
			this.left_opCol = left_opCol;
			this.left_disp = right_disp;
		} else {
			this.left_opCol = left_isReg ? left_opCol : right_opCol;
			this.left_disp = left_isReg ? right_disp : left_disp;
		}
		this.left_opRow = left_opRow;

		if (IA32.changeType(ins)) {
			this.left_rex_r = right_rex_b;
			this.left_rex_b = rex_rb;
		} else {
			this.left_rex_r = left_isReg ? rex_rb : right_rex_b;
			this.left_rex_b = !left_isReg ? rex_rb : right_rex_b;
		}

	}

	public int getOperandSize() {
		return right_opSize;
	}

	public int getExpSize() {
		return right_expSize;
	}

	public void checkPrefix() {
		// prefix 66 -> operand size (btye, word, dword, ..)
		// prefix 67 -> experssion inside mem]
		int opBits = bits;
		int adrBits = bits;
		if (ins.equals("adox") || ins.equals("adcx")) {
			opBits = 32;
		}
		if (ins.equals("outsw")) {
			left_opSize = 16;
			int opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
			if (opPrefix != 0) {
				out.write(opPrefix);
			}
		}
		if (ins.equals("outsd")) {
			left_opSize = 32;
			int opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
			if (opPrefix != 0) {
				out.write(opPrefix);
			}
		}

		if (!do_nothing) {
			if (ins.equals("jcxz")) {
				left_opSize = 16;
				int addrPrefix = IA32.getAddrOverridePrefix(opBits, left_opSize);
				if (addrPrefix != 0) {
					out.write(addrPrefix);
				}
			} else if (ins.equals("jecxz")) {
				left_opSize = 32;
				int addrPrefix = IA32.getAddrOverridePrefix(opBits, left_opSize);
				if (addrPrefix != 0) {
					out.write(addrPrefix);
				}
			} else if (numOfOp == 1) {
				int opPrefix = 0;
				int addrPrefix = 0;
				if (!left_isReg) {
					addrPrefix = IA32.getAddrOverridePrefix(adrBits, left_expSize);
				}

				opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
				if (opPrefix != 0 && opPrefix != -1 && !IA32.NoOperandPrefix(ins)) {
					out.write(opPrefix);
				}

				if (addrPrefix != 0 && addrPrefix != -1) {
					out.write(addrPrefix);
				}
			} else if (!right_isIMM) {
				if (!left_isReg && right_isReg) {
					//if (left_opSize == right_opSize) {
					int addrPrefix = IA32.getAddrOverridePrefix(adrBits, left_expSize);
					int opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(opBits, right_opSize));
					if (opPrefix != 0 && opPrefix != -1 && !IA32.NoOperandPrefix(ins)) {
						out.write(opPrefix);
					}

					if (addrPrefix != 0 && addrPrefix != -1) {
						out.write(addrPrefix);
					}
					/*} else {
						printError("error: invalid combination of opcode and operands");
					}*/

				} else if (left_isReg && !right_isReg) {
					//if (left_opSize == right_opSize) {
					int addrPrefix = IA32.getAddrOverridePrefix(adrBits, right_expSize);
					int opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(opBits, right_opSize));
					if (opPrefix != 0 && opPrefix != -1 && !IA32.NoOperandPrefix(ins)) {
						out.write(opPrefix);
					}

					if (addrPrefix != 0 && addrPrefix != -1) {
						out.write(addrPrefix);
					}
					/*} else {
						printError("error: invalid combination of opcode and operands");
					}*/
				} else if (left_isReg && right_isReg) {

					//if (left_opSize == right_opSize) {
					int opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(opBits, right_opSize));

					if (opPrefix != 0 && opPrefix != -1 && !IA32.NoOperandPrefix(ins)) {
						out.write(opPrefix);
					}
					/*} else {
						printError("error: invalid combination of opcode and operands");
					}*/
				}

			} else {
				int opPrefix = 0;
				int addrPrefix = 0;

				opPrefix = IA32.getOpOverridePrefix(opBits, left_opSize);

				if (!left_isReg) {
					addrPrefix = IA32.getAddrOverridePrefix(adrBits, left_expSize);
				}

				if (opPrefix != 0 && opPrefix != -1) {
					out.write(opPrefix);
				}

				if (addrPrefix != 0 && addrPrefix != -1) {
					out.write(addrPrefix);
				}
			}
		}
	}

	public void printError(String str) {
		if (errorStream == null) {
			return;
		}
		errorStream.println(str);
	}

	public void segmentPrefix() {
		int prefix = IA32.segmentPrefix(segType);
		if (prefix != -1 && !do_nothing) {
			out.write(prefix);
		}
	}

	public void output_left_right(boolean setw, String... args) { //new version of outputLR
		try {
			int left = left_opRow;
			int right = right_opRow;
			int opRow;

			if (!IA32.changeType(ins)) {
				if (!right_isReg) {
					opRow = right;
				} else {
					opRow = left;
				}
			} else {
				if (left_isReg) {
					opRow = right;
				} else {
					opRow = left;
				}
			}
			if (!do_nothing) {

				int opCol = right_opCol;
				if (revert_leftright) {
					opCol = left_opCol;
					//opCol = right_opCol;
					opRow = right_opRow;
					//opRow = left_opRow;
				}

				if (IA32.rexReg(is64, left, right) == 1 && bits != 64) {
					printError("invaild operand in non-64-bit-mode ");
				} else if (IA32.rexReg(is64, left, right) == 1 && bits == 64) {
					if (!ins.equals("popcnt")) {
						if (!IA32.NoREXPrefix(ins, left_opRow, right_opRow, left_opSize, right_opSize)) {
							out.write(IA32.getREX(setw, right_rex_r, rex_x, right_rex_b));
						}

						for (String arg : args) {
							out.write(CommonLib.string2bytes(arg));
						}
					} else {
						out.write(CommonLib.string2bytes(args[0]));
						out.write(IA32.getREX(setw, right_rex_r, rex_x, right_rex_b));
						out.write(CommonLib.string2bytes(args[1]));
						out.write(CommonLib.string2bytes(args[2]));
					}

					int b = IA32.getModRM(bits, opCol, opRow);
					out.write(b);
					if (is_sib) {
						int z = IA32.getSIB(sib_col, sib_row);
						out.write(z);
					}

					if (!right_disp.equals("no")) {
						out.write(CommonLib.string2bytes(right_disp));
					}
				} else {

					for (String arg : args) {
						out.write(CommonLib.string2bytes(arg));
					}

					int b = IA32.getModRM(bits, opCol, opRow);
					out.write(b);
					if (is_sib) {
						int z = IA32.getSIB(sib_col, sib_row);
						out.write(z);
					}
					if ((b == 52 && right_disp.equals("0x00"))) {
						right_disp = "no";
					} else if (b == 52 && right_disp.equals(0x00000000)) {
						right_disp = "0x000000";
					}
					if (!right_disp.equals("no") /*&&(!(b == 52 && right_disp.equals("0x00")))*/) {
						out.write(CommonLib.string2bytes(right_disp));
					}
				}
			}

		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output_left_right(boolean setw, boolean modrm, String... args) { //new version of outputLR
		try {
			int left = left_opRow;
			int right = right_opRow;
			int opRow;

			if (!IA32.changeType(ins)) {
				if (!right_isReg) {
					opRow = right;
				} else {
					opRow = left;
				}
			} else {
				if (left_isReg) {
					opRow = right;
				} else {
					opRow = left;
				}
			}
			if (!do_nothing) {

				int opCol = right_opCol;
				if (revert_leftright) {
					opCol = left_opCol;
					//opCol = right_opCol;
					opRow = right_opRow;
					//opRow = left_opRow;
				}

				if (IA32.rexReg(is64, left, right) == 1 && bits != 64) {
					printError("invaild operand in non-64-bit-mode ");
				} else if (IA32.rexReg(is64, left, right) == 1 && bits == 64) {
					if (!ins.equals("popcnt")) {
						if (!IA32.NoREXPrefix(ins, left_opRow, right_opRow, left_opSize, right_opSize)) {
							out.write(IA32.getREX(setw, right_rex_r, rex_x, right_rex_b));
						}

						for (String arg : args) {
							out.write(CommonLib.string2bytes(arg));
						}
					} else {
						out.write(CommonLib.string2bytes(args[0]));
						out.write(IA32.getREX(setw, right_rex_r, rex_x, right_rex_b));
						out.write(CommonLib.string2bytes(args[1]));
						out.write(CommonLib.string2bytes(args[2]));
					}

					int b = IA32.getModRM(bits, opCol, opRow);
					out.write(b);
					if (is_sib) {
						int z = IA32.getSIB(sib_col, sib_row);
						out.write(z);
					}

					if (!right_disp.equals("no")) {
						out.write(CommonLib.string2bytes(right_disp));
					}
				} else {

					for (String arg : args) {
						out.write(CommonLib.string2bytes(arg));
					}
					if (modrm) {
						int b = IA32.getModRM(bits, opCol, opRow);
						out.write(b);
						if (is_sib) {
							int z = IA32.getSIB(sib_col, sib_row);
							out.write(z);
						}
						if ((b == 52 && right_disp.equals("0x00"))) {
							right_disp = "no";
						} else if (b == 52 && right_disp.equals(0x00000000)) {
							right_disp = "0x000000";
						}
					}
					if (!right_disp.equals("no") /*&&(!(b == 52 && right_disp.equals("0x00")))*/) {
						out.write(CommonLib.string2bytes(right_disp));
					}
				}
			}

		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output_addRD(boolean setw, String opcode, int RD_value) {
		try {
			if (!do_nothing) {
				if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits == 64 && !IA32.NoREXPrefix(ins, left_opRow, right_opRow, left_opSize, right_opSize)) {
					if (numOfOp == 1) {
						out.write(IA32.getREX(setw, false, rex_x, left_rex_b));
					} else {
						out.write(IA32.getREX(setw, false, rex_x, right_rex_b));
					}
					if (!opcode.equals("no")) {
						out.write(CommonLib.string2bytes(opcode));
					}
					if (takeLeft) {
						out.write(IA32.getRD(RD_value, left_opRow));
					} else {
						out.write(IA32.getRD(RD_value, right_opRow));
					}
				} else {
					if (!opcode.equals("no")) {
						out.write(CommonLib.string2bytes(opcode));
					}
					if (takeLeft) {
						out.write(IA32.getRD(RD_value, left_opRow));
					} else {
						out.write(IA32.getRD(RD_value, right_opRow));
					}
				}

			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output(boolean setW, String opcode, String imm) {
		try {
			if (!do_nothing) {
				if (setW) {
					out.write(IA32.getREX(setW, false, rex_x, right_rex_b));
				}

				out.write(CommonLib.string2bytes(opcode));
				if (!left_disp.equals("no")) {
					out.write(CommonLib.string2bytes(left_disp));
				}
				if (!imm.equals("no")) {
					out.write(CommonLib.string2bytes(imm));
				}
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output_one_operand(boolean setW, String... args) {
		try {
			if (!do_nothing) {
				if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
					printError("invaild operand in non-64-bit-mode ");
				} else {
					ArrayList<String> tmp = new ArrayList<>();
					tmp.add("rdfsbase");
					tmp.add("rdgsbase");
					tmp.add("rdpid");
					tmp.add("wrfsbase");
					tmp.add("wrgsbase");
					if (!tmp.contains(ins)) {
						if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && !IA32.NoREXPrefix(ins, left_opRow, right_opRow, left_opSize, right_opSize)) {
							out.write(IA32.getREX(setW, false, rex_x, right_rex_b));
						}
						for (String arg : args) {
							out.write(CommonLib.string2bytes(arg));
						}
					} else {
						out.write(CommonLib.string2bytes(args[0]));
						if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && !IA32.NoREXPrefix(ins, left_opRow, right_opRow, left_opSize, right_opSize)) {
							out.write(IA32.getREX(setW, false, rex_x, right_rex_b));
						}
						out.write(CommonLib.string2bytes(args[1]));
						out.write(CommonLib.string2bytes(args[2]));
					}

					int b = IA32.getModRM(bits, right_opCol, right_opRow);
					out.write(b);
					if (is_sib) {
						int z = IA32.getSIB(sib_col, sib_row);
						out.write(z);
					}

					if ((b == 52 && right_disp.equals("0x00"))) {
						right_disp = "no";
					} else if (b == 52 && right_disp.equals(0x00000000)) {
						right_disp = "0x000000";
					}
					// MessageHandler.println(right_disp);
					if (!right_disp.equals("no")) {
						out.write(CommonLib.string2bytes(left_disp));
					}

				}
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	private IMM immPreProcessing(String imm, int maxIMM) {
		BigInteger num;

		if (imm.contains("0x")) {
			num = new BigInteger(imm.substring(2), 16);
		} else {
			num = new BigInteger(imm);
			num = new BigInteger(num.toString(16), 16);
		}

		int type;
		if (num.compareTo(new BigInteger("-128")) >= 0 && num.compareTo(new BigInteger("127")) <= 0) {
			type = 8;
		} else if (num.compareTo((new BigInteger("32768")).negate()) >= 0 && num.compareTo(new BigInteger("32767")) <= 0) {
			type = 16;
		} else if (num.compareTo((new BigInteger("-2147483648")).negate()) >= 0 && num.compareTo(new BigInteger("2147483647")) <= 0) {
			type = 32;
		} else {
			type = 64;
		}

		if (type > maxIMM) {
			type = maxIMM;
		}

		String res = num.toString(16);
		if (res.length() - type / 4 >= 0) {
			res = res.substring(res.length() - type / 4, res.length());
		}

		return new IMM(res, type);
	}

	private IMM immPreProcessing(String imm, int maxIMM, boolean isRel) {
		BigInteger num;

		if (imm.contains("0x")) {
			num = new BigInteger(imm.substring(2), 16);
		} else {
			num = new BigInteger(imm);
			num = new BigInteger(num.toString(16), 16);
		}
		if (ins.equals("call") || ins.equals("jmp")) {
			if (bits == 16) {
				num = num.subtract(new BigInteger("3", 16));
			} else {
				num = num.subtract(new BigInteger("5", 16));
			}
		} else if (ins.equals("jcxz")) {
			if (bits == 16) {
				num = num.subtract(new BigInteger("2", 16));
			} else {
				num = num.subtract(new BigInteger("3", 16));
			}
		} else if (ins.equals("jecxz")) {
			if (bits == 32) {
				num = num.subtract(new BigInteger("2", 16));
			} else {
				num = num.subtract(new BigInteger("3", 16));
			}
		} else if (ins.equals("jrcxz")) {
			if (bits == 64) {
				num = num.subtract(new BigInteger("2", 16));
			} else {
				num = num.subtract(new BigInteger("3", 16));
			}
		} else {
			if (bits == 16) {
				num = num.subtract(new BigInteger("4", 16));
			} else {
				num = num.subtract(new BigInteger("6", 16));
			}
		}
		int type;
		if (num.compareTo(new BigInteger("-128")) >= 0 && num.compareTo(new BigInteger("127")) <= 0) {
			type = 8;
		} else if (num.compareTo((new BigInteger("32768")).negate()) >= 0 && num.compareTo(new BigInteger("32767")) <= 0) {
			type = 16;
		} else if (num.compareTo((new BigInteger("-2147483648")).negate()) >= 0 && num.compareTo(new BigInteger("2147483647")) <= 0) {
			type = 32;
		} else {
			type = 64;
		}

		if (type > maxIMM) {
			type = maxIMM;
		}

		String res = num.toString(16);
		if (res.length() - type / 4 >= 0) {
			res = res.substring(res.length() - type / 4, res.length());
		}

		return new IMM(res, type);
	}

	public String truncateIMM(String s, int maxIMM) {
		IMM res = immPreProcessing(s, maxIMM);
		String t = "0x";
		return t + res.imm_data;
	}

	private IMM immPostProcessing(IMM imm, int type) {
		if (imm.imm_bits < type) {
			return imm;
		} else if (imm.imm_bits == type) {
			boolean hasToAddZero = false;
			if (imm.imm_data.length() < 2 && type == 8) {
				hasToAddZero = true;
			} else if (imm.imm_data.length() < 4 && type == 16) {
				hasToAddZero = true;
			} else if (imm.imm_data.length() < 8 && type == 32) {
				hasToAddZero = true;
			} else if (imm.imm_data.length() < 16 && type == 64) {
				hasToAddZero = true;
			}

			if (hasToAddZero) {
				StringBuilder sb = new StringBuilder();
				int diff = type / 4 - imm.imm_data.length();

				for (int i = 0; i < diff; i++) {
					sb.append('0');
				}

				IMM res = new IMM(imm);
				res.imm_bits = type;
				res.imm_data = sb.toString() + imm.imm_data;
				return res;
			} else {
				return imm;
			}
		} else {
			IMM res = new IMM(imm);
			res.imm_bits = type;
			if (res.imm_data.length() - type / 4 >= 0) {
				res.imm_data = res.imm_data.substring(res.imm_data.length() - type / 4, res.imm_data.length());
			}
			return res;
		}
	}

	private IMM addLeadingZero(IMM imm, int type, int maxImm) {//imm, leftopsize,maxIMM
		if (imm.imm_bits < maxImm && right_isIMM || imm.imm_bits <= maxImm && !right_isIMM || ins.equals("push")) {
			boolean hasToAddZero = false;
			if (type > maxImm) {
				StringBuilder sb = new StringBuilder();
				int diff = maxImm / 4 - imm.imm_data.length();

				for (int i = 0; i < diff; i++) {
					sb.append('0');
				}

				IMM res = new IMM(imm);
				res.imm_data = sb.toString() + imm.imm_data;
				return res;
			} else {
				if (imm.imm_data.length() < 4 && (type == 16 || type == 8)) {
					type = 16;
					hasToAddZero = true;
				} else if (imm.imm_data.length() < 8 && type == 32) {
					hasToAddZero = true;
				} else if (imm.imm_data.length() < 16 && type == 64) {
					hasToAddZero = true;
				}

				if (hasToAddZero) {
					StringBuilder sb = new StringBuilder();
					int diff = type / 4 - imm.imm_data.length();

					for (int i = 0; i < diff; i++) {
						sb.append('0');
					}

					IMM res = new IMM(imm);
					res.imm_data = sb.toString() + imm.imm_data;
					return res;
				} else {
					return imm;
				}
			}
		} else {
			return imm;
		}
	}

	public void output_imm(String opcode[], String imm, int maxIMM) {
		/*  index	conditions
			0		rm_8    & imm8
			1		rm_> 16 & imm >_16
			2		rm_> 16 & imm8
			3	
		 */
		if (ins.equals("push")) //In push ins, left_opSize temporarily becomes a variable storing size of IMM
		{
			if (this.getBits() == 16) {
				maxIMM = 16;
			}
			IMM a = immPreProcessing(imm, maxIMM);
			left_opSize = a.imm_bits;
		}
		try {
			IMM realImm = immPreProcessing(imm, maxIMM);
			if (left_opSize == 8) {
				realImm = immPostProcessing(realImm, 8);
			} else if (left_opSize == 16) {
				realImm = immPostProcessing(realImm, 16);
			} else if (left_opSize == 32) {
				realImm = immPostProcessing(realImm, 32);
			}
			boolean setw = left_opSize == 64;
			if (!do_nothing) {
				if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
					printError("invaild operand in non-64-bit-mode ");
				} else {
					if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits == 64) {
						out.write(IA32.getREX(setw, false, rex_x, left_rex_b));
					}
					if (realImm.imm_bits == 8) {
						if (left_opSize == 8) {
							if (!opcode[0].equals("")) {
								out.write(CommonLib.string2bytes(opcode[0]));
							}
						} else {
							if (!opcode[2].equals("")) {
								out.write(CommonLib.string2bytes(opcode[2]));
							}
						}
					} else {
						if (ins.equals("push")) {
							realImm = addLeadingZero(realImm, maxIMM, maxIMM);
						} else {
							realImm = addLeadingZero(realImm, left_opSize, maxIMM);/////////////
						}
						if (!opcode[1].equals("")) {
							out.write(CommonLib.string2bytes(opcode[1]));
						}
					}
					int b = IA32.getModRM(bits, right_opCol, left_opRow);
					if (b != 0xfff) {
						out.write(b);
					}

					if (is_sib) {
						int z = IA32.getSIB(sib_col, sib_row);
						out.write(z);
					}
					if (!left_disp.equals("no")) {
						out.write(CommonLib.string2bytes(left_disp));
					}
					out.write(CommonLib.string2bytes("0x" + realImm.imm_data));
				}
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output_opmode(int mode, String... args) {
		try {
			if (mode == 1 && bits != 64) {
				for (String arg : args) {
					out.write(CommonLib.string2bytes(arg));
				}
			} else if (mode == 2 && bits == 64) {
				for (String arg : args) {
					out.write(CommonLib.string2bytes(arg));
				}
			}/* else{
				MessageHandler.errorPrintln("grammar error in line " + leftToken.getLine() + ": position " + leftToken.getCharPositionInLine() + "error : invalid operating bit mode" );
				
			}*/
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output_opmode(String... args) {
		try {
			if (!do_nothing) {
				for (String arg : args) {
					out.write(CommonLib.string2bytes(arg));
				}
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public boolean check_condition(boolean table16, boolean is64) {
		boolean result = true;
		if (right_isIMM) {
			if (table16 && bits == 64) {
				result = false;
			} else if (is64 && bits != 64) {
				result = false;
			} else if (left_opRow > 44 && bits != 64) {
				result = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				result = false;
			} else if (table16 && bits == 64) {
				result = false;
			} else if (left_opRow < 0 || right_opRow < 0) {
				result = false;
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				result = false;
			} else if (is64 && bits != 64) {
				result = false;
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				result = false;
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				result = false;
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				result = false;
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				result = false;
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				result = false;
			}
		}

		return result;
	}

	public String handle_dispvalue(String disp, int max_length) {
		if (disp.equals("0x00") || disp.equals("0x0000") || disp.equals("0x00000000")) {
			return disp;
		} else {
			IMM realdisp = immPreProcessing(disp, max_length);
			//realdisp = immPostProcessing(realdisp, max_length);
			realdisp = addLeadingZero(realdisp, left_opSize, max_length);
			return "0x" + realdisp.imm_data;
		}
	}

	public String handle_dispvalue(String disp, int max_length, boolean isRel) {
		if (disp.equals("0x00") || disp.equals("0x0000") || disp.equals("0x00000000")) {
			return disp;
		} else {
			IMM realdisp = immPreProcessing(disp, max_length, isRel);
			//realdisp = immPostProcessing(realdisp, max_length);
			BigInteger num = new BigInteger(realdisp.imm_data, 16);

			//the if clause below maybe dublicated codes, immPreProcessing() in encoder.java also has the subtracting function
			if (ins.equals("call")) {
				if (bits == 16) {
					num.subtract(new BigInteger("3"));
				} else {
					num.subtract(new BigInteger("5"));
				}
			} else {
				if (bits == 16) {
					num.subtract(new BigInteger("4"));
				} else {
					num.subtract(new BigInteger("6"));
				}
			}
			realdisp.imm_data = num.toString(16);
			if (!ins.equals("jcxz") && !ins.equals("jecxz") && !ins.equals("jrcxz")) {
				realdisp = addLeadingZero(realdisp, 32, max_length);
			}
			return "0x" + realdisp.imm_data;
		}
	}

	public String handle_dispvalue(String disp, int max_length, int opsize) {
		if (disp.equals("0x00") || disp.equals("0x0000") || disp.equals("0x00000000")) {
			return disp;
		} else {
			IMM realdisp = immPreProcessing(disp, max_length);
			//realdisp = immPostProcessing(realdisp, max_length);
			realdisp = addLeadingZero(realdisp, opsize, max_length);
			return "0x" + realdisp.imm_data;
		}
	}

	public String[] getCCOpcode() {

		return IA32.ccToOpcode(cc_flag, ins);
	}

	public boolean check_disp(String disp, int disp_type) {
		BigInteger num;
		if (disp.substring(0, 1).equals("0x")) {
			num = new BigInteger(disp.substring(2), 16);
		} else {
			num = new BigInteger(disp);
			num = new BigInteger(num.toString(16), 16);
		}
		int type;
		if (num.compareTo(new BigInteger("-128")) >= 0 && num.compareTo(new BigInteger("127")) <= 0) {
			type = 8;
		} else if (num.compareTo((new BigInteger("32768")).negate()) >= 0 && num.compareTo(new BigInteger("32767")) <= 0) {
			type = 16;
		} else if (num.compareTo((new BigInteger("-2147483648")).negate()) >= 0 && num.compareTo(new BigInteger("2147483647")) <= 0) {
			type = 32;
		} else {
			type = 64;
		}

		return type == disp_type;
	}

	public int getIMMSize(String imm) {
		BigInteger num;
		if (imm.substring(0, 1).equals("0x")) {
			num = new BigInteger(imm.substring(2), 16);
		} else {
			num = new BigInteger(imm);
			num = new BigInteger(num.toString(16), 16);
		}
		int type;
		if (num.compareTo(new BigInteger("-128")) >= 0 && num.compareTo(new BigInteger("127")) <= 0) {
			type = 8;
		} else if (num.compareTo((new BigInteger("32768")).negate()) >= 0 && num.compareTo(new BigInteger("32767")) <= 0) {
			type = 16;
		} else {
			type = 32;
		}

		return type;
	}
}
