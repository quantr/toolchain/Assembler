package hk.quantr.assembler.ia32;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Label {

	int lineNo;
	int offset;

	public Label(int lineNo, int offset) {
		this.lineNo = lineNo;
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "Label{" + "lineNo=" + lineNo + ", offset=" + offset + '}';
	}

}
