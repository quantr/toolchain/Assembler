package hk.quantr.assembler.ia32;

import hk.quantr.javatexttable.TableRenderer;
import javax.swing.JTable;
import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyTableRenderer implements TableRenderer {

	@Override
	public String render(JTable table, Object value, int row, int col) {
		String str = (String) value;
//		if (col == 2 || col == 3) {
//			boolean ok = table.getValueAt(row, 2).equals(table.getValueAt(row, 3));
//			if (ok) {
//				return ansi().fg(BLUE).a(value).reset().toString();
//			} else {
//				return ansi().fg(RED).a(value).reset().toString();
//			}
//		} else if (col == 4 || col == 5) {
//			boolean ok = table.getValueAt(row, 4).equals(table.getValueAt(row, 5));
//			if (ok) {
//				return ansi().fg(BLUE).a(value).reset().toString();
//			} else {
//				return ansi().fg(RED).a(value).reset().toString();
//			}
//		} else if (col == 6 || col == 7) {
//			boolean ok = table.getValueAt(row, 6).equals(table.getValueAt(row, 7));
//			if (ok) {
//				return ansi().fg(BLUE).a(value).reset().toString();
//			} else {
//				return ansi().fg(RED).a(value).reset().toString();
//			}
//		} else {
		return str;
//		}
	}

}
