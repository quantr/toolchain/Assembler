/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.assembler.ia32;

import java.util.*;
import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class IA32 {

	public static int REXW = 0b1001000;
	public static int REXR = 0b1000100;
	public static int REXB = 0b1000001;
	private static Hashtable<String, String[]> cmoveCC
			= new Hashtable<String, String[]>();

	private static Hashtable<String, String[]> setCC
			= new Hashtable<String, String[]>();

	public static void initConditionTable() {
		cmoveCC.put("a", new String[]{"0x0f", "0x47"});
		cmoveCC.put("ae", new String[]{"0x0f", "0x43"});
		cmoveCC.put("b", new String[]{"0x0f", "0x42"});
		cmoveCC.put("be", new String[]{"0x0f", "0x46"});
		cmoveCC.put("c", new String[]{"0x0f", "0x42"});
		cmoveCC.put("e", new String[]{"0x0f", "0x44"});
		cmoveCC.put("g", new String[]{"0x0f", "0x4f"});
		cmoveCC.put("ge", new String[]{"0x0f", "0x4d"});
		cmoveCC.put("l", new String[]{"0x0f", "0x4c"});
		cmoveCC.put("le", new String[]{"0x0f", "0x4e"});
		cmoveCC.put("na", new String[]{"0x0f", "0x46"});
		cmoveCC.put("nae", new String[]{"0x0f", "0x42"});
		cmoveCC.put("nb", new String[]{"0x0f", "0x43"});
		cmoveCC.put("nbe", new String[]{"0x0f", "0x47"});
		cmoveCC.put("nc", new String[]{"0x0f", "0x43"});
		cmoveCC.put("ne", new String[]{"0x0f", "0x45"});
		cmoveCC.put("ng", new String[]{"0x0f", "0x4e"});
		cmoveCC.put("nge", new String[]{"0x0f", "0x4c"});
		cmoveCC.put("nl", new String[]{"0x0f", "0x4d"});
		cmoveCC.put("nle", new String[]{"0x0f", "0x4f"});
		cmoveCC.put("no", new String[]{"0x0f", "0x41"});
		cmoveCC.put("np", new String[]{"0x0f", "0x4b"});
		cmoveCC.put("ns", new String[]{"0x0f", "0x49"});
		cmoveCC.put("o", new String[]{"0x0f", "0x40"});
		cmoveCC.put("p", new String[]{"0x0f", "0x4a"});
		cmoveCC.put("pe", new String[]{"0x0f", "0x4a"});
		cmoveCC.put("po", new String[]{"0x0f", "0x4b"});
		cmoveCC.put("s", new String[]{"0x0f", "0x48"});
		cmoveCC.put("z", new String[]{"0x0f", "0x44"});

		setCC.put("a", new String[]{"0x0f", "0x97"});
		setCC.put("ae", new String[]{"0x0f", "0x93"});
		setCC.put("b", new String[]{"0x0f", "0x92"});
		setCC.put("be", new String[]{"0x0f", "0x96"});
		setCC.put("c", new String[]{"0x0f", "0x92"});
		setCC.put("e", new String[]{"0x0f", "0x94"});
		setCC.put("g", new String[]{"0x0f", "0x9f"});
		setCC.put("ge", new String[]{"0x0f", "0x9d"});
		setCC.put("l", new String[]{"0x0f", "0x9c"});
		setCC.put("le", new String[]{"0x0f", "0x9e"});
		setCC.put("na", new String[]{"0x0f", "0x96"});
		setCC.put("nae", new String[]{"0x0f", "0x92"});
		setCC.put("nb", new String[]{"0x0f", "0x93"});
		setCC.put("nbe", new String[]{"0x0f", "0x97"});
		setCC.put("nc", new String[]{"0x0f", "0x93"});
		setCC.put("ne", new String[]{"0x0f", "0x95"});
		setCC.put("ng", new String[]{"0x0f", "0x9e"});
		setCC.put("nge", new String[]{"0x0f", "0x9c"});
		setCC.put("nl", new String[]{"0x0f", "0x9d"});
		setCC.put("nle", new String[]{"0x0f", "0x9f"});
		setCC.put("no", new String[]{"0x0f", "0x91"});
		setCC.put("np", new String[]{"0x0f", "0x9b"});
		setCC.put("ns", new String[]{"0x0f", "0x99"});
		setCC.put("nz", new String[]{"0x0f", "0x95"});
		setCC.put("o", new String[]{"0x0f", "0x90"});
		setCC.put("p", new String[]{"0x0f", "0x9a"});
		setCC.put("pe", new String[]{"0x0f", "0x9a"});
		setCC.put("po", new String[]{"0x0f", "0x9b"});
		setCC.put("s", new String[]{"0x0f", "0x98"});
		setCC.put("z", new String[]{"0x0f", "0x94"});
	}

	public static String[] ccToOpcode(String cc_flag, String ins) {

		if (ins.equals("cmovcc")) {
			return cmoveCC.get(cc_flag);
		}

		if (ins.equals("setcc")) {
			return setCC.get(cc_flag);
		}

		return new String[]{"", ""};
	}

	public static int getModRM(int opsize, int digit, int type) {
		if (opsize == 16 | opsize == 8) {
			if (digit == 2) {
				if (type == 0) {
					return 0x10;
				} else if (type == 1) {
					return 0x11;
				} else if (type == 2) {
					return 0x12;
				} else if (type == 3) {
					return 0x13;
				} else if (type == 4) {
					return 0x14;
				} else if (type == 5) {
					return 0x15;
				} else if (type == 6) {
					return 0x16;
				} else if (type == 7) {
					return 0x17;
				} else if (type == 8) {
					return 0x50;
				} else if (type == 9) {
					return 0x51;
				} else if (type == 10) {
					return 0x52;
				} else if (type == 11) {
					return 0x53;
				} else if (type == 12) {
					return 0x54;
				} else if (type == 13) {
					return 0x55;
				} else if (type == 14) {
					return 0x56;
				} else if (type == 15) {
					return 0x57;
				} else if (type == 16) {
					return 0x90;
				} else if (type == 17) {
					return 0x91;
				} else if (type == 18) {
					return 0x92;
				} else if (type == 19) {
					return 0x93;
				} else if (type == 20) {
					return 0x94;
				} else if (type == 21) {
					return 0x95;
				} else if (type == 22) {
					return 0x96;
				} else if (type == 23) {
					return 0x97;
				} else if (type == 24 || type == 2241) {
					return 0xd0;
				} else if (type == 25 || type == 2251) {
					return 0xd1;
				} else if (type == 26 || type == 2261) {
					return 0xd2;
				} else if (type == 27 || type == 2271) {
					return 0xd3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xd4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xd5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) { // new type for bpl
					return 0xd6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xd7;
				} else {
					return -1;
				}
			} else if (digit == 0) { // return value for "/0"
				if (type == 0) {
					return 0x00;
				} else if (type == 1) {
					return 0x01;
				} else if (type == 2) {
					return 0x02;
				} else if (type == 3) {
					return 0x03;
				} else if (type == 4) {
					return 0x04;
				} else if (type == 5) {
					return 0x05;
				} else if (type == 6) {
					return 0x06;
				} else if (type == 7) {
					return 0x07;
				} else if (type == 8) {
					return 0x40;
				} else if (type == 9) {
					return 0x41;
				} else if (type == 10) {
					return 0x42;
				} else if (type == 11) {
					return 0x43;
				} else if (type == 12) {
					return 0x44;
				} else if (type == 13) {
					return 0x45;
				} else if (type == 14) {
					return 0x46;
				} else if (type == 15) {
					return 0x47;
				} else if (type == 16) {
					return 0x80;
				} else if (type == 17) {
					return 0x81;
				} else if (type == 18) {
					return 0x82;
				} else if (type == 19) {
					return 0x83;
				} else if (type == 20) {
					return 0x84;
				} else if (type == 21) {
					return 0x85;
				} else if (type == 22) {
					return 0x86;
				} else if (type == 23) {
					return 0x87;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xc0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xc1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xc2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xc3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xc4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xc5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xc6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xc7;
				} else {
					return -1;
				}
			} else if (digit == 1) {
				if (type == 0) {
					return 0x08;
				} else if (type == 1) {
					return 0x09;
				} else if (type == 2) {
					return 0x0a;
				} else if (type == 3) {
					return 0x0b;
				} else if (type == 4) {
					return 0x0c;
				} else if (type == 5) {
					return 0x0d;
				} else if (type == 6) {
					return 0x0e;
				} else if (type == 7) {
					return 0x0f;
				} else if (type == 8) {
					return 0x48;
				} else if (type == 9) {
					return 0x49;
				} else if (type == 10) {
					return 0x4a;
				} else if (type == 11) {
					return 0x4b;
				} else if (type == 12) {
					return 0x4c;
				} else if (type == 13) {
					return 0x4d;
				} else if (type == 14) {
					return 0x4e;
				} else if (type == 15) {
					return 0x4f;
				} else if (type == 16) {
					return 0x88;
				} else if (type == 17) {
					return 0x89;
				} else if (type == 18) {
					return 0x8a;
				} else if (type == 19) {
					return 0x8b;
				} else if (type == 20) {
					return 0x8c;
				} else if (type == 21) {
					return 0x8d;
				} else if (type == 22) {
					return 0x8e;
				} else if (type == 23) {
					return 0x8f;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xc8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xc9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xca;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xcb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xcc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xcd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xce;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xcf;
				} else {
					return -1;
				}
			} else if (digit == 3) {
				if (type == 0) {
					return 0x18;
				} else if (type == 1) {
					return 0x19;
				} else if (type == 2) {
					return 0x1a;
				} else if (type == 3) {
					return 0x1b;
				} else if (type == 4) {
					return 0x1c;
				} else if (type == 5) {
					return 0x1d;
				} else if (type == 6) {
					return 0x1e;
				} else if (type == 7) {
					return 0x1f;
				} else if (type == 8) {
					return 0x58;
				} else if (type == 9) {
					return 0x59;
				} else if (type == 10) {
					return 0x5a;
				} else if (type == 11) {
					return 0x5b;
				} else if (type == 12) {
					return 0x5c;
				} else if (type == 13) {
					return 0x5d;
				} else if (type == 14) {
					return 0x5e;
				} else if (type == 15) {
					return 0x5f;
				} else if (type == 16) {
					return 0x98;
				} else if (type == 17) {
					return 0x99;
				} else if (type == 18) {
					return 0x9a;
				} else if (type == 19) {
					return 0x9b;
				} else if (type == 20) {
					return 0x9c;
				} else if (type == 21) {
					return 0x9d;
				} else if (type == 22) {
					return 0x9e;
				} else if (type == 23) {
					return 0x9f;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xd8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xd9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xda;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xdb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xdc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xdd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xde;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xdf;
				} else {
					return -1;
				}
			} else if (digit == 4) {
				if (type == 0) {
					return 0x20;
				} else if (type == 1) {
					return 0x21;
				} else if (type == 2) {
					return 0x22;
				} else if (type == 3) {
					return 0x23;
				} else if (type == 4) {
					return 0x24;
				} else if (type == 5) {
					return 0x25;
				} else if (type == 6) {
					return 0x26;
				} else if (type == 7) {
					return 0x27;
				} else if (type == 8) {
					return 0x60;
				} else if (type == 9) {
					return 0x61;
				} else if (type == 10) {
					return 0x62;
				} else if (type == 11) {
					return 0x63;
				} else if (type == 12) {
					return 0x64;
				} else if (type == 13) {
					return 0x65;
				} else if (type == 14) {
					return 0x66;
				} else if (type == 15) {
					return 0x67;
				} else if (type == 16) {
					return 0xa0;
				} else if (type == 17) {
					return 0xa1;
				} else if (type == 18) {
					return 0xa2;
				} else if (type == 19) {
					return 0xa3;
				} else if (type == 20) {
					return 0xa4;
				} else if (type == 21) {
					return 0xa5;
				} else if (type == 22) {
					return 0xa6;
				} else if (type == 23) {
					return 0xa7;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xe0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xe1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xe2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xe3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xe4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xe5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xe6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xe7;
				} else {
					return -1;
				}
			} else if (digit == 5) {
				if (type == 0) {
					return 0x28;
				} else if (type == 1) {
					return 0x29;
				} else if (type == 2) {
					return 0x2a;
				} else if (type == 3) {
					return 0x2b;
				} else if (type == 4) {
					return 0x2c;
				} else if (type == 5) {
					return 0x2d;
				} else if (type == 6) {
					return 0x2e;
				} else if (type == 7) {
					return 0x2f;
				} else if (type == 8) {
					return 0x68;
				} else if (type == 9) {
					return 0x69;
				} else if (type == 10) {
					return 0x6a;
				} else if (type == 11) {
					return 0x6b;
				} else if (type == 12) {
					return 0x6c;
				} else if (type == 13) {
					return 0x6d;
				} else if (type == 14) {
					return 0x6e;
				} else if (type == 15) {
					return 0x6f;
				} else if (type == 16) {
					return 0xa8;
				} else if (type == 17) {
					return 0xa9;
				} else if (type == 18) {
					return 0xaa;
				} else if (type == 19) {
					return 0xab;
				} else if (type == 20) {
					return 0xac;
				} else if (type == 21) {
					return 0xad;
				} else if (type == 22) {
					return 0xae;
				} else if (type == 23) {
					return 0xaf;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xe8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xe9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xea;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xeb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xec;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xed;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xee;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xef;
				} else {
					return -1;
				}
			} else if (digit == 6) {
				if (type == 0) {
					return 0x30;
				} else if (type == 1) {
					return 0x31;
				} else if (type == 2) {
					return 0x32;
				} else if (type == 3) {
					return 0x33;
				} else if (type == 4) {
					return 0x34;
				} else if (type == 5) {
					return 0x35;
				} else if (type == 6) {
					return 0x36;
				} else if (type == 7) {
					return 0x37;
				} else if (type == 8) {
					return 0x70;
				} else if (type == 9) {
					return 0x71;
				} else if (type == 10) {
					return 0x72;
				} else if (type == 11) {
					return 0x73;
				} else if (type == 12) {
					return 0x74;
				} else if (type == 13) {
					return 0x75;
				} else if (type == 14) {
					return 0x76;
				} else if (type == 15) {
					return 0x77;
				} else if (type == 16) {
					return 0xb0;
				} else if (type == 17) {
					return 0xb1;
				} else if (type == 18) {
					return 0xb2;
				} else if (type == 19) {
					return 0xb3;
				} else if (type == 20) {
					return 0xb4;
				} else if (type == 21) {
					return 0xb5;
				} else if (type == 22) {
					return 0xb6;
				} else if (type == 23) {
					return 0xb7;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xf0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xf1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xf2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xf3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xf4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xf5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xf6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xf7;
				} else {
					return -1;
				}
			} else if (digit == 7) {
				if (type == 0) {
					return 0x38;
				} else if (type == 1) {
					return 0x39;
				} else if (type == 2) {
					return 0x3a;
				} else if (type == 3) {
					return 0x3b;
				} else if (type == 4) {
					return 0x3c;
				} else if (type == 5) {
					return 0x3d;
				} else if (type == 6) {
					return 0x3e;
				} else if (type == 7) {
					return 0x3f;
				} else if (type == 8) {
					return 0x78;
				} else if (type == 9) {
					return 0x79;
				} else if (type == 10) {
					return 0x7a;
				} else if (type == 11) {
					return 0x7b;
				} else if (type == 12) {
					return 0x7c;
				} else if (type == 13) {
					return 0x7d;
				} else if (type == 14) {
					return 0x7e;
				} else if (type == 15) {
					return 0x7f;
				} else if (type == 16) {
					return 0xb8;
				} else if (type == 17) {
					return 0xb9;
				} else if (type == 18) {
					return 0xba;
				} else if (type == 19) {
					return 0xbb;
				} else if (type == 20) {
					return 0xbc;
				} else if (type == 21) {
					return 0xbd;
				} else if (type == 22) {
					return 0xbe;
				} else if (type == 23) {
					return 0xbf;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xf8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xf9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xfa;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xfb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xfc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xfd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xfe;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xff;
				} else {
					return -1;
				}
			}

		} else if (opsize == 32 || opsize == 64) {
			if (digit == 2) {
				if (type == 0) {
					return 0x10;
				} else if (type == 1) {
					return 0x11;
				} else if (type == 2) {
					return 0x12;
				} else if (type == 3) {
					return 0x13;
				} else if (type == 4) {
					return 0x14;
				} else if (type == 5) {
					return 0x15;
				} else if (type == 6) {
					return 0x16;
				} else if (type == 7) {
					return 0x17;
				} else if (type == 8) {
					return 0x50;
				} else if (type == 9) {
					return 0x51;
				} else if (type == 10) {
					return 0x52;
				} else if (type == 11) {
					return 0x53;
				} else if (type == 12) {
					return 0x54;
				} else if (type == 13) {
					return 0x55;
				} else if (type == 14) {
					return 0x56;
				} else if (type == 15) {
					return 0x57;
				} else if (type == 16) {
					return 0x90;
				} else if (type == 17) {
					return 0x91;
				} else if (type == 18) {
					return 0x92;
				} else if (type == 19) {
					return 0x93;
				} else if (type == 20) {
					return 0x94;
				} else if (type == 21) {
					return 0x95;
				} else if (type == 22) {
					return 0x96;
				} else if (type == 23) {
					return 0x97;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xd0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xd1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xd2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xd3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xd4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xd5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xd6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) {
					return 0xd7;
				}
			} else if (digit == 0) { // return value for "/0"
				if (type == 0) {
					return 0x00;
				} else if (type == 1) {
					return 0x01;
				} else if (type == 2) {
					return 0x02;
				} else if (type == 3) {
					return 0x03;
				} else if (type == 4) {
					return 0x04;
				} else if (type == 5) {
					return 0x05;
				} else if (type == 6) {
					return 0x06;
				} else if (type == 7) {
					return 0x07;
				} else if (type == 8) {
					return 0x40;
				} else if (type == 9) {
					return 0x41;
				} else if (type == 10) {
					return 0x42;
				} else if (type == 11) {
					return 0x43;
				} else if (type == 12) {
					return 0x44;
				} else if (type == 13) {
					return 0x45;
				} else if (type == 14) {
					return 0x46;
				} else if (type == 15) {
					return 0x47;
				} else if (type == 16) {
					return 0x80;
				} else if (type == 17) {
					return 0x81;
				} else if (type == 18) {
					return 0x82;
				} else if (type == 19) {
					return 0x83;
				} else if (type == 20) {
					return 0x84;
				} else if (type == 21) {
					return 0x85;
				} else if (type == 22) {
					return 0x86;
				} else if (type == 23) {
					return 0x87;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xc0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xc1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xc2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xc3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xc4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xc5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xc6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xc7;
				} else {
					return -1;
				}
			} else if (digit == 1) {
				if (type == 0) {
					return 0x08;
				} else if (type == 1) {
					return 0x09;
				} else if (type == 2) {
					return 0x0a;
				} else if (type == 3) {
					return 0x0b;
				} else if (type == 4) {
					return 0x0c;
				} else if (type == 5) {
					return 0x0d;
				} else if (type == 6) {
					return 0x0e;
				} else if (type == 7) {
					return 0x0f;
				} else if (type == 8) {
					return 0x48;
				} else if (type == 9) {
					return 0x49;
				} else if (type == 10) {
					return 0x4a;
				} else if (type == 11) {
					return 0x4b;
				} else if (type == 12) {
					return 0x4c;
				} else if (type == 13) {
					return 0x4d;
				} else if (type == 14) {
					return 0x4e;
				} else if (type == 15) {
					return 0x4f;
				} else if (type == 16) {
					return 0x88;
				} else if (type == 17) {
					return 0x89;
				} else if (type == 18) {
					return 0x8a;
				} else if (type == 19) {
					return 0x8b;
				} else if (type == 20) {
					return 0x8c;
				} else if (type == 21) {
					return 0x8d;
				} else if (type == 22) {
					return 0x8e;
				} else if (type == 23) {
					return 0x8f;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xc8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xc9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xca;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xcb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xcc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xcd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xce;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xcf;
				} else {
					return -1;
				}
			} else if (digit == 3) {
				if (type == 0) {
					return 0x18;
				} else if (type == 1) {
					return 0x19;
				} else if (type == 2) {
					return 0x1a;
				} else if (type == 3) {
					return 0x1b;
				} else if (type == 4) {
					return 0x1c;
				} else if (type == 5) {
					return 0x1d;
				} else if (type == 6) {
					return 0x1e;
				} else if (type == 7) {
					return 0x1f;
				} else if (type == 8) {
					return 0x58;
				} else if (type == 9) {
					return 0x59;
				} else if (type == 10) {
					return 0x5a;
				} else if (type == 11) {
					return 0x5b;
				} else if (type == 12) {
					return 0x5c;
				} else if (type == 13) {
					return 0x5d;
				} else if (type == 14) {
					return 0x5e;
				} else if (type == 15) {
					return 0x5f;
				} else if (type == 16) {
					return 0x98;
				} else if (type == 17) {
					return 0x99;
				} else if (type == 18) {
					return 0x9a;
				} else if (type == 19) {
					return 0x9b;
				} else if (type == 20) {
					return 0x9c;
				} else if (type == 21) {
					return 0x9d;
				} else if (type == 22) {
					return 0x9e;
				} else if (type == 23) {
					return 0x9f;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xd8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xd9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xda;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xdb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xdc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xdd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xde;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xdf;
				} else {
					return -1;
				}
			} else if (digit == 4) {
				if (type == 0) {
					return 0x20;
				} else if (type == 1) {
					return 0x21;
				} else if (type == 2) {
					return 0x22;
				} else if (type == 3) {
					return 0x23;
				} else if (type == 4) {
					return 0x24;
				} else if (type == 5) {
					return 0x25;
				} else if (type == 6) {
					return 0x26;
				} else if (type == 7) {
					return 0x27;
				} else if (type == 8) {
					return 0x60;
				} else if (type == 9) {
					return 0x61;
				} else if (type == 10) {
					return 0x62;
				} else if (type == 11) {
					return 0x63;
				} else if (type == 12) {
					return 0x64;
				} else if (type == 13) {
					return 0x65;
				} else if (type == 14) {
					return 0x66;
				} else if (type == 15) {
					return 0x67;
				} else if (type == 16) {
					return 0xa0;
				} else if (type == 17) {
					return 0xa1;
				} else if (type == 18) {
					return 0xa2;
				} else if (type == 19) {
					return 0xa3;
				} else if (type == 20) {
					return 0xa4;
				} else if (type == 21) {
					return 0xa5;
				} else if (type == 22) {
					return 0xa6;
				} else if (type == 23) {
					return 0xa7;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xe0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xe1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xe2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xe3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xe4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xe5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xe6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xe7;
				} else {
					return -1;
				}
			} else if (digit == 5) {
				if (type == 0) {
					return 0x28;
				} else if (type == 1) {
					return 0x29;
				} else if (type == 2) {
					return 0x2a;
				} else if (type == 3) {
					return 0x2b;
				} else if (type == 4) {
					return 0x2c;
				} else if (type == 5) {
					return 0x2d;
				} else if (type == 6) {
					return 0x2e;
				} else if (type == 7) {
					return 0x2f;
				} else if (type == 8) {
					return 0x68;
				} else if (type == 9) {
					return 0x69;
				} else if (type == 10) {
					return 0x6a;
				} else if (type == 11) {
					return 0x6b;
				} else if (type == 12) {
					return 0x6c;
				} else if (type == 13) {
					return 0x6d;
				} else if (type == 14) {
					return 0x6e;
				} else if (type == 15) {
					return 0x6f;
				} else if (type == 16) {
					return 0xa8;
				} else if (type == 17) {
					return 0xa9;
				} else if (type == 18) {
					return 0xaa;
				} else if (type == 19) {
					return 0xab;
				} else if (type == 20) {
					return 0xac;
				} else if (type == 21) {
					return 0xad;
				} else if (type == 22) {
					return 0xae;
				} else if (type == 23) {
					return 0xaf;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xe8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xe9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xea;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xeb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xec;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xed;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xee;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type
					return 0xef;
				} else {
					return -1;
				}
			} else if (digit == 6) {
				if (type == 0) {
					return 0x30;
				} else if (type == 1) {
					return 0x31;
				} else if (type == 2) {
					return 0x32;
				} else if (type == 3) {
					return 0x33;
				} else if (type == 4) {
					return 0x34;
				} else if (type == 5) {
					return 0x35;
				} else if (type == 6) {
					return 0x36;
				} else if (type == 7) {
					return 0x37;
				} else if (type == 8) {
					return 0x70;
				} else if (type == 9) {
					return 0x71;
				} else if (type == 10) {
					return 0x72;
				} else if (type == 11) {
					return 0x73;
				} else if (type == 12) {
					return 0x74;
				} else if (type == 13) {
					return 0x75;
				} else if (type == 14) {
					return 0x76;
				} else if (type == 15) {
					return 0x77;
				} else if (type == 16) {
					return 0xb0;
				} else if (type == 17) {
					return 0xb1;
				} else if (type == 18) {
					return 0xb2;
				} else if (type == 19) {
					return 0xb3;
				} else if (type == 20) {
					return 0xb4;
				} else if (type == 21) {
					return 0xb5;
				} else if (type == 22) {
					return 0xb6;
				} else if (type == 23) {
					return 0xb7;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xf0;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xf1;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xf2;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xf3;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xf4;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xf5;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xf6;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xf7;
				} else {
					return -1;
				}
			} else if (digit == 7) {
				if (type == 0) {
					return 0x38;
				} else if (type == 1) {
					return 0x39;
				} else if (type == 2) {
					return 0x3a;
				} else if (type == 3) {
					return 0x3b;
				} else if (type == 4) {
					return 0x3c;
				} else if (type == 5) {
					return 0x3d;
				} else if (type == 6) {
					return 0x3e;
				} else if (type == 7) {
					return 0x3f;
				} else if (type == 8) {
					return 0x78;
				} else if (type == 9) {
					return 0x79;
				} else if (type == 10) {
					return 0x7a;
				} else if (type == 11) {
					return 0x7b;
				} else if (type == 12) {
					return 0x7c;
				} else if (type == 13) {
					return 0x7d;
				} else if (type == 14) {
					return 0x7e;
				} else if (type == 15) {
					return 0x7f;
				} else if (type == 16) {
					return 0xb8;
				} else if (type == 17) {
					return 0xb9;
				} else if (type == 18) {
					return 0xba;
				} else if (type == 19) {
					return 0xbb;
				} else if (type == 20) {
					return 0xbc;
				} else if (type == 21) {
					return 0xbd;
				} else if (type == 22) {
					return 0xbe;
				} else if (type == 23) {
					return 0xbf;
				} else if (type == 24 || type == 241 || type == 2241) {
					return 0xf8;
				} else if (type == 25 || type == 251 || type == 2251) {
					return 0xf9;
				} else if (type == 26 || type == 261 || type == 2261) {
					return 0xfa;
				} else if (type == 27 || type == 271 || type == 2271) {
					return 0xfb;
				} else if (type == 28 || type == 281 || type == 2811 || type == 2281) {
					return 0xfc;
				} else if (type == 29 || type == 291 || type == 2911 || type == 2291) {
					return 0xfd;
				} else if (type == 30 || type == 301 || type == 3011 || type == 3301) {
					return 0xfe;
				} else if (type == 31 || type == 311 || type == 3111 || type == 3311) { //new add for type 31 
					return 0xff;
				} else {
					return -1;
				}
			}
			if (digit == -2)//SIB byte
			{
				if (type == 33) {
					return 0x45;
				} else if (type == 34) {
					return 0x4d;
				} else if (type == 35) {
					return 0x55;
				} else if (type == 36) {
					return 0x5d;
				} else if (type == 37)//none case is skipped, see intel menu
				{
					return 0x6d;
				} else if (type == 38) {
					return 0x75;
				} else if (type == 39) {
					return 0x7d;
				} else if (type == 40) {
					return 0x85;
				} else if (type == 41) {
					return 0x8d;
				} else if (type == 42) {
					return 0x95;
				} else if (type == 43) {
					return 0x9d;
				} else if (type == 44)//none is skipped
				{
					return 0xad;
				} else if (type == 45) {
					return 0xb5;
				} else if (type == 46) {
					return 0xbd;
				} else if (type == 47) {
					return 0xc5;
				} else if (type == 48) {
					return 0xcd;
				} else if (type == 49) {
					return 0xd5;
				} else if (type == 50) {
					return 0xdd;
				} else if (type == 51) {
					return 0xed;
				} else if (type == 52) {
					return 0xf5;
				} else if (type == 53) {
					return 0xfd;
				}
			}
		}
		return 0xfff;
	}

	public static int getOpOverridePrefix(int bits, int effectiveOpSize) {
		if (bits == 16) {
			if (effectiveOpSize == 32) {
				return 0x66;
			} else {
				return 0;
			}
		}

		if (bits == 32 || bits == 64) {
			if (effectiveOpSize == 16) {
				return 0x66;
			} else {
				return 0;
			}
		}

		return -1;
	}

	public static int getAddrOverridePrefix(int bits, int effectiveAddrSize) {
		if (bits == 16) {
			if (effectiveAddrSize == 32) {
				return 0x67;
			} else {
				return 0;
			}
		}

		if (bits == 32) {
			if (effectiveAddrSize == 16) {
				return 0x67;
			} else {
				return 0;
			}
		}

		if (bits == 64) {
			if (effectiveAddrSize == 32) {
				return 0x67;
			} else {
				return 0;
			}
		}
		return -1;
	}

	public static int getRD(int code, int type) {
		if (type == 24 || type == 241 || type == 2241) {
			return code;
		} else if (type == 25 || type == 251 || type == 2251) {
			return code + 1;
		} else if (type == 26 || type == 261 || type == 2261) {
			return code + 2;
		} else if (type == 27 || type == 271 || type == 2271) {
			return code + 3;
		} else if (type == 28 || type == 281 || type == 2281) {
			return code + 4;
		} else if (type == 29 || type == 291 || type == 2291) {
			return code + 5;
		} else if (type == 30 || type == 301 || type == 3301) {
			return code + 6;
		} else if (type == 31 || type == 311 || type == 3311) {
			return code + 7;
		} else {
			return -1;
		}
	}

	public static int getSIB(int col, int row) {
		if (col == 0) {
			if (row == 0) {
				return 0x00;
			} else if (row == 1) {
				return 0x08;
			} else if (row == 2) {
				return 0x10;
			} else if (row == 3) {
				return 0x18;
			} else if (row == 4) {
				return 0x20;
			} else if (row == 5) {
				return 0x28;
			} else if (row == 6) {
				return 0x30;
			} else if (row == 7) {
				return 0x38;
			} else if (row == 8) {
				return 0x40;
			} else if (row == 9) {
				return 0x48;
			} else if (row == 10) {
				return 0x50;
			} else if (row == 11) {
				return 0x58;
			} else if (row == 12) {
				return 0x60;
			} else if (row == 13) {
				return 0x68;
			} else if (row == 14) {
				return 0x70;
			} else if (row == 15) {
				return 0x78;
			} else if (row == 16) {
				return 0x80;
			} else if (row == 17) {
				return 0x88;
			} else if (row == 18) {
				return 0x90;
			} else if (row == 19) {
				return 0x98;
			} else if (row == 20) {
				return 0xa0;
			} else if (row == 21) {
				return 0xa8;
			} else if (row == 22) {
				return 0xb0;
			} else if (row == 23) {
				return 0xb8;
			} else if (row == 24) {
				return 0xc0;
			} else if (row == 25) {
				return 0xc8;
			} else if (row == 26) {
				return 0xd0;
			} else if (row == 27) {
				return 0xd8;
			} else if (row == 28) {
				return 0xe0;
			} else if (row == 29) {
				return 0xe8;
			} else if (row == 30) {
				return 0xf0;
			} else if (row == 31) {
				return 0xf8;
			} else {
				return -1;
			}
		} else if (col == 1) {
			if (row == 0) {
				return 0x01;
			} else if (row == 1) {
				return 0x09;
			} else if (row == 2) {
				return 0x11;
			} else if (row == 3) {
				return 0x19;
			} else if (row == 4) {
				return 0x21;
			} else if (row == 5) {
				return 0x29;
			} else if (row == 6) {
				return 0x31;
			} else if (row == 7) {
				return 0x39;
			} else if (row == 8) {
				return 0x41;
			} else if (row == 9) {
				return 0x49;
			} else if (row == 10) {
				return 0x51;
			} else if (row == 11) {
				return 0x59;
			} else if (row == 12) {
				return 0x61;
			} else if (row == 13) {
				return 0x69;
			} else if (row == 14) {
				return 0x71;
			} else if (row == 15) {
				return 0x79;
			} else if (row == 16) {
				return 0x81;
			} else if (row == 17) {
				return 0x89;
			} else if (row == 18) {
				return 0x91;
			} else if (row == 19) {
				return 0x99;
			} else if (row == 20) {
				return 0xa1;
			} else if (row == 21) {
				return 0xa9;
			} else if (row == 22) {
				return 0xb1;
			} else if (row == 23) {
				return 0xb9;
			} else if (row == 24) {
				return 0xc1;
			} else if (row == 25) {
				return 0xc9;
			} else if (row == 26) {
				return 0xd1;
			} else if (row == 27) {
				return 0xd9;
			} else if (row == 28) {
				return 0xe1;
			} else if (row == 29) {
				return 0xe9;
			} else if (row == 30) {
				return 0xf1;
			} else if (row == 31) {
				return 0xf9;
			} else {
				return -1;
			}
		} else if (col == 2) {
			if (row == 0) {
				return 0x02;
			} else if (row == 1) {
				return 0x0a;
			} else if (row == 2) {
				return 0x12;
			} else if (row == 3) {
				return 0x1a;
			} else if (row == 4) {
				return 0x22;
			} else if (row == 5) {
				return 0x2a;
			} else if (row == 6) {
				return 0x32;
			} else if (row == 7) {
				return 0x3a;
			} else if (row == 8) {
				return 0x42;
			} else if (row == 9) {
				return 0x4a;
			} else if (row == 10) {
				return 0x52;
			} else if (row == 11) {
				return 0x5a;
			} else if (row == 12) {
				return 0x62;
			} else if (row == 13) {
				return 0x6a;
			} else if (row == 14) {
				return 0x72;
			} else if (row == 15) {
				return 0x7a;
			} else if (row == 16) {
				return 0x82;
			} else if (row == 17) {
				return 0x8a;
			} else if (row == 18) {
				return 0x92;
			} else if (row == 19) {
				return 0x9a;
			} else if (row == 20) {
				return 0xa2;
			} else if (row == 21) {
				return 0xaa;
			} else if (row == 22) {
				return 0xb2;
			} else if (row == 23) {
				return 0xba;
			} else if (row == 24) {
				return 0xc2;
			} else if (row == 25) {
				return 0xca;
			} else if (row == 26) {
				return 0xd2;
			} else if (row == 27) {
				return 0xda;
			} else if (row == 28) {
				return 0xe2;
			} else if (row == 29) {
				return 0xea;
			} else if (row == 30) {
				return 0xf2;
			} else if (row == 31) {
				return 0xfa;
			} else {
				return -1;
			}
		} else if (col == 3) {
			if (row == 0) {
				return 0x03;
			} else if (row == 1) {
				return 0x0b;
			} else if (row == 2) {
				return 0x13;
			} else if (row == 3) {
				return 0x1b;
			} else if (row == 4) {
				return 0x23;
			} else if (row == 5) {
				return 0x2b;
			} else if (row == 6) {
				return 0x33;
			} else if (row == 7) {
				return 0x3b;
			} else if (row == 8) {
				return 0x43;
			} else if (row == 9) {
				return 0x4b;
			} else if (row == 10) {
				return 0x53;
			} else if (row == 11) {
				return 0x5b;
			} else if (row == 12) {
				return 0x63;
			} else if (row == 13) {
				return 0x6b;
			} else if (row == 14) {
				return 0x73;
			} else if (row == 15) {
				return 0x7b;
			} else if (row == 16) {
				return 0x83;
			} else if (row == 17) {
				return 0x8b;
			} else if (row == 18) {
				return 0x93;
			} else if (row == 19) {
				return 0x9b;
			} else if (row == 20) {
				return 0xa3;
			} else if (row == 21) {
				return 0xab;
			} else if (row == 22) {
				return 0xb3;
			} else if (row == 23) {
				return 0xbb;
			} else if (row == 24) {
				return 0xc3;
			} else if (row == 25) {
				return 0xcb;
			} else if (row == 26) {
				return 0xd3;
			} else if (row == 27) {
				return 0xdb;
			} else if (row == 28) {
				return 0xe3;
			} else if (row == 29) {
				return 0xeb;
			} else if (row == 30) {
				return 0xf3;
			} else if (row == 31) {
				return 0xfb;
			} else {
				return -1;
			}
		} else if (col == 4) {
			if (row == 0) {
				return 0x04;
			} else if (row == 1) {
				return 0x0c;
			} else if (row == 2) {
				return 0x14;
			} else if (row == 3) {
				return 0x1c;
			} else if (row == 4) {
				return 0x24;
			} else if (row == 5) {
				return 0x2c;
			} else if (row == 6) {
				return 0x34;
			} else if (row == 7) {
				return 0x3c;
			} else if (row == 8) {
				return 0x44;
			} else if (row == 9) {
				return 0x4c;
			} else if (row == 10) {
				return 0x54;
			} else if (row == 11) {
				return 0x5c;
			} else if (row == 12) {
				return 0x64;
			} else if (row == 13) {
				return 0x6c;
			} else if (row == 14) {
				return 0x74;
			} else if (row == 15) {
				return 0x7c;
			} else if (row == 16) {
				return 0x84;
			} else if (row == 17) {
				return 0x8c;
			} else if (row == 18) {
				return 0x94;
			} else if (row == 19) {
				return 0x9c;
			} else if (row == 20) {
				return 0xa4;
			} else if (row == 21) {
				return 0xac;
			} else if (row == 22) {
				return 0xb4;
			} else if (row == 23) {
				return 0xbc;
			} else if (row == 24) {
				return 0xc4;
			} else if (row == 25) {
				return 0xcc;
			} else if (row == 26) {
				return 0xd4;
			} else if (row == 27) {
				return 0xdc;
			} else if (row == 28) {
				return 0xe4;
			} else if (row == 29) {
				return 0xec;
			} else if (row == 30) {
				return 0xf4;
			} else if (row == 31) {
				return 0xfc;
			} else {
				return -1;
			}
		} else if (col == 5) {
			if (row == 0) {
				return 0x05;
			} else if (row == 1) {
				return 0x0d;
			} else if (row == 2) {
				return 0x15;
			} else if (row == 3) {
				return 0x1d;
			} else if (row == 4) {
				return 0x25;
			} else if (row == 5) {
				return 0x2d;
			} else if (row == 6) {
				return 0x35;
			} else if (row == 7) {
				return 0x3d;
			} else if (row == 8) {
				return 0x45;
			} else if (row == 9) {
				return 0x4d;
			} else if (row == 10) {
				return 0x55;
			} else if (row == 11) {
				return 0x5d;
			} else if (row == 12) {
				return 0x65;
			} else if (row == 13) {
				return 0x6d;
			} else if (row == 14) {
				return 0x75;
			} else if (row == 15) {
				return 0x7d;
			} else if (row == 16) {
				return 0x85;
			} else if (row == 17) {
				return 0x8d;
			} else if (row == 18) {
				return 0x95;
			} else if (row == 19) {
				return 0x9d;
			} else if (row == 20) {
				return 0xa5;
			} else if (row == 21) {
				return 0xad;
			} else if (row == 22) {
				return 0xb5;
			} else if (row == 23) {
				return 0xbd;
			} else if (row == 24) {
				return 0xc5;
			} else if (row == 25) {
				return 0xcd;
			} else if (row == 26) {
				return 0xd5;
			} else if (row == 27) {
				return 0xdd;
			} else if (row == 28) {
				return 0xe5;
			} else if (row == 29) {
				return 0xed;
			} else if (row == 30) {
				return 0xf5;
			} else if (row == 31) {
				return 0xfd;
			} else {
				return -1;
			}
		} else if (col == 6) {
			if (row == 0) {
				return 0x06;
			} else if (row == 1) {
				return 0x0e;
			} else if (row == 2) {
				return 0x16;
			} else if (row == 3) {
				return 0x1e;
			} else if (row == 4) {
				return 0x26;
			} else if (row == 5) {
				return 0x2e;
			} else if (row == 6) {
				return 0x36;
			} else if (row == 7) {
				return 0x3e;
			} else if (row == 8) {
				return 0x46;
			} else if (row == 9) {
				return 0x4e;
			} else if (row == 10) {
				return 0x56;
			} else if (row == 11) {
				return 0x5e;
			} else if (row == 12) {
				return 0x66;
			} else if (row == 13) {
				return 0x6e;
			} else if (row == 14) {
				return 0x76;
			} else if (row == 15) {
				return 0x7e;
			} else if (row == 16) {
				return 0x86;
			} else if (row == 17) {
				return 0x8e;
			} else if (row == 18) {
				return 0x96;
			} else if (row == 19) {
				return 0x9e;
			} else if (row == 20) {
				return 0xa6;
			} else if (row == 21) {
				return 0xae;
			} else if (row == 22) {
				return 0xb6;
			} else if (row == 23) {
				return 0xbe;
			} else if (row == 24) {
				return 0xc6;
			} else if (row == 25) {
				return 0xce;
			} else if (row == 26) {
				return 0xd6;
			} else if (row == 27) {
				return 0xde;
			} else if (row == 28) {
				return 0xe6;
			} else if (row == 29) {
				return 0xee;
			} else if (row == 30) {
				return 0xf6;
			} else if (row == 31) {
				return 0xfe;
			} else {
				return -1;
			}
		} else if (col == 7) {
			if (row == 0) {
				return 0x07;
			} else if (row == 1) {
				return 0x0f;
			} else if (row == 2) {
				return 0x17;
			} else if (row == 3) {
				return 0x1f;
			} else if (row == 4) {
				return 0x27;
			} else if (row == 5) {
				return 0x2f;
			} else if (row == 6) {
				return 0x37;
			} else if (row == 7) {
				return 0x3f;
			} else if (row == 8) {
				return 0x47;
			} else if (row == 9) {
				return 0x4f;
			} else if (row == 10) {
				return 0x57;
			} else if (row == 11) {
				return 0x5f;
			} else if (row == 12) {
				return 0x67;
			} else if (row == 13) {
				return 0x6f;
			} else if (row == 14) {
				return 0x77;
			} else if (row == 15) {
				return 0x7f;
			} else if (row == 16) {
				return 0x87;
			} else if (row == 17) {
				return 0x8f;
			} else if (row == 18) {
				return 0x97;
			} else if (row == 19) {
				return 0x9f;
			} else if (row == 20) {
				return 0xa7;
			} else if (row == 21) {
				return 0xaf;
			} else if (row == 22) {
				return 0xb7;
			} else if (row == 23) {
				return 0xbf;
			} else if (row == 24) {
				return 0xc7;
			} else if (row == 25) {
				return 0xcf;
			} else if (row == 26) {
				return 0xd7;
			} else if (row == 27) {
				return 0xdf;
			} else if (row == 28) {
				return 0xe7;
			} else if (row == 29) {
				return 0xef;
			} else if (row == 30) {
				return 0xf7;
			} else if (row == 31) {
				return 0xff;
			} else {
				return -1;
			}
		}
		return -1;
	}

	public static int getREX(boolean w, boolean r, boolean x, boolean b) {
		if (w) {
			if (!r && !x && !b) {
				return 0x48;
			} else if (!r && !x && b) {
				return 0x49;
			} else if (!r && x && !b) {
				return 0x4a;
			} else if (!r && x && b) {
				return 0x4b;
			} else if (r && !x && !b) {
				return 0x4c;
			} else if (r && !x && b) {
				return 0x4d;
			} else if (r && x && !b) {
				return 0x4e;
			} else if (r && x && b) {
				return 0x4f;
			} else {
				return 0x00;
			}
		} else {
			if (!r && !x && !b) {
				return 0x40;
			} else if (!r && !x && b) {
				return 0x41;
			} else if (!r && x && !b) {
				return 0x42;
			} else if (!r && x && b) {
				return 0x43;
			} else if (r && !x && !b) {
				return 0x44;
			} else if (r && !x && b) {
				return 0x45;
			} else if (r && x && !b) {
				return 0x46;
			} else if (r && x && b) {
				return 0x47;
			} else {
				return 0x00;
			}

		}

	}

	public static int getPrecedence(char c) {
		if (c == '@') {
			return -1;
		} else if (c == '+' || c == '-') {
			return 0;
		} else {
			return 1;
		}
	}

	public static int rexReg(boolean is64, int type, int row) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(241);
		list.add(251);
		list.add(261);
		list.add(271);
		list.add(281);
		list.add(291);
		list.add(301);
		list.add(311);
		list.add(2811);
		list.add(2911);
		list.add(3011);
		list.add(3111);
		list.add(2241);
		list.add(2251);
		list.add(2261);
		list.add(2271);
		list.add(2281);
		list.add(2291);
		list.add(3301);
		list.add(3311);
		if (is64 || list.contains(row) || list.contains(type)) {
			return 1;
		} else {
			return 0;
		}
	}

	public static int evaluteExp(Character op, int d1, int d2, int d3, int d4, int d5) {
		if (op == '+') {
			return d1 + d2;
		} else if (op == '-') {
			return d1 - d2;
		} else if (op == '*') {
			return d1 * d2;
		} else {
			return d1 / d2;
		}
	}

	public static int segmentPrefix(int seg_type) {
		if (seg_type == 1) {
			return 0x2E; //CS
		} else if (seg_type == 2) {
			return 0x36; //SS
		} else if (seg_type == 3) {
			return 0x3E; //DS
		} else if (seg_type == 4) {
			return 0x26; //ES
		} else if (seg_type == 5) {
			return 0x64; //FS
		} else if (seg_type == 6) {
			return 0x65;  //GS
		} else {
			return -1;
		}
	}

	public static boolean NoREXPrefix(String ins, int left_type, int right_type, int left_opSize, int right_opSize) {
		ArrayList<String> list1 = new ArrayList<>();
		list1.add("call");
		list1.add("sidt");
		list1.add("cmpxchg8b");
		list1.add("xsaves");
		list1.add("xsaves64");
		list1.add("xsaveopt");
		list1.add("xsaveopt64");
		list1.add("xsavec");
		list1.add("xsavec64");
		list1.add("xsave");
		list1.add("xsave64");
		list1.add("xrstors");
		list1.add("xrstors64");
		list1.add("xrstor");
		list1.add("xrstor64");
		list1.add("invlpg");
		list1.add("pop");
		list1.add("push");
		list1.add("jmp");
		if (rexReg(false, left_type, right_type) == 0 && left_opSize != 64) {
			list1.add("lea");
		}
		list1.add("sgdt");
		list1.add("lgdt");
		list1.add("lidt");
		if (left_opSize == 64 || right_opSize == 64) {
			list1.add("rdpid");
		}
		//list1.add("lsl");
		ArrayList<Integer> list = new ArrayList<>();
		list.add(2241);
		list.add(2251);
		list.add(2261);
		list.add(2271);
		list.add(2281);
		list.add(2291);
		list.add(3301);
		list.add(3311);
		if (list1.contains(ins) && !(list.contains(left_type) || list.contains(right_type))) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean changeType(String ins) {
		boolean result = false;
		ArrayList<String> list = new ArrayList<>();
		list.add("adcx");
		list.add("adox");
		list.add("bsf");
		list.add("bsr");
		list.add("cmovcc");
		list.add("crc32");
		list.add("imul");
		list.add("mul");
		list.add("popcnt");
		list.add("rcl");
		list.add("rol");
		list.add("rcr");
		list.add("ror");
		list.add("sal");
		list.add("sar");
		list.add("shl");
		/*list.add("shld");*/
		list.add("shr");
		list.add("lar");
		list.add("lea");
		list.add("lsl");
		list.add("lzcnt");
		list.add("setcc");
		list.add("movsx");
		list.add("movsxd");
		list.add("movzx");
		//list.add("xchg");
		list.add("tzcnt");
		if (list.contains(ins)) {
			result = true;
		}

		return result;
	}

	public static boolean NoOperandPrefix(String ins) {
		ArrayList<String> list = new ArrayList<>();
		list.add("ltr");
		list.add("lldt");
		list.add("lmsw");
		list.add("lgdt");
		list.add("lidt");
		if (list.contains(ins)) {
			return true;
		} else {
			return false;
		}
	}

	public static String getRealIMM8(String imm) {
		ArrayList<Character> operators = new ArrayList<Character>();
		ArrayList<String> operands = new ArrayList<String>(Arrays.asList(imm.split("[+\\-*/]")));

		for (int i = 0; i < imm.length(); i++) {
			if (imm.charAt(i) == '+' || imm.charAt(i) == '-' || imm.charAt(i) == '*' || imm.charAt(i) == '/') {
				operators.add(imm.charAt(i));
			}
		}
		if (operators.size() != 0) {
			Stack<Character> op = new Stack<Character>();
			Stack<Integer> data = new Stack<Integer>();
			op.push('@');
			int k = 0;
			for (Character operator : operators) {
				data.push(Integer.decode(operands.get(k)));
				while (getPrecedence(operator) <= getPrecedence(op.peek())) {
					int d5 = data.peek();
					data.pop();
					int d4 = data.peek();
					data.pop();
					int d3 = data.peek();
					data.pop();
					int d2 = data.peek();
					data.pop();
					int d1 = data.peek();
					data.pop();
					data.push(evaluteExp(op.peek(), d1, d2, d3, d4, d5));
					op.pop();
				}
				op.push(operator);
				k++;
			}

			while (k < operators.size() + 1) {
				data.push(Integer.decode(operands.get(k)));
				k++;
			}
			while (op.peek() != '@') {
				int d5 = data.peek();
				data.pop();
				int d4 = data.peek();
				data.pop();
				int d3 = data.peek();
				data.pop();
				int d2 = data.peek();
				data.pop();
				int d1 = data.peek();
				data.pop();
				data.push(evaluteExp(op.peek(), d1, d2, d3, d4, d5));
				op.pop();
			}

			return "0x" + Integer.toHexString(data.peek());
		} else {
			return imm;
		}
	}
}
