/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.assembler.ia32;

import java.util.ArrayList;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyTableModel implements TableModel {

	String[] columnNames = {"ID", "Code", "Quantr 16", /*"Nasm 16",*/ /*"Pass", "32 bits",*/ "Quantr 32", /*"Nasm 32",*/ /*"Pass", "64 bits",*/ "Quantr 64", /*"Nasm 64"*//*, "Pass"*/};
	ArrayList<Data> data = new ArrayList<Data>();

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			Data d = data.get(rowIndex);

			switch (columnIndex) {
				case 0:
					return "";
				case 1:
					return d.code16;
				case 2:
					return bytesToString(d.bytes16);
//				case 3:
//					return d.bytesNasm16.getValue() == null ? d.bytesNasm16.getKey() : bytesToString(d.bytesNasm16.getValue());
//				case 4:
//					return prefix16 + d.pass16;
//				case 5:
//					return prefix32 + d.code32;
				case 3:
					return bytesToString(d.bytes32);
//				case 5:
//					return d.bytesNasm32.getValue() == null ? d.bytesNasm32.getKey() : bytesToString(d.bytesNasm32.getValue());
//				case 8:
//					return prefix32 + d.pass32;
//				case 9:
//					return prefix64 + d.code64;
				case 4:
					return bytesToString(d.bytes64);
//				case 7:
//					return d.bytesNasm64.getValue() == null ? d.bytesNasm64.getKey() : bytesToString(d.bytesNasm64.getValue());
//				case 11:
//					return prefix64 + d.pass64;
				default:
					return d;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public Data getData(int row) {
		return data.get(row);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void add(String functionName, String code16, String code32, String code64, byte[] bytes16, byte[] bytes32, byte[] bytes64) {
//		boolean ok16 = Arrays.equals(bytes16, bytesNasm16.getValue());
//		boolean ok32 = Arrays.equals(bytes32, bytesNasm32.getValue());
//		boolean ok64 = Arrays.equals(bytes64, bytesNasm64.getValue());
		data.add(new Data(functionName, code16, code32, code64,
				bytes16, false,
				bytes32, false,
				bytes64, false));
	}

	public class Data {

		String functionName;
		String code16;
		String code32;
		String code64;
		byte bytes16[];
//		Pair<String, byte[]> bytesNasm16;
//		boolean pass16;
		byte bytes32[];
//		Pair<String, byte[]> bytesNasm32;
//		boolean pass32;
		byte bytes64[];
//		Pair<String, byte[]> bytesNasm64;
//		boolean pass64;

		public Data(String functionName, String code16, String code32, String code64, byte[] bytes16, boolean pass16, byte[] bytes32, boolean pass32, byte[] bytes64, boolean pass64) {
			this.functionName = functionName;
			this.code16 = code16;
			this.code32 = code32;
			this.code64 = code64;
			this.bytes16 = bytes16;
//			this.bytesNasm16 = bytesNasm16;
//			this.pass16 = pass16;
			this.bytes32 = bytes32;
//			this.bytesNasm32 = bytesNasm32;
//			this.pass32 = pass32;
			this.bytes64 = bytes64;
//			this.bytesNasm64 = bytesNasm64;
//			this.pass64 = pass64;
		}

	}

	String bytesToString(byte[] bytes) {
		if (bytes == null) {
			return "";
		}
		String str = "";
		for (byte b : bytes) {
			str += Integer.toHexString(b & 0xff) + " ";
		}
		return str;
	}

}
