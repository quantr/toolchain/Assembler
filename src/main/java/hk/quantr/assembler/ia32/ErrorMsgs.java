package hk.quantr.assembler.ia32;

import hk.quantr.assembler.print.MessageHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 * @author Kelvinyu
 */
public class ErrorMsgs {

	private String ins;
	private String msg;
	private String leftTokenType;
	private boolean hasError;
	private boolean leftError;
	private boolean rightError;
	private boolean left_isReg;
	private boolean right_isReg;
	private boolean right_isIMM;
	private boolean table16;
	private boolean is64;
	private int left_opRow;
	private int right_opRow;
	private int left_opSize;
	private int right_opSize; // opSize or imm size
	private int left_expSize;
	private int right_expSize;
	private int bits;
	private boolean startBit;
	private int left_row;
	private int right_row;
	private int numOfOp;

	public ErrorMsgs(String ins, int bits, boolean left_isReg, boolean right_isReg, int left_opSize, int right_opSize,
			int left_expSize, int right_expSize, int left_opRow, int right_opRow, boolean table16, boolean is64,
			boolean right_isIMM, boolean startBit, int left_row, int right_row, int numOfOp) {
		this.ins = ins;
		this.bits = bits;
		this.left_isReg = left_isReg;
		this.right_isReg = right_isReg;
		this.left_opSize = left_opSize;
		this.right_opSize = right_opSize;
		this.left_expSize = left_expSize;
		this.right_expSize = right_expSize;
		this.right_isIMM = right_isIMM;
		this.left_opRow = left_opRow;
		this.right_opRow = right_opRow;
		this.table16 = table16;
		this.is64 = is64;
		this.startBit = startBit;
		this.left_row = left_row;
		this.right_opRow = right_row;
		this.numOfOp = numOfOp;

		try {
			Method m = ErrorMsgs.class.getDeclaredMethod(ins);
			m.invoke(this);
		} catch (NoSuchMethodException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (SecurityException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalAccessException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalArgumentException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (InvocationTargetException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public ErrorMsgs(String ins, int bits, boolean left_isReg, boolean right_isReg, int left_opSize, int right_opSize,
			int left_expSize, int right_expSize, int left_opRow, int right_opRow, boolean table16, boolean is64,
			boolean right_isIMM, boolean startBit, int left_row, int right_row, int numOfOp, String leftTokenType) {
		this.ins = ins;
		this.bits = bits;
		this.left_isReg = left_isReg;
		this.right_isReg = right_isReg;
		this.left_opSize = left_opSize;
		this.right_opSize = right_opSize;
		this.left_expSize = left_expSize;
		this.right_expSize = right_expSize;
		this.right_isIMM = right_isIMM;
		this.left_opRow = left_opRow;
		this.right_opRow = right_opRow;
		this.table16 = table16;
		this.is64 = is64;
		this.startBit = startBit;
		this.left_row = left_row;
		this.right_opRow = right_row;
		this.numOfOp = numOfOp;
		this.leftTokenType = leftTokenType;

		try {
			Method m = ErrorMsgs.class.getDeclaredMethod(ins);
			m.invoke(this);
		} catch (NoSuchMethodException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (SecurityException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalAccessException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalArgumentException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (InvocationTargetException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public boolean isError() {
		return hasError;
	}

	public String getErrorMsg() {
		return msg;
	}

	public boolean getLeftError() {
		return leftError;
	}

	public boolean getRightError() {
		return rightError;
	}

	private String REXREG_Type(int left, int right) {
		if (left == -241 || right == -241) {
			return "r8l";
		} else if (left == -251 || right == -251) {
			return "r9l";
		} else if (left == -261 || right == -261) {
			return "r10l";
		} else if (left == -271 || right == -271) {
			return "r11l";
		} else if (left == -281 || right == -281) {
			return "r12l";
		} else if (left == -291 || right == -291) {
			return "r13l";
		} else if (left == -301 || right == -301) {
			return "r14l";
		} else {
			return "r15l";
		}
	}

	private void aaa() {
		if (bits == 64) {
			hasError = true;
			leftError = true;
			msg = "";
		} else {
			hasError = false;
		}
	}

	private void aad() {
		if (bits == 64) {
			hasError = true;
			leftError = true;
			msg = "";
		} else {
			hasError = false;
		}
	}

	private void aam() {
		if (bits == 64) {
			hasError = true;
			leftError = true;
			msg = "";
		} else {
			hasError = false;
		}
	}

	private void adcx() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}

	}

	private void adox() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void adc() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if (left_row == 44 || right_row == 44) {
				hasError = true;
				leftError = left_row == 44;
				rightError = right_row == 44;
				msg = "Error : invalid effective address";
			} else if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}

	}

	private void add() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}

		}
	}

	private void and() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void bsf() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void bsr() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void bswap() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void bt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void btc() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void btr() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void bts() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void call() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (left_opRow != 99) {
			if (!startBit && !left_isReg && !right_isIMM) {
				hasError = true;
				leftError = true;
				msg = "Error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "Error : impossible combination of address sizes";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				if (left_isReg) {
					msg = "Error : invalid bit-mode with register used";
				} else {
					msg = "Error : impossible combination of address sizes";
				}
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "Error : invalid bit-mode with register used";
			} else if (left_opRow < 0) {
				leftError = true;
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if (left_opSize != 64 && bits == 64) {
				if (leftTokenType.equals("seg")) {
					hasError = false;
					leftError = false;
				} else {
					hasError = true;
					leftError = true;
					msg = "Error: ///instruction not supported in 64-bit mode";
				}
			} else if (left_opSize == 64 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error: instruction not supported in " + Integer.toString(bits) + "-bit mode";
			} else {
				hasError = false;
			}
		} else if (left_opRow == 99) {
			if (bits == 64 && !right_isIMM) {
				hasError = true;
				leftError = true;
				msg = "Error: instruction not supported in 64-bit mode";
			} else {
				hasError = false;
			}
		}
	}

	private void cmovcc() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "Error: invalid effective address";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void cmp() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error: ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				leftError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void cmpsb() {
		hasError = false;
	}

	private void cmpsw() {
		hasError = false;
	}

	private void cmpsd() {
		hasError = false;
	}

	private void cmpsq() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : instruction not support in " + Integer.toString(bits) + "bit mode";
		} else {
			hasError = false;
		}
	}

	private void clac() {
		hasError = false;
	}

	private void clc() {
		hasError = false;
	}

	private void cld() {
		hasError = false;
	}

	private void cldemote() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void clflush() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void clflushopt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void clrssbsy() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void clwb() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			rightError = false;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void cbw() {
		hasError = false;
	}

	private void cwd() {
		hasError = false;
	}

	private void cdq() {
		hasError = false;
	}

	private void cqo() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not support in " + Integer.toString(bits) + "bit mode";
		} else {
			hasError = false;
		}
	}

	private void cwde() {
		hasError = false;
	}

	private void cdqe() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : instruction not support in " + Integer.toString(bits) + "bit mode";
		} else {
			hasError = false;
		}
	}

	private void cli() {
		hasError = false;
	}

	private void cmc() {
		hasError = false;
	}

	private void clts() {
		hasError = false;
	}

	private void cmpxchg() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if ((left_opSize > right_opSize) && left_isReg) {
			leftError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			if (left_opSize < right_opSize) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
			rightError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
			leftError = true;
			hasError = true;
			msg = "error: invalid combination of opcode and operands";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
			leftError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

	private void cmpxchg8b() {
		hasError = false;
	}

	private void cpuid() {
		hasError = false;
	}

	private void crc32() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!right_isReg && !startBit) {
			rightError = true;
			hasError = true;
			msg = "Error : operand size not specified";
		} else if (table16 && bits == 64) {
			if (left_isReg) {
				rightError = true;
			} else {
				leftError = true;
			}
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if (left_opRow < 0 || right_opRow < 0) {
			if (left_opRow < 0) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			if (left_opRow > 44) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			if (!left_isReg) {
				leftError = true;
			} else {
				rightError = true;
			}
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && right_isReg && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}

	}

	private void daa() {
	}

	private void das() {
	}

	private void dec() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void div() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void emms() {
	}

	private void enter() {
	}

	private void f2xm1() {
	}

	private void fabs() {
	}

	private void fchs() {
	}

	private void fclex() {
	}

	private void fnclex() {
	}

	private void fcompp() {
	}

	private void fincstp() {
	}

	private void finint() {
	}

	private void fld1() {
	}

	private void fldl2t() {
	}

	private void fldl2e() {
	}

	private void fldl2pi() {
	}

	private void fldl2lg2() {
	}

	private void fldl2ln2() {
	}

	private void fldz() {
	}

	private void fldcw() {
	}

	private void fldenv() {
	}

	private void fnop() {
	}

	private void fpatan() {
	}

	private void fprem() {
	}

	private void fprem1() {
	}

	private void fptan() {
	}

	private void frndint() {
	}

	private void fsin() {
	}

	private void fsincos() {
	}

	private void fsqrt() {
	}

	private void ftst() {
	}

	private void fucom() {
	}

	private void ftcomp() {
	}

	private void fucompp() {
	}

	private void fxam() {
	}

	private void fxtract() {
	}

	private void fyl2x() {
	}

	private void fyl2xp1() {
	}

	private void hlt() {

	}

	private void idiv() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void imul() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (numOfOp == 1 && !startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified ";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize < right_opSize;
			rightError = left_opSize < right_opSize;
			msg = "Error: invalid combination of opcode and operands";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}

	}

	private void in() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void inc() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void insb() {
		hasError = false;
	}

	private void insw() {
		hasError = false;
	}

	private void insd() {
		hasError = false;
	}

	private void int0() {
		if (bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not supported in 64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void Int() {
		hasError = false;
	}

	private void int3() {
		hasError = false;
	}

	private void int2() {
		hasError = false;
	}

	private void into() {
		if (bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not supported in 64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void invd() {
		hasError = false;
	}

	private void iret() {
		hasError = false;
	}

	private void invlpg() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else {
			hasError = false;
		}
	}

	private void iretd() {
		hasError = false;
	}

	private void iretq() {
		if (bits == 16) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not supported in 16-bit mode";
		} else if (bits == 32) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not supported in 32-bit mode";
		} else {
			hasError = false;
		}
	}

        private void ja()  {
            hasError=false;
        }
        
        private void jae()  {
            hasError=false;
        }
        
        private void jb()  {
            hasError=false;
        }
        
        private void jbe()  {
            hasError=false;
        }
         
        private void jc()  {
            hasError=false;
        }
        
        private void jcxz()  {
                if(bits==64){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 64-bit mode";
                } else {
			hasError = false;
		}
        }
        
        private void je()  {
            hasError=false;
        }
        
        private void jecxz()  {
            hasError=false;
        }
        
        private void jrcxz()  {
                if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
        }
        
        private void jg()  {
            hasError=false;
        }
        
        private void jge()  {
            hasError=false;
        }
        
        private void jl()  {
            hasError=false;
        }
        
        private void jle()  {
            hasError=false;
        }
        
        private void jna()  {
            hasError=false;
        }
        
        private void jnae()  {
            hasError=false;
        }
        
        private void jnb()  {
            hasError=false;
        }
        
        private void jnbe()  {
            hasError=false;
        }
        
        private void jnc()  {
            hasError=false;
        }
        
        private void jne()  {
            hasError=false;
        }
        
        private void jng()  {
            hasError=false;
        }
        
        private void jnge()  {
            hasError=false;
        }
        
        private void jnl()  {
            hasError=false;
        }
        
        private void jnle()  {
            hasError=false;
        }
        
        private void jno()  {
            hasError=false;
        }
        
        private void jnp()  {
            hasError=false;
        }
        
        private void jns()  {
            hasError=false;
        }
        
        private void jnz()  {
            hasError=false;
        }
        
        private void jo()  {
            hasError=false;
        }
        
        private void jp()  {
            hasError=false;
        }
        
        private void jpe()  {
            hasError=false;
        }
        
        private void jpo()  {
            hasError=false;
        }
        //jrcxz defined above
        private void js()  {
            hasError=false;
        }
        
        private void jz()  {
            hasError=false;
        }
        
        private void jmp()  {
            if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (left_opRow != 99) {
                        if(left_expSize<32&&bits==64)
                        {
                            leftError = true;
                            hasError = true;
                            msg = "Error : impossible combination of address sizes";
			//if (!startBit && !left_isReg && !right_isIMM) {
			//	hasError = true;
			//	leftError = true;
			//	msg = "Error: operation size not specified";
			//} else if (table16 && bits == 64) {
			//	leftError = true;
			//	hasError = true;
			//	msg = "Error : impossible combination of address sizes";
			//} else 
                        }else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				if (left_isReg) {
					msg = "Error : invalid bit-mode with register used";
				} else {
					msg = "Error : impossible combination of address sizes";
				}
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "Error : invalid bit-mode with register used";
			} else if (left_opRow < 0) {
				leftError = true;
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if (left_opSize!=64 && left_opSize==left_expSize && bits == 64) {
				if (leftTokenType.equals("seg")) {
					hasError = false;
					leftError = false;
				} else {
                                    //jmp [ecx] 64
					hasError = true;
					leftError = true;
					msg = "Error: ///instruction not supported in 64-bit mode";
				}
			} else if (left_opSize == 64 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error: instruction not supported in " + Integer.toString(bits) + "-bit mode";
			} else {
				hasError = false;
			}
		} else if (left_opRow == 99) {
			if (bits == 64 && !right_isIMM) {
				hasError = true;
				leftError = true;
				msg = "Error: instruction not supported in 64-bit mode";
			} else {
				hasError = false;
			}
		}
        }
        
	private void lar() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void leave() {
		hasError = false;
	}

	private void lfence() {
		hasError = false;
	}

	private void lldt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void lgdt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void lidt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void lmsw() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void lock() {
		hasError = false;
	}

	private void lodsb() {
		hasError = false;
	}

	private void lodsw() {
		hasError = false;
	}

	private void lodsd() {
		hasError = false;
	}

	private void lodsq() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not support in " + Integer.toString(bits) + "bit mode";
		} else {
			hasError = false;
		}
	}

	private void lsl() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void lss() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid effective address";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void les() {
		if (bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid effective address";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void lds() {
		if (bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid effective address";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void lfs() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid effective address";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void lgs() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid effective address";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void ltr() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (left_opSize != 16) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid operand used";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void loop() {
		hasError = false;
	}

	private void loope() {
		hasError = false;
	}

	private void loopne() {
		hasError = false;
	}

	private void lzcnt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else if ((left_opRow >= 241 && left_opRow <= 311) && right_isReg && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			leftError = left_opRow > 44;
			rightError = right_opRow > 44;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid combination of opcode and operands";
		} else {
			hasError = false;
		}
	}

	private void neg() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void nop() {
		if (this.numOfOp == 0) {
			hasError = false;
			return;
		}
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void not() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void mfence() {
		hasError = false;
	}

	private void mov() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else {
			if (!left_isReg && right_isIMM && !startBit) {
				hasError = true;
				leftError = true;
				msg = "Error : operation size not specified";
			} else {
				hasError = false;
			}
		}
	}

	private void movbe() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else {
			if (left_row == 44 || right_row == 44) {
				hasError = true;
				leftError = left_row == 44;
				rightError = right_row == 44;
				msg = "Error : invalid effective address";
			} else if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) || (!left_isReg && !right_isReg)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "Error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				hasError = false;
			}
		}
	}

	private void movsb() {
		hasError = false;
	}

	private void movsw() {
		hasError = false;
	}

	private void movsd() {
		hasError = false;
	}

	private void movsq() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not support in " + Integer.toString(bits) + "bit mode";
		} else {
			hasError = false;
		}
	}

	private void mul() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (numOfOp == 1 && !startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified ";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize < right_opSize;
			rightError = left_opSize < right_opSize;
			msg = "Error: invalid combination of opcode and operands";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void movsx() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for resister used";
		} else if (left_opSize < right_opSize) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid operand used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (left_opSize != 16 && !startBit && !right_isReg) {
			hasError = true;
			rightError = true;
			msg = "Error :  operation size not specified";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void movsxd() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for register or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for register used";
		} else if (right_opSize == 8) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid operand used";
		} else if (left_opSize < right_opSize) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid operand used";
		} else {
			hasError = false;
		}
	}

	private void movzx() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for register or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for register used";
		} else if (left_opSize < right_opSize) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid operand used";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (left_opSize != 16 && !startBit && !right_isReg) {
			hasError = true;
			rightError = true;
			msg = "Error :  operation size not specified";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void monitor() {
		hasError = false;
	}

	private void mwait() {
		hasError = false;
	}

	private void or() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if (left_row == 44 || right_row == 44) {
				hasError = true;
				leftError = left_row == 44;
				rightError = right_row == 44;
				msg = "Error : invalid effective address";
			} else if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "Error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void out() {
		hasError = false;
	}

	private void outs() {
		hasError = false;
	}

	private void outsb() {
		hasError = false;
	}

	private void outsd() {
		hasError = false;
	}

	private void outsw() {
		hasError = false;
	}

	private void pause() {
		hasError = false;
	}

	private void popa() {
		if (bits == 64) {
			msg = "error: instruction not supported in 64-bit mode";
			leftError = true;
			hasError = true;
		} else {
			hasError = false;
		}
	}

	private void popad() {
		if (bits == 64) {
			msg = "error: instruction not supported in 64-bit mode";
			hasError = true;
			leftError = true;
		} else {
			hasError = false;
		}
	}

	private void pop() {
		if (numOfOp == 0) {
			if (left_opSize == 32 && bits == 64) {
				hasError = true;
				leftError = true;
				msg = "Error : instruction not supported in 64-bit mode";
			} else {
				hasError = false;
			}
		} else if ((right_expSize == 0 || left_expSize == 0)) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (left_opSize == 32 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void popcnt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			rightError = true;
			leftError = false;
			msg = "Error : invalid effective address";
		} else if (is64 && right_opSize != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid combination of opcode and operands";
		} else if (is64 && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = left_opSize != 64;
			msg = "Error : invalid bit mode for register or operand used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for register used";
		} else if (left_opSize < right_opSize) {
			hasError = true;
			rightError = true;
			msg = "Error : invalid operand used";
		} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void popf() {
		hasError = false;
	}

	private void popfd() {
		if (bits == 64) {
			msg = "error: instruction not supported in 64-bit mode";
			hasError = true;
			leftError = true;
		} else {
			hasError = false;
		}
	}

	private void popfq() {
		if (bits != 64) {
			msg = "error: instruction not supported in " + bits + "-bit mode";
			hasError = true;
			leftError = true;
		} else {
			hasError = false;
		}
	}

	private void push() {
		if (numOfOp == 0) {
			if (left_opSize == 32 && bits == 64 && left_isReg) {
				hasError = true;
				leftError = true;
				msg = "Error :  instruction not supported in 64-bit mode ";
			} else {
				hasError = false;
			}
		} else if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error: operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (left_opSize == 32 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for register used";
		} else if (is64 && bits != 64) {
			leftError = true;
			hasError = true;
			if (left_isReg) {
				msg = "Error : invalid bit-mode with register used";
			} else {
				msg = "Error : impossible combination of address sizes";
			}
		} else if (left_opRow > 44 && bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : invalid bit-mode with register used";
		} else if (left_opRow < 0) {
			leftError = true;
			hasError = true;
			msg = " error : cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
			leftError = false;
			rightError = false;
		}
	}

	private void pusha() {
		if (bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void pushad() {
		if (bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void pushf() {
		hasError = false;
	}

	private void pushfd() {
		if (bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in 64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void pushfq() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void prefetcht0() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void prefetcht1() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void prefetcht2() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void prefetchnta() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void prefetchw() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void prefetchwt1() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid effective address";
		} else {
			hasError = false;
		}
	}

	private void ptwrite() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error : impossible combination of address sizes";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else if (!startBit && !left_isReg) {
			hasError = true;
			leftError = true;
			msg = "Error : operation size not specified";
		} else {
			hasError = false;
		}
	}

	private void rcl() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void rcr() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void rol() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void ror() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void rdmsr() {
		hasError = false;
	}

	private void rdpmc() {
		hasError = false;
	}

	private void rep() {
		hasError = false;
	}

	private void repe() {
		hasError = false;
	}

	private void repne() {
		hasError = false;
	}

	private void rdfsbase() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not support in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void rdgsbase() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not support in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void rdpid() {
		hasError = false;
	}

	private void rdpkru() {
		hasError = false;
	}

	private void rdrand() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void rdseed() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void rdtsc() {
		hasError = false;
	}

	private void rdtscp() {
		hasError = false;
	}

	private void ret() {
		hasError = false;
	}

	private void rsm() {
		hasError = false;
	}

	private void fwait() {
		hasError = false;
	}

	private void repIns() {
		hasError = false;
	}

	private void xgetbv() {
		hasError = false;
	}

	private void xor() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if (left_row == 44 || right_row == 44) {
				hasError = true;
				leftError = left_row == 44;
				rightError = right_row == 44;
				msg = "Error : invalid effective address";
			} else if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "Error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		}
	}

	private void sahf() {
		hasError = false;
	}

	private void scas() {

	}

	private void scasb() {
		hasError = false;
	}

	private void setcc() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (left_opRow < 0) {
			hasError = true;
			leftError = true;
			msg = " Error: cannot idenify the type of r8l to r15l";
		} else {
			hasError = false;
		}
	}

	private void scasd() {
		hasError = false;
	}

	private void scasw() {
		hasError = false;
	}

	private void scasq() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void sfence() {
		hasError = false;
	}

	private void sal() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void sar() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void shl() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (!left_isReg && !startBit) {
			hasError = true;
			leftError = true;
			msg = "Error :  operation size not specified";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opRow > 100 || right_opRow > 100) && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error : invalid bit mode for resister or operand used";
		} else if (left_opRow > 100 && right_opSize == 8 && (right_opRow >= 28 && right_opRow <= 31)) {
			hasError = true;
			rightError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else {
			hasError = false;
		}
	}

	private void shld() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize < right_opSize;
			rightError = right_opSize < left_opSize;
			msg = "Error : invalid combination of opcode and operand";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opSize == 64 || right_opSize == 64) && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = right_opSize == 64;
			msg = "Error: instruction not supported in " + bits + "-bit mode";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void shrd() {
		if (left_expSize == 0 || right_expSize == 0) {
			hasError = true;
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			msg = "Error : invalid effective address";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize < right_opSize;
			rightError = right_opSize < left_opSize;
			msg = "Error : invalid combination of opcode and operand";
		} else if (table16 && bits == 64) {
			leftError = true;
			hasError = true;
			msg = "Error : impossible combination of address sizes";
		} else if ((left_opSize == 64 || right_opSize == 64) && bits != 64) {
			hasError = true;
			leftError = left_opSize == 64;
			rightError = right_opSize == 64;
			msg = "Error: instruction not supported in " + bits + "-bit mode";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void shr() {
		//if (left_expSize == 0 || right_expSize == 0) {
		//	leftError = true;
		//	hasError = true;
		//	msg = "Error: invalid effective address";
		//} else 
                
                if(bits==64 && left_expSize!=left_opSize){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if (!left_isReg) {//left is not reg, then is memory
			leftError = true;
			hasError = true;
			msg = "error: operation size not specified";
		} else if(bits==32 && left_expSize>32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16 && left_expSize>32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void xsaves64() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && left_expSize<32){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void xsaves() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void sbb() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			} else if (table16 && bits == 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (is64 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used ";
			} else if (left_opRow > 44 && bits != 64) {
				leftError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
			if (left_row == 44 || right_row == 44) {
				hasError = true;
				leftError = left_row == 44;
				rightError = right_row == 44;
				msg = "Error : invalid effective address";
			} else if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				hasError = true;
				leftError = right_isReg;
				rightError = left_isReg;
				msg = "error: invalid combination of address size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				if (left_opRow > 44) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				if (!left_isReg) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if ((left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				if (left_opSize < right_opSize) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((!left_isReg && right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if ((left_isReg && !right_isReg) && (left_opSize != right_opSize)) {
				rightError = true;
				hasError = true;
				msg = "error: invalid combination of opcode and operands";
			} else if (left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31)) {
				leftError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else {
				hasError = false;
			}
		}
	}

	private void sgdt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "error: invalid combination of address size";
		} else {
			hasError = false;
		}
	}

	private void swapgs() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : instruction not support in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void syscall() {
		hasError = false;
	}

	private void sysenter() {
		hasError = false;
	}

	private void stc() {
		hasError = false;
	}

	private void std() {
		hasError = false;
	}

	private void sti() {
		hasError = false;
	}

	private void stac() {
		hasError = false;
	}

	private void sub() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
                        
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
                        leftError = true;
			hasError = true;
			msg = "error: invalid operands in non-64-bit mode";
                } else if (left_opSize != right_opSize) {
			leftError = left_opSize < right_opSize;
			rightError = left_opSize < right_opSize;
			hasError = true;
			msg = "Error : invalid combination of opcode and operands";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "error: invalid combination of address size";
		} else if ((left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) || (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31))) {
			rightError = left_opRow > 1000;
			leftError = right_opRow > 1000;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (right_isIMM) {
			if (!startBit && !left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: operation size not specified";
			}  else if (is64 && bits ==16) {
				leftError = true;
				hasError = true;
				msg = "error: instruction not supported in 16-bit mode";
			} else if (is64 && bits ==32) {
				leftError = true;
				hasError = true;
				msg = "error: instruction not supported in 32-bit mode";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}
		} else {
                        //if (!startBit && !left_isReg) {
			//	leftError = true;
			//	hasError = true;
			//	msg = "error: operation size not specified";
			//}
			if ((left_opSize > right_opSize) && left_isReg) {
				leftError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (table16 && bits == 64) {
				if (left_isReg) {
					rightError = true;
				} else {
					leftError = true;
				}
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else {
				leftError = false;
				rightError = false;
				hasError = false;
			}

		}
	}

	private void str() {
		if (left_expSize == 0) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (table16 && bits == 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid effective address";
		} else if (left_opRow > 100 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode with operand used";
		} else {
			hasError = false;
		}
	}

	private void stosb() {
		hasError = false;
	}

	private void stosw() {
		hasError = false;
	}

	private void stosd() {
		hasError = false;
	}

	private void stosq() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void sysexit() {
		hasError = false;
	}

	private void sysret() {
		hasError = false;
	}

	private void sidt() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void sldt() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void smsw() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error: invalid operands in non-64-bit mode";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void test() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : invalid effective address";
		} else if (!right_isIMM) {
			if (table16 && bits == 64) {
				rightError = true;
				hasError = true;
				msg = "error: mismatch in operand size";
			} else if ((left_opRow >= 241 && left_opRow <= 311) && right_isReg && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
				rightError = true;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if ((left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) || (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31))) {
				rightError = left_opRow > 1000;
				leftError = right_opRow > 1000;
				hasError = true;
				msg = "error : cannot use high register in rex instruction ";
			} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
				leftError = left_opRow > 44;
				rightError = right_opRow > 44;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (is64 && bits != 64) {
				rightError = true;
				hasError = true;
				msg = "error : invalid bit-mode with register used";
			} else if (left_opSize == 64 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error : invalid bit mode for register used";
			} else if (left_opRow < 0 || right_opRow < 0) {
				leftError = left_opRow < 0;
				rightError = right_opRow < 0;
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
			} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error: invalid operands in non-64-bit mode";
			} else if (left_opSize != right_opSize) {
				leftError = left_opSize < right_opSize;
				rightError = left_opSize < right_opSize;
				hasError = true;
				msg = "Error : invalid combination of opcode and operands";
			}
		} else if (right_isIMM) {
			if (!left_isReg && !startBit) {
				leftError = true;
				hasError = true;
				msg = "Error : operand size not specified";
			} else if (IA32.rexReg(is64, left_opRow, left_opRow) == 1 && bits != 64) {
				hasError = true;
				leftError = true;
				msg = "Error: invalid operands in non-64-bit mode";
			}
		} else {
			hasError = false;
		}
	}

	private void tzcnt() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error : error: invalid effective address";
		} else if (table16 && bits == 64) {
			rightError = true;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else if ((left_opRow >= 241 && left_opRow <= 311) && right_isReg && (right_opRow >= 28 && right_opRow <= 31) && right_opSize == 8) {
			rightError = true;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			leftError = left_opRow > 44;
			rightError = right_opRow > 44;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			rightError = true;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (left_opSize == 64 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error : invalid bit mode for register used";
		} else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: invalid operands in non-64-bit mode";
		} else if (left_opSize != right_opSize) {
			leftError = left_opSize < right_opSize;
			rightError = left_opSize < right_opSize;
			hasError = true;
			msg = "Error : invalid combination of opcode and operands";
		} else {
			hasError = false;
		}
	}

	private void ud0() {
		hasError = false;
	}

	private void ud1() {
		hasError = false;
	}

	private void ud2() {
		hasError = false;
	}

	private void wait1() {
		hasError = false;
	}

	private void wbinvd() {
		hasError = false;
	}

	private void wrmsr() {
		hasError = false;
	}

	private void wrfsbase() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not support in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void wrgsbase() {
		if (bits != 64) {
			hasError = true;
			leftError = true;
			msg = "Error: instruction not support in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void xadd() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if ((left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) || (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31))) {
			rightError = left_opRow > 1000;
			leftError = right_opRow > 1000;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (left_opRow < 0 || right_opRow < 0) {
				if (left_opRow < 0) {
					leftError = true;
				} else {
					rightError = true;
				}
				hasError = true;
				msg = " error : cannot idenify the type of r8l to r15l";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize > right_opSize;
			rightError = right_opSize > left_opSize;
			msg = "Error : invalid combination of opcode and operands";
		} else if(bits==64 && table16){
                        //dont need error: xadd ah, ah
                        //needs error: xadd [bp+0x1234], ax
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32 && left_expSize>32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16 && left_expSize>32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else if (IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64) {
			hasError = true;
			leftError = left_opRow > 100;
			rightError = right_opRow > 100;
			msg = "Error: invalid operands in non-64-bit mode";
		} else {
			hasError = false;
		}
	}

	private void xbegin() {
		hasError = false;
	}

	private void wrpkru() {
		if (bits != 64) {
			leftError = true;
			hasError = true;
			msg = "Error : instruction not supported in " + bits + "-bit mode";
		} else {
			hasError = false;
		}
	}

	private void xacquire() {
		hasError = false;
	}

	private void xrelease() {
		hasError = false;
	}

	private void xabort() {
		hasError = false;
	}

	private void xchg() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "Error :invalid effective address";
		} else if (left_opRow < 0 || right_opRow < 0) {
			hasError = true;
			leftError = left_opRow < 0;
			rightError = right_opRow < 0;
			msg = "Error : sybmol " + REXREG_Type(left_opRow, right_opRow) + " undefined";
		} else if (left_opSize != right_opSize) {
			hasError = true;
			leftError = left_opSize > right_opSize;
			rightError = right_opSize > left_opSize;
			msg = "Error : invalid combination of opcode and operands";
		} else if ((left_opRow > 44 || right_opRow > 44) && bits != 64) {
			leftError = left_opRow > 44;
			rightError = right_opRow > 44;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if (is64 && bits != 64) {
			leftError = left_opRow > 44;
			rightError = right_opRow > 44;
			hasError = true;
			msg = "error : invalid bit-mode with register used";
		} else if ((left_opRow > 1000 && (right_opRow >= 28 && right_opRow <= 31)) || (right_opRow > 1000 && (left_opRow >= 28 && left_opRow <= 31))) {
			rightError = left_opRow > 1000;
			leftError = right_opRow > 1000;
			hasError = true;
			msg = "error : cannot use high register in rex instruction ";
		} else if (table16 && bits == 64) {
			leftError = !left_isReg;
			rightError = !right_isReg;
			hasError = true;
			msg = "error: mismatch in operand size";
		} else {
			hasError = false;
		}
	}

	private void xend() {
		hasError = false;
	}

	private void xlat() {
		hasError = false;
	}

	private void xlatb() {
		hasError = false;
	}

	private void xsetbv() {
		hasError = false;
	}

	private void xtest() {
		hasError = false;
	}

	private void xrstor() {
		if (left_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void xrstor64() {
		if (left_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void xrstors() {
		if (left_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void xrstors64() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void xsave() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void xsave64() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void xsavec() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void xsavec64() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void xsaveopt() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else {
			hasError = false;
		}
	}

	private void xsaveopt64() {
		if (left_expSize == 0 || right_expSize == 0) {
			leftError = true;
			hasError = true;
			msg = "Error: invalid effective address";
		} else if(bits==64 && table16){
                        leftError = true;
			hasError = true;
                        msg = "Error: impossible combination of address sizes";
                } else if(bits==32){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 32-bit mode";
                } else if(bits==16){
                        leftError = true;
			hasError = true;
                        msg = "Error: instruction not supported in 16-bit mode";
                } else {
			hasError = false;
		}
	}

	private void lea() {
		if (right_expSize == 0 || left_expSize == 0) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if (right_expSize == 8 || left_expSize == 8) {
			leftError = left_expSize == 0;
			rightError = right_expSize == 0;
			hasError = true;
			msg = "error: invalid effective address";
		} else if ((left_opSize == 8) || (left_opSize == 64 && bits != 64 && right_expSize == 64) || (right_expSize == 16 && bits == 64)) {
			leftError = true;
			hasError = true;
			msg = "Error: impossible combination of address sizes";
		} else if ((left_opSize == 64 && bits != 64 && right_expSize != 64) || (left_opSize != 64 && IA32.rexReg(is64, left_opRow, right_opRow) == 1 && bits != 64)) {
			leftError = true;
			hasError = true;
			msg = "Error: instruction not supported in " + Integer.toString(bits) + "-bit mode";
		} else {
			leftError = false;
			rightError = false;
			hasError = false;
		}
	}

}
