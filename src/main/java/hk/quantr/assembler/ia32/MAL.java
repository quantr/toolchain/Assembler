package hk.quantr.assembler.ia32;

import hk.quantr.assembler.antlr.AssemblerParser;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.javalib.CommonLib;
import java.io.ByteArrayOutputStream;
import java.util.Stack;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.lang.Math;
import java.math.BigInteger;
import java.util.logging.Logger;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MAL /*extends AssemblerParserBaseListener*/ {

	ArrayList<Integer> ints;
	Stack<Integer> stack = new Stack<Integer>();

	public static Logger logger = Logger.getLogger(MAL.class.getName());
	//Stack<Integer> bits = new Stack<>();
	public int bits = 16;
	public int modrm_row = 44;
	public int oprow = 0;
	public int opcol = 0;
	public int leftOpSize;
	public int rightOpSize;
	public int leftExpSize;
	public int rightExpSize;
	public boolean leftIsReg;
	public boolean rightIsReg;
	public boolean rex_rb;
	public boolean issetb;
	public boolean issib;
	public int sib_col = 5;
	public int sib_row = 4;
	public boolean rex_r;
	public boolean rex_x;
	public boolean rex_b;
	public boolean table16;
	public String disp;
	public boolean is64;
	public PrintStream errorStream = System.err;

	public ByteArrayOutputStream out = new ByteArrayOutputStream();
	AssemblerParser parser;

	public Encoder encoder = new Encoder();
	public ExtraHandling ex = new ExtraHandling(encoder);

	public void printError(String str) {
		if (errorStream == null) {
			return;
		}
		errorStream.println(str);
	}

	public void output(String opcode, String... args) {
		try {
			out.write(CommonLib.string2int(opcode));
			for (String arg : args) {
				out.write(CommonLib.string2bytes(arg));
				// out.write(CommonLib.string2bytes(IA32.getRealIMM8(arg)));

			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void outputEa(boolean setw, int digit, String disp, String imm, String... opcode) {
		try {
			if (IA32.rexReg(is64, modrm_row, oprow) == 1 && bits != 64) {
				printError("invaild operand in non-64-bit-mode ");
			} else {
				if (IA32.rexReg(is64, modrm_row, oprow) == 1 && bits == 64) {
					out.write(IA32.getREX(setw, rex_r, rex_x, rex_b));
				}
				for (String arg : opcode) {

					out.write(CommonLib.string2bytes(arg));

				}
				int b = IA32.getModRM(bits, digit, modrm_row);
				out.write(b);
				if (issib) {
					int z = IA32.getSIB(sib_col, sib_row);
					out.write(z);
				}
				if (disp != "no" && disp != null) {
					out.write(CommonLib.string2bytes(disp));
				}

				out.write(CommonLib.string2bytes(IA32.getRealIMM8(imm)));
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void outputReg(String opcode, int destSize) {
		int effectiveOpSize = Math.max(bits, destSize);

		if (effectiveOpSize != -1) {
			if (effectiveOpSize != bits) {
				int operandPrefix = IA32.getOpOverridePrefix(bits, effectiveOpSize);
				out.write(operandPrefix);
			}
		}
		out.write(CommonLib.string2int(opcode));
	}

	public void outputAddr(String opcode, int destSize, boolean memAccess, int sourceSize) {
		int effectiveOpSize = Math.max(bits, destSize);

		if (memAccess) {
			int addrPrefix = IA32.getAddrOverridePrefix(bits, effectiveOpSize);
			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}
		} else {
			int operandPrefix = IA32.getOpOverridePrefix(bits, effectiveOpSize);
			out.write(operandPrefix);
		}

		out.write(CommonLib.string2int(opcode));
	}

	public void checkPrefix(boolean leftIsReg, int leftSize, boolean rightIsReg, int rightSize, int leftExpSize, int rightExpSize, boolean rightIsIMM) {
		// prefix 66 -> operand size (btye, word, dword, ..)
		// prefix 67 -> experssion inside mem

		if (!rightIsIMM) {
			if (!leftIsReg && rightIsReg) {
				if (leftSize == rightSize) {
					int addrPrefix = IA32.getAddrOverridePrefix(bits, leftExpSize);
					int opPrefix = IA32.getOpOverridePrefix(bits, leftSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(bits, rightSize));
					if (opPrefix != 0 && opPrefix != -1) {
						out.write(opPrefix);
					}

					if (addrPrefix != 0 && addrPrefix != -1) {
						out.write(addrPrefix);
					}
				} else {
					printError("error: invalid combination of opcode and operands");
				}

			} else if (leftIsReg && !rightIsReg) {
				if (leftSize == rightSize) {
					int addrPrefix = IA32.getAddrOverridePrefix(bits, rightExpSize);
					int opPrefix = IA32.getOpOverridePrefix(bits, leftSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(bits, rightSize));
					if (opPrefix != 0 && opPrefix != -1) {
						out.write(opPrefix);
					}

					if (addrPrefix != 0 && addrPrefix != -1) {
						out.write(addrPrefix);
					}
				} else {
					printError("error: invalid combination of opcode and operands");
				}
			} else if (leftIsReg && rightIsReg) {

				if (leftSize == rightSize) {
					int opPrefix = IA32.getOpOverridePrefix(bits, leftSize);
					opPrefix = Math.max(opPrefix, IA32.getOpOverridePrefix(bits, rightSize));

					if (opPrefix != 0 && opPrefix != -1) {
						out.write(opPrefix);
					}
				} else {
					printError("error: invalid combination of opcode and operands");
				}
			}

		} else {
			int opPrefix = 0;
			int addrPrefix = 0;

			opPrefix = IA32.getOpOverridePrefix(bits, leftSize);

			if (!leftIsReg) {
				addrPrefix = IA32.getAddrOverridePrefix(bits, leftExpSize);
			}

			if (opPrefix != 0 && opPrefix != -1) {
				out.write(opPrefix);
			}

			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}
		}
	}

	public void checkPrefix(boolean leftIsReg, int leftSize, boolean rightIsReg, int rightSize) {
		int leftEffective = leftSize;
		int rightEffective = rightSize;

		if (!leftIsReg && rightIsReg) {
			int addrPrefix = IA32.getAddrOverridePrefix(bits, leftEffective);
			int opPrefix = IA32.getOpOverridePrefix(bits, rightEffective);

			if (opPrefix != 0 && opPrefix != -1) {
				out.write(opPrefix);
			}

			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}

		} else if (leftIsReg && !rightIsReg) {
			int addrPrefix = IA32.getAddrOverridePrefix(bits, rightEffective);
			int opPrefix = IA32.getOpOverridePrefix(bits, leftEffective);

			if (opPrefix != 0 && opPrefix != -1) {
				out.write(opPrefix);
			}

			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}
		} else if (leftIsReg && rightIsReg) {

			if (leftSize == rightSize) {
				int opPrefix = IA32.getOpOverridePrefix(bits, Math.max(leftSize, rightSize));

				if (opPrefix != 0 && opPrefix != -1) {
					out.write(opPrefix);
				}
			} else {
				printError("error: invalid combination of opcode and operands");
			}
		}
	}

	public void outputLR(int col, boolean setw, String displacement, String... args) {
		try {
			int left = oprow;
			int right = modrm_row;
			if (!rightIsReg) {
				oprow = modrm_row;
			}

			if (IA32.rexReg(is64, left, right) == 1 && bits != 64) {
				printError("invaild operand in non-64-bit-mode ");
			} else if (IA32.rexReg(is64, left, right) == 1 && bits == 64) {
				out.write(IA32.getREX(setw, rex_r, rex_x, rex_b));
				for (String arg : args) {
					out.write(CommonLib.string2bytes(arg));
				}
				int b = IA32.getModRM(bits, col, oprow);
				out.write(b);
				if (issib) {
					int z = IA32.getSIB(sib_col, sib_row);
					out.write(z);
				}

				if (displacement != "no" && displacement != null) {
					out.write(CommonLib.string2bytes(displacement));
				}
			} else {
				for (String arg : args) {
					out.write(CommonLib.string2bytes(arg));
				}
				int b = IA32.getModRM(bits, col, oprow);
				out.write(b);
				if (issib) {
					int z = IA32.getSIB(sib_col, sib_row);
					out.write(z);
				}
				if (displacement != "no" && displacement != null) {
					out.write(CommonLib.string2bytes(displacement));
				}
			}
			rex_b = false;
			rex_x = false;
			rex_r = false;
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void checkPrefix(boolean leftIsReg, int leftSize, boolean rightIsReg, int rightSize, boolean rightIsIMM) {
		if (leftSize == 8) {
			leftSize = 16;
		}
		if (rightSize == 8) {
			rightSize = 16;
		}

		int leftEffective;
		int rightEffective;

		if (!rightIsIMM) {
			leftEffective = leftSize;
			rightEffective = rightSize;
		} else {
			leftEffective = leftSize;
			rightEffective = bits;
		}

		if (!leftIsReg && rightIsReg) {

			int addrPrefix = IA32.getAddrOverridePrefix(bits, leftEffective);
			int opPrefix = IA32.getOpOverridePrefix(bits, rightEffective);

			if (opPrefix != 0 && opPrefix != -1) {
				out.write(opPrefix);
			}

			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}

		} else if (leftIsReg && !rightIsReg) {
			int addrPrefix = 0;

			if (!rightIsIMM) {
				addrPrefix = IA32.getAddrOverridePrefix(bits, rightEffective);
			}
			int opPrefix = IA32.getOpOverridePrefix(bits, leftEffective);

			if (opPrefix != 0 && opPrefix != -1) {
				out.write(opPrefix);
			}

			if (addrPrefix != 0 && addrPrefix != -1) {
				out.write(addrPrefix);
			}

		} else if (leftIsReg && rightIsReg) {
			if (leftSize == rightSize) {
				int opPrefix = IA32.getOpOverridePrefix(bits, Math.max(leftSize, rightSize));

				if (opPrefix != 0 && opPrefix != -1) {
					out.write(opPrefix);
				}
			} else {
				printError("error: invalid combination of opcode and operands");
			}
		} else {
			if (rightIsIMM) {
				int addrPrefix = IA32.getAddrOverridePrefix(bits, leftEffective);
				if (addrPrefix != 0 && addrPrefix != -1) {
					out.write(addrPrefix);
				}
			}
		}
	}

	public void outputR(int col, String... args) {
		try {
			for (String arg : args) {
				out.write(CommonLib.string2bytes(arg));
			}
			int b = IA32.getModRM(bits, col, oprow);
			out.write(b);
			if (issib) {
				int z = IA32.getSIB(sib_col, sib_row);
				out.write(z);
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void output(boolean prefix, String... args) {
		for (String arg : args) {
			out.write(CommonLib.string2int(arg));
		}
	}

	public boolean checkcondition(boolean LeftIsReg, int LeftSize, boolean RightIsReg, int RightSize) {
		if ((LeftSize > RightSize) && LeftIsReg) {
			return false;
		} else if (table16 && bits == 64) {
			return false;
		}/*else if(!LeftIsReg && LeftSize ==32 && !RightIsReg ){
		return true;
		}else if(LeftIsReg && !RightIsReg){
		return true;
		}*/ else if ((modrm_row > 44 | oprow > 44) && bits != 64) {
			return false;
		} else if (is64 && bits != 64) {
			return false;
		} else if ((LeftIsReg && RightIsReg) && (LeftSize < RightSize)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean checkcondition(boolean LeftIsReg, int LeftSize, boolean RightIsReg, int RightSize, int row, int type) {
		if ((LeftSize > RightSize) && LeftIsReg) {
			return false;
		} else if (table16 && bits == 64) {
			return false;
		} else if ((LeftIsReg && RightIsReg) && (LeftSize < RightSize)) {
			return false;
		} else if ((row > 44 | type > 44) && bits != 64) {
			return false;
		} else if (is64 && bits != 64) {
			return false;
		} else {
			return true;
		}
	}

	public void outputREX(boolean w) {
		if (bits == 64) {
			out.write(IA32.getREX(w, rex_r, rex_x, rex_b));
		}
	}

	public void outputAddRD(int code, String... args) {
		try {
			for (String arg : args) {
				out.write(CommonLib.string2bytes(arg));
			}
			out.write(IA32.getRD(code, modrm_row));

		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	public void outputOPMODE(int mode, String... args) {
		try {
			if (mode == 1) {
				if (bits != 64) {
					/*
                                        String s = "2B00FFEC";
    StringBuilder  result = new StringBuilder();
    for (int i = 0; i <=s.length()-2; i=i+2) {
        result.append(new StringBuilder(s.substring(i,i+2)).reverse());
     }
    MessageHandler.println(result.reverse().toString()); 
    
					 */

					for (String arg : args) {//reverse bytes of a string
						StringBuilder result = new StringBuilder();
						for (int i = 2; i <= arg.length() - 2; i = i + 2) {
							result.append(new StringBuilder(arg.substring(i, i + 2)).reverse());
						}
						String StringHex = result.reverse().toString();
						byte[] leading0 = new BigInteger(StringHex, 16).toByteArray();
						if (leading0[0] == 0) {
							leading0 = Arrays.copyOfRange(leading0, 1, leading0.length);
						}
						out.write(leading0);
						//out.write(CommonLib.string2bytes(IA32.getRealIMM8(arg)));
					}

				}
			} else if (mode == 2) {
				if (bits == 64) {
					for (String arg : args) {
						// out.write(CommonLib.string2bytes(IA32.getRealIMM8(arg)));
						out.write(CommonLib.string2bytes(arg));
					}
				}
			}
		} catch (IOException ex) {
			MessageHandler.errorPrintln(ex.getMessage());
		}
	}
}
