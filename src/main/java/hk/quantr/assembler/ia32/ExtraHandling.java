package hk.quantr.assembler.ia32;

import hk.quantr.assembler.print.MessageHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author Kelvinyu
 */
public class ExtraHandling {

	private String ins;
	private boolean left_isReg;
	private boolean right_isReg;
	private boolean left_isIMM;
	private boolean right_isIMM;
	private int left_opSize;
	private int right_opSize;
	private int left_expSize;
	private int right_expSize;
	private int left_opCol;
	private int right_opCol;
	private int left_opRow;
	private int right_opRow;
	private String left_disp;
	private String right_disp;
	private boolean rex_x;
	private boolean right_rex_r;
	private boolean left_rex_b;
	private boolean right_rex_b;
	private boolean passed;
	private boolean left_table16;
	private boolean right_table16;
	private boolean left_is64;
	private boolean right_is64;
	private Token leftToken;
	private Token rightToken;
	private String leftTokenType;
	private String rightTokenType;
	private String left_IMM;
	private String right_IMM;
	private int nop_opSize;
	private int numOfOp = 0;
	private Encoder en;

	public ExtraHandling(Encoder en) {
		this.en = en;
	}
	//function below newly added 

	public void initLeftParameter(String leftTokenType, Token leftToken, boolean left_isReg, boolean left_isIMM, int left_opSize, int left_expSize,
			int left_opCol, int left_opRow, String left_disp, boolean rex_rb, boolean left_table16, boolean left_is64, String imm) {

		this.leftTokenType = leftTokenType;
		this.leftToken = leftToken;
		this.left_isReg = left_isReg;
		this.left_isIMM = left_isIMM;
		this.left_opSize = left_opSize;
		this.left_expSize = left_expSize;
		this.left_opCol = left_opCol;
		this.left_opRow = left_opRow;
		this.left_disp = left_disp;
		this.left_rex_b = rex_rb;
		this.left_table16 = left_table16;
		this.left_is64 = left_is64;
		this.left_IMM = imm;
		en.initRightToken(leftToken);
		en.leftInit(left_isReg, left_isIMM, left_opSize, left_expSize, left_opCol, left_opRow, left_disp, left_rex_b);
	}

	public void initLeftParameter(String leftTokenType, Token leftToken, boolean left_isReg, int left_opSize, int left_expSize,
			int left_opCol, int left_opRow, String left_disp, boolean left_rex_rb) {

		this.leftTokenType = leftTokenType;
		this.leftToken = leftToken;
		this.left_isReg = left_isReg;
		this.left_opSize = left_opSize;
		this.left_expSize = left_expSize;

		this.left_opCol = left_opCol;
		this.left_opRow = left_opRow;
		this.left_disp = left_disp;

		this.left_rex_b = left_rex_rb;

		en.initLeftToken(leftToken);
		en.leftInit(left_isReg, left_opSize, left_expSize, left_opCol, left_opRow, left_disp, left_rex_b);
	}

	public void initLeftParameter(String leftTokenType, Token leftToken, String imm) {
		this.leftTokenType = leftTokenType;
		this.leftToken = leftToken;
		this.left_IMM = imm;

		en.initLeftToken(leftToken);
	}

	public void initRightParameter(String tokenType, String imm) {
		this.rightTokenType = tokenType;
		this.right_IMM = imm;
	}

	public void initRightParameter(String tokenType, int right_opCol, int right_opRow) {
		this.rightTokenType = tokenType;
		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		//this.right_expSize = 8;
	}

	public void initRightParameter(String rightTokenType, Token rightToken, boolean right_isReg, boolean right_isIMM, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean rex_rb, boolean right_table16, boolean right_is64) {

		this.rightTokenType = rightTokenType;
		this.rightToken = rightToken;
		this.right_isReg = right_isReg;
		this.right_isIMM = right_isIMM;
		this.right_opSize = right_opSize;
		this.right_expSize = right_expSize;
		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		this.right_disp = right_disp;
		this.right_rex_b = rex_rb;
		this.right_table16 = right_table16;
		this.right_is64 = right_is64;
		en.initRightToken(rightToken);
		en.rightInit(right_isReg, right_isIMM, right_opSize, right_expSize, right_opCol, right_opRow, right_disp, right_rex_b);
	}

	////function below newly added
	public void initRightParameter(String rightTokenType, Token rightToken, boolean right_isReg, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean right_rex_rb) {

		this.rightTokenType = rightTokenType;
		this.rightToken = rightToken;
		this.right_isReg = right_isReg;
		this.right_opSize = right_opSize;
		this.right_expSize = right_expSize;

		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		this.right_disp = right_disp;

		this.right_rex_b = right_rex_rb;

		en.initRightToken(rightToken);
		en.rightInit(right_isReg, right_opSize, right_expSize, right_opCol, right_opRow, right_disp, right_rex_b);
	}

	public void initRightParameter(String rightTokenType, Token rightToken, boolean right_isReg, boolean right_isIMM, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean rex_rb, boolean right_table16, boolean right_is64, String imm) {

		this.rightTokenType = rightTokenType;
		this.rightToken = rightToken;
		this.right_isReg = right_isReg;
		this.right_isIMM = right_isIMM;
		this.right_opSize = right_opSize;
		this.right_expSize = right_expSize;
		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		this.right_disp = right_disp;
		this.right_rex_b = rex_rb;
		this.right_table16 = right_table16;
		this.right_is64 = right_is64;
		this.right_IMM = imm;
		en.initRightToken(rightToken);
		en.rightInit(right_isReg, right_isIMM, right_opSize, right_expSize, right_opCol, right_opRow, right_disp, right_rex_b);
	}

	public void initRightParameter(boolean right_isReg, boolean right_isIMM, int right_opSize, int right_expSize,
			int right_opCol, int right_opRow, String right_disp, boolean rex_rb, boolean right_table16, boolean right_is64) {

		this.right_isReg = right_isReg;
		this.right_isIMM = right_isIMM;
		this.right_opSize = right_opSize;
		this.right_expSize = right_expSize;
		this.right_opCol = right_opCol;
		this.right_opRow = right_opRow;
		this.right_disp = right_disp;
		this.right_rex_b = rex_rb;
		this.right_table16 = right_table16;
		this.right_is64 = right_is64;

		en.rightInit(right_isReg, right_isIMM, right_opSize, right_expSize, right_opCol, right_opRow, right_disp, right_rex_b);
	}

	public void initOneOp(String tokenType, Token token, boolean isReg, boolean is_IMM, int opSize, int expSize, int col, int row,
			String disp, boolean rex, boolean table16, boolean is64, String IMM) {
		this.leftTokenType = tokenType;
		this.leftToken = token;
		this.rightTokenType = "None";
		this.left_isReg = isReg;
		this.left_opSize = opSize;
		this.left_expSize = expSize;
		this.left_rex_b = rex;
		en.initLeftToken(leftToken);
		en.initRightToken(leftToken);
		en.setNumofOp(1);

		en.leftInit(isReg, opSize, expSize, col, row, disp, rex);
		en.rightInit(isReg, is_IMM, opSize, expSize, col, row, disp, isReg ? rex : false);
		this.right_disp = disp;
		this.right_table16 = table16;
		this.right_is64 = is64;
		this.right_IMM = IMM;

	}

	public void initNOP(String tokenType, Token token) {
		this.leftTokenType = tokenType;
		this.leftToken = token;

		this.rightTokenType = "None";
		en.setNumofOp(0);
		en.initLeftToken(leftToken);
	}

	public void initNOP(String tokenType, Token token, int nop_opSize) {
		this.leftTokenType = tokenType;
		this.leftToken = token;

		this.rightTokenType = "None";
		this.nop_opSize = nop_opSize;
		en.initLeftToken(leftToken);
		en.setNumofOp(0);
	}

	public void initNOP(String fTokenType, Token fToken, String sTokenType, Token sToken, String imm) {
		this.leftTokenType = fTokenType;
		this.leftToken = fToken;

		this.rightTokenType = sTokenType;
		this.rightToken = sToken;
		this.right_IMM = imm;
		en.setNumofOp(0);
		nop_opSize = en.getBits();
		en.initLeftToken(leftToken);
		en.initRightToken(rightToken);
	}

	public void initNOP(String fTokenType, Token fToken, String sTokenType, Token sToken) {
		this.leftTokenType = fTokenType;
		this.leftToken = fToken;

		this.rightTokenType = sTokenType;
		this.rightToken = sToken;

		en.initLeftToken(leftToken);
		en.initRightToken(rightToken);
	}

	public void processInstruction() {
		try {
			this.ins = en.getIns();
			Method m = ExtraHandling.class.getDeclaredMethod(ins);
			m.invoke(this);
		} catch (NoSuchMethodException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (SecurityException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalAccessException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (IllegalArgumentException ex) {
			MessageHandler.errorPrintln(ex);
		} catch (InvocationTargetException ex) {
			MessageHandler.errorPrintln(ex);
		}
	}

	private void aaa() {
		en.initCondition(false, false);
		en.output_opmode(1, "0x37");
	}

	private void aad() {
		en.initCondition(false, false);
		if (leftTokenType.equals("aad") && rightTokenType.equals("None")) {
			en.output_opmode(1, "0xD5", "0x0A");
		} else if (leftTokenType.equals("aad") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output_opmode(1, "0xD5", imm_value);
		}
	}

	private void aam() {
		en.initCondition(false, false);
		if (leftTokenType.equals("aam") && rightTokenType.equals("None")) {
			en.output_opmode(1, "0xD4", "0x0A");
		} else if (leftTokenType.equals("aam") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output_opmode(1, "0xD4", imm_value);
		}
	}

	private void aas() {
		en.initCondition(false, false);
		en.output_opmode(1, "0x3F");
	}

	private void adcx() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output(false, "0x66", "no");

		if (leftTokenType.equals("r32") && rightTokenType.equals("r_m32")) {
			en.output_left_right(false, "0x0F", "0x38", "0xF6");
		} else if (leftTokenType.equals("r64") && rightTokenType.equals("r_m64")) {
			en.output_left_right(true, "0x0F", "0x38", "0xF6");
		}
	}

	private void adox() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output(false, "0xf3", "no");

		boolean isLongMode = false;
		if (leftTokenType.equals("r32") && rightTokenType.equals("r_m32")) {
			isLongMode = false;
		} else if (leftTokenType.equals("r64") && rightTokenType.equals("r_m64")) {
			isLongMode = true;
		}

		en.output_left_right(isLongMode, "0x0F", "0x38", "0xF6");
	}

	private void adc() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x14", imm_value);
		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x15", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x15", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x15", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x10" : "0x12");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x11" : "0x13");
				} else {
					en.output_left_right(false, right_isReg ? "0x11" : "0x13");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void add() {

		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x04", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x05", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x05", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x05", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x00" : "0x02");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x01" : "0x03");
				} else {
					en.output_left_right(false, right_isReg ? "0x01" : "0x03");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void arpl() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m16") && rightTokenType.equals("r16")) {
			en.output_left_right(false, "0x63");
		}

	}

	private void and() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x24", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x25", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x25", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x25", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x20" : "0x22");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x21" : "0x23");
				} else {
					en.output_left_right(false, right_isReg ? "0x21" : "0x23");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void bsf() {

		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r16") && rightTokenType.equals("r_m16")) {
			en.output_left_right(false, "0x0F", "0xBC");
		} else if (leftTokenType.equals("r32") && rightTokenType.equals("r_m32")) {
			en.output_left_right(false, "0x0F", "0xBC");
		} else if (leftTokenType.equals("r64") && rightTokenType.equals("r_m64")) {
			en.output_left_right(true, "0x0F", "0xBC");
		}

	}

	private void bsr() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r16") && rightTokenType.equals("r_m16")) {
			en.output_left_right(false, "0x0F", "0xBD");
		} else if (leftTokenType.equals("r32") && rightTokenType.equals("r_m32")) {
			en.output_left_right(false, "0x0F", "0xBD");
		} else if (leftTokenType.equals("r64") && rightTokenType.equals("r_m64")) {
			en.output_left_right(true, "0x0F", "0xBD");
		}
	}

	private void bswap() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r32")) {
			en.output_addRD(false, "0x0F", 0xC8);
		} else if (leftTokenType.equals("r64")) {
			en.output_addRD(true, "0x0F", 0xC8);
		}
	}

	private void bt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("r16") || rightTokenType.equals("r32")) {
				en.output_left_right(false, "0x0F", "0xA3");
			} else if (rightTokenType.equals("r64")) {
				en.output_left_right(true, "0x0F", "0xA3");
			} else if (rightTokenType.equals("imm")) {
				String[] a = {"0xBA0F", "0xBA0F", "0xBA0F"};
				en.output_imm(a, right_IMM, 8);
			}
		}
	}

	private void btc() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("r16") || rightTokenType.equals("r32")) {
				en.output_left_right(false, "0x0F", "0xBB");
			} else if (rightTokenType.equals("r64")) {
				en.output_left_right(true, "0x0F", "0xBB");
			} else if (rightTokenType.equals("imm")) {
				String[] a = {"0xBA0F", "0xBA0F", "0xBA0F"};
				en.output_imm(a, right_IMM, 8);
			}
		}
	}

	private void btr() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("r16") || rightTokenType.equals("r32")) {
				en.output_left_right(false, "0x0F", "0xB3");
			} else if (rightTokenType.equals("r64")) {
				en.output_left_right(true, "0x0F", "0xB3");
			} else if (rightTokenType.equals("imm")) {
				String[] a = {"0xBA0F", "0xBA0F", "0xBA0F"};
				en.output_imm(a, right_IMM, 8);
			}
		}
	}

	private void bts() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("r16") || rightTokenType.equals("r32")) {
				en.output_left_right(false, "0x0F", "0xAB");
			} else if (rightTokenType.equals("r64")) {
				en.output_left_right(true, "0x0F", "0xAB");
			} else if (rightTokenType.equals("imm")) {
				String[] a = {"0xBA0F", "0xBA0F", "0xBA0F"};
				en.output_imm(a, right_IMM, 8);
			}
		}
	}

	private void call() {
		en.initCondition(right_table16, right_is64, leftTokenType);

		if (leftTokenType.equals("ptr")) {
			en.output_opmode("0x9A", right_IMM, right_disp);
		} else if (leftTokenType.equals("rel")) {
			/*int number = Integer.parseInt(right_IMM.substring(2), 16);
			int n = en.getBits() == 16 ? 3 : 5;
			String no = Integer.toHexString(number - n);
			no = "0x" + no;
			no = en.handle_dispvalue(no, 32, 32);*/
			en.output_opmode("0xE8", right_IMM);
		} else if (leftTokenType.equals("seg")) {
			en.segmentPrefix();
			en.checkPrefix();
			en.output_one_operand(false, "0xFF");
		} else {
			en.checkPrefix();
			en.output_one_operand(false, "0xFF");
		}
	}

	private void cmovcc() {
		en.resetCol(left_opCol);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		String[] op = en.getCCOpcode();
		if (left_opSize == 64) {
			en.output_left_right(true, op[0], op[1]);
		} else {
			en.output_left_right(false, op[0], op[1]);
		}
	}

	private void cmp() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output_opmode("0x3C", imm_value);
		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x3D", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x3D", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x3D", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x38" : "0x3A");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x39" : "0x3B");
				} else {
					en.output_left_right(false, right_isReg ? "0x39" : "0x3B");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void cmpsb() {
		en.initCondition(false, false);
		en.output_opmode("0xA6");
	}

	private void cmpsw() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xA7");
	}

	private void cmpsd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xA7");
	}

	private void cmpsq() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0xA7");
	}

	private void clac() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xCA");
	}

	private void clc() {
		en.initCondition(false, false);
		en.output_opmode("0xF8");
	}

	private void cld() {
		en.initCondition(false, false);
		en.output_opmode("0xFC");
	}

	private void cldemote() {
		en.output_one_operand(false, "0x0F", "0x1C");
	}

	private void clflush() {
		en.initCondition(false, false);
		en.output_one_operand(false, "0x0F", "0xAE");
	}

	private void clflushopt() {
		en.initCondition(false, false);
		en.output_one_operand(false, "0x66", "0x0F", "0xAE");
	}

	private void clrssbsy() {
		en.initCondition(false, false);
		en.output_one_operand(true, "0xF3", "0x0F", "0xAE");
	}

	private void clwb() {
		en.initCondition(false, false);
		en.output_one_operand(false, "0x66", "0x0F", "0xAE");
	}

	private void cbw() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x98");
	}

	private void cwd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x99");
	}

	private void cdq() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x99");
	}

	private void cqo() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0x99");
	}

	private void cwde() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x98");
	}

	private void cdqe() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0x98");
	}

	private void cli() {
		en.initCondition(false, false);
		en.output_opmode("0xFA");
	}

	private void cmc() {
		en.initCondition(false, false);
		en.output_opmode("0xF5");
	}

	private void clts() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x06");
	}

	private void cmpxchg() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (opSize == 8) {
			en.output_left_right(false, "0x0F", "0xB0");
		} else if (opSize == 64) {
			en.output_left_right(true, "0x0F", "0xB1");
		} else {
			en.output_left_right(false, "0x0F", "0xB1");
		}
	}

	private void cmpxchg8b() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xC7");
		}
	}

	private void cpuid() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0xA2");
	}

	private void crc32() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetLeftOpSize(right_opSize);
		int opSize = en.getOperandSize();
		en.checkPrefix();
		en.output_opmode("0xF2");
		if (left_opSize == 32) {
			if (opSize == 8) {
				en.output_left_right(false, "0x0F", "0x38", "0xF0");
			} else {
				en.output_left_right(false, "0x0F", "0x38", "0xF1");
			}
		} else {
			if (opSize == 8) {
				en.output_left_right(true, "0x0F", "0x38", "0xF0");
			} else {
				en.output_left_right(true, "0x0F", "0x38", "0xF1");
			}
		}

	}

	private void daa() {
		en.initCondition(false, false);
		en.output_opmode(1, "0x27");
	}

	private void das() {
		en.initCondition(false, false);
		en.output_opmode(1, "0x2f");
	}

	private void dec() {
		en.initCondition(right_table16, right_is64);
		en.setNumofOp(1);
		en.checkPrefix();

		if (leftTokenType.equals("r_m8")) {
			en.output_one_operand(false, "0xFE");
		} else if (leftTokenType.equals("r_m16") || leftTokenType.equals("r_m32")) {
			if (left_isReg && en.getBits() != 64) {
				en.output_addRD(false, "no", 0x48);
			} else {
				en.output_one_operand(false, "0xFF");
			}
		} else if (leftTokenType.equals("r_m64")) {
			en.output_one_operand(true, "0xFF");
		}
	}

	private void div() {
		en.initCondition(right_table16, right_is64);
		en.setNumofOp(1);
		en.checkPrefix();
		if (leftTokenType.equals("r_m8")) {
			en.output_one_operand(false, "0xF6");
		} else if (leftTokenType.equals("r_m16") || leftTokenType.equals("r_m32")) {
			en.output_one_operand(false, "0xF7");
		} else if (leftTokenType.equals("r_m64")) {
			en.output_one_operand(true, "0xF7");
		}
	}

	private void emms() {
		en.initCondition(false, false);
		en.output_opmode("0x0f", "0x77");
	}

	private void enter() {
		if (leftTokenType.equals("imm") && rightTokenType.equals("0")) {
			en.output_opmode("0xC8", left_IMM);
			en.output_opmode("0x00");
		} else if (leftTokenType.equals("imm") && rightTokenType.equals("1")) {
			en.output_opmode("0xC8", left_IMM);
			en.output_opmode("0x01");
		} else if (leftTokenType.equals("imm") && rightTokenType.equals("imm")) {
			en.output_opmode("0xC8", left_IMM);
			en.output_opmode(right_IMM);
		}
	}

	private void f2xm1() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0XF0");
	}

	private void fabs() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xE1");
	}

	private void fchs() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xE0");
	}

	private void fclex() {
		en.initCondition(false, false);
		en.output_opmode("0x9B", "0xDB", "0xE2");
	}

	private void fwait() {
		en.initCondition(false, false);
		en.output_opmode("0x9B");
	}

	private void fnclex() {
		en.initCondition(false, false);
		en.output_opmode("0xDB", "0xE2");
	}

	private void fcompp() {
		en.initCondition(false, false);
		en.output_opmode("0xde", "0xd9");
	}

	private void fcos() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xff");
	}

	private void fdecstp() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf6");
	}

	private void fincstp() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf7");
	}

	private void finit() {
		en.initCondition(false, false);
		en.output_opmode("0x9b", "0xdb", "0xe3");
	}

	private void fninit() {
		en.initCondition(false, false);
		en.output_opmode("0xdb", "0xe3");
	}

	private void fld1() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xe8");
	}

	private void fldl2t() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xe9");
	}

	private void fldl2e() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xea");
	}

	private void fldl2pi() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xeb");
	}

	private void fldl2lg2() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xec");
	}

	private void fldl2ln2() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xed");
	}

	private void fldz() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xee");
	}

	private void fldcw() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xd0");
	}

	private void fldenv() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf3");
	}

	private void fnop() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xd0");
	}

	private void fpatan() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf3");
	}

	private void fprem() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf8");
	}

	private void fprem1() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf5");
	}

	private void fptan() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xf2");
	}

	private void frndint() {
		en.initCondition(false, false);
		en.output_opmode("0xd9", "0xfc");
	}

	private void fsin() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xFE");
	}

	private void fsincos() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xFB");
	}

	private void fsqrt() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xFA");
	}

	private void ftst() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xE4");
	}

	private void fscale() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xFD");
	}

	private void fucom() {
	}

	private void ftcomp() {
	}

	private void fucompp() {
	}

	private void fxam() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xE5");
	}

	private void fxtract() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xF4");
	}

	private void fyl2x() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xF1");
	}

	private void fyl2xp1() {
		en.initCondition(false, false);
		en.output_opmode("0xD9", "0xF9");
	}

	private void hlt() {
		en.initCondition(false, false);
		en.output_opmode("0xF4");

	}

	private void imul() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opsize = en.getOperandSize();
		if (rightTokenType.equals("None")) {
			if (opsize == 8) {
				en.output_one_operand(false, "0xF6");
			} else if (opsize == 64) {
				en.output_one_operand(true, "0xF7");
			} else {
				en.output_one_operand(false, "0xF7");
			}
		} else if (rightTokenType.equals("r_m")) {
			en.resetCol(left_opCol);
			en.output_left_right(opsize == 64, "0x0F", "0xAf");
		} else {
			en.resetCol(left_opCol);
			if (en.check_disp(right_IMM, 8)) {
				en.output_left_right(opsize == 64, "0x6B");
				en.output_opmode(en.handle_dispvalue(right_IMM, 8));
			} else {
				en.output_left_right(opsize == 64, "0x69");
				en.output_opmode(en.handle_dispvalue(right_IMM, 32));
			}
		}
	}

	private void idiv() {
		en.initCondition(right_table16, right_is64);
		//en.setNumofOp(1);
		int opsize = en.getOperandSize();
		en.checkPrefix();
		/*if (leftTokenType.equals("r_m8")) {
			en.output_one_operand(false, "0xF6");
		} else if (leftTokenType.equals("r_m16") || leftTokenType.equals("r_m32")) {
			en.output_one_operand(false, "0xF7");
		} else if (leftTokenType.equals("r_m64")) {
			en.output_one_operand(true,"0xF7");
		}*/

		if (opsize == 8) {
			en.output_one_operand(false, "0xF6");
		} else if (opsize == 64) {
			en.output_one_operand(true, "0xF7");
		} else {
			en.output_one_operand(false, "0xF7");
		}
	}

	private void in() {
		en.resetRightOpSize(left_opSize);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (rightTokenType.equals("imm")) {
			if (leftTokenType.equals("al")) {
				String imm_value = en.handle_dispvalue(right_IMM, 8);
				en.output_opmode("0xE4", imm_value);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 8);
				en.output_opmode("0xE5", imm_value);
			}
		} else {
			if (leftTokenType.equals("al")) {
				en.output_opmode("0xEC");
			} else {
				en.output_opmode("0xED");
			}
		}
	}

	private void inc() {
		en.initCondition(right_table16, right_is64);
		en.setNumofOp(1);
		en.checkPrefix();

		if (leftTokenType.equals("r_m8")) {
			en.output_one_operand(false, "0xFE");
		} else if (leftTokenType.equals("r_m16") || leftTokenType.equals("r_m32")) {
			if (left_isReg && en.getBits() != 64) {
				en.output_addRD(false, "no", 0x40);
			} else {
				en.output_one_operand(false, "0xFF");
			}
		} else if (leftTokenType.equals("r_m64")) {
			en.output_one_operand(true, "0xFF");
		}

	}

	private void insb() {
		en.initCondition(false, false);
		en.output_opmode("0x6C");
	}

	private void insw() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x6D");
	}

	private void insd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x6D");
	}

	private void into() {
		en.initCondition(right_table16, right_is64);
		en.output_opmode("0xCE");
	}

	private void Int() {
		en.initCondition(false, false);
		en.output_opmode("0xCD", right_IMM);
	}
///////////////////////////////////////////////////////////////////////////////////////////////
        private void ja() {
                en.initCondition(false, false);
                //String cwvalue=leftToken.value-0x04;4 is instruction size, 32 bits becomes 6
                en.output_opmode("0x0F","0x87", right_IMM);
        }
        
        private void jae() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x83", right_IMM);
        }
        
        private void jb() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x82", right_IMM);
        }
        
        private void jbe() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x86", right_IMM);
        }
        
        private void jc() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x82", right_IMM);
        }
        
        private void jcxz() {
                en.initCondition(false, false);
                en.checkPrefix();
                en.output_opmode("0xE3", right_IMM);
        }
        
        private void je() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x84", right_IMM);
        }
        
        private void jecxz() {
                en.initCondition(false, false);
                en.checkPrefix();
                en.output_opmode("0xE3", right_IMM);
        }
        
        private void jrcxz() {
                en.initCondition(false, false);
                en.output_opmode("0xE3", right_IMM);
        }
       
        private void jg() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8F", right_IMM);
        }
        
        private void jge() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8D", right_IMM);
        }
        
        private void jl() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8C", right_IMM);
        }
        
        private void jle() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8E", right_IMM);
        }
        
        private void jna() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x86", right_IMM);
        }
        
        private void jnae() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x82", right_IMM);
        }
              
        private void jnb() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x83", right_IMM);
        }
        
        private void jnbe() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x87", right_IMM);
        }
        
        private void jnc() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x83", right_IMM);
        }
        
        private void jne() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x85", right_IMM);
        }
        
        private void jng() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8E", right_IMM);
        }
        
        private void jnge() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8C", right_IMM);
        }
        
        private void jnl() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8D", right_IMM);
        }
        
        private void jnle() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8F", right_IMM);
        }
        
        private void jno() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x81", right_IMM);
        }
        
        private void jnp() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8B", right_IMM);
        }
        
        private void jns() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x89", right_IMM);
        }
        
        private void jnz() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x85", right_IMM);
        }
        
        private void jo() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x80", right_IMM);
        }
        
        private void jp() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8A", right_IMM);
        }
        
        private void jpe() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8A", right_IMM);
        }
        
        private void jpo() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x8B", right_IMM);
        }
        //jrcxz defined above
        private void js() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x88", right_IMM);
        }
        
        private void jz() {
                en.initCondition(false, false);
                en.output_opmode("0x0F","0x84", right_IMM);
        }
        
        private void jmp() {
		en.initCondition(right_table16, right_is64, leftTokenType);

		if (leftTokenType.equals("ptr")) {
			en.output_opmode("0xEA", right_IMM, right_disp);
		} else if (leftTokenType.equals("rel")) {
			if(left_expSize==8)
                        {
			en.output_opmode("0xEB", right_IMM);
                        }
                        else
                        en.output_opmode("0xE9", right_IMM);
		} else if (leftTokenType.equals("seg")) {
			en.segmentPrefix();
			en.checkPrefix();
                        en.setRight_opCol(4);
			en.output_one_operand(false, "0xFF");
		} else {
			en.checkPrefix();
			en.output_one_operand(false, "0xFF");
		}
	}
        
	private void lea() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		//        en.setRightRex_rb(left_rex_b);
		if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m") && left_isReg) {
			en.output_left_right(left_opSize == 64, "0x8D");
//			if (opSize == 8) {
//				en.output_left_right(false, "0x8D"); //00 02
//			} else if (opSize == 64) {
//				en.output_left_right(true, "0x8D"); //01 03
//			} else {
//				en.output_left_right(false, "0x8D");
//			}
		}
	}

	private void invlpg() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x01");
		}
	}

	private void int3() {
		en.initCondition(false, false);
		en.output_opmode("0xCC");
	}

	private void int1() {
		en.initCondition(false, false);
		en.output_opmode("0xF1");
	}

	private void invd() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x08");
	}

	private void iret() {
		en.initCondition(false, false);
		en.output_opmode("0xCF");
	}

	private void iretd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xCF");
	}

	private void iretq() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0xCF");
	}

	private void lahf() {
		en.initCondition(false, false);
		en.output_opmode("0x9F");
	}

	private void lar() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);

		en.resetRightOpSize(left_opSize);

		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0x0F", "0x02");
	}

	private void leave() {
		en.initCondition(false, false);
		en.output_opmode("0xC9");
	}

	private void lfence() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0xAE", "0xE8");
	}

	private void lldt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output_one_operand(false, "0x0F", "0x00");
	}

	private void lgdt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output_one_operand(false, "0x0F", "0x01");
	}

	private void lidt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output_one_operand(false, "0x0F", "0x01");
	}

	private void lmsw() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output_one_operand(false, "0x0F", "0x01");
	}

	private void lock() {
		en.initCondition(false, false);
		en.output_opmode("0xF0");
	}

	private void lodsb() {
		en.initCondition(false, false);
		en.output_opmode("0xAC");
	}

	private void lodsw() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xAD");
	}

	private void lodsd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xAD");
	}

	private void lodsq() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x48", "0xAD");
	}

	private void lsl() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.checkPrefix();

		if (leftTokenType.equals("r_m") && left_isReg) {

			if (rightTokenType.equals("r_m")) {
				en.output_left_right(left_opSize == 64, "0x0F", "0x03");
			}
		}
	}

	private void lss() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.segmentPrefix();
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0x0F", "0xB2");
	}

	private void lds() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.segmentPrefix();
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0xc5");
	}

	private void les() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.segmentPrefix();
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0xC4");
	}

	private void lfs() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.segmentPrefix();
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0x0F", "0xB4");
	}

	private void lgs() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.segmentPrefix();
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0x0F", "0xB5");
	}

	private void ltr() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.output_one_operand(false, "0x0F", "0x00");
	}

	private void loop() {
		int number = Integer.parseInt(right_IMM.substring(2), 16);
		String no = Integer.toHexString(number - 2);
		en.initCondition(right_table16, right_is64);
		en.output_opmode("0xE2", "0x" + no);
	}

	private void loope() {
		int number = Integer.parseInt(right_IMM.substring(2), 16);
		String no = Integer.toHexString(number - 2);
		en.initCondition(right_table16, right_is64);
		en.output_opmode("0xE1", "0x" + no);
	}

	private void loopne() {
		int number = Integer.parseInt(right_IMM.substring(2), 16);
		String no = Integer.toHexString(number - 2);
		en.initCondition(right_table16, right_is64);
		en.output_opmode("0xE0", "0x" + no);
	}

	private void lzcnt() {
		en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		if (left_isReg) {
			en.checkPrefix();
			en.output_opmode("0xF3");
			en.output_left_right(left_opSize == 64, "0x0F", "0xBD");
		}
	}

	private void mfence() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0xAE", "0xF0");
	}

	private void monitor() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xC8");
	}

	private void mov() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
                 if (leftTokenType.equals("al") && !right_isReg && right_disp!=null)  {
                    
                    //moffs must be a number address, cannot be a register address [0x1234] vs [ax]
                    en.output_left_right(false, false, "0xA0");
                    
                } else if (leftTokenType.equals("ax") && !right_isReg && right_disp!=null) {
                    
                    en.output_left_right(false, false, "0xA1");
                    
                } else if (leftTokenType.equals("eax") && !right_isReg && right_disp!=null) {
                    
                    en.output_left_right(false, false, "0xA1");
                    
                } else if (leftTokenType.equals("rax") && !right_isReg && right_disp!=null) {
                    
                    en.output_left_right(false, false, "0xA1");
                    
                } else if (rightTokenType.equals("al") && !left_isReg && left_disp!=null && en.getBits()!=64)  {
                    
                    //moffs must be a number address, cannot be a register address [0x1234] vs [ax]
                    en.output_left_right(false, false, "0xA2");
                    
                } else if (rightTokenType.equals("ax") && !left_isReg && left_disp!=null) {
                    
                    en.output_left_right(false, true, "0xA3");
                    
                } else if (rightTokenType.equals("eax") && !left_isReg && left_disp!=null) {
                    
                    en.output_left_right(false, false, "0xA3");
                    
                } else if (rightTokenType.equals("rax") && !left_isReg && left_disp!=null) {
                    
                    en.output_left_right(false, false, "0xA3");
                    
                } else if ((leftTokenType.equals("r_m8")||leftTokenType.equals("r8")) && rightTokenType.equals("r8")) {
                    en.output_left_right(false, true, "0x88");
                } else if ((leftTokenType.equals("r_m8")||leftTokenType.equals("r16")) && rightTokenType.equals("r16")) {
                    en.output_left_right(false, true, "0x89");
                } else if ((leftTokenType.equals("r_m8")||leftTokenType.equals("r32")) && rightTokenType.equals("r32")) {
                    en.output_left_right(false, true, "0x89");
                } else if ((leftTokenType.equals("r_m8")||leftTokenType.equals("r64")) && rightTokenType.equals("r64")) {
                    en.output_left_right(false, true, "0x89");
                } else if (leftTokenType.equals("r8") && rightTokenType.equals("r_m8") && left_isReg)  {
                    en.output_left_right(false, true, "0x8A");
                }  else if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m") && left_isReg)  {
                    en.output_left_right(false, true, "0x8B");
                }  else if (leftTokenType.equals("r_m") && rightTokenType.equals("seg") && left_opSize<64 && left_expSize<32) {   
                    
                    //Sreg case
                    en.output_left_right(false, true, "0x8C");
                    
                }  else if (leftTokenType.equals("seg") && rightTokenType.equals("r_m") && right_opSize!=32 && right_expSize!=32) {   
                    
                    //Sreg case
                    en.output_left_right(false, true, "0x8E");
                    
                } else if (leftTokenType.equals("r8") && rightTokenType.equals("imm")){
                        String imm = en.handle_dispvalue(right_IMM, left_opSize);
                        en.output_addRD(false, "no", 0xB0);
			en.output_opmode(imm);
                } else if (leftTokenType.equals("r16") && rightTokenType.equals("imm")){
                        String imm = en.handle_dispvalue(right_IMM, left_opSize);
                        en.output_addRD(false, "no", 0xB8);
			en.output_opmode(imm);
                } else if (leftTokenType.equals("r32") && rightTokenType.equals("imm")){
                        String imm = en.handle_dispvalue(right_IMM, left_opSize);
                        en.output_addRD(false, "no", 0xB8);
			en.output_opmode(imm);
                } else if (leftTokenType.equals("r64") && rightTokenType.equals("imm")){
                        String imm = en.handle_dispvalue(right_IMM, left_opSize);
                        en.output_addRD(false, "no", 0xB8);
			en.output_opmode(imm);
                } else if (leftTokenType.equals("r_m") && rightTokenType.equals("imm")) {
			//String imm = en.handle_dispvalue(right_IMM, left_opSize);
			//if (left_isReg) {
			//	en.output_addRD(left_opSize == 64, "no", left_opSize == 8 ? 0xB0 : 0xB8);
			//	en.output_opmode(imm);
			//} else {
				String a[] = {"0xC6", "0xC7", "0xC7"};
				en.output_imm(a, right_IMM, 32);
			//}
		} else if (leftTokenType.equals("r16") && rightTokenType.equals("r_m16")) {
			en.output_left_right(false, "0x0F", "0x38", "0xF6");
		} else if (leftTokenType.equals("r32") && rightTokenType.equals("r_m32")) {
			en.output_left_right(false, "0x0F", "0x38", "0xF6");
		} else if (leftTokenType.equals("r64") && rightTokenType.equals("r_m64")) {
			en.output_left_right(true, "0x0F", "0x38", "0xF6");
		}

	}

	private void movbe() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (left_isReg) {
			en.output_left_right(en.getOperandSize() == 64, "0x0F", "0x38", "0xF0");
		} else {
			en.output_left_right(en.getOperandSize() == 64, "0x0F", "0x38", "0xF1");
		}

	}

	private void movsb() {
		en.initCondition(false, false);
		en.output_opmode("0xA4");
	}

	private void movsw() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xA5");
	}

	private void movsd() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0xA5");
	}

	private void movsq() {
		en.initCondition(false, false);
		en.initForOne(true, nop_opSize, nop_opSize);
		en.checkPrefix();
		en.output_opmode("0x48", "0xA5");
	}

	private void mul() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opsize = en.getOperandSize();
		if (opsize == 8) {
			en.output_one_operand(false, "0xF6");
		} else if (opsize == 64) {
			en.output_one_operand(true, "0xF7");
		} else {
			en.output_one_operand(false, "0xF7");
		}
	}

	private void mwait() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xC9");
	}

	private void movsx() {
		en.initCondition(right_table16, right_is64);
		int opsize = en.getOperandSize();
		en.resetCol(left_opCol);
		en.resetRightOpSize(left_opSize);
		en.checkPrefix();
		if (left_opSize == 16) {
			en.output_left_right(left_opSize == 64, "0x0F", "0xBE");
		} else {
			en.output_left_right(left_opSize == 64, "0x0F", opsize == 8 ? "0xBE" : "0xBF");
		}
	}

	private void movsxd() {
		en.initCondition(right_table16, right_is64);
		int opsize = en.getOperandSize();
		en.resetCol(left_opCol);
		en.checkPrefix();
		en.output_left_right(left_opSize == 64, "0x63");

	}

	private void movzx() {
		en.initCondition(right_table16, right_is64);
		int opsize = en.getOperandSize();
		en.resetCol(left_opCol);
		//en.resetLeftOpSize(left_opSize);
		en.resetRightOpSize(left_opSize);
		en.checkPrefix();
		if (left_opSize == 16) {
			en.output_left_right(left_opSize == 64, "0x0F", "0xB6");
		} else {
			en.output_left_right(left_opSize == 64, "0x0F", opsize == 8 ? "0xB6" : "0xB7");
		}
	}

	private void neg() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opsize = en.getOperandSize();
		if (opsize == 8) {
			en.output_one_operand(false, "0xF6");
		} else if (opsize == 64) {
			en.output_one_operand(true, "0xF7");
		} else {
			en.output_one_operand(false, "0xF7");
		}
	}

	private void nop() {
		en.initCondition(right_table16, right_is64);
		int opsize = en.getOperandSize();
		if (en.getNumofOp() == 0) {
			en.output_opmode("0x90");
		} else {
			en.checkPrefix();
			en.output_one_operand(false, "0x0F", "0x1F");
		}
	}

	private void not() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opsize = en.getOperandSize();
		if (opsize == 8) {
			en.output_one_operand(false, "0xF6");
		} else if (opsize == 64) {
			en.output_one_operand(true, "0xF7");
		} else {
			en.output_one_operand(false, "0xF7");
		}
	}

	private void or() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x0C", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x0D", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x0D", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x0D", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x08" : "0x0A");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x09" : "0x0B");
				} else {
					en.output_left_right(false, right_isReg ? "0x09" : "0x0B");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}

	}

	private void out() {

		int opSize = en.getOperandSize();
		en.initCondition(left_table16, left_is64);

		if (leftTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(left_IMM, 8);

			if (rightTokenType.equals("al")) {
				en.checkPrefix();
				en.output_opmode("0xE6", imm_value);
			} else if (rightTokenType.equals("ax")) {
				en.checkPrefix();
				en.output_opmode("0xE7", imm_value);
			} else if (rightTokenType.equals("eax")) {
				en.checkPrefix();
				en.output_opmode("0xE7", imm_value);
			} else {
			}
		} else if (leftTokenType.equals("dx")) {
			if (rightTokenType.equals("al")) {
				en.output_opmode("0xEE");
			} else if (rightTokenType.equals("ax")) {
				en.checkPrefix();
				en.output_opmode("0xEF");
			} else if (rightTokenType.equals("eax")) {

				////////////////////not yet fixed well
				if (en.getBits() == 16) {
					en.checkPrefix();
					en.output_opmode("0xEF");
				} else {
					en.output_opmode("0xEF");
				}
			} else {
			}

		}

	}

	private void outs() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("dx")) {
			if (rightTokenType.equals("m8")) {
				en.output_opmode(1, "0x6E");
			} else if (rightTokenType.equals("m16") | rightTokenType.equals("m32")) {
				en.output_opmode(1, "0x6F");
			} else {
			}

		}
	}

	private void outsb() {
		en.initCondition(false, false);
		en.output_opmode("0x6E");
	}
////////////////////////////////////////////

	private void outsw() {
		en.initCondition(false, false);
		en.checkPrefix();
		en.output_opmode("0x6F");
	}

	private void outsd() {
		en.initCondition(false, false);
		en.checkPrefix();
		en.output_opmode("0x6F");
	}
////////////////////////////////////////////

	private void pause() {
		en.initCondition(false, false);
		en.output_opmode("0xF3", "0x90");
	}

	private void pop() {
		//en.initCondition(right_table16, right_is64);
		en.resetCol(left_opCol);
		if (leftTokenType.equals("r_m16")) {
			en.initCondition(right_table16, right_is64);
			en.setNumofOp(1);
			en.checkPrefix();

			if (left_isReg) {
				en.output_addRD(false, "no", 0x58);
			} else {

				en.output_one_operand(false, "0x8F");
			}
		} else if (leftTokenType.equals("r_m32")) {
			en.initCondition(right_table16, right_is64);
			en.setNumofOp(1);
			en.checkPrefix();

			if (left_isReg) {
				en.output_addRD(false, "no", 0x58);
			} else {

				en.output_one_operand(false, "0x8F");
			}

		} else if (leftTokenType.equals("r_m64")) {
			en.initCondition(right_table16, right_is64);
			en.checkPrefix();

			if (left_isReg) {
				en.output_addRD(false, "no", 0x58);
			} else {
				en.output_one_operand(true, "0x8F");
			}
		} else if (leftTokenType.equals("ds")) {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			en.output_opmode("0x1f");
		} else if (leftTokenType.equals("es")) {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			en.output_opmode("0x07");
		} else if (leftTokenType.equals("ss")) {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			en.output_opmode("0x17");
		} else if (leftTokenType.equals("fs")) {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			en.output_opmode("0x0F", "0xA1");
		} else if (leftTokenType.equals("gs")) {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			en.output_opmode("0x0F", "0xA9");
		}
	}

	private void popa() {
		en.initCondition(false, false);
		en.output_opmode("0x61");
	}

	private void popad() {
		en.initCondition(false, false);
		if (en.getBits() == 16) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0x61");
	}

	private void popcnt() {
		en.resetCol(left_opCol);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m")) {
			if (left_opSize == 64) {
				en.output_left_right(true, "0xF3", "0x0F", "0xB8");
			} else {
				en.output_left_right(false, "0xF3", "0x0F", "0xB8");
			}
		}
	}

	private void popf() {
		en.initCondition(false, false);
		en.output_opmode("0x9D");
	}

	private void popfd() {
		en.initCondition(false, false);
		if (en.getBits() == 16) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0x9D");
	}

	private void popfq() {
		en.initCondition(false, false);
		en.output_opmode("0x9D");
	}

	private void push() {

		int opsize = left_opSize;
		if (leftTokenType.equals("r_m")) {
			en.setNumofOp(1);
			en.initCondition(right_table16, right_is64);
			en.checkPrefix();
			if (!left_isReg) {
				if (opsize == 16) {
					en.output_one_operand(false, "0xFF");
				} else if (opsize == 32) {
					en.output_one_operand(true, "0xFF");
				} else if (opsize == 64) {
					en.output_one_operand(true, "0xFF");
				}
			} else {
				if (opsize == 16) {
					en.output_addRD(false, "no", 0x50);
				} else if (opsize == 32) {
					en.output_addRD(false, "no", 0x50);
				} else if (opsize == 64) {
					en.output_addRD(false, "no", 0x50);
				}
			}
		} else if (rightTokenType.equals("imm")) {
			en.initForOne(false, nop_opSize, nop_opSize);
			//boolean size16 = en.check_disp(right_IMM, 16);
			//if(size16 && en.getBits() == 16)
			/*
                        if(en.getBits()==16)
				en.resetLeftOpSize(8);
			else
				en.resetLeftOpSize(en.check_disp(right_IMM, 8)?8:32);
			 */

			en.initCondition(false, false);
			en.resetCol(9);
			en.setLeftDisp("no");
			//String imm_value = en.handle_dispvalue(right_IMM, 32);
			//en.output(false, "0x68", imm_value);
			en.output_imm(new String[]{"0x6A", "0x68", "0x68"}, right_IMM, 32);
		} else {
			en.initForOne(true, nop_opSize, nop_opSize);
			en.initCondition(false, false);
			if (leftTokenType.equals("cs")) {
				en.output_opmode("0x0E");
			} else if (leftTokenType.equals("ss")) {
				en.output_opmode("0x16");
			} else if (leftTokenType.equals("ds")) {
				en.output_opmode("0x1E");
			} else if (leftTokenType.equals("es")) {
				en.output_opmode("0x06");
			} else if (leftTokenType.equals("fs")) {
				en.output_opmode("0x0F", "0xA0");
			} else if (leftTokenType.equals("gs")) {
				en.output_opmode("0x0F", "0xA8");
			}
		}
	}

	private void pusha() {
		en.initCondition(right_table16, right_is64);
		en.output_opmode("0x60");
	}

	private void pushad() {
		en.initCondition(false, false);
		if (en.getBits() == 16) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0x60");
	}

	private void pushf() {
		en.initCondition(false, false);
		en.output_opmode("0x9C");
	}

	private void pushfd() {
		en.initCondition(false, false);
		if (en.getBits() == 16) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0x9C");
	}

	private void pushfq() {
		en.initCondition(false, false);
		en.output_opmode("0x9C");
	}

	private void prefetcht0() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x18");
		}
	}

	private void prefetcht1() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x18");
		}
	}

	private void prefetcht2() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x18");
		}
	}

	private void prefetchnta() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x18");
		}
	}

	private void prefetchw() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x0D");
		}
	}

	private void prefetchwt1() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0x0D");
		}
	}

	private void ptwrite() {
		en.initCondition(right_table16, right_is64);
		int opSize = en.getOperandSize();
		int expSize = en.getExpSize();
		if (opSize == 64 || expSize == 64) {
			en.output_one_operand(true, "0x0F", "0xAE");
		} else {
			en.output_one_operand(false, "0x0F", "0xAE");
		}
	}

	private void rcl() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);
		en.setRightDisp("no");
		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void rcr() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);
		en.setRightDisp("no");

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void ror() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void rol() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void rdfsbase() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 32) {
				en.output_one_operand(false, "0xF3", "0x0F", "0xAE");
			} else if (opSize == 64) {
				en.output_one_operand(true, "0xF3", "0x0F", "0xAE");
			}
		}
	}

	private void rdgsbase() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 32) {
				en.output_one_operand(false, "0xF3", "0x0F", "0xAE");
			} else if (opSize == 64) {
				en.output_one_operand(true, "0xF3", "0x0F", "0xAE");
			}
		}
	}

	private void rdpid() {
		en.initCondition(right_table16, right_is64);

		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 32) {
				en.output_one_operand(false, "0xF3", "0x0F", "0xC7");
			} else if (opSize == 64) {
				en.output_one_operand(false, "0xF3", "0x0F", "0xC7");
			}
		}
	}

	private void rdpkru() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xEE");
	}

	private void rdrand() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 16) {
				en.output_one_operand(false, "0x0F", "0xC7");
			} else if (opSize == 32) {
				en.output_one_operand(false, "0x0F", "0xC7");
			} else if (opSize == 64) {
				en.output_one_operand(true, "0x0F", "0xC7");
			}
		}
	}

	private void rdseed() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 16) {
				en.output_one_operand(false, "0x0F", "0xC7");
			} else if (opSize == 32) {
				en.output_one_operand(false, "0x0F", "0xC7");
			} else if (opSize == 64) {
				en.output_one_operand(true, "0x0F", "0xC7");
			}
		}
	}

	private void rdtsc() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x31");
	}

	private void rdtscp() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xF9");
	}

	private void rdmsr() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x32");
	}

	private void rdpmc() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x33");
	}

	private void rep() {
		en.initCondition(false, false);

		if (leftTokenType.equals("insb")) {
			en.output_opmode("0xF3", "0x6C");
		} else if (leftTokenType.equals("insw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0x6D");
		} else if (leftTokenType.equals("movsb")) {
			en.output_opmode("0xF3", "0xA4");
		} else if (leftTokenType.equals("movsw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xA5");
		} else if (leftTokenType.equals("outsb")) {
			en.output_opmode("0xF3", "0x6E");
		} else if (leftTokenType.equals("outsw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0x6F");
		} else if (leftTokenType.equals("lodsb")) {
			en.output_opmode("0xF3", "0xAC");
		} else if (leftTokenType.equals("lodsw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xAD");

		} else if (leftTokenType.equals("stosb")) {
			en.output_opmode("0xF3", "0xAA");
		} else if (leftTokenType.equals("stosw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xAB");
		}
	}

	private void repe() {
		if (leftTokenType.equals("cmpsb")) {
			en.output_opmode("0xF3", "0xA6");
		} else if (leftTokenType.equals("cmpsw")) {
			en.output_opmode("0xF3");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xA7");
		} else if (leftTokenType.equals("scasb")) {
			en.output_opmode("0xF3", "0xAE");
		} else if (leftTokenType.equals("scasw")) {
			en.output_opmode("0xF3");
			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xAF");
		}
	}

	private void repne() {
		if (leftTokenType.equals("cmpsb")) {
			en.output_opmode("0xF2", "0xA6");
		} else if (leftTokenType.equals("cmpsw")) {
			en.output_opmode("0xF2");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xA7");
		} else if (leftTokenType.equals("scasb")) {
			en.output_opmode("0xF2", "0xAE");
		} else if (leftTokenType.equals("scasw")) {
			en.output_opmode("0xF2");

			if (en.getBits() != 16) {
				en.output_opmode("0x66");
			}

			en.output_opmode("0xAF");
		}
	}

	private void ret() {
		en.initCondition(false, false);
		if (rightTokenType.equals("None")) {
			en.output_opmode("0xC3");
		} else if (rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 16);
			en.output_opmode("0xC2", imm_value);
		}
	}

	private void rsm() {
		en.initCondition(false, false);

		en.output_opmode("0x0F", "0xAA");

	}

	private void repIns() {
		en.initCondition(false, false);
		if (rightTokenType.equals("dx")) {
			if (leftTokenType.equals("m8")) {
				en.output_opmode("0xF3", "0x6C");
			} else {
				en.output_opmode("0xF3", "0x6D");
			}
		}
	}

	private void sahf() {
		en.initCondition(false, false);
		en.output_opmode("0x9E");
	}

	private void scas() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (opSize == 8) {
				en.output_one_operand(false, "0xAE");
			} else if (opSize == 16 || opSize == 32) {
				en.output_one_operand(false, "0xAF");
			} else if (opSize == 64) {
				en.output_one_operand(true, "0xAF");
			}
		}
	}

	private void scasb() {
		en.initCondition(false, false);
		en.output_opmode("0xAE");
	}

	private void scasd() {
		en.initCondition(false, false);
		if (en.getBits() == 16) {
			en.output_opmode("0x66");
		}

		en.output_opmode("0xAF");
	}

	private void scasw() {
		en.initCondition(false, false);
		if (en.getBits() == 32 || en.getBits() == 64) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0xAF");
	}

	private void scasq() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0xAF");
	}

	private void sfence() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0xAE", "0xF8");
	}

	private void sal() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void sar() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void shl() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void shr() {
		int opSize = left_opSize;
		en.setNumofOp(1);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.setRightRex_rb(left_rex_b);
		en.setRight_opCol(right_opCol);
		en.setRight_opRow(right_opRow);

		if (leftTokenType.equals("r_m")) {
			if (rightTokenType.equals("CL")) {
				String op = (opSize == 8) ? "0xD2" : "0xD3";
				boolean is64 = opSize == 64;
				en.output_one_operand(is64, op);
			} else if (rightTokenType.equals("imm")) {
				if (right_IMM.equals("1")) {
					String op = (opSize == 8) ? "0xD0" : "0xD1";
					boolean is64 = opSize == 64;
					en.output_one_operand(is64, op);
				} else {
					String a[] = {"0xC0", "0xC1", "0xC1"};
					en.output_imm(a, right_IMM, 8);
				}
			}
		}
	}

	private void shld() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		//en.setRight_opRow(left_opRow);
		if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m_IMM")) {
			en.output_left_right(opSize == 64, "0x0F", "0xA4");
			en.output_opmode(en.handle_dispvalue(right_IMM, 8));
		} else if (leftTokenType.equals("r_m") && rightTokenType.equals("CL")) {
			if (right_isReg) {
				en.output_left_right(opSize == 64, "0x0F", "0xA5");
			}
		}
	}

	private void shrd() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m_IMM")) {
			en.output_left_right(opSize == 64, "0x0F", "0xAC");
			en.output_opmode(en.handle_dispvalue(right_IMM, 8));
		} else if (leftTokenType.equals("r_m") && rightTokenType.equals("CL")) {
			if (right_isReg) {
				en.output_left_right(opSize == 64, "0x0F", "0xAD");
			}
		}
	}

	private void sbb() {

		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x1C", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x1D", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x1D", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x1D", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x18" : "0x1A");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x19" : "0x1B");
				} else {
					en.output_left_right(false, right_isReg ? "0x19" : "0x1B");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void sub() {

		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x2C", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x2D", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x2D", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x2D", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x28" : "0x2A");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x29" : "0x2B");
				} else {
					en.output_left_right(false, right_isReg ? "0x29" : "0x2B");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void sgdt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (opSize == 64) {
				en.output_one_operand(false, "0x0F", "0x01");
			} else {
				en.output_one_operand(false, "0x0F", "0x01");
			}
		}
	}

	private void str() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();

		en.output_one_operand(false, "0x0F", "0x00");
	}

	private void setcc() {
		en.resetCol(left_opCol);
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		String[] op = en.getCCOpcode();

		en.output_one_operand(false, op[0], op[1]);
	}

	private void stac() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xCB");
	}

	private void stc() {
		en.initCondition(false, false);
		en.output_opmode("0xF9");
	}

	private void std() {
		en.initCondition(false, false);
		en.output_opmode("0xFD");
	}

	private void stosb() {
		en.initCondition(false, false);
		en.output_opmode("0xAA");
	}

	private void stosw() {
		en.initCondition(false, false);
		if (en.getBits() > 16) {
			en.output_opmode("0x66");
		}

		en.output_opmode("0xAB");
	}

	private void stosd() {
		en.initCondition(false, false);
		if (en.getBits() < 32) {
			en.output_opmode("0x66");
		}
		en.output_opmode("0xAB");
	}

	private void stosq() {
		en.initCondition(false, false);
		en.output_opmode("0x48", "0xAB");
	}

	private void sti() {
		en.initCondition(false, false);
		en.output_opmode("0xFB");
	}

	private void swapgs() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xF8");
	}

	private void syscall() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x05");
	}

	private void sysenter() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x34");
	}

	private void sysexit() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x35");
	}

	private void sysret() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x07");
	}

	private void sidt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();

		if (opSize == 64) {
			en.output_one_operand(true, "0x0F", "0x01");
		} else {
			en.output_one_operand(false, "0x0F", "0x01");
		}
	}

	private void sldt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();

		if (opSize == 64) {
			en.output_one_operand(true, "0x0F", "0x00");
		} else {
			en.output_one_operand(false, "0x0F", "0x00");
		}
	}

	private void smsw() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (opSize == 64) {
			en.output_one_operand(true, "0x0F", "0x01");
		} else {
			en.output_one_operand(false, "0x0F", "0x01");
		}
	}

	private void test() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();

		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0xA8", imm_value);

		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0xA9", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0xF6", "0xF7", "0xF7"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0xA9", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0xF6", "0xF7", "0xF7"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0xA9", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					if (right_isReg) {
						en.output_left_right(false, "0x84");
					}
				} else if (opSize == 64) {
					if (right_isReg) {
						en.output_left_right(true, "0x85");
					}
				} else {
					if (right_isReg) {
						en.output_left_right(false, "0x85");
					}
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0xF6", "0xF7", "0xF7"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void tzcnt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		en.resetCol(left_opCol);
		if (leftTokenType.equals("r_m") && left_isReg) {
			if (rightTokenType.equals("r_m")) {
				en.output_opmode("0xF3");
				en.output_left_right(left_opSize == 64, "0x0F", "0xBC");
			}
		}
	}

	private void ud0() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (left_isReg) {
			en.output_one_operand(false, "0x0F", "0xFF");
		}
	}

	private void ud1() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (left_isReg) {
			en.output_one_operand(false, "0x0F", "0xB9");
		}
	}

	private void ud2() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x0B");
	}

	private void wait1() {
		en.initCondition(false, false);
		en.output_opmode("0x9B");
	}

	private void wbinvd() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0X09");
	}

	private void wrmsr() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x30");
	}

	private void wrpkru() {
		en.initCondition(false, false);
		en.output_opmode("0x0F", "0x01", "0xEF");
	}

	private void wrfsbase() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 64) {
				en.output_one_operand(true, "0xF3", "0x0F", "0xAE");
			} else {
				en.output_one_operand(false, "0xF3", "0x0F", "0xAE");
			}
		}

	}

	private void wrgsbase() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (left_isReg) {
			if (opSize == 64) {
				en.output_one_operand(true, "0xF3", "0x0F", "0xAE");
			} else {
				en.output_one_operand(false, "0xF3", "0x0F", "0xAE");
			}
		}
	}

	private void xacquire() {
		en.initCondition(false, false);
		en.output_opmode("0xF2");
	}

	private void xrelease() {
		en.initCondition(false, false);
		en.output_opmode("0xF3");
	}

	private void xabort() {
		String imm_value = en.handle_dispvalue(right_IMM, 8);
		en.output_opmode("0xC6", "0xF8", imm_value);
	}

	private void xchg() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		//en.revertREX();
		en.checkPrefix();
		if (leftTokenType.equals("ax") && rightTokenType.equals("r16") || leftTokenType.equals("r16") && rightTokenType.equals("ax")) {
			en.SetRDSide(leftTokenType.equals("r16"));
			en.output_addRD(false, "no", 0x90);

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("r32") || leftTokenType.equals("r32") && rightTokenType.equals("eax")) {
			en.SetRDSide(leftTokenType.equals("r32"));
                        en.setRightRex_rb(right_rex_b);
			if (en.getBits() != 64 || !(left_opRow == 24 && right_opRow == 24)) {
				en.output_addRD(false, "no", 0x90);
			} else {
				en.output_left_right(false, "0x87");
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("r64") || leftTokenType.equals("r64") && rightTokenType.equals("rax")) {
			en.SetRDSide(leftTokenType.equals("r64"));
                        en.setRightRex_rb(right_rex_b);
			en.output_addRD(true, "no", 0x90);
		} else if (leftTokenType.equals("r_m")) {
			if (opSize == 8) {
				if (left_isReg) {
					//en.setRight_opCol(left_opCol);
                                        //en.setLeft_opRow(right_opRow);
                                        en.setRevert_leftright(true);
				}
				en.output_left_right(false, "0x86");
			} else if (opSize == 16){
				if (left_isReg) {
					//en.revertREX();
                                        en.setRevert_leftright(true);
					en.output_left_right(false, "0x87");
				} else {
					//en.setRight_opCol(left_opCol);
                                        en.output_left_right(false, "0x87");
				}
			} else if (opSize == 64) {
				if (left_isReg) {
                                    if(right_isReg) {
                                        en.revertREX();
                                    }
                                    else {
                                        
                                    }
				   /////en.revertREX();//xchg r14, rsi (64)
                                   //en.revertREX();// xchg r10, [0x1234]
                                    //en.setRight_opCol(left_opCol);
                                    //en.setLeft_opRow(right_opRow);
                                    en.setRevert_leftright(true);
                                    en.output_left_right(true, "0x87");
				} else {
					//en.setRight_opCol(left_opCol);
                                        en.output_left_right(true, "0x87");
				}
			} else {
				if (left_isReg) {
                                        if(right_isReg)
                                        {
					en.revertREX();
                                        }
                                        en.setRevert_leftright(true);//xchg ebp, ebx
					en.output_left_right(false, "0x87");
				} else {
					//en.setRevert_leftright(true);
                                        //en.setRevert_leftright(false);xchg [eax*4], edi
                                        //xchg [eax*2], r10d
                                        
                                        en.output_left_right(false, "0x87");//xchg [eax], r8d
				}
			}
		}
	}

	private void xadd() {

		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("r_m") && rightTokenType.equals("r_m") && right_isReg) {

			if (opSize == 8) {
				en.output_left_right(false, "0x0F", "0xC0"); //00 02
			} else if (opSize == 64) {
				en.output_left_right(true, "0x0F", "0xC1"); //01 03
			} else {
				en.output_left_right(false, "0x0F", "0xC1");
			}
		}
	}

	private void xbegin() {
		en.initCondition(false, false);

		en.output_opmode("0xC7", "0xF8", right_IMM);
	}

	private void xgetbv() {
		en.output_opmode("0x0F", "0x01", "0xD0");
	}

	private void xend() {
		en.output_opmode("0x0F", "0x01", "0xD5");
	}

	private void xlat() {
		en.initCondition(false, false);
		en.output_opmode("0xD7");
	}

	private void xlatb() {
		en.initCondition(false, false);
		en.output_opmode("0xD7");
	}

	private void xsetbv() {
		en.output_opmode("0x0F", "0x01", "0xD1");
	}

	private void xor() {
		int opSize = en.getOperandSize();
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (leftTokenType.equals("al") && rightTokenType.equals("imm")) {
			String imm_value = en.handle_dispvalue(right_IMM, 8);
			en.output(false, "0x34", imm_value);
		} else if (leftTokenType.equals("ax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 16)) {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x35", imm_value);
			} else {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}

		} else if (leftTokenType.equals("eax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(false, "0x35", imm_value);
			}
		} else if (leftTokenType.equals("rax") && rightTokenType.equals("imm")) {
			if (en.check_disp(right_IMM, 8)) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			} else {
				String imm_value = en.handle_dispvalue(right_IMM, 32);
				en.output(true, "0x35", imm_value);
			}
		} else if (leftTokenType.equals("r_m")) {

			if (rightTokenType.equals("r_m")) {
				if (opSize == 8) {
					en.output_left_right(false, right_isReg ? "0x30" : "0x32");
				} else if (opSize == 64) {
					en.output_left_right(true, right_isReg ? "0x31" : "0x33");
				} else {
					en.output_left_right(false, right_isReg ? "0x31" : "0x33");
				}

			} else if (rightTokenType.equals("imm")) {
				String a[] = {"0x80", "0x81", "0x83"};
				en.output_imm(a, right_IMM, 32);
			}
		}
	}

	private void xtest() {
		en.output_opmode("0x0F", "0x01", "0xD6");
	}

	private void xrstor() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xAE");
		}
	}

	private void xrstor64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (en.getBits() == 64) {
				en.output_one_operand(false, "0x48", "0x0F", "0xAE");
			}
		}
	}

	private void xrstors() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int expSize = en.getExpSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xC7");
		}
	}

	private void xrstors64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		if (!left_isReg) {
			en.output_one_operand(false, "0x48", "0x0F", "0xC7");
		}
	}

	private void xsave() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xAE");
		}
	}

	private void xsave64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (en.getBits() == 64) {
				en.output_one_operand(false, "0x48", "0x0F", "0xAE");
			}
		}
	}

	private void xsavec() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xC7");
		}
	}

	private void xsavec64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (en.getBits() == 64) {
				en.output_one_operand(false, "0x48", "0x0F", "0xC7");
			}
		}
	}

	private void xsaveopt() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(false, "0x0F", "0xAE");
		}
	}

	private void xsaveopt64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (en.getBits() == 64) {
				en.output_one_operand(false, "0x48", "0x0F", "0xAE");
			}
		}
	}

	private void xsaves() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			en.output_one_operand(true, "0x0F", "0xC7");
		}

	}

	private void xsaves64() {
		en.initCondition(right_table16, right_is64);
		en.checkPrefix();
		int opSize = en.getOperandSize();
		if (!left_isReg) {
			if (en.getBits() == 64) {
				en.output_one_operand(false, "0x48", "0x0F", "0xC7");
			}
		}

	}
}
