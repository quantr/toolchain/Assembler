![logo](https://gitlab.com/quantr/toolchain/Assembler/raw/master/image/Toolchain%20Special%20Interest%20Group.png)

# We are toolchain research group

This project aims to create a workable x86 and arm assembler using java, this is our first stage, and then we will use it to support all the embedded-asm-syntax in our tiny kernel

[Official website](https://www.quantr.hk/assembler)

# Compile the assembler

1. git clone https://gitlab.com/quantr/toolchain/Assembler.git
2. cd Assembler
3. mvn -Dmaven.test.skip=true clean package
		
if compile success, you got a jar in target folder

# How to start the development

We are using junit, we build different "tests" to perform some functions, such as assemble a file, assemble a string or etc... Just right click the junit test file, then click "Test File'

![](https://www.quantr.hk/wp-content/uploads/2020/06/run-riscv-test.png)

If you want to run a full test, you need to push the code to gitlab with commit message contains "test-riscv" or "test-ia32". Then our gitlab CI/CD will auto run to perform a full test. A full test means it run all the test cases from testcase.txt. Below is the full test architecture.

Full test is in FullTest2.java, it will read the testcase.txt, calculate all the combinations of all instructions, execute "nasm" or "gas" to compile it and compile the output byte of our assembler to them, then we know we assemble insturction correctly or not. You can go to https://gitlab.com/quantr/toolchain/Assembler/-/pipelines and wait the full test is finish. Then go to https://www.quantr.hk/asmweb to get the result.

See below image, the table has two columns "gas" and "quantr". Gas column show you the compiled byte code from gnu assembler, and quantr column show you the byte code of ours. If both column showing you the same byte code, that mean we are correct.

![](https://www.quantr.hk/wp-content/uploads/2020/06/quantr-and-gas.png)

# Source code explain

The whole project is build by maven, so [pom.xml](https://gitlab.com/quantr/toolchain/Assembler/blob/master/pom.xml) is the only build file. When you execute "mvn package" or "mvn compile". Maven will call antlr to compile the [grammar file](https://gitlab.com/quantr/toolchain/Assembler/blob/master/src/main/java/hk/quantr/assembler/antlr/Assembler.g4) into source code. [MyAssemblerListener.java](https://gitlab.com/quantr/toolchain/Assembler/blob/master/src/main/java/hk/quantr/assembler/MyAssemblerListener.java) responses to translate assmebly code into opcode.

# We have these major test files
		
1. [TestString.java](https://gitlab.com/quantr/toolchain/Assembler/blob/master/src/test/java/hk/quantr/assembler/TestString.java) translate assmebly code in string to opcode
2. [TestFile.java](https://gitlab.com/quantr/toolchain/Assembler/blob/master/src/test/java/hk/quantr/assembler/TestFile.java) tranlate the while ass file
3. [FullTest.java](https://gitlab.com/quantr/toolchain/Assembler/blob/master/src/test/java/hk/quantr/assembler/FullTest.java) is the MAJOR test file, it runs all tests

# Examples to run test:

1. mvn -Dtest=hk.quantr.assembler.TestString test
2. mvn -Dtest=hk.quantr.assembler.TestFile test
3. mvn -Dtest=hk.quantr.assembler.FullTest2 test

# Test workbench

Everytime we push the code to gitlab, it auto run all tests and put the result in [https://www.quantr.hk/asmweb](https://www.quantr.hk/asmweb/)

# Intel manual
		
[The Intel instruction manual is in](https://gitlab.com/quantr/toolchain/Assembler/tree/master/doc)

# Netbeans

I use netbeans as my primary IDE, i got my [own antlr plugin](http://plugins.netbeans.org/plugin/73187/netbeans-antlr), it is a helper tool for easier to debug antlr grammar error, you don't need that unless you modify the grammar. To run maven test in netbeans is fast, just right click the test file then click "Test File"

![](https://gitlab.com/quantr/toolchain/Assembler/raw/master/doc/test%20file.png)

# Coding notes

1. I use https://github.com/peterremote1980/to-regex-range to generate ranged number for antlr, run `node peter-test.js` then you will know
2. Netbeans is our primary IDE
3. Must disable following option

![](https://peter.quantr.hk/wp-content/uploads/2020/05/disable-convert-tab-to-space.png)

# Docker

## Build

docker image build -t riscv-assembler .

docker image tag riscv-assembler quantrpeter/riscv-assembler:1.5

docker image push quantrpeter/riscv-assembler:1.5

## Run

docker run quantrpeter/riscv-assembler:1.5

